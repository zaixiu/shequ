function ajax_method(url,data,method,success) {
    var ajax = new XMLHttpRequest();
    // get 跟post  需要分别写不同的代码
    if (method=='get') {
        // get请求
        if (data) {
            // 如果有值
            url+='?';
            url+=data;
        }else{

        }
        // 设置 方法 以及 url
        ajax.open(method,url);
        // send即可
        ajax.send();
    }else{
        // post请求 url 是不需要改变
        ajax.open(method,url);
        // 需要设置请求报文
        ajax.setRequestHeader("Content-type","application/x-www-form-urlencoded");
        // 判断data send发送数据
        if (data) {
            // 如果有值 从send发送
            ajax.send(data);
        }else{
            // 木有值 直接发送即可
            ajax.send();
        }
    }

    // 注册事件
    ajax.onreadystatechange = function () {
        // 在事件中 获取数据 并修改界面显示
        if (ajax.readyState==4&&ajax.status==200) {
            // console.log(ajax.responseText);
            // 将 数据 让 外面可以使用
            // return ajax.responseText;
            // 当 onreadystatechange 调用时 说明 数据回来了
            // ajax.responseText;
            // 如果说 外面可以传入一个 function 作为参数 success
            success(ajax.response);
        }
    }

}
var adcodes = [{"adcode":100000,"name":"全国"},{"adcode":110000,"name":"北京市"},{"adcode":120000,"name":"天津市"},{"adcode":130000,"name":"河北省"},{"adcode":140000,"name":"山西省"},{"adcode":150000,"name":"内蒙古自治区"},{"adcode":210000,"name":"辽宁省"},{"adcode":220000,"name":"吉林省"},{"adcode":230000,"name":"黑龙江省"},{"adcode":310000,"name":"上海市"},{"adcode":320000,"name":"江苏省"},{"adcode":330000,"name":"浙江省"},{"adcode":340000,"name":"安徽省"},{"adcode":350000,"name":"福建省"},{"adcode":360000,"name":"江西省"},{"adcode":370000,"name":"山东省"},{"adcode":410000,"name":"河南省"},{"adcode":420000,"name":"湖北省"},{"adcode":430000,"name":"湖南省"},{"adcode":440000,"name":"广东省"},{"adcode":450000,"name":"广西壮族自治区"},{"adcode":460000,"name":"海南省"},{"adcode":500000,"name":"重庆市"},{"adcode":510000,"name":"四川省"},{"adcode":520000,"name":"贵州省"},{"adcode":530000,"name":"云南省"},{"adcode":540000,"name":"西藏自治区"},{"adcode":610000,"name":"陕西省"},{"adcode":620000,"name":"甘肃省"},{"adcode":630000,"name":"青海省"},{"adcode":640000,"name":"宁夏回族自治区"},{"adcode":650000,"name":"新疆维吾尔自治区"},{"adcode":710000, "name": "台湾省"},{"adcode":810000,"name":"香港特别行政区"},{"adcode":820000,"name":"澳门特别行政区"}]

function setConHeight(){
    let videoHeight=document.getElementById("myvideo").offsetHeight
    let bodyHeight=document.body.clientHeight
    let containerHeight=bodyHeight-videoHeight
    let containerDiv=document.getElementById("container")
    containerDiv.style.height=containerHeight+'px'
}

