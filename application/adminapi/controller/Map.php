<?php
namespace app\adminapi\controller;
use app\common\model\UserRole;
use think\Db;
use think\Request;
class Map extends Common
{


    public function getList(){
        $adcode = \app\common\model\Map::getList();
        $config = Db::name('map_config')->where(['id'=>1])->find();
        $is_all = $config['map_all'];
        return $this->jsonSuccess(['adcode'=>$adcode['data'],'is_all'=>$is_all]);
    }

}
