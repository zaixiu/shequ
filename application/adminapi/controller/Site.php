<?php
namespace app\adminapi\controller;

use app\common\helper\Cookie;
use think\Cache;
use think\Db;
use think\Request;
use think\response\Json;
use think\Session;

class Site extends Common
{

    /**
     * 登录
     */
    public function login(){
        if(Request::instance()->post()){
            try {
                $input = input("post.");
                $validate = $this->validate($input, 'User.login');
                if($validate !== true){
                    exception($validate);
                }
                $name = $input["name"];
                $password = $input["password"];
                Db::startTrans();
                $user = Db::name('user')->where(['mobile|name'=>$name])->find();
                if(empty($user)){
                    exception('账号不存在');
                }
                if($user['password'] != md5($password)){
                    exception('账号密码错误');
                }
                unset($user["password"]);
                session('user_id',$user["id"]);
                session('user',$user);
                Db::commit();
                return $this->jsonSuccess(['token'=>md5($user['name'].$user['mobile'])]);
            } catch (\Exception $exception) {
                Db::rollback();
                return $this->jsonFail([],$exception->getMessage());
            }
        }
    }

    public function userInfo(){
        try {
            $data = \app\common\model\User::getList(['id'=>$this->userId]);
            $user = $data['data'][0];
            return $this->jsonSuccess([
                'roles'=>['admin'],
                'ability' => ['READ', 'WRITE', 'DELETE'],
                'username'=>$user['name'],
                'avatar'=>'https://i.gtimg.cn/club/item/face/img/2/16022_100.gif',
            ]);

        } catch (\Exception $exception) {
            Db::rollback();
            return $this->jsonFail([],$exception->getMessage());
        }
    }


    public function logout(){
        Session::clear();
    }

}
