<?php
namespace app\adminapi\controller;

use think\Db;
use think\Request;
use think\response\Json;

class UserRole extends Common
{

    /**
     * 列表
     */
    public function getList(){
        $input = array_filter(input('','','trim'));
        $input['type'] = session('user.type');
        $list = \app\common\model\UserRole::getList($input);
        return $this->jsonSuccess(['list'=>$list['data'],'total'=>$list['count']]);
    }



    /**
     * 保存
     */
    public function save(){
        try{
            $data = input('post.','','trim');

            $validate = $this->validate($data, 'UserRole');
            if ($validate !== true) {
                exception($validate);
            }

            $data['menu_ids'] = implode(',',$data['menu_ids']);

            Db::startTrans();
            if(isset($data['id']) && !empty($data['id'])){
                unset($data['menu_ids_array']);
                $role = \app\common\model\UserRole::get(['id'=>$data['id']]);
                if($role->force()->update($data) === false){
                    exception('保存失败');
                }
            }else{
                $role = new \app\common\model\UserRole($data);
                $data['user_id'] = $this->userId;
                if(!$role->force()->save($data)){
                    exception('保存失败');
                }
            }
            Db::commit();
            return $this->jsonSuccess([]);
        } catch (\Exception $e) {
            Db::rollback();
            return $this->jsonFail([],$e->getMessage());
        }

    }

    /**
     * 权限列表
     */
    public function menuList()
    {
        try {
            $roleArray = [];
            $role_id = input('roleId');

            if($role_id){
                $roleInfo = \app\common\model\UserRole::where(['id'=>$role_id])->column('menu_ids');
                $roleArray = explode(",",$roleInfo[0]);
            }

            $con = [
                'model'=>'admin',
                'is_show'=>1,
            ];

//            $role_id = session('user.role_id');
//
//            if($role_id != 1){
//                $user_role = \app\common\model\UserRole::get(['id'=>session('user.role_id')]);
//                $con['id'] = ['in',explode(',',$user_role['menu_ids'])];
//            }

            $menu = Db::name('user_menu')->where($con)->field('id,name as text,level,parent_id,url')->order(['sort'=>'desc'])->select();
            $parent_array = array_column($menu,'parent_id');

            if($role_id != -1){
                foreach ($menu as $k => $v){
                    if(in_array($v['id'],$roleArray) && !in_array($v['id'],$parent_array)){
                        $menu[$k]['state'] = ['selected'=>true];
                    }
                }
            }
            $tree = list_to_tree($menu, 'id', 'parent_id', 'children');
            return $this->jsonSuccess(['list'=>$tree]);
        } catch (\Exception $exception) {
            return $this->jsonFail([],$exception->getMessage());
        }
    }


    /**
     * 删除
     */
    public function delete(){
        try{
            $ids = input('ids');
            $id_array = explode(',',$ids);

            $user = \app\common\model\User::where('role_id','in',$id_array)->select();
            if(!empty($user)){
                exception('存在用户使用该角色,不可删除');
            }
            \app\common\model\UserRole::where('id','in',$id_array)->delete();
            return $this->jsonSuccess([]);

        }catch (\Exception $exception) {
            return $this->jsonFail([],$exception->getMessage());
        }
    }


}
