<?php
namespace app\adminapi\controller;
use app\common\model\UserRole;
use think\Db;
use think\Request;
class User extends Common
{


    /**
     * 管理员
     */
    public function getList(){
        $role = UserRole::getRoleSelect();
        $data = array_filter(input('','','trim'));
        $data['user_type'] = 1;
        $data = \app\common\model\User::getList($data);
        return $this->jsonSuccess(['list'=>$data['data'],'total'=>$data['count'],'role'=>$role]);
    }



    /**
     * 保存
     */
    public function save(){
        try{
            $data = input('post.','','trim');
            $data['status'] = isset($data['status'])?$data['status']:2;
            $data['type'] = isset($data['type'])?$data['type']:1;
            Db::startTrans();

            $validate = $this->validate($data, 'User');
            if ($validate !== true) {
                exception($validate);
            }
            if(isset($data['id']) && !empty($data['id'])){
                $user = \app\common\model\User::get(['id'=>$data['id']]);
                if($user->force()->allowField(true)->update($data) === false){
                    exception('修改失败');
                }
            }else{
                $user = new \app\common\model\User();
                $data['password'] = md5('123456');
                $data['user_id'] = $this->userId;
                $data['user_type'] = 1;
                if($user->force()->allowField(true)->save($data) ===false){
                    exception('保存失败');
                }
            }
            Db::commit();
            return  json(['code'=>0,'msg'=>'操作成功']);
        } catch (\Exception $exception) {
            Db::rollback();
            return json(['code' => 1, 'msg' => $exception->getMessage(), 'data' => []]);
        }

    }


    /**
     * 删除
     */
    public function delete(){
        try{
            $ids = input('ids');
            $id_array = explode(',',$ids);
            \app\common\model\User::where('id','in',$id_array)->delete();
            return $this->jsonSuccess([]);
        }catch (\Exception $exception) {
            return $this->jsonFail([],$exception->getMessage());
        }
    }


    /**
     * 重置密码
     */
    public function reset(){
        try{
            $id = input('id');
            if(empty($id)){
                exception('错误请求');
            }
            $user = \app\common\model\User::get($id);
            if (empty($user)){
                exception('信息不存在');
            }
            $user->password = md5('123456');
            $user->save();
            return $this->jsonSuccess();
        }catch (\Exception $e) {
            return $this->jsonFail([],$e->getMessage());
        }
    }

}
