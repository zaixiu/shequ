<?php
namespace app\admin\controller;

use app\common\model\Message;
use app\common\model\UserRole;
use think\Db;
use think\Request;
use think\Validate;

class Question extends Common
{

    /**
     * 列表
     */
    public function index(){
        $alias = \app\common\model\Article::$alias;
        if(Request::instance()->isAjax()){
            $input = array_filter(input('','','trim'));
            $input['type'] = 2;
            return \app\common\model\Article::getList($input);
        }
        return view('',['alias'=>$alias]);
    }


    /**
     * 回复
     * @return \think\response\Json|\think\response\View
     */
    public function reply(){
        try{
            $this->view->engine->layout('common/iframe');
            if(Request::instance()->isAjax()){
                $data = input('post.','','trim');
                $data['is_reply'] = 1;
                $data['reply_time'] = date('Y-m-d H:i:s');
                $data['doctor_id'] = $this->userId;
                \app\common\model\Article::update($data);
                $article = \app\common\model\Article::get($data['id']);
                $message = new Message();
                $message->type = 5;
                $message->parent_id = $article->parent_id;
                $message->operate_id = $data['doctor_id'];
                $message->relate_id = $article['id'];
                $message->save();
                return  json(['code'=>0,'msg'=>'操作成功']);
            }
            return view('',['id'=>input('id')]);
        } catch (\Exception $e) {
            Db::rollback();
            return json(['code' => 1, 'msg' => $e->getMessage(), 'data' => []]);
        }


    }

    /**
     * 是否常见问题
     * @return \think\response\Json|\think\response\View
     */
    public function isCommon(){
        $data = input('post.','','trim');
        $article = \app\common\model\Article::get($data['id']);
        $article->is_common = $data['is_common'] === "true"?1:0;
        $article->save(false);
//        $data['is_reply'] = 1;
//        $data['reply_time'] = date('Y-m-d H:i:s');
//        $data['doctor_id'] = $this->userId;
//        \app\common\model\Article::update($data);
        return  json(['code'=>0,'msg'=>'操作成功']);
    }

}
