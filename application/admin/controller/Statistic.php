<?php


namespace app\admin\controller;


use think\Db;
use think\Request;

class Statistic extends Common{

    /**
     * 费用分析
     * @return float|int|\think\response\View
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function fee(){
        $alias = \app\common\model\Customer::$alias;
        $current_month = date('Y-m');
        $next_month = date( "Y-m", strtotime( " +1 month" ) );
        if(Request::instance()->isAjax()){
            $data = array_filter(input('','','trim'));
            if(!isset($data['time_range'])){
                $data['time_range'] = $current_month.' ~ '.$next_month;
            }
            $data = \app\common\model\CustomerPactFeeDetail::feeByItem($data);
            return arrayNumericFormat($data);
        }
        return view('',['alias'=>$alias,'current_month'=>$current_month,'next_month'=>$next_month]);
    }



    /**
     * 行业分析
     */
    public function etprs(){
        $alias = \app\common\model\Customer::$alias;
        if(Request::instance()->isAjax()){
            $data = array_filter(input('','','trim'));
//            var_dump($data);die;
            $data = \app\common\model\Customer::customerByIndustry($data);
            return arrayNumericFormat($data);
        }
        return view('',['alias'=>$alias]);
    }



    /*
     *
     */
    public function report(){
        $alias = \app\common\model\Report::$alias;
        if(Request::instance()->isAjax()){
            $data = array_filter(input('','','trim'));
            $con['year'] = isset($data['year'])?$data['year']:date('Y');
            isset($data['half'])?$con['half'] = $data['half']:'';
//           var_dump($con);die;
            $item = [
                'income'=>'销售收入（万元）',
                'tax'=>'税收（万元）',
                'output'=>'产值（万元）',
                'staff'=>'企业总人数',
                'doctor'=>'博士人数',
                'thousand'=>'硕士人数',
                'student'=>'本科毕业人数',
                'invent'=>'专利数量',
                'soft'=>'软件著作权数量',
                'rdinput'=>'研发经费投入（万元）',
                'investment'=>'投资额（万元）',
                'is_high_etprs'=>'高企数量',
                'is_tech_etprs'=>'科技型企业数量',
            ];
            $sta_list = [];

            $list =  Db::name('report')->where($con)->order(['year'=>'desc','half'=>'desc'])->select();

            if(empty($list)){
                foreach ($item as $k => $v){
                    $sta_list[$k]['item'] = $v;
                    $sta_list[$k]['total'] = 0;
                }
            }else{
                $statistic = $list[0];
                $statistic['income'] = 0;
                $statistic['tax']  = 0;
                $statistic['output']  = 0;
                $statistic['rdinput']  = 0;
                $statistic['investment']  = 0;
                $statistic['is_high_etprs']  = 0;
                $statistic['is_tech_etprs']  = 0;

                foreach ($list as $k => $v){
                    $statistic['income'] += $v['income'];
                    $statistic['tax'] += $v['tax'];
                    $statistic['output'] += $v['output'];
                    $statistic['rdinput'] += $v['rdinput'];
                    $statistic['investment'] += $v['investment'];
                    $statistic['is_high_etprs'] += $v['is_high_etprs']==1?1:0;
                    $statistic['is_tech_etprs'] += $v['is_tech_etprs']==1?1:0;
                }
                foreach ($item as $k => $v){
                    $sta_list[$k]['item'] = $v;
                    $sta_list[$k]['total'] = $statistic[$k];
                }
            }

            return ['code'=>0,'data'=>$sta_list,'msg'=>''];
        }

        return view('',['alias'=>$alias]);

    }



}
