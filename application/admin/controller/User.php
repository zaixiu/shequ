<?php
namespace app\admin\controller;
use app\common\model\UserRole;
use think\Db;
use think\Request;
class User extends Common
{


    /**
     * 管理员
     */
    public function index(){
        $role = UserRole::getRoleSelect();
        if(Request::instance()->isAjax()){
            $data = array_filter(input('','','trim'));
            $data['user_type'] = 1;
            $data= \app\common\model\User::getList($data);
            return $data;
        }
        return view('',['role'=>$role]);
    }



    /**
     * 创建
     */
    public function create(){
        $this->view->engine->layout('common/iframe');
        $role = UserRole::getRoleSelect();
        return view('',['role'=>$role]);
    }


    /**
     * 编辑
     */
    public function edit(){
        try{
            $this->view->engine->layout('common/iframe');
            $id = input('id');
            $user = \app\common\model\User::getList(['id'=>$id]);
            if($user['code'] == 1){
                exception($user['msg']);
            }
            if($user['count'] == 0){
                exception('用户不存在');
            }

            $user = $user['data'][0];
            $role = UserRole::getRoleSelect();

            return view('create',['role'=>$role,'user'=>$user]);
        }catch (\Exception $e) {
            return view('common/error',['msg' => $e->getMessage()]);
        }

    }


    /**
     * 修改客户状态
     */
    public function status(){
        try{
            $this->view->engine->layout('common/iframe');
            $id = input('id');
            if(empty($id)){
                exception('错误请求');
            }
            $user = \app\common\model\User::get(['id'=>$id]);
            if(empty($user)){
                exception('信息不存在');
            }

            return view('status',['user'=>$user]);
        }catch (\Exception $e) {
            return view('common/error',['msg' => $e->getMessage()]);
        }

    }


    /**
     * 保存
     */
    public function save(){
        try{
            $data = input('post.','','trim');
            $data['status'] = isset($data['status'])?$data['status']:2;
            $data['type'] = isset($data['type'])?$data['type']:1;
            Db::startTrans();

            $validate = $this->validate($data, 'User');
            if ($validate !== true) {
                exception($validate);
            }
            if(isset($data['id']) && !empty($data['id'])){
                $user = \app\common\model\User::get(['id'=>$data['id']]);
                if($user->force()->update($data) === false){
                    exception('修改失败');
                }
            }else{
                $user = new \app\common\model\User();
                $data['password'] = md5('123456');
                $data['user_id'] = $this->userId;
                $data['user_type'] = 1;
                if($user->force()->save($data) ===false){
                    exception('保存失败');
                }
            }
            Db::commit();
            return  json(['code'=>0,'msg'=>'操作成功']);
        } catch (\Exception $exception) {
            Db::rollback();
            return json(['code' => 1, 'msg' => $exception->getMessage(), 'data' => []]);
        }

    }


    /**
     * 删除
     */
    public function delete(){
        try{
            $id = input('id');
            $user = \app\common\model\User::destroy($id);
            if ($user){
                return ['code'=>'0','msg'=>'删除成功'];
            }else{
                return ['code'=>'1','msg'=>'删除失败'];
            }
        }catch (\Exception $exception) {
            return json(['code' => 1, 'msg' => $exception->getMessage(), 'data' => []]);
        }
    }


    /**
     * 重置密码
     */
    public function reset(){
        try{
            $id = input('id');
            if(empty($id)){
                exception('错误请求');
            }
            $user = \app\common\model\User::get($id);
            if (empty($user)){
                exception('信息不存在');
            }
            $user->password = md5('123456');
            $user->save();
            return ['code'=>'0','msg'=>'操作成功'];
        }catch (\Exception $e) {
            return json(['code' => 1, 'msg' => $e->getMessage(), 'data' => []]);
        }
    }

}
