<?php

namespace app\admin\controller;

use app\common\model\Menu;
use think\Controller;
use think\Db;
use app\common\model\User;
use app\common\model\NoticeLog;

class Common extends Controller
{
    public $userId = '';
    static  $current_menu = [];

    public function _initialize(){
        $user = session("user");

        $userId = session('user_id');

        $this->userId = $userId;

        $module = request()->module();

        $controller = request()->controller();

        $action = request()->action();

        if(($module != 'admin'  || $controller !='Site') && empty($userId)){
            return $this->redirect('site/login');
        }

        $bread_array = Menu::getCurrentMenu();
        if(!empty($bread_array)){
            self::$current_menu[$bread_array['level']] = ['id'=>$bread_array['id'],'name'=>$bread_array['name'],'url'=>Menu::generateUrlById($bread_array['id'])];
            $this->currentMenu($bread_array['parent_id']);
            $menu_parent_id = isset(self::$current_menu[1])?self::$current_menu[1]['id']:'';
            $second_parent_id = isset(self::$current_menu[2])?self::$current_menu[2]['id']:'';
            ksort(self::$current_menu);
            $menu_id = $bread_array['id'];
        }else{
            $menu_id = '';
            $menu_parent_id = '';
            $second_parent_id = '';
        }

        $menu_array= User::getUserRoleArray($user);

        $filter_action = [
            [
                'controller'=>'Customer',
                'action'=>'edit',
            ],
            [
                'controller'=>'CustomerPactApply',
                'action'=>'create',
            ],
            [
                'controller'=>'CustomerPactApply',
                'action'=>'index',
            ],
        ];

        if(!empty($bread_array) && $bread_array['is_show']){
            if(!in_array($menu_id,$menu_array) && (($user['role_id']!= 1 && $controller !='Site' && $controller !='Index'))){

                if(session("user.role_id") ==3){
                    if(!($controller=='Customer' && $action =='edit')){
                        $this->error('没有操作权限');
                    }
                }elseif(session("user.user_type") ==2){
                    $flag = false;
                    foreach ($filter_action as $v){
                        if($controller == $v['controller'] && $action == $v['action']){
                            $flag = true;
                        }
                    }
                    if(!$flag){
                        $this->error('没有操作权限');
                    }
                }
            }
        }

        $menu = $this->menu();

        $this->assign([
            'menu'  => $menu,
            'menu_id' => $menu_id,
            'menu_parent_id' => $menu_parent_id,
            'second_parent_id' => $second_parent_id,
            'current_menu' => self::$current_menu,
            'current_user' => $user,
        ]);
    }


    function currentMenu($parent){
        if($parent != 0){
            $data = Menu::find(['id'=>$parent]);
            if(!empty($data)){
                self::$current_menu[$data['level']] = ['id'=>$data['id'],'name'=>$data['name'],'url'=>$data['url']];
                $this->currentMenu($data['parent_id']);
            }
        }
    }



    function menu(){
        $user = session('user');
        $role_id = session('user.role_id');

        if (empty($role_id)) {
            return [];
        }else{
            $con = [
                'level'=>['<',3],
                'model'=> 'admin',
                'is_show'=> 1,
            ];

            if($role_id !=1){
                $menu_arr= User::getUserRoleArray($user);
                $con['id'] = ['in',$menu_arr];
            }

            $result =Db::name('user_menu')
                ->where($con)
                ->field(['name as  title','url','param','icon','id','is_show','level','parent_id','is_iframe'])
                ->order(['level'=>'asc','sort'=>'desc'])->select();
            return Menu::treeList($result);
        }
    }


    public function _empty($name){
        return view($name);
    }

}
