<?php
namespace app\admin\controller;

use app\common\model\UserRole;
use think\Db;
use think\Log;
use think\Request;
use think\response\Json;
use think\Validate;

class Consult extends Common
{

    /**
     * 列表
     */
    public function index(){
        $alias = \app\common\model\Consult::$alias;
        if(Request::instance()->isAjax()){
            $input = array_filter(input('','','trim'));
            return \app\common\model\Consult::getList($input);
        }
        return view('',['alias'=>$alias]);
    }



    public function create(){
        $this->view->engine->layout('common/iframe');
        return view('');
    }


    /**
     * 编辑
     */
    public function edit(){
        try{
            $this->view->engine->layout('common/iframe');
            $id = input('id');
            $role = \app\common\model\Consult::get($id);
            if(empty($role)){
                exception('信息不存在');
            }
            return view('create',['data'=>$role]);
        }catch (\Exception $e) {
            return view('common/error',['msg' => $e->getMessage()]);
        }
    }





    /**
     * 删除
     */
    public function delete(){
        try{
            $id = input('id');
            $role = \app\common\model\Consult::destroy($id);
            if ($role){
                return ['code'=>'0','msg'=>'删除成功'];
            }else{
                return ['code'=>'1','msg'=>'删除成功'];
            }
        }catch (\Exception $exception) {
            return json(['code' => 1, 'msg' => $exception->getMessage(), 'data' => []]);
        }
    }




    /**
     * 回复
     * @return \think\response\View
     * @throws \think\exception\DbException
     */
    public function reply(){
        $this->view->engine->layout('common/iframe');
        $id = input('id');
        if(empty($id)){
            exception('错误请求');
        }
        $opinion = \app\common\model\Consult::get($id);
        if(empty($opinion)){
            exception('信息不存在');
        }
        return view('',['data'=>$opinion]);
    }






    /**
     * 导出
     * 导出excel
     */
    public function export()
    {
        try {
            $data = array_filter(input('','','trim'));
            $data= \app\common\model\Consult::getList($data);
            if ($data['code'] != 0) {
                throw new \think\Exception("查询失败");
            }
            vendor("PHPExcel");
            vendor("PHPExcel.Writer.Excel2007");
            vendor("PHPExcel.IOFactory");

            //列名
            $header = array( '问题','答案', '创建时间',);
            $field = ['question','content','create_time'];

            if (!empty($data["data"])) {
                $name_list =[];
                array_walk($data['data'], function($value, $key) use (&$name_list ,$field){
                    foreach ($field as $val){
                        $name_list [$key][$val] = $value[$val];
                    }
                });
                getExcel("咨询管理表".date('YmdHis'), $header, $name_list);
            } else {
                return '没有相关数据';
            }
        }catch(\Exception $e) {
            Log::notice($e);
            return $e->getMessage();
        }
    }



    /**
     * 保存
     */
    public function save(){
        try{
            $data = input('post.','','trim');
            $data['type'] = 1;
            Db::startTrans();
            if(isset($data['id']) && !empty($data['id'])){
                $role = \app\common\model\Consult::get(['id'=>$data['id']]);
                if($role->force()->update($data) === false){
                    exception('保存失败');
                }
            }else{
                $role = new \app\common\model\Consult($data);
                if(!$role->force()->save($data)){
                    exception('保存失败');
                }
            }
            Db::commit();
            return  json(['code'=>0,'msg'=>'操作成功']);
        } catch (\Exception $e) {
            Db::rollback();
            return json(['code' => 1, 'msg' => $e->getMessage(), 'data' => []]);
        }

    }
}
