<?php
namespace app\admin\controller;

use think\Db;
use think\Request;
use think\Validate;

class Baby extends Common
{

    /**
     * 列表
     */
    public function index(){
        if(Request::instance()->isAjax()){
            $input = array_filter(input('','','trim'));
            return \app\common\model\Baby::getList($input);
        }
        return view('',[]);
    }

    /**
     * 删除
     */
    public function delete(){
        try{
            $id = input('id');
            $role = \app\common\model\Parents::where(['id'=>$id])->delete();
            if ($role){
                return ['code'=>'0','msg'=>'删除成功'];
            }else{
                return ['code'=>'1','msg'=>'删除失败'];
            }
        }catch (\Exception $exception) {
            return json(['code' => 1, 'msg' => $exception->getMessage(), 'data' => []]);
        }
    }


}
