<?php
namespace app\admin\controller;



use think\Db;
use think\Request;

class Index extends Common
{
    public function index(){
        $current = \app\common\model\User::get($this->userId);
        if($current['mobile'] == '16666666666'){
            return $this->redirect('map/index');
        }

        $con = [
            'is_delete'=>2,
        ];
        $parent_count = Db::name('parents')->count();
        $baby_count = Db::name('baby')->count();
        $topic_count = Db::name('topic')->where(['is_delete'=>2])->count();
        $article_count = Db::name('article')->where(['type'=>1,'is_delete'=>2])->count();
        $question_count = Db::name('article')->where(['type'=>2,'is_delete'=>2])->count();

        $total=['parent_count'=>$parent_count ,'baby_count'=>$baby_count,'topic_count'=>$topic_count,'article_count'=>$article_count,'question_count'=>$question_count];
        if(Request::instance()->isAjax()){
            $boy_count = Db::name('baby')->where(['sex'=>0])->count();
            $pie_data = [
                [
                    'name'=>'男宝宝',
                    'value'=>$boy_count,
                ],[
                    'name'=>'女宝宝',
                    'value'=>$baby_count - $boy_count ,
                ]
            ];

            //以后的月份
            for ($i = 6; $i > 0; $i--) {
                $dateMonth[] = GetMonth(-$i);
                $dataM[] = GetMonth(-$i, true);
            }
            //现在的月份
            $dateMonth[6][] = date('Y-m') . '-01';
            $dateMonth[6][] = date('Y-m-d', strtotime(date('Y-m-01', time()) . ' +1 month -1 day'));
            $dataM[] = date('Y-m');
            //之后的月份
            for ($i = 1; $i < 6; $i++) {
                $dateMonth[] = GetMonth($i);
                $dataM[] = GetMonth($i, true);
            }

            foreach ($dateMonth as $k => $v) {
                //洽谈
                $z_qiatan[] = Db::name('parents')->alias('a')
                    ->whereTime('a.create_time', 'between', [$v[0], $v[1]])->count();
                //意向
                $z_yixiang[] = Db::name('baby')->alias('a')->whereTime('a.create_time', 'between', [$v[0], $v[1]])->count();

            }
            $inc = [
                [
                    'name' => '家长',
                    'type' => 'line',
                    'data' => $z_qiatan,
                ], [
                    'name' => '宝宝',
                    'type' => 'line',
                    'data' => $z_yixiang,
                ],
            ];

            return ['pie_data'=>$pie_data,'dateMonth'=>$dataM,'inc'=>$inc];
        }else{
            return view('',['total'=>$total]);
        }

    }
}
