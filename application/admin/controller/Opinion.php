<?php
namespace app\admin\controller;

use app\common\model\UserRole;
use think\Db;
use think\Request;
use think\response\Json;
use think\Validate;

class Opinion extends Common
{

    /**
     * 列表
     */
    public function index(){
        $alias = \app\common\model\Article::$alias;
        if(Request::instance()->isAjax()){
            $input = array_filter(input('','','trim'));
            return \app\common\model\Opinion::getList($input);
        }
        return view('',['alias'=>$alias]);
    }


    public function photos(){
        $this->view->engine->layout('common/iframe');
        $id = input('id');
        if(empty($id)){
            exception('错误请求');
        }
        $opinion = \app\common\model\Opinion::get($id);
        if(empty($opinion)){
            exception('信息不存在');
        }
        if(!empty($opinion['image'])){
            $opinion['image_array'] = explode(',',$opinion['image']);
        }
        return view('',['data'=>$opinion]);

    }


}
