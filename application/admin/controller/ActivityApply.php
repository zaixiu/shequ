<?php

namespace app\admin\controller;

use app\common\model\HotelHall;
use app\common\model\HotelRoom;
use app\common\model\SysFile;
use app\common\model\UserRole;
use think\Config;
use think\Db;
use think\Log;
use think\Request;

class ActivityApply extends Common
{

    public function index(){
        $alias = \app\common\model\ActivityApply::$alias;
        if(Request::instance()->isAjax()){
            $data = array_filter(input('','','trim'));
//            var_dump($data);
            return \app\common\model\ActivityApply::getList($data);
        }
        return view('',['alias'=>$alias]);
    }




    public function create(){
        $this->view->engine->layout('common/iframe');
        $id = input('id');
        if(empty($id)){
            exception('错误请求');
        }
        $activity = \app\common\model\Activity::get(['id'=>$id]);
        if(empty($activity)){
            exception('信息不存在');
        }

        return view('',['activity'=>$activity]);
    }




    /*
    * 审核
    */
    public function audit(){
        $this->view->engine->layout('common/iframe');
        $id = input('id');
        if(empty($id)){
            exception('错误请求');
        }
        $apply = \app\common\model\ActivityApply::get(['id'=>$id]);
        if(empty($apply)){
            exception('信息不存在');
        }

        if(Request::instance()->isAjax()){
            try{
                $data = input();
                $apply->note = $data['note'];
                $apply->status = isset($data['status'])?$data['status']:2;
                $apply->save();
                $activity = \app\common\model\Activity::get($apply['activity_id']);
                //审核失败后 短信通知用户
                if($apply['status'] == 2){
                    $tplId =  Config::get('sms_tpl_id.activity_fail');
                    $smsRes = alsms($tplId, $apply['mobile'], ['customer'=>$apply['contact'],'activity'=>$activity['name']]);
                    if($smsRes['code'] == 1){
                        return json(['code'=>1,'msg'=>'短信发送失败：'.$smsRes['msg']]);
                    }
                }
//                if($apply['status'] == 3){//报名成功
//                    $tplId =  Config::get('sms_tpl_id.activity_success');
//                    $smsRes = alsms($tplId, $apply['mobile'], ['customer'=>$apply['contact'],'activity'=>$activity['name']]);
//
//                }else{//报名失败
//                    $tplId =  Config::get('sms_tpl_id.activity_fail');
//                    $smsRes = alsms($tplId, $apply['mobile'], ['customer'=>$apply['contact'],'activity'=>$activity['name']]);
//                }
//                if($smsRes['code'] == 1){
//                    return json(['code'=>1,'msg'=>'短信发送失败：'.$smsRes['msg']]);
//                }
                return ['code' => 0, 'msg' => '操作成功'];
            }catch (\Exception $e) {
                Db::rollback();
                return json(['code' => 1, 'msg' => $e->getMessage(), 'data' => []]);
            }

        }
        return view('',['data'=>$apply]);
    }




    /**
     * 保存
     * @return \think\response\Json
     */
    public function save(){
        try{
            $data = input('post.','','trim');
            $activity = \app\common\model\Activity::where(['is_delete'=>2,'id'=>$data['activity_id']])->find();
            if(empty($activity)){
                exception('活动信息不存在');
            }
            //添加判断报名人数的限制
//            $left = \app\common\model\Activity::left($activity['id']);
//            if($data['number'] > $left){
//                exception('活动剩余名额'.$left.'人');
//            }
//            $data['money'] = $activity['fee']*$data['number'];
//            if($activity['is_audit'] == 1){
//                $data['status'] = 1;
//            }else{
//                $data['status'] = 3;
//            }

            $apply = new \app\common\model\ActivityApply();
            if($apply->force()->allowfield(true)->save($data) ===false){
                throw new \think\Exception('保存失败');
            }
            $result['msg'] = '操作成功';
            $result['code'] = 0;
            Db::commit();
            return  json($result);
        } catch (\Exception $e) {
            Db::rollback();
            return json(['code' => 1, 'msg' => $e->getMessage(), 'data' => []]);
        }
    }






    /**
     * 导出
     * 导出excel
     */
    public function export()
    {
        try {
            $data = array_filter(input('','','trim'));
            $data= \app\common\model\ActivityApply::getList($data);
            if ($data['code'] != 0) {
                throw new \think\Exception("查询失败");
            }
            vendor("PHPExcel");
            vendor("PHPExcel.Writer.Excel2007");
            vendor("PHPExcel.IOFactory");

            //列名
            $header = array( '活动名称','宝宝名称', '联系方式','宝宝年龄','报名时间');
            $field = ['activity_name','contact','mobile','age','create_time'];

            if (!empty($data["data"])) {
                $name_list =[];
                array_walk($data['data'], function($value, $key) use (&$name_list ,$field){
                    foreach ($field as $val){
                        $name_list [$key][$val] = $value[$val];
                    }
                });
                getExcel("活动报名表".date('YmdHis'), $header, $name_list);
            } else {
                return '没有相关数据';
            }
        }catch(\Exception $e) {
            Log::notice($e);
            return $e->getMessage();
        }
    }
}
