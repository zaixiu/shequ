<?php

namespace app\admin\controller;

use app\common\model\MapConfig;
use think\Db;
use think\Request;

class Map extends Common{

    public function index(){
        $config =  MapConfig::where(['id'=>1])->find()->toArray();
//        var_dump($config);die;
        if(Request::instance()->isAjax()){
            $data = input();
            return \app\common\model\Map::getList($data);
        }
        return view('index',['is_all'=>$config['map_all']]);
    }


    public function add(){
        $this->view->engine->layout("common/iframe");
        $province = model('region')->where('level',1)->select();
        return view('create',['province'=>$province]);
    }


    public function save(){
        $data = input();
        return model('map')->saveData($data,isset($data['id'])?$data['id']:'');
    }


    public function edit(){
        $this->view->engine->layout("common/iframe");
        $id = input('id');
        $data = \app\common\model\Map::getList(['id'=>$id]);

        $data = $data['data'][0];
        $province = model('region')->where('level',1)->select();
        return view('edit',['data'=>$data,'province'=>$province]);
    }


    /**
     * 删除
     */
    public function delete(){
        try{
            $id = input('id');
            \app\common\model\Map::where(['id'=>$id])->delete();
            return ['code'=>'0','msg'=>'删除成功'];
        }catch (\Exception $exception) {
            return json(['code' => 1, 'msg' => $exception->getMessage(), 'data' => []]);
        }
    }


    public function all(){
        try{
            $config = MapConfig::where(['id'=>1])->find();
            $config->map_all =1;
            $config->save();
            return ['code'=>'0','msg'=>'操作成功'];
        }catch (\Exception $exception) {
            return json(['code' => 1, 'msg' => $exception->getMessage(), 'data' => []]);
        }
    }

    public function cancel(){
        try{
            $config = MapConfig::where(['id'=>1])->find();
            $config->map_all =0;
            $config->save();
            return ['code'=>'0','msg'=>'操作成功'];
        }catch (\Exception $exception) {
            return json(['code' => 1, 'msg' => $exception->getMessage(), 'data' => []]);
        }
    }

}
