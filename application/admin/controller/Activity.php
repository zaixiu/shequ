<?php

namespace app\admin\controller;

use app\common\model\HotelHall;
use app\common\model\HotelRoom;
use app\common\model\SysFile;
use app\common\model\UserRole;
use TCPDF;
use TCPDF_COLORS;
use think\Db;
use think\Log;
use think\Request;

class Activity extends Common
{

    public function index(){
        $alias = \app\common\model\Activity::$alias;
        if(Request::instance()->isAjax()){
            $data = array_filter(input('','','trim'));
            if(session('user.role_id') == 5){
                $data['status'] = 2;
            }
            $data= \app\common\model\Activity::getList($data);
            return arrayNumericFormat($data);
        }
        return view('',['alias'=>$alias]);
    }



    public function create(){
        $this->view->engine->layout('common/iframe');
        return view('');
    }


    /**
     * Notes:导出活动详情
     * @throws \think\exception\DbException
     */
    public function pdf(){
        $id = input('id');
        $activity = \app\common\model\Activity::get(['id'=>$id]);
        if(empty($activity)){
            exception('活动信息不存在');
        }
        $activity['is_audit'] =  $activity['is_audit']==1?'是':'否';
        $activity['status'] = $activity['status']==1?'否':'是';
//        var_dump($activity);die;
        vendor("tcpdf_include");
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf->SetTitle('活动详情');
// set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
// set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// ---------------------------------------------------------
// set font
        $pdf->SetFont('stsongstdlight', 'B', 14);


// add a page
        $pdf->AddPage();

        $html = '<h1>活动详情</h1>
	<p><label>活动海报：</label><img src="'.$activity['poster'].'"  width="300"  /> </p>
	<p><label>活动名称：</label>'.$activity['name'].'</p>
	<p><label>活动地点：</label>'.$activity['address'].'</p>
	<p><label>活动时间：</label>'.$activity['start_time'].'~'.$activity['end_time'].'</p>
	<p><label>活动标签：</label>'.$activity['label'].'</p>
	<p><label>活动人数：</label>'.$activity['number'].'</p>
	<p><label>活动价格：</label>'.$activity['fee'].'元/人</p>
	<p><label>是否审核：</label>'.$activity['is_audit'].'</p>
	<p><label>是否发布：</label>'.$activity['status'].'</p>
	<p><label>联系人 ：</label>'.$activity['contact'].'</p>
	<p><label>联系电话：</label>'.$activity['mobile'].'</p>
	<p><label>活动详情：</label>'.$activity['desc'].'</p>
	<p><label>活动总结：</label>'.$activity['summary'].'</p>';

// output the HTML content
        $pdf->writeHTML($html, true, false, true, false, '');
// reset pointer to the last page
        $pdf->lastPage();
//        $pdf->Output('活动详情'.date('YmdHis').'.pdf', 'I');
        $pdf->Output('活动详情'.date('YmdHis').'.pdf', 'D');
    }



    /**
     * 导出
     * 导出excel
     */
    public function export()
    {
        try {
            $data = array_filter(input('','','trim'));
            $data= \app\common\model\Activity::getList($data);
            if ($data['code'] != 0) {
                throw new \think\Exception("查询失败");
            }
            vendor("PHPExcel");
            vendor("PHPExcel.Writer.Excel2007");
            vendor("PHPExcel.IOFactory");

            //列名
            $header = array('活动名称', '联系人', '联系方式', '开始时间','结束时间','是否发布','报名人数','待审核人数');
            $field = ['name','contact','mobile','start_time','end_time','status_alias','total_number','unaudit'];

            if (!empty($data["data"])) {
                $name_list =[];
                array_walk($data['data'], function($value, $key) use (&$name_list ,$field){
                    foreach ($field as $val){
                        $name_list [$key][$val] = $value[$val];
                    }
                });
                getExcel("活动表".date('YmdHis'), $header, $name_list);
            } else {
                return '没有相关数据';
            }
        }catch(\Exception $e) {
            Log::notice($e);
            return $e->getMessage();
        }
    }

    /*
     * 活动总结
     */
    public function summary(){
        $this->view->engine->layout('common/iframe');
        $id = input('id');
        if(empty($id)){
            exception('错误请求');
        }
        $activity = \app\common\model\Activity::get(['id'=>$id]);
        if(empty($activity)){
            exception('信息不存在');
        }

        return view('',['activity'=>$activity]);
    }



    public function summarySave(){
        try{
            $data = input();

            if(empty($data['id'])){
                exception('错误请求');
            }
            if(!$data['summary']){
                exception('请填写活动总结');
            }
            $activity = \app\common\model\Activity::get(['id'=>$data['id']]);
            $activity->summary = $data['summary'];
            $activity->save();
            return ['code' => 0, 'msg' => '操作成功'];
        }catch (\Exception $e) {
            Db::rollback();
            return json(['code' => 1, 'msg' => $e->getMessage(), 'data' => []]);
        }

    }



    public function apply(){
        $this->view->engine->layout('common/iframe');
        $id = input('id');
        if(empty($id)){
            exception('错误请求');
        }
        $activity = \app\common\model\Activity::get(['id'=>$id]);
        if(empty($activity)){
            exception('信息不存在');
        }
        $customer = [];
        if(session('user.role_id') == 5){
            $customer_id = session('user.etprs_id');
            $customer = \app\common\model\Customer::get($customer_id);

        }


        return view('',['activity'=>$activity,'customer'=>$customer]);
    }

    public function edit(){
        try{
            $this->view->engine->layout('common/iframe');
            $id = input('id');
            if(empty($id)){
                exception('错误请求');
            }
            $activity = \app\common\model\Activity::get(['id'=>$id]);
            if(empty($activity)){
                exception('信息不存在');
            }
            return view('',['activity'=>$activity]);
        }catch (\Exception $e) {
            return view('common/error',['msg' => $e->getMessage()]);
        }

    }


    public function detail(){
        try{
            $this->view->engine->layout('common/iframe');
            $id = input('id');
            if(empty($id)){
                exception('错误请求');
            }
            $activity = \app\common\model\Activity::get(['id'=>$id]);
            if(empty($activity)){
                exception('错误请求');
            }

            return view('',['activity'=>$activity]);
        }catch (\Exception $e) {
            return view('common/error',['msg' => $e->getMessage()]);
        }

    }

    /**
     * 保存用户
     * @return \think\response\Json
     */
    public function save(){
        try{
            $data = input('post.','','trim');
            $data['is_audit'] = isset($data['is_audit'])?$data['is_audit']:2;
            $data['status'] = isset($data['status'])?$data['status']:1;
            $date = explode(' ~ ',$data['date_range']);
            $data['start_time'] = $date[0];
            $data['end_time'] = $date[1];
            $data['type'] = 1;
            unset($data['date_range']);
            unset($data['image']);
            unset($data['file']);
//            if(!$data['desc']){
//                exception('请填写活动介绍');
//            }
            if(!$data['poster']){
                exception('请上传活动海报');
            }
            Db::startTrans();
            if(isset($data['id']) && !empty($data['id'])){
                $activity = \app\common\model\Activity::get(['id'=>$data['id']]);
                if($activity->force()->allowfield(true)->update($data) ===false){
                    throw new \think\Exception('修改失败');
                }
            }else{
                $activity = new \app\common\model\Activity();

                if($activity->force()->allowfield(true)->save($data) ===false){
                    throw new \think\Exception('保存失败');
                }
            }

            $result['msg'] = '操作成功';
            $result['code'] = 0;
            Db::commit();
            return  json($result);
        } catch (\Exception $e) {
            Db::rollback();
            return json(['code' => 1, 'msg' => $e->getMessage(), 'data' => []]);
        }
    }


    public function delete(){
        try{
            $id = input('id');
            if(empty($id)){
                exception('错误请求');
            }
            $activity = \app\common\model\Activity::get($id);
            if(empty($activity)){
                exception('信息不存在');
            }
            $activity->is_delete = 1;
            $activity->save();
            return ['code'=>'0','msg'=>'删除成功'];
        }catch (\Exception $e) {
            return json(['code' => 1, 'msg' => $e->getMessage(), 'data' => []]);
        }
    }

}
