<?php
namespace app\admin\controller;

use think\Config;
use think\Controller;
use think\Db;
use think\Exception;
use think\Log;

/**
 * 定时任务处理
 * Class Task
 * @package app\admin\controller
 */
class Task extends Controller
{


    /**
     *退出
     * @return \think\response\Json
     */
    public function quit(){
        try{
            $date = date('Y-m-d');
            $applys = \app\common\model\QuitApply::where(['status'=>6,'quit_time'=>$date])->select();
            foreach($applys as $v){
                \app\common\model\QuitApply::quit($v['id']);
            }
        }catch (Exception $e){
            Log::record($e->getMessage());
        }

    }



    /*
     * 合同快到期短信提醒
     */
    public function pact(){
        try{
            $con = [
                'a.is_delete'=>2
            ];
            $date = date("Y-m-d",strtotime("+30 day"));
            $con['a.end_time'] = $date;
            $con['a.status'] = 2;
            $join = [
                ['customer b', 'a.customer_id = b.id', 'left'],
            ];

            $user = \app\common\model\CustomerPact::alias('a')->where($con)->field('a.pact_name,a.end_time,b.mobile,b.name customer')->join($join)->select();

            if(!empty($user)){
                $tplId =  Config::get('sms_tpl_id.pact_deadline');
                foreach($user as $k => $v){
                    alsms($tplId, $v['mobile'], $v);
                }
            }
        }catch (Exception $e){
            Log::record($e->getMessage());
        }

    }


    /**
     * 缴费快到期短信提醒
     */
    public function pactFee(){
        try{
            $con = [
                'a.is_delete'=>2
            ];

            $date = date("Y-m-d",strtotime("+15 day"));
            $con['a.pay_time'] = $date;
            $con['a.status'] = 1;
            $join = [
                ['customer b', 'a.customer_id = b.id', 'left'],
                ['hotel_room e', 'a.room_id = e.id', 'left'],
                ['hotel_hall f', 'e.hotel_hall_id = f.id', 'left'],
                ['hotel_floor g', 'e.hotel_floor_id = g.id', 'left'],
            ];

            $user = \app\common\model\CustomerPactFeeDetail::alias('a')->where($con)->field('a.pay_time,b.mobile,b.name customer,e.name room_name,f.name hall_name,g.name floor_name')->join($join)->select();

            if(!empty($user)){
                $tplId =  Config::get('sms_tpl_id.fee_deadline');
                foreach($user as $k => &$v){
                    $v['room_name'] = $v['hall_name'].'-'.$v['floor_name'].'-'.$v['room_name'];
                    $res = alsms($tplId, $v['mobile'], $v);
//                    var_dump($res);die;
                }
            }
        }catch (Exception $e){
            Log::record($e->getMessage());
        }

    }


}
