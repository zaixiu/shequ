<?php

namespace app\admin\controller;
use think\Controller;
use think\Request;

class Region extends Controller{

    public function get_city(){
        $parentid = input('parentid');
        $city = model('region')->where('parentid',$parentid)->select();
        return $city;
    }
}
