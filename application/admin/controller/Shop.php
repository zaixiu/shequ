<?php

namespace app\admin\controller;

use think\Request;

class Shop extends Common{

    public function index(){
        if(Request::instance()->isAjax()){
            $data = input();
            return \app\common\model\Shop::getList($data);
        }
        return view('index');
    }


    public function add(){
        $this->view->engine->layout("common/iframe");
        $province = model('region')->where('level',1)->select();
        return view('create',['province'=>$province]);
    }


    public function save(){
        $data = input();
        return model('shop')->saveData($data,isset($data['id'])?$data['id']:'');
    }


    public function edit(){
        $this->view->engine->layout("common/iframe");
        $id = input('id');
        $data = \app\common\model\Shop::getList(['id'=>$id]);

        $data = $data['data'][0];
        $province = model('region')->where('level',1)->select();
        return view('edit',['data'=>$data,'province'=>$province]);
    }


    /**
     * 删除
     */
    public function delete(){
        try{
            $id = input('id');
            $poster = \app\common\model\Shop::where(['id'=>$id])->update(['is_delete'=>1]);
            if ($poster){
                return ['code'=>'0','msg'=>'删除成功'];
            }else{
                return ['code'=>'1','msg'=>'删除失败'];
            }
        }catch (\Exception $exception) {
            return json(['code' => 1, 'msg' => $exception->getMessage(), 'data' => []]);
        }
    }

}
