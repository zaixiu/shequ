<?php
namespace app\admin\controller;

use app\common\model\Message;
use think\Db;
use think\Request;
use think\Validate;

class Article extends Common
{

    /**
     * 列表
     */
    public function index(){
        $topic = \app\common\model\Topic::getSelectArray();
        $doctor = \app\common\model\User::getSelectArray(['type'=>2]);
        if(Request::instance()->isAjax()){
            $input = array_filter(input('','','trim'));
            $input['type'] = 1;
            return \app\common\model\Article::getList($input);
        }
        return view('',['topic'=>$topic,'doctor'=>$doctor]);
    }


    /**
     * 创建
     */
    public function create(){
        $this->view->engine->layout('common/iframe');
        $topic = \app\common\model\Topic::getSelectArray();
        $doctor = \app\common\model\User::getSelectArray(['type'=>2]);
        return view('',['topic'=>$topic,'doctor'=>$doctor]);
    }


    /**
     * 编辑
     */
    public function edit(){
        try{
            $this->view->engine->layout('common/iframe');
            $id = input('id');
            $article = \app\common\model\Article::get($id);
            if(empty($article)){
                exception('角色信息不存在');
            }
            $topic = \app\common\model\Topic::getSelectArray();
            $doctor = \app\common\model\User::getSelectArray(['type'=>2]);
            return view('create',['data'=>$article,'topic'=>$topic,'doctor'=>$doctor]);
        }catch (\Exception $e) {
            return view('common/error',['msg' => $e->getMessage()]);
        }
    }


    /**
     * 删除
     */
    public function delete(){
        try{
            $id = input('id');
            $role = \app\common\model\Article::where(['id'=>$id])->update(['is_delete'=>1]);
            if ($role){
                return ['code'=>'0','msg'=>'删除成功'];
            }else{
                return ['code'=>'1','msg'=>'删除失败'];
            }
        }catch (\Exception $exception) {
            return json(['code' => 1, 'msg' => $exception->getMessage(), 'data' => []]);
        }
    }


    /**
     * 保存
     */
    public function save(){
        try{
            $data = input('post.','','trim');
            $data['is_show'] = isset($data['is_show'])?$data['is_show']:0;
            $data['is_report'] = isset($data['is_report'])?$data['is_report']:0;
            $data['type'] = 1;
            unset($data['file']);
            Db::startTrans();
            if(empty($data['image'])){
                \exception('请上传封面图片');
            }
            if(empty($data['content'])){
                \exception('请上传文章内容');
            }
            if(isset($data['id']) && !empty($data['id'])){
                $role = \app\common\model\Article::get(['id'=>$data['id']]);
                if($role->allowField(true)->force()->update($data) === false){
                    exception('保存失败');
                }
            }else{
                $role = new \app\common\model\Article($data);
                $data['user_id'] = $this->userId;
                if(!$role->allowField(true)->force()->save($data)){
                    exception('保存失败');
                }
                //message
                $follow = \app\common\model\Follow::where(['type'=>3,'doctor_id'=>$this->userId])->column('operate_id');
                if(!empty($follow)){
                    foreach ($follow as  $v){
                        $message = new Message();
                        $message->type = 4;
                        $message->parent_id = $v;
                        $message->operate_id = $this->userId;
                        $message->relate_id = $role['id'];
                        $message->save();
                    }
                }
            }
            Db::commit();
            return  json(['code'=>0,'msg'=>'操作成功']);
        } catch (\Exception $e) {
            Db::rollback();
            return json(['code' => 1, 'msg' => $e->getMessage(), 'data' => []]);
        }

    }

}
