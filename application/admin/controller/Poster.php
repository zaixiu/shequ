<?php

namespace app\admin\controller;

use app\common\model\SysFile;
use think\Db;
use think\Image;
use think\Request;
use think\Validate;

class Poster extends Common{

    public function index(){
        if(Request::instance()->isAjax()){
            $data = input();
            return \app\common\model\Poster::getList($data);
        }
        return view('index');
    }


    public function add(){
        $this->view->engine->layout("common/iframe");
        return view('create');
    }


    public function save(){
        $data = input();
        return model('poster')->saveData($data,isset($data['id'])?$data['id']:'');
    }


    public function edit(){
        $this->view->engine->layout("common/iframe");
        $id = input('id');
        $data = \app\common\model\Poster::where('id',$id)->find();
        return view('edit',['data'=>$data]);
    }


    /**
     * 删除
     */
    public function delete(){
        try{
            $id = input('id');
            $poster = \app\common\model\Poster::where(['id'=>$id])->update(['is_delete'=>1]);
            if ($poster){
                return ['code'=>'0','msg'=>'删除成功'];
            }else{
                return ['code'=>'1','msg'=>'删除失败'];
            }
        }catch (\Exception $exception) {
            return json(['code' => 1, 'msg' => $exception->getMessage(), 'data' => []]);
        }
    }

}
