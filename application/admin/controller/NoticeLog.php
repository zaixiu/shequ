<?php
namespace app\admin\controller;
use app\common\model\UserRole;
use think\Request;
class NoticeLog extends Common
{

    public function index(){
        $role = UserRole::getRoleSelect();
        if(Request::instance()->isAjax()){
            $data = array_filter(input('','','trim'));
            $data['user_id'] = session('user.id');
            $data= \app\common\model\NoticeLog::getList($data);
            return arrayNumericFormat($data);
        }
        return view('',['role'=>$role]);
    }


    //
    public function detail(){
        try{
            $this->view->engine->layout('common/iframe');
            $id = input('id');
            if(empty($id)){
                exception('错误请求');
            }
            $notice_log = \app\common\model\NoticeLog::get($id);
            if(empty($notice_log)){
                exception('信息不存在');
            }
            $notice_log->is_read = 1;
            $notice_log->save();
            $notice = \app\common\model\Notice::get($notice_log['notice_id']);
            if(empty($notice)){
                exception('信息不存在');
            }
            return view('',['data'=>$notice]);
        }catch (\Exception $e) {
            return view('common/error',['msg' => $e->getMessage()]);
        }

    }


}
