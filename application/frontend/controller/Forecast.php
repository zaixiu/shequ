<?php

namespace app\frontend\controller;

use app\admin\controller\SendCode;
use app\common\model\BabyGrowLog;
use app\common\model\Parents;
use app\common\model\StandardHeight;
use think\Db;
use think\Exception;
use think\Request;
use think\Validate;

class Forecast extends Common
{

    /**
     * 列表
     */
    public function index()
    {
        $before_measure_time = date('Y-m-d', strtotime(" -3 month"));
        return view('', ['before_measure_time' => $before_measure_time]);
    }


    public function detail()
    {
        $grow_log_id = input('grow_log_id',null,'trim');
//        $grow_log_id = 22;
        $log = \app\common\model\BabyGrowLog::get($grow_log_id);
        $baby = \app\common\model\Baby::get($log['baby_id']);
        $log['rate'] = \app\common\model\BabyGrowLog::rate($log['before_measure_time'], $log['measure_time'], $log['before_height'], $log['height']);
        $log['month_later'] = $log['rate'] ? $log['height'] + round($log['rate'] / 12 * 3, 1) : false;
        $log['bmi'] = round($log['weight'] / ($log['height'] * $log['height'] / 10000), 1);
        $baby['inherit_height'] = \app\common\model\Baby::inheritHeight($baby['id']);
        $baby['inherit_height_val'] = \app\common\model\Baby::inheritHeightVal($baby['id']);
        $baby['age'] = \app\common\model\Baby::age($baby['birth']);
        $age_array = \app\common\model\BabyGrowLog::age($baby['birth'], $log['measure_time']);
        $baby['int_age'] = $age_array['year'];
        $report = StandardHeight::heightReportH5($baby['sex'] == 1 ? 2 : 1, $age_array['months'], $log['height']);
        $report['forecast_diff'] = $report['forecast'] - $baby['hope_height'];
        $report['forecast_diff_desc'] = $report['forecast_diff'] > 0 ? '高' . $report['forecast_diff'] . 'cm' : '矮' . abs($report['forecast_diff']) . 'cm';
        $report['avg_rate'] = StandardHeight::avgRate($baby['sex'] == 1 ? 2 : 1, $age_array['year']);

        $fat_boy = [
            [
                'class' => '一年级',
                'level_1' => '13.5-18.1',
                'level_2' => '≤13.4',
                'level_3' => '18.2-20.3',
                'level_4' => '≥20.4',
            ], [
                'class' => '二年级',
                'level_1' => '13.7-18.4',
                'level_2' => '≤13.6',
                'level_3' => '18.5-20.4',
                'level_4' => '≥20.5',
            ], [
                'class' => '三年级',
                'level_1' => '13.9-19.4',
                'level_2' => '≤13.8',
                'level_3' => '19.5-22.1',
                'level_4' => '≥22.2',
            ], [
                'class' => '四年级',
                'level_1' => '14.2-20.1',
                'level_2' => '≤14.1',
                'level_3' => '20.2-22.6',
                'level_4' => '≥22.7',
            ], [
                'class' => '五年级',
                'level_1' => '14.4-21.4',
                'level_2' => '≤14.3',
                'level_3' => '21.5-24.1',
                'level_4' => '≥24.2',
            ], [
                'class' => '六年级',
                'level_1' => '14.7-21.8',
                'level_2' => '≤14.6',
                'level_3' => '21.9-24.5',
                'level_4' => '≥24.6',
            ], [
                'class' => '初一',
                'level_1' => '15.5-22.1',
                'level_2' => '≤15.4',
                'level_3' => '22.2-24.9',
                'level_4' => '≥25.0',
            ], [
                'class' => '初二',
                'level_1' => '15.7-22.5',
                'level_2' => '≤15.6',
                'level_3' => '22.6-25.2',
                'level_4' => '≥25.3',
            ], [
                'class' => '初三',
                'level_1' => '15.8-22.8',
                'level_2' => '≤15.7',
                'level_3' => '22.9-26.0',
                'level_4' => '≥26.1',
            ], [
                'class' => '高一',
                'level_1' => '16.5-23.2',
                'level_2' => '≤16.4',
                'level_3' => '23.3-26.3',
                'level_4' => '≥26.4',
            ], [
                'class' => '高二',
                'level_1' => '16.8-23.7',
                'level_2' => '≤16.7',
                'level_3' => '23.8-26.5',
                'level_4' => '≥26.6',
            ], [
                'class' => '高三',
                'level_1' => '17.3-23.8',
                'level_2' => '≤17.2',
                'level_3' => '23.9-27.3',
                'level_4' => '≥27.4',
            ], [
                'class' => '大学',
                'level_1' => '17.9-23.9',
                'level_2' => '≤17.8',
                'level_3' => '24.0-27.9',
                'level_4' => '≥28.0',
            ]
        ];

        $fat_girl = [
            [
                'class' => '一年级',
                'level_1' => '13.3-17.3',
                'level_2' => '≤13.2',
                'level_3' => '17.2-19.2',
                'level_4' => '≥19.3',
            ], [
                'class' => '二年级',
                'level_1' => '13.5-17.8',
                'level_2' => '≤13.4',
                'level_3' => '17.9-20.2',
                'level_4' => '≥20.3',
            ], [
                'class' => '三年级',
                'level_1' => '13.6-18.6',
                'level_2' => '≤13.5',
                'level_3' => '18.7-21.1',
                'level_4' => '≥21.2',
            ], [
                'class' => '四年级',
                'level_1' => '13.7-19.4',
                'level_2' => '≤13.6',
                'level_3' => '19.5-22.0',
                'level_4' => '≥22.1',
            ], [
                'class' => '五年级',
                'level_1' => '13.8-20.5',
                'level_2' => '≤13.7',
                'level_3' => '20.6-22.9',
                'level_4' => '≥23.0',
            ], [
                'class' => '六年级',
                'level_1' => '14.2-20.8',
                'level_2' => '≤14.1',
                'level_3' => '20.9-23.6',
                'level_4' => '≥23.7',
            ], [
                'class' => '初一',
                'level_1' => '14.8-21.7',
                'level_2' => '≤14.7',
                'level_3' => '21.8-24.4',
                'level_4' => '≥24.5',
            ], [
                'class' => '初二',
                'level_1' => '15.3-22.2',
                'level_2' => '≤15.2',
                'level_3' => '22.3-24.8',
                'level_4' => '≥24.9',
            ], [
                'class' => '初三',
                'level_1' => '16.0-22.6',
                'level_2' => '≤15.9',
                'level_3' => '22.7-25.1',
                'level_4' => '≥25.2',
            ], [
                'class' => '高一',
                'level_1' => '16.5-22.7',
                'level_2' => '≤16.4',
                'level_3' => '22.8-25.2',
                'level_4' => '≥25.3',
            ], [
                'class' => '高二',
                'level_1' => '16.9-23.2',
                'level_2' => '≤16.8',
                'level_3' => '23.3-25.4',
                'level_4' => '≥25.5',
            ], [
                'class' => '高三',
                'level_1' => '17.1-23.3',
                'level_2' => '≤17.0',
                'level_3' => '23.4-25.7',
                'level_4' => '≥25.8',
            ], [
                'class' => '大学',
                'level_1' => '17.2-23.9',
                'level_2' => '≤17.1',
                'level_3' => '24.0-27.9',
                'level_4' => '≥28.0',
            ],
        ];

        $fat = $baby['sex'] == 1? $fat_girl:$fat_boy;

        $product = Db::name('product')->alias('a')->where(['id'=>1])->find();
        $spokesman = Db::name('spokesman')->alias('a')->where(['id'=>1])->find();
        $case_show = Db::name('case_show')->alias('a')->where(['id'=>1])->find();
        $activity = Db::name('activity')->alias('a')->where(['type'=>1,'status'=>2,'is_delete'=>2])->find();
        $consult = Db::name('consult')->alias('a')->where(['type'=>1])->select();


        return view('', ['grow_log_id' => $grow_log_id, 'report' => $report, 'child' => $baby, 'log' => $log,'fat'=>$fat,
            'product'=>$product,'spokesman'=>$spokesman,'case_show'=>$case_show,'activity'=>$activity,'consult'=>$consult]);
    }


    /**
     * 添加保存
     * @return \think\response\Json
     * @throws \Exception
     */
    public function add()
    {
        try {
            Db::startTrans();
            $input = input('post.', null, 'trim');
            $validate_code = SendCode::validateMobileCode($input['mobile'], $input['code']);
            if ($validate_code['code'] == 0) {
                exception($validate_code['msg']);
            }
            //添加parent
            $parent = Parents::where(['mobile' => $input['mobile']])->find();
            if (empty($parent)) {
                $parent = new Parents();
                if ($parent->allowField(true)->force()->save(["mobile" => $input['mobile'], 'nickname' => $input['nickname'], 'source' => 2]) === false) {
                    exception('家长信息保存失败');
                }
            }
            //添加baby
            $baby = \app\common\model\Baby::where(['parent_id' => $parent->id, 'name' => $input['name']])->find();
            if (empty($baby)) {
                $count = \app\common\model\Baby::where(['parent_id' => $parent->id])->count();
                $baby = new \app\common\model\Baby();
                $baby->name = $input['name'];
                $baby->is_default = $count == 0 ? 1 : 0;
                $baby->parent_id = $parent->id;
            }
            $baby->image = $input['image'];
            $baby->sex = $input['sex'];
            $baby->birth = $input['birth'];
            $baby->farther_height = $input['farther_height'];
            $baby->mother_height = $input['mother_height'];
            $baby->hope_height = $input['hope_height'];
            $baby->allowField(true)->force()->save();

            //身高记录
            $log = new BabyGrowLog();
            $log->parent_id = $parent->id;
            $log->baby_id = $baby->id;

            $log->measure_time = date('Y-m-d');
            $log->height = $input['height'];
            $log->weight = $input['weight'];
            $log->bone_age = $input['bone_age'];
            if (!empty($input['before_height'])) {
                $log->before_height = $input['before_height'];
                $log->before_measure_time = $input['before_measure_time'];
            }
            $log->force()->save();
            Db::commit();
            return $this->succeed('保存成功', ['log_id' => $log['id']]);
        } catch (Exception $e) {
            Db::rollback();
            return $this->fail($e->getMessage());
        }
    }


    /**
     * 身高测评报告
     * @return \think\response\Json
     */
    public function report()
    {
        try {
            $input = input('post.', null, 'trim');
            $log = \app\common\model\BabyGrowLog::get($input['grow_log_id']);
            $baby = \app\common\model\Baby::get($log['baby_id']);
            $log['rate'] = \app\common\model\BabyGrowLog::rate($log['before_measure_time'], $log['measure_time'], $log['before_height'], $log['height']);
            $log['month_later'] = $log['rate'] ? $log['height'] + round($log['rate'] / 12 * 3, 1) : false;
            $log['bmi'] = round($log['weight'] / ($log['height'] * $log['height'] / 10000), 1);
            $baby['inherit_height'] = \app\common\model\Baby::inheritHeight($baby['id']);
            $baby['inherit_height_val'] = \app\common\model\Baby::inheritHeightVal($baby['id']);
            $baby['age'] = \app\common\model\Baby::age($baby['birth']);
            $age_array = \app\common\model\BabyGrowLog::age($baby['birth'], $log['measure_time']);
            $baby['int_age'] = $age_array['year'];
            $report = StandardHeight::heightReport($baby['sex'] == 1 ? 2 : 1, $age_array['months'], $log['height']);
            $report['forecast_diff'] = $report['forecast'] - $baby['hope_height'];
            $report['forecast_diff_desc'] = $report['forecast_diff'] > 0 ? '高' . $report['forecast_diff'] . 'cm' : '矮' . abs($report['forecast_diff']) . 'cm';
            $report['avg_rate'] = StandardHeight::avgRate($baby['sex'] == 1 ? 2 : 1, $age_array['year']);
            return $this->succeed('操作成功', ['report' => $report, 'baby' => $baby, 'log' => $log]);

        } catch (\Exception $e) {
            return $this->fail($e->getMessage());
        }

    }


    /**
     * 身高统计
     * @return \think\response\Json
     */
    public function height()
    {
        try {
            $input = input('post.', null, 'trim');
            $log = \app\common\model\BabyGrowLog::get($input['grow_log_id']);
            $baby = \app\common\model\Baby::get($log['baby_id']);
            $data = StandardHeight::getCanvasData($baby['sex'] == 1 ? '2' : '1', $baby['id']);
            return $this->succeed('操作成功', $data);
        } catch (Exception $e) {
            Db::rollback();
            return $this->fail($e->getMessage());
        }
    }


}
