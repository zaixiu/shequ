<?php

namespace app\frontend\controller;

use app\admin\controller\SendCode;
use app\common\model\BabyGrowLog;
use app\common\model\Parents;
use app\common\model\Region;
use app\common\model\StandardHeight;
use think\Db;
use think\Exception;
use think\Request;
use think\Validate;

class Statement extends Common
{
    public function index()
    {
        return view('index',['top_title'=>'关于百适滴DHA配方相关声明']);
    }


    public function evaluate()
    {
        return view('evaluate',['top_title'=>'百适滴增乐高功能评价报告']);
    }


}
