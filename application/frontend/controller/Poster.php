<?php

namespace app\frontend\controller;

use app\admin\controller\SendCode;
use app\common\model\BabyGrowLog;
use app\common\model\Parents;
use app\common\model\StandardHeight;
use think\Db;
use think\Exception;
use think\Request;
use think\Validate;

class Poster extends Common
{
    public function index()
    {
        $poster_id = input('poster_id',null,'trim');
//        $poster_id = 7;
        $poster = \app\common\model\Poster::where(['is_delete'=>2,'id'=>$poster_id])->find();
//        var_dump($poster);die;
        if(empty($poster)){
            return '海报信息不存在';
        }
        return view('', ['data' => $poster,'top_title'=>$poster['title']]);
    }


    public function share(){
        $path = input('path');
        $poster_id = input('poster_id');
        $poster = \app\common\model\Poster::where(['is_delete'=>2,'id'=>$poster_id])->find();
        if(empty($poster)){
            return '海报信息不存在';
        }
        $url = url('/frontend/poster/share',['path'=>$path,'poster_id'=>$poster_id],true,true);
        return view('', ['path' => $path,'data' => $poster,'top_title'=>$poster['title'],'url'=>$url]);
    }


    public function orientation(){
        $orientation = input();
        var_dump($orientation);die;
    }
}
