<?php

namespace app\frontend\controller;

use think\Controller;
use think\Db;

class Common extends Controller
{
    public $userId = '';
    public $top_title = '';

    public function _initialize(){

    }

    /**
     * 成功反馈封装
     * @param $message
     * @param array $data
     * @return \think\response\Json
     */
    public function succeed($message, $data = [])
    {
        return json([
            'code' => 1,
            'msg' => $message,
            'data' => $data
        ]);
    }

    /**
     * 失败反馈
     * @param $message
     * @param int $code
     * @param array $data
     * @return \think\response\Json
     */
    protected function fail($message, $code = 0, $data = [])
    {
        return json([
            'code' => $code,
            'msg' => $message,
            'data' => $data
        ]);
    }
}
