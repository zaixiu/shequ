<?php

namespace app\frontend\controller;

use app\admin\controller\SendCode;
use app\common\model\BabyGrowLog;
use app\common\model\Parents;
use app\common\model\Region;
use app\common\model\StandardHeight;
use think\Db;
use think\Exception;
use think\Request;
use think\Validate;

class Shop extends Common
{
    public function index()
    {
        $provice = Region::where(['level'=>1])->select();
        if(Request::instance()->isAjax()){
            $data = input();
            return \app\common\model\Shop::getList($data);
        }
        return view('index',['top_title'=>'店铺列表','province'=>$provice]);
    }

}
