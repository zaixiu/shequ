<?php

namespace app\frontend\controller;

use app\admin\controller\SendCode;
use app\common\model\BabyGrowLog;
use app\common\model\Parents;
use app\common\model\StandardHeight;
use think\Cache;
use think\Db;
use think\Exception;
use think\Request;
use think\Validate;

class Auth extends Common
{
    private $appid = 'wx32edfcf44314a839';
    private $appsecret  = '84b578277ec951c7977df05d1605db14';

    public function getWxAccessToken(){
        $access_token = Cache::get('wx_access_token');
        if($access_token){
            return ['code'=>0,'access_token'=>$access_token];
        }else{
            //1.请求url地址
            $url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=".$this->appid."&secret=".$this->appsecret;
            $res = $this->curl_request($url);
            if(isset($res['errcode']) && $res['errcode']!=0){
                return ['code'=>$res['errcode'],'msg'=>$res['errmsg']];
            }
            $access_token = $res['access_token'];
            Cache::set('wx_access_token',$access_token,7000);
            return ['code'=>0,'access_token'=>$access_token];
        }
    }


    public function getjsapiTicket(){
        $jsapi_ticket = Cache::get('wx_jsapi_ticket');
        if($jsapi_ticket){
            return ['code'=>0,'jsapi_ticket'=>$jsapi_ticket];
        }else{
            $access_token_res = $this->getWxAccessToken();
            if($access_token_res['code'] != 0){
                return $access_token_res;
            }
            $access_token = $access_token_res['access_token'];
            $url = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token=".$access_token."&type=jsapi";
            $res = $this->curl_request($url);
            if(isset($res['errcode']) && $res['errcode']!=0){
                return ['code'=>$res['errcode'],'msg'=>$res['errmsg']];
            }
            $jsapi_ticket = $res['ticket'];
            Cache::set('wx_jsapi_ticket',$jsapi_ticket,7000);
            return ['code'=>0,'jsapi_ticket'=>$jsapi_ticket];
        }

    }
    public  function getsignature(){
        $url = input('url');
//        $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
//        $url = "$protocol$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $jsapi_ticket_res = $this->getjsapiTicket();
        if($jsapi_ticket_res['code'] != 0){
            return $jsapi_ticket_res;
        }
        $jsapi_ticket = $jsapi_ticket_res['jsapi_ticket'];
        $noncestr = $this->createNonceStr();
        $timestamp = time();
        $string = "jsapi_ticket=$jsapi_ticket&noncestr=$noncestr&timestamp=$timestamp&url=$url";
//        $str = 'jsapi_ticket='.$jsapi_ticket.'&noncestr='.$noncestr.'&timestamp='.$timestamp.'url='.$url;
        $signature = sha1($string);
        return json(['code'=>0,'signature'=>$signature,'timestamp'=>$timestamp,'noncestr'=>$noncestr,'jsapi_ticket'=>$jsapi_ticket]);
    }

    private function createNonceStr($length = 16) {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $str = "";
        for ($i = 0; $i < $length; $i++) {
            $str .= substr($chars, mt_rand(0, strlen($chars) - 1), 1);
        }
        return $str;
    }

    //参数1：访问的URL，参数2：post数据(不填则为GET)，参数3：提交的$cookies,参数4：是否返回$cookies
    function curl_request($url,$post='',$cookie='', $returnCookie=0){
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; Trident/6.0)');
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_AUTOREFERER, 1);
//        curl_setopt($curl, CURLOPT_REFERER, "http://XXX");
        curl_setopt($curl, CURLOPT_REFERER, $url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER , false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST  , false);
        if($post) {
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($post));
        }
        if($cookie) {
            curl_setopt($curl, CURLOPT_COOKIE, $cookie);
        }
        curl_setopt($curl, CURLOPT_HEADER, $returnCookie);
        curl_setopt($curl, CURLOPT_TIMEOUT, 10);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $data = curl_exec($curl);
        if (curl_errno($curl)) {
            return curl_error($curl);
        }
        curl_close($curl);
        if($returnCookie){
            list($header, $body) = explode("\r\n\r\n", $data, 2);
            preg_match_all("/Set\-Cookie:([^;]*);/", $header, $matches);
            $info['cookie']  = substr($matches[1][0], 1);
            $info['content'] = $body;
            return $info;
        }else{
            return json_decode($data,true);
        }
    }
}

