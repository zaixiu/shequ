<?php
namespace app\app\controller;

use think\Db;

class TestNutrition extends Base
{

    public function index()
    {
        try{
            $result =Db::name('test_nutrition')->order(['parent_id'=>'asc','sort'=>'desc'])->select();
            $list = \app\common\model\TestNutrition::treeList($result);
            return $this->succeed('操作成功',['data'=>$list]);
        }catch (\Exception $e){
            return $this->fail($e->getMessage());
        }

    }

}
