<?php
namespace app\app\controller;


use app\common\model\Comment;
use app\common\model\User;
use think\Db;
use think\Exception;

class Opinion extends Base
{

    /**
     * 添加保存
     * @return \think\response\Json
     * @throws \Exception
     */
    public function add(){
        try{
            Db::startTrans();
            $input = input('post.',null,'trim');
            $input['operate_id'] = $this->parent_id;
            $follow = new \app\common\model\Opinion();
            $follow->allowField(true)->force()->save($input);
            Db::commit();
            return $this->succeed('保存成功',[]);
        }catch (Exception $e){
            Db::rollback();
            return $this->fail($e->getMessage());
        }
    }



}
