<?php
namespace app\app\controller;

use app\common\model\BabyGrowLog;
use app\common\model\Comment;
use think\Db;
use think\Exception;
use think\Validate;

class Doctor extends Base
{
    /**
     * 医生详情页
     * @return \think\response\Json
     */
    public function detail()
    {
        try{
            $input = input('post.',null,'trim');
            $doctor = \app\common\model\User::where(['type'=>2,'id'=>$input['id']])->find();
            if(empty($doctor)){
                exception('医生不存在');
            }
            $list = \app\common\model\Article::getList(['type'=>1,'doctor_id'=>$input['id']]);
            if($list['code'] == 1){
                \exception($list['msg']);
            }
            return $this->succeed('操作成功',['doctor'=>$doctor,'article'=>$list['data']]);
        }catch (\Exception $e){
            return $this->fail($e->getMessage());
        }

    }


}
