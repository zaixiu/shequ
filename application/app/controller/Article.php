<?php
namespace app\app\controller;


use app\common\model\Comment;
use app\common\model\User;

class Article extends Base
{

    public function index()
    {
        try{
            $input = input('post.',null,'trim');
            $input['type'] = explode(',',$input['type']);
            if(in_array(1,$input['type'])){
                $input['is_show'] = 1;
            }
            $input['parent_id'] = $this->parent_id;
            $list = \app\common\model\Article::getList($input);
            if($list['code'] == 1){
                exception($list['msg']);
            }

            return $this->succeed('操作成功',['pageList'=>$list['data'],'pages'=>$list['pages']]);
        }catch (\Exception $e){
            return $this->fail($e->getMessage());
        }

    }

    /**
     * 浏览次数
     * @return \think\response\Json
     */
    public function visit()
    {
        try{
            $input['id'] = input('post.id',null,'trim');
            if(!empty($input['id'])){
                $article = \app\common\model\Article::get($input['id']);
                if(!empty($article)){
                    $article->visit_number += 1;
                    $article->save();
                }
            }

            return $this->succeed('操作成功',[]);
        }catch (\Exception $e){
            return $this->fail($e->getMessage());
        }

    }

    public function articleDetail()
    {
        try{
//            $input = input('post.',null,'trim');
//            $article = \app\common\model\Article::get($input['id']);
//            if(empty($article)){
//                exception('文章不存在');
//            }
//            $doctor =  User::where(['id'=>$article['doctor_id']])->field('name,position,department')->find();
//            if(empty($doctor)){
//                exception('医生信息不存在');
//            }
//            $article['doctor_name'] = $doctor?$doctor['name']:'';
//            $article['position'] = $doctor?$doctor['position']:'';
//            $article['department'] = $doctor?$doctor['department']:'';

            $input['parent_id'] = $this->parent_id;
            $input['type'] = 1;
            $input['id'] = input('post.id',null,'trim');
            $list = \app\common\model\Article::getList($input);
            if($list['code'] == 1){
                \exception($list['msg']);
            }
            if(empty($list['data'])){
                \exception('信息不存在');
            }
            $article = $list['data'][0];
            $article['content'] = str_replace("<img ", "<img style='max-width:100%;height:auto;'", $article['content']);
            $comment = Comment::getList(['related_id'=>$article['id'],'parent_id'=>$this->parent_id]);
            if($comment['code'] == 1){
                \exception($comment['msg']);
            }
            return $this->succeed('操作成功',['article'=>$article,'comment'=>$comment['data']]);
        }catch (\Exception $e){
            return $this->fail($e->getMessage());
        }

    }



}
