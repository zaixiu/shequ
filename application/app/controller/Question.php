<?php
namespace app\app\controller;


use app\common\helper\MessageCheck;
use app\common\model\Comment;
use app\common\model\TopicArticle;
use app\common\model\User;
use think\Db;
use think\Exception;

class Question extends Base
{


    /**
     * 我的提问
     * @return \think\response\Json
     */
    public function my()
    {
        try{
            $question = \app\common\model\Article::where(['type'=>2,'parent_id'=>$this->parent_id])->order('id desc')->select();
            foreach ($question as $k => $v){
                $doctor =  User::where(['id'=>$v['doctor_id']])->field('image,name,position,department')->find();
                $v['doctor_name'] = $doctor?$doctor['name']:'';
            }
            return $this->succeed('操作成功',['data'=>$question]);
        }catch (\Exception $e){
            return $this->fail($e->getMessage());
        }

    }



    /**
     * 常见问题
     * @return \think\response\Json
     */
    public function common()
    {
        try{
            $data = input('post.',null,'trim');
            $limit = 10;
            if(!empty($data['page'])){
                $offset = $limit*($data['page']-1);
            }

            $question = \app\common\model\Article::where(['type'=>2,'is_common'=>1])->limit($offset,$limit)->select();
            $count = \app\common\model\Article::where(['type'=>2,'is_common'=>1])->count();
            return $this->succeed('操作成功',['count' => $count,'pageList'=>$question,'msg'=>'','pages'=>$limit?ceil($count/$limit):'']);
        }catch (\Exception $e){
            return $this->fail($e->getMessage());
        }

    }

    /**
     * 添加分享
     * @return \think\response\Json
     */
    public function add(){
        try{
            Db::startTrans();
            $input = input('post.',null,'trim');
            $check  = new MessageCheck();
            $check->validate($input['title'].$input['content']);
            $input['parent_id'] = $this->parent_id;
            $input['type'] = 2;
            $input['is_show'] = 1;
            $data = new \app\common\model\Article();
            $data->allowField(true)->force()->save($input);

            //关联表
            $topic = $input['topicList'];
            if(!empty($topic)){
                $topic_array = explode(',',$topic);
                foreach ($topic_array as $v){
                    $model = new TopicArticle();
                    $model->topic_id = $v;
                    $model->article_id = $data['id'];
                    $model->save();
                }
            }
            Db::commit();
            return $this->succeed('提交成功',[]);
        }catch (Exception $e){
            Db::rollback();
            return $this->fail($e->getMessage());
        }
    }

    /**
     * Notes:问题详情
     * Date: 2020/10/3
     * Time: 14:46
     * @return \think\response\Json
     * @throws \Exception
     */
    public function detail(){
        try{
            $input['parent_id'] = $this->parent_id;
            $input['id'] = input('post.id',null,'trim');
            $list = \app\common\model\Article::getList($input);
            if($list['code'] == 1){
                \exception($list['msg']);
            }
            $detail = $list['data'][0];
            return $this->succeed('问题详情',['data'=>$detail]);
        }catch (Exception $e){
            Db::rollback();
            return $this->fail($e->getMessage());
        }
    }

}
