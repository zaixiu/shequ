<?php
namespace app\app\controller;

use app\common\model\BabyGrowLog;
use think\Db;
use think\Exception;
use think\Validate;

class Follow extends Base
{
    public function index()
    {
        try{
            $input = input('post.',null,'trim');
            $input['operate_id'] = $this->parent_id;
            $list = \app\common\model\Follow::getList($input);
            if($list['code'] == 1){
                exception($list['msg']);
            }

            return $this->succeed('操作成功',['pageList'=>$list['data'],'pages'=>$list['pages']]);
        }catch (\Exception $e){
            return $this->fail($e->getMessage());
        }

    }



    /**
     * 取消关注
     * @return \think\response\Json
     */
    public function deleteById(){
        try{
            $input = input('post.',null,'trim');
            \app\common\model\Follow::destroy(['id'=>$input['id']]);
            return $this->succeed('操作成功');
        }catch (Exception $e){
            Db::rollback();
            return $this->fail($e->getMessage());
        }
    }

    /**
     * 取消关注
     * @return \think\response\Json
     */
    public function delete(){
        try{
            $input = input('post.',null,'trim');
            $con = [
                'operate_id' => $this->parent_id,
                'type' => $input['type'],
            ];

            if($input['type'] == 1){
                $con['parent_id'] = $input['parent_id'];
            }elseif ($input['type'] == 2){
                $con['topic_id'] = $input['topic_id'];
            }elseif ($input['type'] == 3){
                $con['doctor_id'] = $input['doctor_id'];
            }

            \app\common\model\Follow::destroy($con);
            return $this->succeed('操作成功');
        }catch (Exception $e){
            Db::rollback();
            return $this->fail($e->getMessage());
        }
    }

    /**
     * 添加保存
     * @return \think\response\Json
     * @throws \Exception
     */
    public function add(){
        try{
            Db::startTrans();
            $input = input('post.',null,'trim');
            $input['operate_id'] = $this->parent_id;
            $follow = new \app\common\model\Follow();
            $follow->allowField(true)->force()->save($input);
            Db::commit();
            return $this->succeed('保存成功',[]);
        }catch (Exception $e){
            Db::rollback();
            return $this->fail($e->getMessage());
        }
    }


}
