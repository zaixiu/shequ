<?php
namespace app\app\controller;

use app\common\model\BabyGrowLog;
use think\Db;
use think\Exception;
use think\Validate;

class Thumb extends Base
{
    /**
     * 点赞、取消点赞
     * @return \think\response\Json
     * @throws \Exception
     */
    public function save(){
        try{
            Db::startTrans();
            $input = input('post.',null,'trim');
            $con = [
                'operate_id'=>$this->parent_id,
                'type'=>$input['type'],
                'related_id'=>$input['related_id'],
            ];
            $thumb = \app\common\model\Thumb::where($con)->find();
            if(!empty($thumb)){
                \app\common\model\Thumb::where($con)->delete();
            }else{
                $model = new \app\common\model\Thumb();
                $model->allowField(true)->force()->save($con);
            }
            Db::commit();
            return $this->succeed('操作成功',[]);
        }catch (Exception $e){
            Db::rollback();
            return $this->fail($e->getMessage());
        }
    }


}
