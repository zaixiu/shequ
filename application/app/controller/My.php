<?php
namespace app\app\controller;


use app\common\model\Comment;
use app\common\model\Topic;
use app\common\model\User;
use think\Db;
use think\Exception;

class My extends Base
{

    /**
     * 我的
     * @return \think\response\Json
     */
    public function index()
    {
        try{
            $question_count = \app\common\model\Article::where(['type'=>2,'parent_id'=>$this->parent_id])->count();
            $collect_count = \app\common\model\Collect::where(['operate_id'=>$this->parent_id])->count();
            $follow_count = \app\common\model\Follow::where(['operate_id'=>$this->parent_id])->count();
            $follow_ids = \app\common\model\Follow::where(['operate_id'=>$this->parent_id,'type'=>2])->column('topic_id');
            $unable_topic = 0;
            if(!empty($follow_ids)){
                $follow_ids = implode(',',$follow_ids);
                $unable_topic = Db::query('select count("id") count from sq_topic where (is_delete = 1 or is_show = 0) and id in ('.$follow_ids .' )');
                $unable_topic = $unable_topic[0]['count'];
            }
            $collect_ids = \app\common\model\Collect::where(['operate_id'=>$this->parent_id])->column('related_id');
            $input['ids'] = $collect_ids;
            $input['is_show'] = 1;
            $list = \app\common\model\Article::getList($input);
            if($list['code'] == 1){
                exception($list['msg']);
            }
            $collect_count = $list['count'];
            return $this->succeed('操作成功',[
                'data'=>[
                    'question_count'=>$question_count,
                    'collect_count'=>$collect_count,
                    'follow_count'=>$follow_count,
                    'unable_topic'=>$unable_topic,
                    'guanzhu_count'=>$follow_count-$unable_topic,
                ]
            ]);
        }catch (\Exception $e){
            return $this->fail($e->getMessage());
        }

    }




}
