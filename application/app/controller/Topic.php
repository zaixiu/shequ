<?php
namespace app\app\controller;

use app\common\model\Article;

class Topic extends Base
{

    public function index()
    {
        try{
            $hot = \app\common\model\Topic::getList(['is_hot'=>1]);
            if($hot['code'] == 1){
                exception($hot['msg']);
            }
            $all = \app\common\model\Topic::getList();
            if($all['code'] == 1){
                exception($all['msg']);
            }

            return $this->succeed('操作成功',['hot'=>$hot['data'],'all'=>$all['data']]);
        }catch (\Exception $e){
            return $this->fail($e->getMessage());
        }

    }

    public function detail(){
        try{
            $input = input('post.',null,'trim');
            $topic = \app\common\model\Topic::get($input['topic_id']);
            if(empty($topic)){
                exception('话题不存在');
            }
            $topic['article_num'] = Article::shareNumber($topic['id'],1);
            $topic['question_num'] = Article::shareNumber($topic['id'],2);
            $topic['share_num'] = Article::shareNumber($topic['id'],3);
            $topic['follow'] = \app\common\model\Follow::is_follow(2,$topic['id'],$this->parent_id);
            return $this->succeed('操作成功',['topic'=>$topic,'article'=>[]]);
        }catch (\Exception $e){
            return $this->fail($e->getMessage());
        }
    }


}
