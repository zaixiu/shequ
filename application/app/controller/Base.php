<?php
namespace app\app\controller;


use app\common\model\Parents;
use think\Controller;

class Base extends Controller
{
    protected $parent_id = '';
    //验证失败时抛出异常
    protected $failException = true;

    protected function _initialize()
    {
        //判断token
        $request = request();
        $controller = $request->controller();
        $action = $request->action();
        if(!($controller == 'Upload' && $action =='file')){
            if(!($controller == 'Site' && ($action =='token' || $action =='openid'))){
                $token = input('api_token');
                if($token != base64_encode('chengzhangxcx')){
                    echo '无权限访问'; exit;
                }else{
                    $openid = input('openid');
                    $parent = Parents::where(['openid'=>$openid])->find();
                    if(!empty($parent)){
                        $this->parent_id = $parent['id'];
                    }
                }
            }
        }

    }


    public function checkCustomer($openid){
        $parent = Parents::where(['openid'=>$openid])->find();
        if(empty($parent)){
            json(['code'=>2,'message'=>'未注册'])->send();exit();
        }
    }

    /**
     * 成功反馈封装
     * @param $message
     * @param array $data
     * @return \think\response\Json
     */
    public function succeed($message, $data = [])
    {
        return json([
            'code' => 1,
            'msg' => $message,
            'data' => $data
        ]);
    }

    /**
     * 失败反馈
     * @param $message
     * @param int $code
     * @param array $data
     * @return \think\response\Json
     */
    protected function fail($message, $code = 0, $data = [])
    {
        return json([
            'code' => $code,
            'msg' => $message,
            'data' => $data
        ]);
    }
}
