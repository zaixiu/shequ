<?php
namespace app\app\controller;

use app\common\model\BabyGrowLog;
use app\common\model\Comment;
use think\Db;
use think\Exception;
use think\Validate;

class Parents extends Base
{
    /**
     * 家长详情页
     * @return \think\response\Json
     */
    public function detail()
    {
        try{
            $input = input('post.',null,'trim');
            $parent = \app\common\model\Parents::get($input['id']);
            if(empty($parent)){
                exception('家长信息不存在');
            }
            $list = \app\common\model\Article::getList(['type'=>3,'article_parent_id'=>$input['id'],'parent_id'=>$this->parent_id]);
            if($list['code'] == 1){
                \exception($list['msg']);
            }
            return $this->succeed('操作成功',['parent'=>$parent,'article'=>$list['data']]);
        }catch (\Exception $e){
            return $this->fail($e->getMessage());
        }

    }



}
