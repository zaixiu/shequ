<?php
namespace app\app\controller;

use app\common\model\BabyGrowLog;
use think\Db;
use think\Exception;
use think\Validate;

class Collect extends Base
{

    public function index()
    {
        try{
            $input = input('post.',null,'trim');
            $input['parent_id'] = $this->parent_id;
            $collect_ids = \app\common\model\Collect::where(['operate_id'=>$this->parent_id])->column('related_id');
            $input['ids'] = $collect_ids;
            $list = \app\common\model\Article::getList($input);
            if($list['code'] == 1){
                exception($list['msg']);
            }
            return $this->succeed('操作成功',['data'=>$list['data']]);
        }catch (\Exception $e){
            return $this->fail($e->getMessage());
        }

    }

    /**
     * 收藏、取消收藏
     * @return \think\response\Json
     * @throws \Exception
     */
    public function save(){
        try{
            Db::startTrans();
            $input = input('post.',null,'trim');
            $con = [
                'operate_id'=>$this->parent_id,
                'related_id'=>$input['related_id'],
            ];
            $collect = \app\common\model\Collect::where($con)->find();
            if(!empty($collect)){
                \app\common\model\Collect::where($con)->delete();
            }else{
                $model = new \app\common\model\Collect();
                $model->allowField(true)->force()->save($con);
            }
            Db::commit();
            return $this->succeed('操作成功',[]);
        }catch (Exception $e){
            Db::rollback();
            return $this->fail($e->getMessage());
        }
    }


}
