<?php
namespace app\app\controller;


use app\common\model\Comment;
use app\common\model\Topic;
use app\common\model\User;
use think\Db;
use think\Exception;

class Message extends Base
{

    public function index()
    {
        try{
            $input = input('post.',null,'trim');
            $input['parent_id'] = $this->parent_id;
            if($input['cate'] == 1){
                $input['type'] = [1,2,3,4];
            }elseif($input['cate'] == 3){
                $input['type'] = 5;
            }else{
                $input['type'] = ['in',[]];
            }
            $list = \app\common\model\Message::getList($input);
            if($list['code'] == 1){
                exception($list['msg']);
            }

            return $this->succeed('操作成功',['pageList'=>$list['data'],'pages'=>$list['pages']]);
        }catch (\Exception $e){
            return $this->fail($e->getMessage());
        }

    }

    /**
     * 标为已读
     * @return \think\response\Json
     */
    public function read()
    {
        try{
            $input = input('post.',null,'trim');
            $mesage = \app\common\model\Message::get($input['id']);
            if(!empty($mesage)){
                $mesage->is_read = 1;
                $mesage->save();
            }
            return $this->succeed('操作成功',[]);
        }catch (\Exception $e){
            return $this->fail($e->getMessage());
        }

    }
}
