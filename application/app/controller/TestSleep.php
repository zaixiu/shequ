<?php
namespace app\app\controller;

use app\common\model\BabyGrowLog;
use think\Db;
use think\Exception;
use think\Validate;

class TestSleep extends Base
{

    public function index()
    {
        try{
            $list = \app\common\model\TestSleep::getList([]);
            if($list['code'] == 1){
                exception($list['msg']);
            }

            return $this->succeed('操作成功',['data'=>$list['data'],'count'=>$list['count']]);
        }catch (\Exception $e){
            return $this->fail($e->getMessage());
        }

    }

}
