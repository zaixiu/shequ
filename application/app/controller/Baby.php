<?php
namespace app\app\controller;

use app\common\model\BabyGrowLog;
use think\Db;
use think\Exception;
use think\Validate;

class Baby extends Base
{

    public function index()
    {
        try{
            $baby = \app\common\model\Baby::getList(['parent_id'=>$this->parent_id]);
            if($baby['code'] == 1){
                exception($baby['msg']);
            }

            return $this->succeed('操作成功',['data'=>$baby['data']]);
        }catch (\Exception $e){
            return $this->fail($e->getMessage());
        }

    }



    /**
     * 宝贝详情
     * @return \think\response\Json
     */
    public function detail()
    {
        try{
            $input = input('post.',null,'trim');
            $baby = \app\common\model\Baby::get($input['baby_id']);
            if(empty($baby)){
                exception('宝宝信息不存在');
            }
            $baby['inherit_height'] = \app\common\model\Baby::inheritHeight($baby['id']);
            $baby['age'] = \app\common\model\Baby::age($baby['birth']);
            $date = date('Y-m-d',time());
            $age_array = BabyGrowLog::age($baby['birth'],$date);
            $baby['int_age'] = $age_array['year'];
            $baby['age'] = \app\common\model\Baby::age($baby['birth']);
            return $this->succeed('操作成功',['baby'=>$baby]);
        }catch (\Exception $e){
            return $this->fail($e->getMessage());
        }

    }


    /**
     * 获取当前小孩信息
     * @return \think\response\Json
     */
    public function currentChild()
    {
        try{
            $baby = \app\common\model\Baby::getList(['parent_id'=>$this->parent_id,'is_default'=>1]);
            if($baby['code'] == 1){
                exception($baby['msg']);
            }

            return $this->succeed('操作成功',['data'=>$baby['data'][0]]);
        }catch (\Exception $e){
            return $this->fail($e->getMessage());
        }
    }


    /**
     * 切换
     * @return \think\response\Json
     */
    public function switchChild()
    {
        try{
            $id = input('id',null,'trim');
            \app\common\model\Baby::where(['parent_id'=>$this->parent_id])->update(['is_default'=>0]);
            \app\common\model\Baby::where(['parent_id'=>$this->parent_id,'id'=>$id])->update(['is_default'=>1]);
            return $this->succeed('操作成功',['baby_default_id'=>$id]);
        }catch (\Exception $e){
            return $this->fail($e->getMessage());
        }

    }

    /**
     * 编辑
     * @return \think\response\Json
     */
    public function edit()
    {
        try{
            $id = input('id',null,'trim');
            $baby = \app\common\model\Baby::where(['parent_id'=>$this->parent_id,'id'=>$id])->find();
            return $this->succeed('操作成功',['data'=>$baby]);
        }catch (\Exception $e){
            return $this->fail($e->getMessage());
        }

    }

    /**
     * 修改
     * @return \think\response\Json
     * @throws \Exception
     */
    public function update(){
        try{
            Db::startTrans();
            $input = input('post.',null,'trim');
            $check = \app\common\model\Baby::where(['parent_id'=>$this->parent_id,'name'=>$input['name'],'id'=>['<>',$input['id']]])->count();
            if($check > 0){
                \exception('宝宝名称请勿重复添加');
            }
            $count = \app\common\model\Baby::where(['parent_id'=>$this->parent_id])->count();
            $baby = \app\common\model\Baby::get($input['id']);
            $baby->name = $input['name'];
            if($count == 0){
                $baby->is_default = 1;
            }
            $baby->parent_id = $this->parent_id;
            $baby->image =  $input['image'];
            $baby->sex =  $input['sex'];
            $baby->birth =  $input['birth'];
            $baby->farther_height =  $input['farther_height'];
            $baby->mother_height =  $input['mother_height'];
            $baby->hope_height =  $input['hope_height'];
            $baby->allowField(true)->force()->save();
            Db::commit();
            $baby = \app\common\model\Baby::where(['parent_id'=>$this->parent_id,'is_default'=>1])->find();
            return $this->succeed('保存成功',['baby_default_id'=>$baby['id']]);
        }catch (Exception $e){
            Db::rollback();
            return $this->fail($e->getMessage());
        }
    }


    public function delete(){
        try{
            $input = input('post.',null,'trim');
            \app\common\model\Baby::destroy($input['id']);
            $baby = \app\common\model\Baby::where(['parent_id'=>$this->parent_id,'is_default'=>1])->find();
            if(!empty($baby)){
                return $this->succeed('保存成功',['baby_default_id'=>$baby['id']]);
            }else{
                $data = \app\common\model\Baby::where(['parent_id'=>$this->parent_id])->find();
                if(empty($data)){
                    return $this->succeed('保存成功',['baby_default_id'=>'']);
                }else{
                    $data->is_default = 1;
                    $data->save();
                    return $this->succeed('保存成功',['baby_default_id'=>$data['id']]);
                }
            }
        }catch (Exception $e){
            Db::rollback();
            return $this->fail($e->getMessage());
        }
    }

    /**
     * 添加保存
     * @return \think\response\Json
     * @throws \Exception
     */
    public function add(){
        try{
            Db::startTrans();
            $input = input('post.',null,'trim');
            $check = \app\common\model\Baby::where(['parent_id'=>$this->parent_id,'name'=>$input['name']])->count();
            if($check > 0){
                \exception('宝宝名称请勿重复添加');
            }
            $count = \app\common\model\Baby::where(['parent_id'=>$this->parent_id])->count();
            $baby = new \app\common\model\Baby();
            $baby->name = $input['name'];
            $baby->is_default = $count == 0?1:0;
            $baby->parent_id = $this->parent_id;
            $baby->image =  $input['image'];
            $baby->sex =  $input['sex'];
            $baby->birth =  $input['birth'];
            $baby->farther_height =  $input['farther_height'];
            $baby->mother_height =  $input['mother_height'];
            $baby->hope_height =  $input['hope_height'];
            $baby->allowField(true)->force()->save();

            //身高记录
            $log = new BabyGrowLog();
            $log->parent_id = $this->parent_id;
            $log->baby_id = $baby->id;

            $log->measure_time = date('Y-m-d');
            $log->height = $input['height'];
            $log->weight = $input['weight'];
            $log->bone_age = $input['bone_age'];
            if(!empty($input['before_height'])){
                $log->before_height = $input['before_height'];
                $log->before_measure_time = $input['before_measure_time'];
            }
           $log->force()->save();
            Db::commit();
            $baby = \app\common\model\Baby::where(['parent_id'=>$this->parent_id,'is_default'=>1])->find();
            return $this->succeed('保存成功',['baby_default_id'=>$baby['id']]);
        }catch (Exception $e){
            Db::rollback();
            return $this->fail($e->getMessage());
        }
    }


}
