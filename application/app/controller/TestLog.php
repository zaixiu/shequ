<?php
namespace app\app\controller;

use app\common\model\StandardHeight;
use app\common\model\StandardWeight;
use think\Db;
use think\Exception;
use think\Validate;

class TestLog extends Base
{

    public function index()
    {
        try{
            $input = input('post.',null,'trim');
            $input['parent_id'] = $this->parent_id;
            $baby = \app\common\model\TestLog::getList($input);
            if($baby['code'] == 1){
                exception($baby['msg']);
            }
            return $this->succeed('操作成功',['pageList'=>$baby['data'],'pages'=>$baby['pages']]);

        }catch (\Exception $e){
            return $this->fail($e->getMessage());
        }

    }


    /**
     * 添加保存
     * @return \think\response\Json
     * @throws \Exception
     */
    public function add(){
        try{
            Db::startTrans();
            $input = input('post.',null,'trim');
            $input['parent_id'] = $this->parent_id;
            $log = new \app\common\model\TestLog();
            if(!$log->allowField(true)->force()->save($input)){
                exception('保存失败');
            }
            Db::commit();
            return $this->succeed('保存成功',['id'=>$log->id]);
        }catch (Exception $e){
            Db::rollback();
            return $this->fail($e->getMessage());
        }
    }


    /**
     * 获取测试详情
     * @return \think\response\Json
     */
    public function detail(){
        try{
            Db::startTrans();
            $id = input('post.id',null,'trim');
            $log = \app\common\model\TestLog::get($id);
            return $this->succeed('保存成功',['data'=>$log]);
        }catch (Exception $e){
            Db::rollback();
            return $this->fail($e->getMessage());
        }
    }

}
