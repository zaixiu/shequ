<?php
namespace app\app\controller;

use app\common\model\StandardHeight;
use app\common\model\StandardWeight;
use think\Db;
use think\Exception;
use think\Validate;

class BabyGrowLog extends Base
{

    public function index()
    {
        try{
            $input = input('post.',null,'trim');
            $input['order'] = "measure_time desc, id desc";
            $baby = \app\common\model\BabyGrowLog::getList($input);
            if($baby['code'] == 1){
                exception($baby['msg']);
            }
            return $this->succeed('操作成功',['pageList'=>$baby['data'],'pages'=>$baby['pages']]);

        }catch (\Exception $e){
            return $this->fail($e->getMessage());
        }

    }

    /**
     * 身高测评报告
     * @return \think\response\Json
     */
    public function report()
    {
        try{
            $input = input('post.',null,'trim');
            $baby = \app\common\model\Baby::get($input['baby_id']);
            $log = \app\common\model\BabyGrowLog::get($input['grow_log_id']);
            $log['rate'] = \app\common\model\BabyGrowLog::rate($log['before_measure_time'],$log['measure_time'],$log['before_height'],$log['height']);
            $log['month_later'] = $log['rate']?$log['height']+round($log['rate']/12*3,1):false;
            $log['bmi'] = round($log['weight']/($log['height']*$log['height']/10000),1);
            $baby['inherit_height'] = \app\common\model\Baby::inheritHeight($baby['id']);
            $baby['inherit_height_val'] = \app\common\model\Baby::inheritHeightVal($baby['id']);
            $baby['age'] = \app\common\model\Baby::age($baby['birth']);
            $age_array = \app\common\model\BabyGrowLog::age($baby['birth'],$log['measure_time']);
            $baby['int_age'] = $age_array['year'];
            $report = StandardHeight::heightReport($baby['sex']==1?2:1,$age_array['months'],$log['height']);
            $report['forecast_diff'] = $report['forecast']-$baby['hope_height'];
            $report['forecast_diff_desc'] = $report['forecast_diff']>0?'高'.$report['forecast_diff'].'cm':'矮'.abs($report['forecast_diff']).'cm';
            $report['avg_rate'] = StandardHeight::avgRate($baby['sex']==1?2:1,$age_array['year']);
            $article = \app\common\model\Article::where(['type'=>1,'is_report'=>1,'is_show'=>1,'is_delete'=>2])->select();
            return $this->succeed('操作成功',['report'=>$report,'baby'=>$baby,'log'=>$log,'article'=>$article]);

        }catch (\Exception $e){
            return $this->fail($e->getMessage());
        }

    }



    /**
     * 身高统计
     * @return \think\response\Json
     */
    public function height(){
        try{
            $input = input('post.',null,'trim');
            $baby = \app\common\model\Baby::get($input['baby_id']);
            $data = StandardHeight::getCanvasData($baby['sex']==1?'2':'1',$baby['id']);
            return $this->succeed('操作成功',$data);
        }catch (Exception $e){
            Db::rollback();
            return $this->fail($e->getMessage());
        }
    }

    /**
     * 体重统计
     * @return \think\response\Json
     */
    public function weight(){
        try{
            $input = input('post.',null,'trim');
            $baby = \app\common\model\Baby::get($input['baby_id']);
            $data = StandardWeight::getCanvasData($baby['sex']==1?'2':'1',$baby['id']);
            return $this->succeed('操作成功',$data);
        }catch (Exception $e){
            Db::rollback();
            return $this->fail($e->getMessage());
        }
    }


    public function delete(){
        try{
            $input = input('post.',null,'trim');
            \app\common\model\BabyGrowLog::destroy($input['id']);
            return $this->succeed('操作成功',[]);
        }catch (Exception $e){
            Db::rollback();
            return $this->fail($e->getMessage());
        }
    }

    /**
     * 添加保存
     * @return \think\response\Json
     * @throws \Exception
     */
    public function add(){
        try{
            Db::startTrans();
            $input = input('post.',null,'trim');
            //身高记录
            $log = new \app\common\model\BabyGrowLog();
            $log->parent_id = $this->parent_id;
            $log->baby_id = $input['baby_id'];
            $log->measure_time = date('Y-m-d');
            $log->height = $input['height'];
            $log->weight = $input['weight'];
            $log->bone_age = $input['bone_age'];
            if(!empty($input['before_height'])){
                $log->before_height = $input['before_height'];
                $log->before_measure_time = $input['before_measure_time'];
            }
           $log->force()->save();
            Db::commit();
            return $this->succeed('保存成功',[]);
        }catch (Exception $e){
            Db::rollback();
            return $this->fail($e->getMessage());
        }
    }


}
