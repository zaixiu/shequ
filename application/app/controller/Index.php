<?php
namespace app\app\controller;


use app\common\model\Comment;
use app\common\model\User;
use think\Db;
use think\Exception;

class Index extends Base
{

    /**
     * 首页
     * @return \think\response\Json
     */
    public function index()
    {
        try{
            $message_count = 0;
            if($this->parent_id){
                $message_count = \app\common\model\Message::noReadCount($this->parent_id);
            }
            //宝贝
            $child = [];
            if($this->parent_id){
                $baby = \app\common\model\Baby::getList(['parent_id'=>$this->parent_id,'is_default'=>1]);
                if($baby['code'] == 1){
                    exception($baby['msg']);
                }
                if($baby['count'] > 0 ){
                    $child = $baby['data'][0];
                }
            }

            //身高记录
            $log =[];
            if(!empty($child)){
                $result = \app\common\model\BabyGrowLog::getList(['baby_id'=>$child['id'],'limit'=>1]);
                if($result['code'] == 0)
                $log = $result['data'][0];
            }

            //今日热门
            $article = \app\common\model\Article::where(['type'=>1,'is_delete'=>2,'is_show'=>1])->order(['visit_number'=>'desc','create_time'=>'desc'])->limit(0,15)->column('id');
            $share = \app\common\model\Article::where(['type'=>3])->order(['visit_number'=>'desc','create_time'=>'desc'])->limit(0,30)->column('id');

            $article_list = \app\common\model\Article::getList(['ids'=>$article,'parent_id'=>$this->parent_id]);
            if($article_list['code'] == 1){
                exception($article_list['msg']);
            }
            $article_list = $article_list['data'];

            $share_list = \app\common\model\Article::getList(['ids'=>$share,'parent_id'=>$this->parent_id]);
            if($share_list['code'] == 1){
                exception($share_list['msg']);
            }
            $share_list = $share_list['data'];
            $list = [];
            for($i = 0;$i<count($article_list);$i++){
                $list[] = $article_list[$i];
                if(isset($share_list[2*$i])){
                    $list[] = $share_list[2*$i];
                }else{
                    break;
                }
                if(isset($share_list[2*$i+1])){
                    $list[] = $share_list[2*$i+1];
                }else{
                    break;
                }
            }
            return $this->succeed('操作成功',['list'=>$list,'child'=>$child,'log'=>$log,'has_child'=>count($child),
                'has_log'=>count($log),
                'message_count'=>$message_count
                ]);
        }catch (\Exception $e){
            return $this->fail($e->getMessage());
        }

    }




}
