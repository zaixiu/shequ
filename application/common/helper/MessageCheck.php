<?php

namespace app\common\helper;

class MessageCheck {



    function validate($content){
        $url = 'https://api.weixin.qq.com/wxa/msg_sec_check?access_token='.$this->accessToken();
        $data = json_encode(array('content'=>$content),JSON_UNESCAPED_UNICODE);
        $check = $this->curl_request($url,$data);
        $data_array = json_decode($check,true);
        if($data_array['errcode'] != 0 ){
            exception('内容含有违法违规内容');
        }
    }



    function accessToken(){
        if(\think\Cookie::get('xcxAccessToken')){
            return  \think\Cookie::get('xcxAccessToken');
        }else{
            $url = 'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid='.config('xcx_config.appid').'&secret='.config('xcx_config.secret');
            $data = $this->curl_request($url);
            $data_array = json_decode($data,true);
            if(isset($data_array['errcode']) && $data_array['errcode'] != 0 ){
                exception($data_array['errmsg']);
            }else{
                \think\Cookie::set('xcxAccessToken', $data_array['access_token'],$data_array['expires_in']-200);
                return $data_array['access_token'];
            }
        }
    }



    //参数1：访问的URL，参数2：post数据(不填则为GET)，参数3：提交的$cookies,参数4：是否返回$cookies
    function curl_request($url,$post='',$cookie='', $returnCookie=0){
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; Trident/6.0)');
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_AUTOREFERER, 1);
//        curl_setopt($curl, CURLOPT_REFERER, "http://XXX");
        curl_setopt($curl, CURLOPT_REFERER, $url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER , false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST  , false);
        if($post) {
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $post);
        }
        if($cookie) {
            curl_setopt($curl, CURLOPT_COOKIE, $cookie);
        }
        curl_setopt($curl, CURLOPT_HEADER, $returnCookie);
        curl_setopt($curl, CURLOPT_TIMEOUT, 10);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $data = curl_exec($curl);
        if (curl_errno($curl)) {
            return curl_error($curl);
        }
        curl_close($curl);
        if($returnCookie){
            list($header, $body) = explode("\r\n\r\n", $data, 2);
            preg_match_all("/Set\-Cookie:([^;]*);/", $header, $matches);
            $info['cookie']  = substr($matches[1][0], 1);
            $info['content'] = $body;
            return $info;
        }else{
            return $data;
        }
    }

}
