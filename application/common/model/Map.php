<?php


namespace app\common\model;

use think\Db;
use think\Exception;
use think\Model;

class Map extends Model{

    public static $alias = [

    ];



    /**
     * 保存
     */
    public function saveData($data,$id = ''){
        if(!isset($data['user_id']) || empty($data['user_id'])){
            $data['user_id'] = session('userId');
        }
        try{
            if(empty($data['province_id'])){
                \exception('请选择省');
            }
            if(!empty($data['city_id'])){
                $data['adcode'] = Gaode::getCityCode($data['city_id']);
            }else{
                $data['adcode'] = Gaode::getCityCode($data['province_id']);
            }
            if(empty($id)){
                $result = $this->allowField(true)->isUpdate(false)->save($data);
            }else{
                $result = $this->allowField(true)->isUpdate(true)->save($data,['id'=>$id]);
            }

            if ($result === false){
                return ['code'=>1,'msg'=>$this->getError()];
            }

            return ['code'=>'0','msg'=>'保存成功'];
        }catch (\Exception $exception){
            return ['code'=>1,'msg'=>$exception->getMessage()];
        }
    }


    /**
     * 小程序端数据
     * @param $data
     * @return array
     */
    public static function getList($data = []){
        try{
            $con = [];
            $limit = null;
            $offset = 0;
            if(!empty($data['limit'])){
                $limit = $data['limit'];
            }
            if(!empty($data['page'])){
                $offset = $limit*($data['page']-1);
            }

            if(!empty($data['id'])){
                $con['a.id'] = $data['id'];
            }

//            if(!empty($data['city_id'])){
//                $con['a.city_id'] = $data['city_id'];
//            }
            $topic = Db::name('map')->alias('a')->where($con)
                ->order('id desc')
                ->field('a.*')
                ->limit($offset,$limit)
                ->select();
            foreach ($topic as $k => &$v){
                $v['province_text'] = Region::getProvinceText($v);
                $v['city_text'] = Region::getCityText($v);

            }
            $count = Db::name('map')->alias('a')->where($con)->field('a.*')->count();

            return ['code'=>0,'count' => $count,'data'=>$topic,'msg'=>''];
        }catch(\Exception $exception) {
            return ['code' => 1, 'data' => [], 'msg' => $exception->getMessage()];
        }
    }


    public static function getAlias($key,$value)
    {
        return isset(self::$alias[$key][$value])?self::$alias[$key][$value]:'';
    }

}
