<?php


namespace app\common\model;

use think\Db;
use think\Exception;
use think\Model;

class Topic extends Model{

    public static $alias = [
        'is_hot_alias'=> [
            1=>'是',
            0=>'否',
        ],
        'is_show_alias'=> [
            1=>'显示',
            0=>'隐藏',
        ],
    ];


    /**
     * 列表数据
     */
    public function getDataArr($page = 1,$limit = 10,$where = []){
        if(empty($data['is_delete'])){
            $where['is_delete'] = 2;
        }
        $data = $this->where($where)->limit(($page-1)*$limit,$limit)->order('sort desc')->select();
        $count = $this->where($where)->count();
        foreach ($data as $k => &$v){
            $v['is_hot_alias'] = self::getAlias('is_hot_alias',$v['is_hot']);
            $v['is_show_alias'] = self::getAlias('is_show_alias',$v['is_show']);
        }
        return ['code'=>0,'msg'=>'','count'=>$count,'data'=>$data];
    }


    /**
     * 保存
     */
    public function saveData($data,$id = ''){
        if(!isset($data['user_id']) || empty($data['user_id'])){
            $data['user_id'] = session('userId');
        }
        $data['is_hot'] = isset($data['is_hot'])?$data['is_hot']:0;
        $data['is_show'] = isset($data['is_show'])?$data['is_show']:0;
        try{
            if(empty($data['image'])){
                \exception('请上传图片');
            }
            if(empty($id)){
                $result = $this->allowField(true)->isUpdate(false)->save($data);
            }else{
                $result = $this->allowField(true)->isUpdate(true)->save($data,['id'=>$id]);
            }

            if ($result === false){
                return ['code'=>1,'msg'=>$this->getError()];
            }

            return ['code'=>'0','msg'=>'保存成功'];
        }catch (\Exception $exception){
            return ['code'=>1,'msg'=>$exception->getMessage()];
        }
    }


    /**
     * 获取下拉话题
     */
    public static function getSelectArray(){
        try {
            $array = [];
            $topic = self::field(['id','title'])->where(['is_delete'=>2])->order(['sort'=>'desc'])->select();
            foreach ($topic as $k => $v){
                $array[$v['id']] = $v['title'];
            }
            return $array;
        }catch (Exception $exception){
            return [];
        }
    }


    /**
     * 小程序端数据
     * @param $data
     * @return array
     */
    public static function getList($data = []){
        try{
            $con = [
                'a.is_delete'=>2,
                'a.is_show'=>1,
            ];
            $limit = null;
            $offset = 0;
            if(!empty($data['limit'])){
                $limit = $data['limit'];
            }
            if(!empty($data['page'])){
                $offset = $limit*($data['page']-1);
            }

            if(!empty($data['is_hot'])){
                $con['a.is_hot'] = $data['is_hot'];
            }

            if(!empty($data['id'])){
                $con['a.id'] = $data['id'];
            }

            if(!empty($data['info'])){
                $con['a.title|a.sub_title'] = ['like',"%{$data['info']}%"];
            }

            $topic = Db::name('topic')->alias('a')->where($con)
                ->order('sort desc ,id desc')
                ->field('a.*')
                ->limit($offset,$limit)
                ->select();

            foreach ($topic as $k => &$v){
                $v['article_num'] = Article::shareNumber($v['id'],1);
                $v['question_num'] = Article::shareNumber($v['id'],2);
                $v['share_num'] = Article::shareNumber($v['id'],3);

            }
            $count = Db::name('topic')->alias('a')->where($con)->field('a.*')->count();

            return ['code'=>0,'count' => $count,'data'=>$topic,'msg'=>''];
        }catch(\Exception $exception) {
            return ['code' => 1, 'data' => [], 'msg' => $exception->getMessage()];
        }
    }


    public static function getAlias($key,$value)
    {
        return isset(self::$alias[$key][$value])?self::$alias[$key][$value]:'';
    }

}
