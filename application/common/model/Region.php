<?php
/**
 * User: hollowcg@foxmail.com
 * Date: 2019/2/20
 * Time: 16:06
 * Description:
 */

namespace app\common\model;


use think\Model;

class Region extends Model
{

    public static function getRegionText($data){
        $regionModel = new Region();
        $province = '';
        $province .= $regionModel->field('name')->find($data['province_id'])['name'];
        $province .= $regionModel->field('name')->find($data['city_id'])['name'];
        $province .= $regionModel->field('name')->find($data['district_id'])['name'];
        return $province;
    }
    public static function getProvinceText($data){
        $hotelRegionModel = new Region();
        return $hotelRegionModel->field('name')->find($data['province_id'])['name'];
    }
    public static function getCityText($data){
        $regionModel = new Region();
        return $regionModel->field('name')->find($data['city_id'])['name'];
    }
    public  static function getDistrictText($data){
        $regionModel = new Region();
        return $regionModel->field('name')->find($data['district_id'])['name'];
    }

}
