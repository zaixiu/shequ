<?php


namespace app\common\model;

use think\Db;
use think\Exception;
use think\Model;

class Shop extends Model{

    public static $alias = [
//        'is_hot_alias'=> [
//            1=>'是',
//            0=>'否',
//        ],
//        'is_show_alias'=> [
//            1=>'显示',
//            0=>'隐藏',
//        ],
    ];



    /**
     * 保存
     */
    public function saveData($data,$id = ''){
        if(!isset($data['user_id']) || empty($data['user_id'])){
            $data['user_id'] = session('userId');
        }
        try{
            if(empty($data['image'])){
                \exception('请上传图片');
            }
            if(empty($data['province_id']) || empty($data['city_id']) || empty($data['district_id'])){
                \exception('请选择省市区');
            }
            if(empty($id)){
                $result = $this->allowField(true)->isUpdate(false)->save($data);
            }else{
                $result = $this->allowField(true)->isUpdate(true)->save($data,['id'=>$id]);
            }

            if ($result === false){
                return ['code'=>1,'msg'=>$this->getError()];
            }

            return ['code'=>'0','msg'=>'保存成功'];
        }catch (\Exception $exception){
            return ['code'=>1,'msg'=>$exception->getMessage()];
        }
    }


    /**
     * 小程序端数据
     * @param $data
     * @return array
     */
    public static function getList($data = []){
        try{
            $con = [
                'a.is_delete'=>2,
            ];
            $limit = null;
            $offset = 0;
            if(!empty($data['limit'])){
                $limit = $data['limit'];
            }
            if(!empty($data['page'])){
                $offset = $limit*($data['page']-1);
            }

            if(!empty($data['id'])){
                $con['a.id'] = $data['id'];
            }

            if(!empty($data['name'])){
                $con['a.name'] = ['like',"%{$data['name']}%"];
            }
            if(!empty($data['province_id'])){
                $con['a.province_id'] = $data['province_id'];
            }
            if(!empty($data['city_id'])){
                $con['a.city_id'] = $data['city_id'];
            }
            $topic = Db::name('shop')->alias('a')->where($con)
                ->order('id desc')
                ->field('a.*')
                ->limit($offset,$limit)
                ->select();
            foreach ($topic as $k => &$v){
                $v['region_text'] = Region::getRegionText($v);
                $v['province_text'] = Region::getProvinceText($v);
                $v['city_text'] = Region::getCityText($v);
                $v['district_text'] = Region::getDistrictText($v);
            }
            $count = Db::name('shop')->alias('a')->where($con)->field('a.*')->count();

            return ['code'=>0,'count' => $count,'data'=>$topic,'msg'=>''];
        }catch(\Exception $exception) {
            return ['code' => 1, 'data' => [], 'msg' => $exception->getMessage()];
        }
    }


    public static function getAlias($key,$value)
    {
        return isset(self::$alias[$key][$value])?self::$alias[$key][$value]:'';
    }

}
