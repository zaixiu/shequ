<?php


namespace app\common\model;

use think\Db;
use think\Model;

class Message extends Model{

    /**
     * 未读数量
     * @param $parent_id
     * @return int|string
     * @throws \think\Exception
     */
    public static function  noReadCount($parent_id){
       return self::where(['parent_id'=>$parent_id,'is_read'=>0])->count();
    }


    /**
     * 列表
     * @param $data
     * @return array
     */
    public static function getList($data){
        try{
            $con = [];
            $limit = null;
            $offset = 0;
            if(!empty($data['limit'])){
                $limit = $data['limit'];
            }
            if(!empty($data['page'])){
                $offset = $limit*($data['page']-1);
            }

            if(!empty($data['id'])){
                $con['a.id'] = $data['id'];
            }
            if(!empty($data['pageNo'])){
                $limit = 10;
                $offset = $limit*($data['pageNo']-1);
            }

            if(!empty($data['type'])){
                if(is_array($data['type'])){
                    $con['a.type'] = ['in',$data['type']];
                }else{
                    $con['a.type'] = $data['type'];
                }
            }

            if(!empty($data['parent_id'])){
                $con['a.parent_id'] = $data['parent_id'];
            }

            $user = Db::name('message')->alias('a')->where($con)
                ->order('id desc')
                ->field('a.*')
                ->limit($offset,$limit)
                ->select();
            foreach ($user as $k => &$v){

                if($v['type'] == 1){
                    //对分享进行评论
                    $operator = Parents::get($v['operate_id']);
                    $v['nickname'] = !empty($operator)?$operator['nickname']:'';
                    $v['image'] = !empty($operator)?$operator['image']:'';
                    $comment = Comment::get($v['relate_id']);
                    $article = Article::get($comment['related_id']);
                    $v['article'] = !empty($article)?$article['title']:'';
                    $v['article_id'] = !empty($article)?$article['id']:'';
                }elseif($v['type'] == 2){
                    //对留言进行回复
                    $operator = Parents::get($v['operate_id']);
                    $v['nickname'] = !empty($operator)?$operator['nickname']:'';
                    $v['image'] = !empty($operator)?$operator['image']:'';
                    $comment = Comment::get($v['relate_id']);
                    $v['comment'] = !empty($comment)?$comment['answer']:'';
                    $v['article_id'] = !empty($comment)?$comment['related_id']:'';
                }elseif($v['type'] == 3){
                    //关注的人的文章
                    $operator = Parents::get($v['operate_id']);
                    $v['nickname'] = !empty($operator)?$operator['nickname']:'';
                    $v['image'] = !empty($operator)?$operator['image']:'';
                    $article = Article::get($v['relate_id']);
                    $v['article'] = !empty($article)?$article['title']:'';
                }elseif($v['type'] == 4){
                    //关注的医生的文章
                    $doctor = User::get($v['operate_id']);
                    $v['name'] = !empty($doctor)?$doctor['name']:'';
                    $v['image'] = !empty($doctor)?$doctor['image']:'';
                    $article = Article::get($v['relate_id']);
                    $v['article'] = !empty($article)?$article['title']:'';
                }elseif($v['type'] == 5){
                    //提问问题的回复
                    $doctor = User::get($v['operate_id']);
                    $v['name'] = !empty($doctor)?$doctor['name']:'';
                    $v['image'] = !empty($doctor)?$doctor['image']:'';
                    $article = Article::get($v['relate_id']);
                    $v['article'] = !empty($article)?$article['title']:'';
                }
            }

            $count = Db::name('message')->alias('a')->where($con)->field('a.*')->count();

            return ['code'=>0,'count' => $count,'data'=>$user,'msg'=>'','pages'=>$limit?ceil($count/$limit):''];
        }catch(\Exception $exception) {
            return ['code' => 1, 'data' => [], 'msg' => $exception->getMessage()];
        }
    }


}
