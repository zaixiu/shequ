<?php


namespace app\common\model;

use think\Db;
use think\Exception;
use think\Model;

class Gaode extends Model{

    public static $alias = [

    ];


    /**
     * Notes:获取高德的adcode
     * User: zzx
     * Date: 2022/5/16
     * Time: 16:34
     * @param $region_id
     * @return mixed|string
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public static function getCityCode($region_id){

        $region = Region::get($region_id);
        if(empty($region)){
            return'';
        }
        $gaode = self::where(['name'=>$region['name']])->find();
        return $gaode?$gaode['adcode']:'';
    }
}
