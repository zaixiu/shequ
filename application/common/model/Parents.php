<?php


namespace app\common\model;

use think\Db;
use think\Model;

class Parents extends Model{

    /**
     * 小程序端数据
     * @param $data
     * @return array
     */
    public static function getList($data = []){
        try{
            $con = [];
            $limit = null;
            $offset = 0;
            if(!empty($data['limit'])){
                $limit = $data['limit'];
            }
            if(!empty($data['page'])){
                $offset = $limit*($data['page']-1);
            }

            if(!empty($data['info'])){
                $con['a.mobile|a.nickname'] = ['like',"%{$data['info']}%"];
            }

            $topic = Db::name('parents')->alias('a')->where($con)
                ->order('id desc')
                ->field('a.*')
                ->limit($offset,$limit)
                ->select();

            foreach ($topic as $k => &$v){
                $v['gender_alias'] = $v['gender']==''?'':($v['gender'] == 2?'女':'男');
                $v['source_alias'] = $v['source'] == 2?'h5':'小程序';
                $v['image_label'] = "<img  style='width: 38px;height: 38px;border-radius: 50%' src='".$v['image']."'/>";
            }
            $count = Db::name('parents')->alias('a')->where($con)->field('a.*')->count();

            return ['code'=>0,'count' => $count,'data'=>$topic,'msg'=>''];
        }catch(\Exception $exception) {
            return ['code' => 1, 'data' => [], 'msg' => $exception->getMessage()];
        }
    }
}
