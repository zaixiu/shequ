<?php

namespace app\common\model;


use think\Db;
use think\Model;

class SysSetting extends Model
{

    /**
     * 系统参数key获取值
     */
    public static function getValueByKey($key)
    {
        $data = self::where(['key'=>$key])->find();
        return empty($data)?false:$data['value'];
    }


}
