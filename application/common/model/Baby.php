<?php


namespace app\common\model;

use think\Db;
use think\Model;

class Baby extends Model{

    public static $alias = [
        'sex_alias'=> [
            1=>'女',
            0=>'男',
        ],
    ];
    /**
     * @param $data
     * @return array
     */
    public static function getList($data = []){
        try{
            $con = [];
            $limit = null;
            $offset = 0;
            if(!empty($data['limit'])){
                $limit = $data['limit'];
            }
            if(!empty($data['page'])){
                $offset = $limit*($data['page']-1);
            }

            if(!empty($data['parent_id'])){
                $con['a.parent_id'] = $data['parent_id'];
            }

            if(!empty($data['id'])){
                $con['a.id'] = $data['id'];
            }
            if(!empty($data['is_default'])){
                $con['a.is_default'] = $data['is_default'];
            }

            if(!empty($data['name'])){
                $con['a.name'] = ['like',"%{$data['name']}%"];
            }
            if(!empty($data['info'])){
                $con['a.name|b.nickname|b.mobile'] = ['like',"%{$data['info']}%"];
            }

            $join = [
                ['parents b', 'a.parent_id = b.id', 'left'],
            ];

            $topic = Db::name('baby')->alias('a')->where($con)
                ->order('is_default desc ,id desc')
                ->field('a.*,b.nickname parent_name,b.mobile')
                ->join($join)
                ->limit($offset,$limit)
                ->select();

            foreach ($topic as $k => &$v){
                $v['age'] = self::age($v['birth']);
                $v['sex_alias'] = self::getAlias('sex_alias',$v['sex']);
                $v['image_label'] = "<img  style='width: 38px;height: 38px;border-radius: 50%' src='".$v['image']."'/>";
            }
            $count = Db::name('baby')->alias('a')->where($con)->field('a.*,b.nickname parent_name')->join($join)->count();

            return ['code'=>0,'count' => $count,'data'=>$topic,'msg'=>''];
        }catch(\Exception $exception) {
            return ['code' => 1, 'data' => [], 'msg' => $exception->getMessage()];
        }
    }


    /**
     * 计算遗传身高
     * @param $baby_id
     * @return string
     * @throws \think\exception\DbException
     */
    public static function  inheritHeight($baby_id){
        $result = self::inheritHeightVal($baby_id);
        return  $result.'(±7.5)cm';
    }

    /**
     * 计算遗传身高值
     * @param $baby_id
     * @return string
     * @throws \think\exception\DbException
     */
    public static function  inheritHeightVal($baby_id){
        $baby = self::get($baby_id);
        if($baby->sex == 1){
            $height = round(($baby->farther_height+$baby->mother_height-13)/2,1);
            return $height;
        }else{
            $height = round(($baby->farther_height+$baby->mother_height+13)/2,1);
            return $height;
        }

    }

    public static function  age($birth){
        $age = strtotime($birth);
        list($y1,$m1,$d1) = explode("-",date("Y-m-d",$age));
        $now = strtotime("now");
        list($y2,$m2,$d2) = explode("-",date("Y-m-d",$now));
        $age = $y2 - $y1;
        $month = $m2 - $m1;
        if((int)($m2.$d2) < (int)($m1.$d1)){
            $age -= 1;
            $month += 12;
        }
        return $age.'岁'.$month.'月';
    }




    public static function getAlias($key,$value)
    {
        return isset(self::$alias[$key][$value])?self::$alias[$key][$value]:'';
    }

}
