<?php

namespace app\common\widget;


use app\common\model\Activity;
use app\common\model\Mtroom;
use think\Db;

class Common{


    /**
     * 空间下拉
     */
    public function get_hotel_select($default = ''){
        $where['is_delete'] =2 ;
        $hotel = Db::name('hotel')->field('id,name')->where($where)->select();
        $str = '';
        if (count($hotel) > 0){
            foreach ($hotel as $k => $v){
                if ($v['id'] == $default)
                    $str .= '<option selected value="'.$v['id'].'">'.$v['name'].'</option>';
                else
                    $str .= '<option value="'.$v['id'].'">'.$v['name'].'</option>';
            }
        }
        return $str;
    }


    /**
     * 空间设施
     */
    public function get_hotel_facilities_checkbox($name = '',$default = ''){
        $data = [
            1=>'创业培训',
            2=>'创业咖啡',
            3=>'代理服务（工商，财务等）',
            4=>'咨询服务（法务等）',
            5=>'投融资',
            6=>'人力资源服务',
            7=>'导师指导',
        ];
        $default_array = explode(',',$default);
        $str = '';
        if (count($data) > 0){
            foreach ($data as $k=>$v){
                if (in_array($k,$default_array)){
                    $str .= '<input  type="checkbox"  checked name="'.$name.'[]" title="'.$v.'" value="'.$k.'">';
                }else{
                    $str .= '<input type="checkbox" name="'.$name.'[]" title="'.$v.'" value="'.$k.'">';
                }
            }
        }
        return $str;
    }


    /**
     * 空间配套
     */
    public function get_hotel_peitao_checkbox($name='',$default=''){
        $data = [
          1=>'餐厅',
          2=>'车位',
          3=>'会议室',
        ];
        $default_array = explode(',',$default);
        $str = '';
        if (count($data) > 0){
            foreach ($data as $k=>$v){
                if (in_array($k,$default_array)){
                    $str .= '<input  type="checkbox" checked name="'.$name.'[]" title="'.$v.'" value="'.$k.'">';
                }else{
                    $str .= '<input type="checkbox" name="'.$name.'[]" title="'.$v.'" value="'.$k.'">';
                }
            }
        }
        return $str;
    }


    /**
     * 活动标签
     */
    public function get_activity_label_checkbox($name = '',$default = '',$rule = ''){
        $data = Activity::$alias['label_alias'];
        $default_array = explode(',',$default);
        $str = '';
        if (count($data) > 0){
            foreach ($data as $k=>$v){
                if (in_array($k,$default_array)){
                    $str .= '<input checked type="checkbox" '.$rule.' name="'.$name.'[]" title="'.$v.'" value="'.$k.'">';
                }else{
                    $str .= '<input type="checkbox" '.$rule.' name="'.$name.'[]" title="'.$v.'" value="'.$k.'">';
                }
            }
        }
        return $str;
    }


    /**
     * 会议室设施
     */
    public function get_mtroom_label_checkbox($name = '',$default = '',$rule = ''){
        $data = Mtroom::$alias['facility_alias'];
        $default_array = explode(',',$default);
        $str = '';
        if (count($data) > 0){
            foreach ($data as $k=>$v){
                if (in_array($k,$default_array)){
                    $str .= '<input checked type="checkbox" '.$rule.' name="'.$name.'[]" title="'.$v.'" value="'.$k.'">';
                }else{
                    $str .= '<input type="checkbox" '.$rule.' name="'.$name.'[]" title="'.$v.'" value="'.$k.'">';
                }
            }
        }
        return $str;
    }


    /**
     *楼宇下拉
     */
    public function get_hotel_hall_select($default = '',$hotel_id = '',$category = 1){
        $where = [
            'is_delete'=>2,
            'category'=>$category
        ];

        if($category == 1){
            if (!empty($hotel_id)){
                $where['hotel_id'] = $hotel_id;
            }
        }
        $data = model('hotelHall')->where($where)->select();
        $str = '';
        if (count($data) > 0){
            foreach ($data as $k => $v){
                if ($v['id'] == $default){
                    $str .= '<option selected value="'.$v['id'].'">'.$v['name'].'</option>';
                }else{
                    $str .= '<option value="'.$v['id'].'">'.$v['name'].'</option>';
                }
            }
        }
        return $str;
    }


//    /**
//     * 楼层下拉
//     */
//    public function get_hotel_floor_select($default='',$hotel_id='',$hotel_hall_id='',$category = 1){
//        $where['is_delete'] = 2;
//        $hotel = Hotel::getHotelSelect();
//        if(session('user.role_id') != 1 && $category == 1){
//            $where['hotel_id'] = ['in',array_column($hotel,'id')];
//        }
//        if (!empty($hotel_hall_id)){
//            $where['hotel_hall_id'] = $hotel_hall_id;
//        }
//
//        $data = model('hotelFloor')->where($where)->select();
//        $str = '';
//        if (count($data) > 0){
//            foreach ($data as $k=>$v){
//                if ($v['id'] == $default){
//                    $str .= '<option selected value="'.$v['id'].'">'.$v['name'].'</option>';
//                }else{
//                    $str .= '<option value="'.$v['id'].'">'.$v['name'].'</option>';
//                }
//            }
//        }
//        return $str;
//    }
//
}
