/*
 Navicat Premium Data Transfer

 Source Server         : baishidi_new
 Source Server Type    : MySQL
 Source Server Version : 50734
 Source Host           : 121.40.44.186:3306
 Source Schema         : shequ

 Target Server Type    : MySQL
 Target Server Version : 50734
 File Encoding         : 65001

 Date: 18/01/2022 17:11:58
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sq_activity
-- ----------------------------
DROP TABLE IF EXISTS `sq_activity`;
CREATE TABLE `sq_activity`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '活动名称',
  `number` int(11) NULL DEFAULT NULL COMMENT '活动人数',
  `start_time` datetime(0) NULL DEFAULT NULL COMMENT '开始时间',
  `end_time` datetime(0) NULL DEFAULT NULL COMMENT '结束时间',
  `province_id` int(11) NULL DEFAULT NULL COMMENT '省',
  `city_id` int(11) NULL DEFAULT NULL COMMENT '市',
  `district_id` int(11) NULL DEFAULT NULL COMMENT '区',
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '详细地址',
  `poster` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '海报',
  `label` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标签',
  `desc` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '活动描述',
  `fee` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '活动费用',
  `contact` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系人',
  `mobile` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '手机号',
  `status` tinyint(1) NULL DEFAULT 1 COMMENT '1草稿 2发布',
  `summary` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '活动总结',
  `user_id` int(11) NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `is_delete` tinyint(1) NULL DEFAULT 2 COMMENT '1是 2否',
  `is_audit` tinyint(1) NULL DEFAULT 2 COMMENT '是否需要审核 1是 2否',
  `type` tinyint(3) NULL DEFAULT NULL COMMENT '类型',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '活动表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of sq_activity
-- ----------------------------
INSERT INTO `sq_activity` VALUES (1, '234', 2, '2021-06-09 00:02:02', '2021-07-20 00:00:00', NULL, NULL, NULL, '13123', '/files/20210517/-1621222134-32848.png', NULL, '1312', 0.00, '2', '15852356253', 2, NULL, NULL, '2021-04-12 11:35:16', '2021-05-17 15:14:27', 2, 2, 1);
INSERT INTO `sq_activity` VALUES (4, '234', NULL, '2021-05-04 00:00:00', '2021-06-16 00:00:00', NULL, NULL, NULL, NULL, '/files/20210517/-1621235683-8283.png', NULL, NULL, 0.00, NULL, NULL, 2, NULL, NULL, '2021-05-17 15:14:45', '2021-05-17 15:14:55', 1, 2, 1);

-- ----------------------------
-- Table structure for sq_activity_apply
-- ----------------------------
DROP TABLE IF EXISTS `sq_activity_apply`;
CREATE TABLE `sq_activity_apply`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `activity_id` int(11) NULL DEFAULT NULL COMMENT '活动id',
  `etprs_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '企业名称',
  `contact` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系人',
  `mobile` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系方式',
  `age` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '年龄',
  `number` int(11) NULL DEFAULT 0 COMMENT '报名人数',
  `status` tinyint(1) NULL DEFAULT NULL COMMENT '报名状态',
  `money` decimal(10, 2) NULL DEFAULT NULL COMMENT '费用',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `customer_id` int(11) NULL DEFAULT NULL,
  `note` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '活动申请表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of sq_activity_apply
-- ----------------------------

-- ----------------------------
-- Table structure for sq_article
-- ----------------------------
DROP TABLE IF EXISTS `sq_article`;
CREATE TABLE `sq_article`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` tinyint(1) NULL DEFAULT NULL COMMENT '1 文章 2 问题 3分享',
  `parent_id` int(11) NULL DEFAULT NULL COMMENT '家长id 23',
  `child_id` int(11) NULL DEFAULT NULL COMMENT '孩子id 23',
  `topic_id` int(11) NULL DEFAULT NULL COMMENT '话题id  1文章',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `sub_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `image` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `doctor_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `is_reply` tinyint(1) NULL DEFAULT 0 COMMENT '问题 医生是否回复 1是 0否',
  `reply_time` datetime(0) NULL DEFAULT NULL COMMENT '回复时间',
  `answer` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '医生回复',
  `sort` int(11) NULL DEFAULT 1 COMMENT '1文章',
  `is_delete` tinyint(1) NULL DEFAULT 2 COMMENT '1是 2否 1文章',
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `is_common` tinyint(1) NULL DEFAULT 0 COMMENT '是否常见问答 1是 0否',
  `is_show` tinyint(1) NULL DEFAULT 1 COMMENT '1是 0否 1文章',
  `user_id` int(11) NULL DEFAULT NULL COMMENT '1文章',
  `visit_number` int(11) NULL DEFAULT 0 COMMENT '浏览量',
  `is_report` tinyint(1) NULL DEFAULT 0 COMMENT '是否同步到身高报告中 1是 0否',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 45 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '（文章 问答 分享）' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of sq_article
-- ----------------------------

-- ----------------------------
-- Table structure for sq_baby
-- ----------------------------
DROP TABLE IF EXISTS `sq_baby`;
CREATE TABLE `sq_baby`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NULL DEFAULT NULL,
  `is_default` tinyint(1) NULL DEFAULT NULL COMMENT '是否默认 1是 0否',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '姓名',
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '头像',
  `sex` tinyint(255) NULL DEFAULT NULL COMMENT '性别',
  `birth` date NULL DEFAULT NULL COMMENT '出生年月',
  `farther_height` double NULL DEFAULT NULL COMMENT '父亲身高',
  `mother_height` double NULL DEFAULT NULL COMMENT '母亲身高',
  `hope_height` double NULL DEFAULT NULL COMMENT '期望身高',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '最后修改时间',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '注册时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 53 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '孩子' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of sq_baby
-- ----------------------------
INSERT INTO `sq_baby` VALUES (52, 7, 1, '111', '/files/20211216/-1639626215-36954.jpg', 0, '2010-01-01', 180, 165, 183, '2021-12-16 11:44:00', '2021-12-16 11:44:00');

-- ----------------------------
-- Table structure for sq_baby_grow_log
-- ----------------------------
DROP TABLE IF EXISTS `sq_baby_grow_log`;
CREATE TABLE `sq_baby_grow_log`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NULL DEFAULT NULL,
  `baby_id` int(11) NULL DEFAULT NULL,
  `measure_time` date NULL DEFAULT NULL COMMENT '测量时间',
  `height` decimal(5, 1) NULL DEFAULT NULL COMMENT '身高',
  `weight` decimal(6, 1) NULL DEFAULT NULL COMMENT '体重',
  `bone_age` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '骨龄',
  `before_measure_time` date NULL DEFAULT NULL COMMENT '既往测量时间',
  `before_height` decimal(5, 1) NULL DEFAULT NULL COMMENT '既往测量身高',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '最后修改时间',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '注册时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 37 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '宝宝成长记录' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of sq_baby_grow_log
-- ----------------------------
INSERT INTO `sq_baby_grow_log` VALUES (36, 7, 52, '2021-12-16', 150.0, 13.0, '', NULL, NULL, '2021-12-16 11:44:00', '2021-12-16 11:44:00');

-- ----------------------------
-- Table structure for sq_case_show
-- ----------------------------
DROP TABLE IF EXISTS `sq_case_show`;
CREATE TABLE `sq_case_show`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '最后修改时间',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '注册时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '案例展示' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of sq_case_show
-- ----------------------------
INSERT INTO `sq_case_show` VALUES (1, '/files/20210412/-1618207622-34524.jpg', '222', '<video src=\"http://yun.com/files/video/-1621300535-66144.mp4\" controls=\"controls\" style=\"max-width:100%\"></video>', '2021-05-18 09:15:42', '2021-04-12 14:07:05');

-- ----------------------------
-- Table structure for sq_collect
-- ----------------------------
DROP TABLE IF EXISTS `sq_collect`;
CREATE TABLE `sq_collect`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `operate_id` int(11) NULL DEFAULT NULL COMMENT '用户id',
  `related_id` int(11) NULL DEFAULT NULL COMMENT '关联id',
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 25 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '收藏 （文章  问答 分享）' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of sq_collect
-- ----------------------------

-- ----------------------------
-- Table structure for sq_comment
-- ----------------------------
DROP TABLE IF EXISTS `sq_comment`;
CREATE TABLE `sq_comment`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `operate_id` int(11) NULL DEFAULT NULL COMMENT '用户id',
  `related_id` int(11) NULL DEFAULT NULL COMMENT '关联id',
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '留言内容',
  `answer` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '回复内容',
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 30 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '留言（文章  分享）' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of sq_comment
-- ----------------------------

-- ----------------------------
-- Table structure for sq_consult
-- ----------------------------
DROP TABLE IF EXISTS `sq_consult`;
CREATE TABLE `sq_consult`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NULL DEFAULT NULL COMMENT '用户id',
  `question` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '问题',
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '意见内容',
  `status` tinyint(1) NULL DEFAULT 1 COMMENT '状态',
  `note` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `type` tinyint(3) NULL DEFAULT NULL COMMENT '类型',
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'h5端咨询表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of sq_consult
-- ----------------------------

-- ----------------------------
-- Table structure for sq_follow
-- ----------------------------
DROP TABLE IF EXISTS `sq_follow`;
CREATE TABLE `sq_follow`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` tinyint(1) NULL DEFAULT NULL COMMENT '1 用户 2话题 3医生',
  `operate_id` int(11) NULL DEFAULT NULL COMMENT '用户ID',
  `topic_id` int(11) NULL DEFAULT NULL COMMENT '话题id',
  `doctor_id` int(11) NULL DEFAULT NULL COMMENT '医生id',
  `parent_id` int(11) NULL DEFAULT NULL COMMENT '父母id',
  `child_id` int(11) NULL DEFAULT NULL COMMENT '小孩id',
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 38 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '关注 （用户 话题  医生）' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of sq_follow
-- ----------------------------

-- ----------------------------
-- Table structure for sq_message
-- ----------------------------
DROP TABLE IF EXISTS `sq_message`;
CREATE TABLE `sq_message`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` tinyint(1) NULL DEFAULT NULL COMMENT '类型 1评论 2回复 3关注的人的文章，4关注的医生的文章 5问答回复',
  `parent_id` int(11) NULL DEFAULT NULL COMMENT '发送人id',
  `operate_id` int(11) NULL DEFAULT NULL COMMENT '操作人id',
  `relate_id` int(11) NULL DEFAULT NULL COMMENT '关联id',
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '留言内容',
  `is_read` tinyint(1) NULL DEFAULT 0 COMMENT '是否已读 1是0否',
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 49 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '消息提醒' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of sq_message
-- ----------------------------

-- ----------------------------
-- Table structure for sq_opinion
-- ----------------------------
DROP TABLE IF EXISTS `sq_opinion`;
CREATE TABLE `sq_opinion`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `operate_id` int(11) NULL DEFAULT NULL COMMENT '用户id',
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '意见内容',
  `image` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '图片',
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 28 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '收藏 （文章  问答 分享）' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of sq_opinion
-- ----------------------------

-- ----------------------------
-- Table structure for sq_parents
-- ----------------------------
DROP TABLE IF EXISTS `sq_parents`;
CREATE TABLE `sq_parents`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '头像',
  `mobile` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `openid` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `gender` tinyint(1) NULL DEFAULT NULL COMMENT '2女',
  `nickname` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `status` tinyint(1) NULL DEFAULT 1 COMMENT '用户状态 1正常 2冻结',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '最后修改时间',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '注册时间',
  `source` tinyint(1) NULL DEFAULT 1 COMMENT '1小程序 2h5',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `user_name`(`nickname`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of sq_parents
-- ----------------------------
INSERT INTO `sq_parents` VALUES (7, 'https://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJvxTO5l5tNHe8hr8icBpnrZ7tLjNR5WS9YYfr3YWR6kzPBcvPym9V9SQyoolzHw6QDxMROO4gRiaPQ/132', '15054223320', 'onZR75Eb_PWptj1ENPHPNBvSRB40', 0, '亚克西', 1, '2021-12-16 11:35:33', '2021-12-16 11:35:33', 1);
INSERT INTO `sq_parents` VALUES (8, 'https://thirdwx.qlogo.cn/mmopen/vi_32/POgEwh4mIHO4nibH0KlMECNjjGxQUq24ZEaGT4poC6icRiccVGKSyXwibcPq4BWmiaIGuG1icwxaQX6grC9VemZoJ8rg/132', '18653219189', 'onZR75JhUeZCXLJZpM3J6WKkd6xA', 0, '微信用户', 1, '2021-12-16 16:18:10', '2021-12-16 16:18:10', 1);
INSERT INTO `sq_parents` VALUES (9, 'https://thirdwx.qlogo.cn/mmopen/vi_32/POgEwh4mIHO4nibH0KlMECNjjGxQUq24ZEaGT4poC6icRiccVGKSyXwibcPq4BWmiaIGuG1icwxaQX6grC9VemZoJ8rg/132', '15131137091', 'onZR75DaB2UWafmkltXcTcXDvdh4', 0, '微信用户', 1, '2022-01-10 12:32:24', '2022-01-10 12:32:24', 1);

-- ----------------------------
-- Table structure for sq_poster
-- ----------------------------
DROP TABLE IF EXISTS `sq_poster`;
CREATE TABLE `sq_poster`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `desc` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `is_delete` tinyint(1) NULL DEFAULT 2 COMMENT '1是 2否',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '海报管理' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of sq_poster
-- ----------------------------
INSERT INTO `sq_poster` VALUES (7, '金标点亮计划', '金标点亮计划', '/files/20210811/-1628664408-30207.jpg', NULL, '2021-08-03 14:01:55', '2021-12-16 11:21:49', 2);
INSERT INTO `sq_poster` VALUES (8, '345', NULL, '/files/20210803/-1627972127-28783.jpg', NULL, '2021-08-03 14:28:48', '2021-08-03 14:28:48', 1);
INSERT INTO `sq_poster` VALUES (9, '456', NULL, '/files/20210803/-1627982370-28464.jpg', NULL, '2021-08-03 17:19:36', '2021-08-03 17:19:36', 1);
INSERT INTO `sq_poster` VALUES (10, '45645', '345345345', '/files/20210803/-1627982479-35222.jpg', NULL, '2021-08-03 17:21:26', '2021-08-20 15:55:41', 1);
INSERT INTO `sq_poster` VALUES (11, '345345', '345345', '/files/20210820/-1629446151-19387.jpg', NULL, '2021-08-20 15:55:52', '2021-08-20 15:55:52', 1);

-- ----------------------------
-- Table structure for sq_product
-- ----------------------------
DROP TABLE IF EXISTS `sq_product`;
CREATE TABLE `sq_product`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '最后修改时间',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '注册时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '产品表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of sq_product
-- ----------------------------
INSERT INTO `sq_product` VALUES (1, '/files/20210412/-1618190003-30251.jpg', '万哺乐HN001鼠李糖乳杆菌滴剂加拿大进口12ml', '', '2021-12-16 11:24:30', '2020-12-01 10:31:10');

-- ----------------------------
-- Table structure for sq_region
-- ----------------------------
DROP TABLE IF EXISTS `sq_region`;
CREATE TABLE `sq_region`  (
  `id` int(11) NOT NULL DEFAULT 0,
  `name` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `level` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `provinceid` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `cityid` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `parentid` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `isDelete` tinyint(2) NULL DEFAULT 0,
  `haierId` int(11) NULL DEFAULT 0,
  `code` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `alsname` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '省的别名，为了跟地图对应',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `region_isdelete`(`isDelete`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '区域表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of sq_region
-- ----------------------------
INSERT INTO `sq_region` VALUES (11, '北京市', '1', '11', '0', '0', 0, 18, '', '北京');
INSERT INTO `sq_region` VALUES (12, '天津市', '1', '12', '0', '0', 0, 323, '', '天津');
INSERT INTO `sq_region` VALUES (13, '河北省', '1', '13', '0', '0', 0, NULL, '', '河北');
INSERT INTO `sq_region` VALUES (14, '山西省', '1', '14', '0', '0', 0, NULL, '', '山西');
INSERT INTO `sq_region` VALUES (15, '内蒙古自治区', '1', '15', '0', '0', 0, NULL, '', '内蒙古');
INSERT INTO `sq_region` VALUES (21, '辽宁省', '1', '21', '0', '0', 0, NULL, '', '辽宁');
INSERT INTO `sq_region` VALUES (22, '吉林省', '1', '22', '0', '0', 0, NULL, '', '吉林');
INSERT INTO `sq_region` VALUES (23, '黑龙江省', '1', '23', '0', '0', 0, NULL, '', '黑龙江');
INSERT INTO `sq_region` VALUES (31, '上海市', '1', '31', '0', '0', 0, 283, '', '上海');
INSERT INTO `sq_region` VALUES (32, '江苏省', '1', '32', '0', '0', 0, NULL, '', '江苏');
INSERT INTO `sq_region` VALUES (33, '浙江省', '1', '33', '0', '0', 0, NULL, '', '浙江');
INSERT INTO `sq_region` VALUES (34, '安徽省', '1', '34', '0', '0', 0, NULL, '', '安徽');
INSERT INTO `sq_region` VALUES (35, '福建省', '1', '35', '0', '0', 0, NULL, '', '福建');
INSERT INTO `sq_region` VALUES (36, '江西省', '1', '36', '0', '0', 0, NULL, '', '江西');
INSERT INTO `sq_region` VALUES (37, '山东省', '1', '37', '0', '0', 0, NULL, '', '山东');
INSERT INTO `sq_region` VALUES (41, '河南省', '1', '41', '0', '0', 0, NULL, '', '河南');
INSERT INTO `sq_region` VALUES (42, '湖北省', '1', '42', '0', '0', 0, NULL, '', '湖北');
INSERT INTO `sq_region` VALUES (43, '湖南省', '1', '43', '0', '0', 0, NULL, '', '湖南');
INSERT INTO `sq_region` VALUES (44, '广东省', '1', '44', '0', '0', 0, NULL, '', '广东');
INSERT INTO `sq_region` VALUES (45, '广西壮族自治区', '1', '45', '0', '0', 0, NULL, '', '广西');
INSERT INTO `sq_region` VALUES (46, '海南省', '1', '46', '0', '0', 0, NULL, '', '海南');
INSERT INTO `sq_region` VALUES (50, '重庆市', '1', '50', '0', '0', 0, 390, '', '重庆');
INSERT INTO `sq_region` VALUES (51, '四川省', '1', '51', '0', '0', 0, NULL, '', '四川');
INSERT INTO `sq_region` VALUES (52, '贵州省', '1', '52', '0', '0', 0, NULL, '', '贵州');
INSERT INTO `sq_region` VALUES (53, '云南省', '1', '53', '0', '0', 0, NULL, '', '云南');
INSERT INTO `sq_region` VALUES (54, '西藏自治区', '1', '54', '0', '0', 0, NULL, '', '西藏');
INSERT INTO `sq_region` VALUES (61, '陕西省', '1', '61', '0', '0', 0, NULL, '', '陕西');
INSERT INTO `sq_region` VALUES (62, '甘肃省', '1', '62', '0', '0', 0, NULL, '', '甘肃');
INSERT INTO `sq_region` VALUES (63, '青海省', '1', '63', '0', '0', 0, NULL, '', '青海');
INSERT INTO `sq_region` VALUES (64, '宁夏回族自治区', '1', '64', '0', '0', 0, NULL, '', '宁夏');
INSERT INTO `sq_region` VALUES (65, '新疆维吾尔自治区', '1', '65', '0', '0', 0, NULL, '', '新疆');
INSERT INTO `sq_region` VALUES (71, '台湾省', '1', '71', '0', '0', 0, NULL, '', '台湾');
INSERT INTO `sq_region` VALUES (81, '香港特别行政区', '1', '81', '0', '0', 0, NULL, '', '香港');
INSERT INTO `sq_region` VALUES (82, '澳门', '1', '82', '0', '0', 0, NULL, '', '澳门');
INSERT INTO `sq_region` VALUES (1101, '北京市', '2', '11', '1101', '11', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (1201, '天津市', '2', '12', '1201', '12', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (1301, '石家庄市', '2', '13', '1301', '13', 0, 112, '', '');
INSERT INTO `sq_region` VALUES (1302, '唐山市', '2', '13', '1302', '13', 0, 113, '', '');
INSERT INTO `sq_region` VALUES (1303, '秦皇岛市', '2', '13', '1303', '13', 0, 111, '', '');
INSERT INTO `sq_region` VALUES (1304, '邯郸市', '2', '13', '1304', '13', 0, 108, '', '');
INSERT INTO `sq_region` VALUES (1305, '邢台市', '2', '13', '1305', '13', 0, 114, '', '');
INSERT INTO `sq_region` VALUES (1306, '保定市', '2', '13', '1306', '13', 0, 105, '', '');
INSERT INTO `sq_region` VALUES (1307, '张家口市', '2', '13', '1307', '13', 0, 115, '', '');
INSERT INTO `sq_region` VALUES (1308, '承德市', '2', '13', '1308', '13', 0, 107, '', '');
INSERT INTO `sq_region` VALUES (1309, '沧州市', '2', '13', '1309', '13', 0, 106, '', '');
INSERT INTO `sq_region` VALUES (1310, '廊坊市', '2', '13', '1310', '13', 0, 110, '', '');
INSERT INTO `sq_region` VALUES (1311, '衡水市', '2', '13', '1311', '13', 0, 109, '', '');
INSERT INTO `sq_region` VALUES (1390, '省直辖县级行政区划', '2', '13', '1390', '13', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (1401, '太原市', '2', '14', '1401', '14', 0, 269, '', '');
INSERT INTO `sq_region` VALUES (1402, '大同市', '2', '14', '1402', '14', 0, 263, '', '');
INSERT INTO `sq_region` VALUES (1403, '阳泉市', '2', '14', '1403', '14', 0, 271, '', '');
INSERT INTO `sq_region` VALUES (1404, '长治市', '2', '14', '1404', '14', 0, 262, '', '');
INSERT INTO `sq_region` VALUES (1405, '晋城市', '2', '14', '1405', '14', 0, 264, '', '');
INSERT INTO `sq_region` VALUES (1406, '朔州市', '2', '14', '1406', '14', 0, 268, '', '');
INSERT INTO `sq_region` VALUES (1407, '晋中市', '2', '14', '1407', '14', 0, 265, '', '');
INSERT INTO `sq_region` VALUES (1408, '运城市', '2', '14', '1408', '14', 0, 272, '', '');
INSERT INTO `sq_region` VALUES (1409, '忻州市', '2', '14', '1409', '14', 0, 270, '', '');
INSERT INTO `sq_region` VALUES (1410, '临汾市', '2', '14', '1410', '14', 0, 266, '', '');
INSERT INTO `sq_region` VALUES (1411, '吕梁市', '2', '14', '1411', '14', 0, 267, '', '');
INSERT INTO `sq_region` VALUES (1501, '呼和浩特市', '2', '15', '1501', '15', 0, 225, '', '');
INSERT INTO `sq_region` VALUES (1502, '包头市', '2', '15', '1502', '15', 0, 222, '', '');
INSERT INTO `sq_region` VALUES (1503, '乌海市', '2', '15', '1503', '15', 0, 228, '', '');
INSERT INTO `sq_region` VALUES (1504, '赤峰市', '2', '15', '1504', '15', 0, 223, '', '');
INSERT INTO `sq_region` VALUES (1505, '通辽市', '2', '15', '1505', '15', 0, 227, '', '');
INSERT INTO `sq_region` VALUES (1506, '鄂尔多斯市', '2', '15', '1506', '15', 0, 224, '', '');
INSERT INTO `sq_region` VALUES (1507, '呼伦贝尔市', '2', '15', '1507', '15', 0, 226, '', '');
INSERT INTO `sq_region` VALUES (1508, '巴彦淖尔市', '2', '15', '1508', '15', 0, 221, '', '');
INSERT INTO `sq_region` VALUES (1509, '乌兰察布市', '2', '15', '1509', '15', 0, 229, '', '');
INSERT INTO `sq_region` VALUES (1522, '兴安盟', '2', '15', '1522', '15', 0, 231, '', '');
INSERT INTO `sq_region` VALUES (1525, '锡林郭勒盟', '2', '15', '1525', '15', 0, 230, '', '');
INSERT INTO `sq_region` VALUES (1529, '阿拉善盟', '2', '15', '1529', '15', 0, 220, '', '');
INSERT INTO `sq_region` VALUES (2101, '沈阳市', '2', '21', '2101', '21', 0, 217, '', '');
INSERT INTO `sq_region` VALUES (2102, '大连市', '2', '21', '2102', '21', 0, 209, '', '');
INSERT INTO `sq_region` VALUES (2103, '鞍山市', '2', '21', '2103', '21', 0, 206, '', '');
INSERT INTO `sq_region` VALUES (2104, '抚顺市', '2', '21', '2104', '21', 0, 211, '', '');
INSERT INTO `sq_region` VALUES (2105, '本溪市', '2', '21', '2105', '21', 0, 207, '', '');
INSERT INTO `sq_region` VALUES (2106, '丹东市', '2', '21', '2106', '21', 0, 210, '', '');
INSERT INTO `sq_region` VALUES (2107, '锦州市', '2', '21', '2107', '21', 0, 214, '', '');
INSERT INTO `sq_region` VALUES (2108, '营口市', '2', '21', '2108', '21', 0, 219, '', '');
INSERT INTO `sq_region` VALUES (2109, '阜新市', '2', '21', '2109', '21', 0, 212, '', '');
INSERT INTO `sq_region` VALUES (2110, '辽阳市', '2', '21', '2110', '21', 0, 215, '', '');
INSERT INTO `sq_region` VALUES (2111, '盘锦市', '2', '21', '2111', '21', 0, 216, '', '');
INSERT INTO `sq_region` VALUES (2112, '铁岭市', '2', '21', '2112', '21', 0, 218, '', '');
INSERT INTO `sq_region` VALUES (2113, '朝阳市', '2', '21', '2113', '21', 0, 208, '', '');
INSERT INTO `sq_region` VALUES (2114, '葫芦岛市', '2', '21', '2114', '21', 0, 213, '', '');
INSERT INTO `sq_region` VALUES (2201, '长春市', '2', '22', '2201', '22', 0, 175, '', '');
INSERT INTO `sq_region` VALUES (2202, '吉林市', '2', '22', '2202', '22', 0, 176, '', '');
INSERT INTO `sq_region` VALUES (2203, '四平市', '2', '22', '2203', '22', 0, 178, '', '');
INSERT INTO `sq_region` VALUES (2204, '辽源市', '2', '22', '2204', '22', 0, 177, '', '');
INSERT INTO `sq_region` VALUES (2205, '通化市', '2', '22', '2205', '22', 0, 180, '', '');
INSERT INTO `sq_region` VALUES (2206, '白山市', '2', '22', '2206', '22', 0, 174, '', '');
INSERT INTO `sq_region` VALUES (2207, '松原市', '2', '22', '2207', '22', 0, 179, '', '');
INSERT INTO `sq_region` VALUES (2208, '白城市', '2', '22', '2208', '22', 0, 173, '', '');
INSERT INTO `sq_region` VALUES (2224, '延边朝鲜族自治州', '2', '22', '2224', '22', 0, 181, '', '');
INSERT INTO `sq_region` VALUES (2301, '哈尔滨市', '2', '23', '2301', '23', 0, 135, '', '');
INSERT INTO `sq_region` VALUES (2302, '齐齐哈尔市', '2', '23', '2302', '23', 0, 142, '', '');
INSERT INTO `sq_region` VALUES (2303, '鸡西市', '2', '23', '2303', '23', 0, 138, '', '');
INSERT INTO `sq_region` VALUES (2304, '鹤岗市', '2', '23', '2304', '23', 0, 136, '', '');
INSERT INTO `sq_region` VALUES (2305, '双鸭山市', '2', '23', '2305', '23', 0, 143, '', '');
INSERT INTO `sq_region` VALUES (2306, '大庆市', '2', '23', '2306', '23', 0, 133, '', '');
INSERT INTO `sq_region` VALUES (2307, '伊春市', '2', '23', '2307', '23', 0, 145, '', '');
INSERT INTO `sq_region` VALUES (2308, '佳木斯市', '2', '23', '2308', '23', 0, 139, '', '');
INSERT INTO `sq_region` VALUES (2309, '七台河市', '2', '23', '2309', '23', 0, 141, '', '');
INSERT INTO `sq_region` VALUES (2310, '牡丹江市', '2', '23', '2310', '23', 0, 140, '', '');
INSERT INTO `sq_region` VALUES (2311, '黑河市', '2', '23', '2311', '23', 0, 137, '', '');
INSERT INTO `sq_region` VALUES (2312, '绥化市', '2', '23', '2312', '23', 0, 144, '', '');
INSERT INTO `sq_region` VALUES (2327, '大兴安岭地区', '2', '23', '2327', '23', 0, 134, '', '');
INSERT INTO `sq_region` VALUES (3101, '上海市', '2', '31', '3101', '31', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (3201, '南京市', '2', '32', '3201', '32', 0, 185, '', '');
INSERT INTO `sq_region` VALUES (3202, '无锡市', '2', '32', '3202', '32', 0, 190, '', '');
INSERT INTO `sq_region` VALUES (3203, '徐州市', '2', '32', '3203', '32', 0, 191, '', '');
INSERT INTO `sq_region` VALUES (3204, '常州市', '2', '32', '3204', '32', 0, 182, '', '');
INSERT INTO `sq_region` VALUES (3205, '苏州市', '2', '32', '3205', '32', 0, 187, '', '');
INSERT INTO `sq_region` VALUES (3206, '南通市', '2', '32', '3206', '32', 0, 186, '', '');
INSERT INTO `sq_region` VALUES (3207, '连云港市', '2', '32', '3207', '32', 0, 184, '', '');
INSERT INTO `sq_region` VALUES (3208, '淮安市', '2', '32', '3208', '32', 0, 183, '', '');
INSERT INTO `sq_region` VALUES (3209, '盐城市', '2', '32', '3209', '32', 0, 192, '', '');
INSERT INTO `sq_region` VALUES (3210, '扬州市', '2', '32', '3210', '32', 0, 193, '', '');
INSERT INTO `sq_region` VALUES (3211, '镇江市', '2', '32', '3211', '32', 0, 194, '', '');
INSERT INTO `sq_region` VALUES (3212, '泰州市', '2', '32', '3212', '32', 0, 189, '', '');
INSERT INTO `sq_region` VALUES (3213, '宿迁市', '2', '32', '3213', '32', 0, 188, '', '');
INSERT INTO `sq_region` VALUES (3301, '杭州市', '2', '33', '3301', '33', 0, 379, '', '');
INSERT INTO `sq_region` VALUES (3302, '宁波市', '2', '33', '3302', '33', 0, 384, '', '');
INSERT INTO `sq_region` VALUES (3303, '温州市', '2', '33', '3303', '33', 0, 387, '', '');
INSERT INTO `sq_region` VALUES (3304, '嘉兴市', '2', '33', '3304', '33', 0, 381, '', '');
INSERT INTO `sq_region` VALUES (3305, '湖州市', '2', '33', '3305', '33', 0, 380, '', '');
INSERT INTO `sq_region` VALUES (3306, '绍兴市', '2', '33', '3306', '33', 0, 385, '', '');
INSERT INTO `sq_region` VALUES (3307, '金华市', '2', '33', '3307', '33', 0, 382, '', '');
INSERT INTO `sq_region` VALUES (3308, '衢州市', '2', '33', '3308', '33', 0, 389, '', '');
INSERT INTO `sq_region` VALUES (3309, '舟山市', '2', '33', '3309', '33', 0, 388, '', '');
INSERT INTO `sq_region` VALUES (3310, '台州市', '2', '33', '3310', '33', 0, 386, '', '');
INSERT INTO `sq_region` VALUES (3311, '丽水市', '2', '33', '3311', '33', 0, 383, '', '');
INSERT INTO `sq_region` VALUES (3401, '合肥市', '2', '34', '3401', '34', 0, 7, '', '');
INSERT INTO `sq_region` VALUES (3402, '芜湖市', '2', '34', '3402', '34', 0, 15, '', '');
INSERT INTO `sq_region` VALUES (3403, '蚌埠市', '2', '34', '3403', '34', 0, 2, '', '');
INSERT INTO `sq_region` VALUES (3404, '淮南市', '2', '34', '3404', '34', 0, 9, '', '');
INSERT INTO `sq_region` VALUES (3405, '马鞍山市', '2', '34', '3405', '34', 0, 12, '', '');
INSERT INTO `sq_region` VALUES (3406, '淮北市', '2', '34', '3406', '34', 0, 8, '', '');
INSERT INTO `sq_region` VALUES (3407, '铜陵市', '2', '34', '3407', '34', 0, 14, '', '');
INSERT INTO `sq_region` VALUES (3408, '安庆市', '2', '34', '3408', '34', 0, 1, '', '');
INSERT INTO `sq_region` VALUES (3410, '黄山市', '2', '34', '3410', '34', 0, 10, '', '');
INSERT INTO `sq_region` VALUES (3411, '滁州市', '2', '34', '3411', '34', 0, 5, '', '');
INSERT INTO `sq_region` VALUES (3412, '阜阳市', '2', '34', '3412', '34', 0, 6, '', '');
INSERT INTO `sq_region` VALUES (3413, '宿州市', '2', '34', '3413', '34', 0, 13, '', '');
INSERT INTO `sq_region` VALUES (3415, '六安市', '2', '34', '3415', '34', 0, 11, '', '');
INSERT INTO `sq_region` VALUES (3416, '亳州市', '2', '34', '3416', '34', 0, 17, '', '');
INSERT INTO `sq_region` VALUES (3417, '池州市', '2', '34', '3417', '34', 0, 4, '', '');
INSERT INTO `sq_region` VALUES (3418, '宣城市', '2', '34', '3418', '34', 0, 16, '', '');
INSERT INTO `sq_region` VALUES (3501, '福州市', '2', '35', '3501', '35', 0, 36, '', '');
INSERT INTO `sq_region` VALUES (3502, '厦门市', '2', '35', '3502', '35', 0, 43, '', '');
INSERT INTO `sq_region` VALUES (3503, '莆田市', '2', '35', '3503', '35', 0, 40, '', '');
INSERT INTO `sq_region` VALUES (3504, '三明市', '2', '35', '3504', '35', 0, 42, '', '');
INSERT INTO `sq_region` VALUES (3505, '泉州市', '2', '35', '3505', '35', 0, 41, '', '');
INSERT INTO `sq_region` VALUES (3506, '漳州市', '2', '35', '3506', '35', 0, 44, '', '');
INSERT INTO `sq_region` VALUES (3507, '南平市', '2', '35', '3507', '35', 0, 38, '', '');
INSERT INTO `sq_region` VALUES (3508, '龙岩市', '2', '35', '3508', '35', 0, 37, '', '');
INSERT INTO `sq_region` VALUES (3509, '宁德市', '2', '35', '3509', '35', 0, 39, '', '');
INSERT INTO `sq_region` VALUES (3601, '南昌市', '2', '36', '3601', '36', 0, 200, '', '');
INSERT INTO `sq_region` VALUES (3602, '景德镇市', '2', '36', '3602', '36', 0, 198, '', '');
INSERT INTO `sq_region` VALUES (3603, '萍乡市', '2', '36', '3603', '36', 0, 201, '', '');
INSERT INTO `sq_region` VALUES (3604, '九江市', '2', '36', '3604', '36', 0, 199, '', '');
INSERT INTO `sq_region` VALUES (3605, '新余市', '2', '36', '3605', '36', 0, 203, '', '');
INSERT INTO `sq_region` VALUES (3606, '鹰潭市', '2', '36', '3606', '36', 0, 205, '', '');
INSERT INTO `sq_region` VALUES (3607, '赣州市', '2', '36', '3607', '36', 0, 196, '', '');
INSERT INTO `sq_region` VALUES (3608, '吉安市', '2', '36', '3608', '36', 0, 197, '', '');
INSERT INTO `sq_region` VALUES (3609, '宜春市', '2', '36', '3609', '36', 0, 204, '', '');
INSERT INTO `sq_region` VALUES (3610, '抚州市', '2', '36', '3610', '36', 0, 195, '', '');
INSERT INTO `sq_region` VALUES (3611, '上饶市', '2', '36', '3611', '36', 0, 202, '', '');
INSERT INTO `sq_region` VALUES (3701, '济南市', '2', '37', '3701', '37', 0, 249, '', '');
INSERT INTO `sq_region` VALUES (3702, '青岛市', '2', '37', '3702', '37', 0, 254, '', '');
INSERT INTO `sq_region` VALUES (3703, '淄博市', '2', '37', '3703', '37', 0, 261, '', '');
INSERT INTO `sq_region` VALUES (3704, '枣庄市', '2', '37', '3704', '37', 0, 260, '', '');
INSERT INTO `sq_region` VALUES (3705, '东营市', '2', '37', '3705', '37', 0, 247, '', '');
INSERT INTO `sq_region` VALUES (3706, '烟台市', '2', '37', '3706', '37', 0, 259, '', '');
INSERT INTO `sq_region` VALUES (3707, '潍坊市', '2', '37', '3707', '37', 0, 258, '', '');
INSERT INTO `sq_region` VALUES (3708, '济宁市', '2', '37', '3708', '37', 0, 250, '', '');
INSERT INTO `sq_region` VALUES (3709, '泰安市', '2', '37', '3709', '37', 0, 256, '', '');
INSERT INTO `sq_region` VALUES (3710, '威海市', '2', '37', '3710', '37', 0, 257, '', '');
INSERT INTO `sq_region` VALUES (3711, '日照市', '2', '37', '3711', '37', 0, 255, '', '');
INSERT INTO `sq_region` VALUES (3712, '莱芜市', '2', '37', '3712', '37', 0, 251, '', '');
INSERT INTO `sq_region` VALUES (3713, '临沂市', '2', '37', '3713', '37', 0, 253, '', '');
INSERT INTO `sq_region` VALUES (3714, '德州市', '2', '37', '3714', '37', 0, 246, '', '');
INSERT INTO `sq_region` VALUES (3715, '聊城市', '2', '37', '3715', '37', 0, 252, '', '');
INSERT INTO `sq_region` VALUES (3716, '滨州市', '2', '37', '3716', '37', 0, 245, '', '');
INSERT INTO `sq_region` VALUES (3717, '菏泽市', '2', '37', '3717', '37', 0, 248, '', '');
INSERT INTO `sq_region` VALUES (4101, '郑州市', '2', '41', '4101', '41', 0, 128, '', '');
INSERT INTO `sq_region` VALUES (4102, '开封市', '2', '41', '4102', '41', 0, 119, '', '');
INSERT INTO `sq_region` VALUES (4103, '洛阳市', '2', '41', '4103', '41', 0, 120, '', '');
INSERT INTO `sq_region` VALUES (4104, '平顶山市', '2', '41', '4104', '41', 0, 122, '', '');
INSERT INTO `sq_region` VALUES (4105, '安阳市', '2', '41', '4105', '41', 0, 116, '', '');
INSERT INTO `sq_region` VALUES (4106, '鹤壁市', '2', '41', '4106', '41', 0, 117, '', '');
INSERT INTO `sq_region` VALUES (4107, '新乡市', '2', '41', '4107', '41', 0, 125, '', '');
INSERT INTO `sq_region` VALUES (4108, '焦作市', '2', '41', '4108', '41', 0, 118, '', '');
INSERT INTO `sq_region` VALUES (4109, '濮阳市', '2', '41', '4109', '41', 0, 132, '', '');
INSERT INTO `sq_region` VALUES (4110, '许昌市', '2', '41', '4110', '41', 0, 127, '', '');
INSERT INTO `sq_region` VALUES (4111, '漯河市', '2', '41', '4111', '41', 0, 131, '', '');
INSERT INTO `sq_region` VALUES (4112, '三门峡市', '2', '41', '4112', '41', 0, 123, '', '');
INSERT INTO `sq_region` VALUES (4113, '南阳市', '2', '41', '4113', '41', 0, 121, '', '');
INSERT INTO `sq_region` VALUES (4114, '商丘市', '2', '41', '4114', '41', 0, 124, '', '');
INSERT INTO `sq_region` VALUES (4115, '信阳市', '2', '41', '4115', '41', 0, 126, '', '');
INSERT INTO `sq_region` VALUES (4116, '周口市', '2', '41', '4116', '41', 0, 129, '', '');
INSERT INTO `sq_region` VALUES (4117, '驻马店市', '2', '41', '4117', '41', 0, 130, '', '');
INSERT INTO `sq_region` VALUES (4190, '省直辖县级行政区划', '2', '41', '4190', '41', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (4201, '武汉市', '2', '42', '4201', '42', 0, 154, '', '');
INSERT INTO `sq_region` VALUES (4202, '黄石市', '2', '42', '4202', '42', 0, 149, '', '');
INSERT INTO `sq_region` VALUES (4203, '十堰市', '2', '42', '4203', '42', 0, 152, '', '');
INSERT INTO `sq_region` VALUES (4205, '宜昌市', '2', '42', '4205', '42', 0, 158, '', '');
INSERT INTO `sq_region` VALUES (4206, '襄阳市', '2', '42', '4206', '42', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (4207, '鄂州市', '2', '42', '4207', '42', 0, 146, '', '');
INSERT INTO `sq_region` VALUES (4208, '荆门市', '2', '42', '4208', '42', 0, 150, '', '');
INSERT INTO `sq_region` VALUES (4209, '孝感市', '2', '42', '4209', '42', 0, 157, '', '');
INSERT INTO `sq_region` VALUES (4210, '荆州市', '2', '42', '4210', '42', 0, 151, '', '');
INSERT INTO `sq_region` VALUES (4211, '黄冈市', '2', '42', '4211', '42', 0, 148, '', '');
INSERT INTO `sq_region` VALUES (4212, '咸宁市', '2', '42', '4212', '42', 0, 155, '', '');
INSERT INTO `sq_region` VALUES (4213, '随州市', '2', '42', '4213', '42', 0, 153, '', '');
INSERT INTO `sq_region` VALUES (4228, '恩施土家族苗族自治州', '2', '42', '4228', '42', 0, 147, '', '');
INSERT INTO `sq_region` VALUES (4290, '省直辖县级行政区划', '2', '42', '4290', '42', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (4301, '长沙市', '2', '43', '4301', '43', 0, 160, '', '');
INSERT INTO `sq_region` VALUES (4302, '株洲市', '2', '43', '4302', '43', 0, 172, '', '');
INSERT INTO `sq_region` VALUES (4303, '湘潭市', '2', '43', '4303', '43', 0, 166, '', '');
INSERT INTO `sq_region` VALUES (4304, '衡阳市', '2', '43', '4304', '43', 0, 162, '', '');
INSERT INTO `sq_region` VALUES (4305, '邵阳市', '2', '43', '4305', '43', 0, 165, '', '');
INSERT INTO `sq_region` VALUES (4306, '岳阳市', '2', '43', '4306', '43', 0, 170, '', '');
INSERT INTO `sq_region` VALUES (4307, '常德市', '2', '43', '4307', '43', 0, 159, '', '');
INSERT INTO `sq_region` VALUES (4308, '张家界市', '2', '43', '4308', '43', 0, 171, '', '');
INSERT INTO `sq_region` VALUES (4309, '益阳市', '2', '43', '4309', '43', 0, 168, '', '');
INSERT INTO `sq_region` VALUES (4310, '郴州市', '2', '43', '4310', '43', 0, 161, '', '');
INSERT INTO `sq_region` VALUES (4311, '永州市', '2', '43', '4311', '43', 0, 169, '', '');
INSERT INTO `sq_region` VALUES (4312, '怀化市', '2', '43', '4312', '43', 0, 163, '', '');
INSERT INTO `sq_region` VALUES (4313, '娄底市', '2', '43', '4313', '43', 0, 164, '', '');
INSERT INTO `sq_region` VALUES (4331, '湘西土家族苗族自治州', '2', '43', '4331', '43', 0, 167, '', '');
INSERT INTO `sq_region` VALUES (4401, '广州市', '2', '44', '4401', '44', 0, 62, '', '');
INSERT INTO `sq_region` VALUES (4402, '韶关市', '2', '44', '4402', '44', 0, 72, '', '');
INSERT INTO `sq_region` VALUES (4403, '深圳市', '2', '44', '4403', '44', 0, 73, '', '');
INSERT INTO `sq_region` VALUES (4404, '珠海市', '2', '44', '4404', '44', 0, 79, '', '');
INSERT INTO `sq_region` VALUES (4405, '汕头市', '2', '44', '4405', '44', 0, 70, '', '');
INSERT INTO `sq_region` VALUES (4406, '佛山市', '2', '44', '4406', '44', 0, 61, '', '');
INSERT INTO `sq_region` VALUES (4407, '江门市', '2', '44', '4407', '44', 0, 65, '', '');
INSERT INTO `sq_region` VALUES (4408, '湛江市', '2', '44', '4408', '44', 0, 76, '', '');
INSERT INTO `sq_region` VALUES (4409, '茂名市', '2', '44', '4409', '44', 0, 67, '', '');
INSERT INTO `sq_region` VALUES (4412, '肇庆市', '2', '44', '4412', '44', 0, 77, '', '');
INSERT INTO `sq_region` VALUES (4413, '惠州市', '2', '44', '4413', '44', 0, 64, '', '');
INSERT INTO `sq_region` VALUES (4414, '梅州市', '2', '44', '4414', '44', 0, 68, '', '');
INSERT INTO `sq_region` VALUES (4415, '汕尾市', '2', '44', '4415', '44', 0, 71, '', '');
INSERT INTO `sq_region` VALUES (4416, '河源市', '2', '44', '4416', '44', 0, 63, '', '');
INSERT INTO `sq_region` VALUES (4417, '阳江市', '2', '44', '4417', '44', 0, 74, '', '');
INSERT INTO `sq_region` VALUES (4418, '清远市', '2', '44', '4418', '44', 0, 69, '', '');
INSERT INTO `sq_region` VALUES (4419, '东莞市', '2', '44', '4419', '44', 0, 60, '', '');
INSERT INTO `sq_region` VALUES (4420, '中山市', '2', '44', '4420', '44', 0, 78, '', '');
INSERT INTO `sq_region` VALUES (4451, '潮州市', '2', '44', '4451', '44', 0, 59, '', '');
INSERT INTO `sq_region` VALUES (4452, '揭阳市', '2', '44', '4452', '44', 0, 66, '', '');
INSERT INTO `sq_region` VALUES (4453, '云浮市', '2', '44', '4453', '44', 0, 75, '', '');
INSERT INTO `sq_region` VALUES (4501, '南宁市', '2', '45', '4501', '45', 0, 90, '', '');
INSERT INTO `sq_region` VALUES (4502, '柳州市', '2', '45', '4502', '45', 0, 89, '', '');
INSERT INTO `sq_region` VALUES (4503, '桂林市', '2', '45', '4503', '45', 0, 84, '', '');
INSERT INTO `sq_region` VALUES (4504, '梧州市', '2', '45', '4504', '45', 0, 92, '', '');
INSERT INTO `sq_region` VALUES (4505, '北海市', '2', '45', '4505', '45', 0, 81, '', '');
INSERT INTO `sq_region` VALUES (4506, '防城港市', '2', '45', '4506', '45', 0, 83, '', '');
INSERT INTO `sq_region` VALUES (4507, '钦州市', '2', '45', '4507', '45', 0, 91, '', '');
INSERT INTO `sq_region` VALUES (4508, '贵港市', '2', '45', '4508', '45', 0, 85, '', '');
INSERT INTO `sq_region` VALUES (4509, '玉林市', '2', '45', '4509', '45', 0, 93, '', '');
INSERT INTO `sq_region` VALUES (4510, '百色市', '2', '45', '4510', '45', 0, 80, '', '');
INSERT INTO `sq_region` VALUES (4511, '贺州市', '2', '45', '4511', '45', 0, 87, '', '');
INSERT INTO `sq_region` VALUES (4512, '河池市', '2', '45', '4512', '45', 0, 86, '', '');
INSERT INTO `sq_region` VALUES (4513, '来宾市', '2', '45', '4513', '45', 0, 88, '', '');
INSERT INTO `sq_region` VALUES (4514, '崇左市', '2', '45', '4514', '45', 0, 82, '', '');
INSERT INTO `sq_region` VALUES (4601, '海口市', '2', '46', '4601', '46', 0, 103, '', '');
INSERT INTO `sq_region` VALUES (4602, '三亚市', '2', '46', '4602', '46', 0, 104, '', '');
INSERT INTO `sq_region` VALUES (4603, '三沙市', '2', '46', '4603', '46', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (4604, '儋州市', '2', '46', '4604', '46', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (4690, '省直辖县级行政区划', '2', '46', '4690', '46', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (5001, '重庆市', '2', '50', '5001', '50', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (5002, '县', '2', '50', '5002', '50', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (5101, '成都市', '2', '51', '5101', '51', 0, 304, '', '');
INSERT INTO `sq_region` VALUES (5103, '自贡市', '2', '51', '5103', '51', 0, 321, '', '');
INSERT INTO `sq_region` VALUES (5104, '攀枝花市', '2', '51', '5104', '51', 0, 316, '', '');
INSERT INTO `sq_region` VALUES (5105, '泸州市', '2', '51', '5105', '51', 0, 322, '', '');
INSERT INTO `sq_region` VALUES (5106, '德阳市', '2', '51', '5106', '51', 0, 306, '', '');
INSERT INTO `sq_region` VALUES (5107, '绵阳市', '2', '51', '5107', '51', 0, 313, '', '');
INSERT INTO `sq_region` VALUES (5108, '广元市', '2', '51', '5108', '51', 0, 309, '', '');
INSERT INTO `sq_region` VALUES (5109, '遂宁市', '2', '51', '5109', '51', 0, 317, '', '');
INSERT INTO `sq_region` VALUES (5110, '内江市', '2', '51', '5110', '51', 0, 315, '', '');
INSERT INTO `sq_region` VALUES (5111, '乐山市', '2', '51', '5111', '51', 0, 310, '', '');
INSERT INTO `sq_region` VALUES (5113, '南充市', '2', '51', '5113', '51', 0, 314, '', '');
INSERT INTO `sq_region` VALUES (5114, '眉山市', '2', '51', '5114', '51', 0, 312, '', '');
INSERT INTO `sq_region` VALUES (5115, '宜宾市', '2', '51', '5115', '51', 0, 319, '', '');
INSERT INTO `sq_region` VALUES (5116, '广安市', '2', '51', '5116', '51', 0, 308, '', '');
INSERT INTO `sq_region` VALUES (5117, '达州市', '2', '51', '5117', '51', 0, 305, '', '');
INSERT INTO `sq_region` VALUES (5118, '雅安市', '2', '51', '5118', '51', 0, 318, '', '');
INSERT INTO `sq_region` VALUES (5119, '巴中市', '2', '51', '5119', '51', 0, 303, '', '');
INSERT INTO `sq_region` VALUES (5120, '资阳市', '2', '51', '5120', '51', 0, 320, '', '');
INSERT INTO `sq_region` VALUES (5132, '阿坝藏族羌族自治州', '2', '51', '5132', '51', 0, 302, '', '');
INSERT INTO `sq_region` VALUES (5133, '甘孜藏族自治州', '2', '51', '5133', '51', 0, 307, '', '');
INSERT INTO `sq_region` VALUES (5134, '凉山彝族自治州', '2', '51', '5134', '51', 0, 311, '', '');
INSERT INTO `sq_region` VALUES (5201, '贵阳市', '2', '52', '5201', '52', 0, 96, '', '');
INSERT INTO `sq_region` VALUES (5202, '六盘水市', '2', '52', '5202', '52', 0, 97, '', '');
INSERT INTO `sq_region` VALUES (5203, '遵义市', '2', '52', '5203', '52', 0, 102, '', '');
INSERT INTO `sq_region` VALUES (5204, '安顺市', '2', '52', '5204', '52', 0, 94, '', '');
INSERT INTO `sq_region` VALUES (5205, '毕节市', '2', '52', '5205', '52', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (5206, '铜仁市', '2', '52', '5206', '52', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (5223, '黔西南布依族苗族自治州', '2', '52', '5223', '52', 0, 100, '', '');
INSERT INTO `sq_region` VALUES (5226, '黔东南苗族侗族自治州', '2', '52', '5226', '52', 0, 98, '', '');
INSERT INTO `sq_region` VALUES (5227, '黔南布依族苗族自治州', '2', '52', '5227', '52', 0, 99, '', '');
INSERT INTO `sq_region` VALUES (5301, '昆明市', '2', '53', '5301', '53', 0, 369, '', '');
INSERT INTO `sq_region` VALUES (5303, '曲靖市', '2', '53', '5303', '53', 0, 373, '', '');
INSERT INTO `sq_region` VALUES (5304, '玉溪市', '2', '53', '5304', '53', 0, 377, '', '');
INSERT INTO `sq_region` VALUES (5305, '保山市', '2', '53', '5305', '53', 0, 363, '', '');
INSERT INTO `sq_region` VALUES (5306, '昭通市', '2', '53', '5306', '53', 0, 378, '', '');
INSERT INTO `sq_region` VALUES (5307, '丽江市', '2', '53', '5307', '53', 0, 370, '', '');
INSERT INTO `sq_region` VALUES (5308, '普洱市', '2', '53', '5308', '53', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (5309, '临沧市', '2', '53', '5309', '53', 0, 371, '', '');
INSERT INTO `sq_region` VALUES (5323, '楚雄彝族自治州', '2', '53', '5323', '53', 0, 364, '', '');
INSERT INTO `sq_region` VALUES (5325, '红河哈尼族彝族自治州', '2', '53', '5325', '53', 0, 368, '', '');
INSERT INTO `sq_region` VALUES (5326, '文山壮族苗族自治州', '2', '53', '5326', '53', 0, 375, '', '');
INSERT INTO `sq_region` VALUES (5328, '西双版纳傣族自治州', '2', '53', '5328', '53', 0, 376, '', '');
INSERT INTO `sq_region` VALUES (5329, '大理白族自治州', '2', '53', '5329', '53', 0, 365, '', '');
INSERT INTO `sq_region` VALUES (5331, '德宏傣族景颇族自治州', '2', '53', '5331', '53', 0, 366, '', '');
INSERT INTO `sq_region` VALUES (5333, '怒江傈僳族自治州', '2', '53', '5333', '53', 0, 372, '', '');
INSERT INTO `sq_region` VALUES (5334, '迪庆藏族自治州', '2', '53', '5334', '53', 0, 367, '', '');
INSERT INTO `sq_region` VALUES (5401, '拉萨市', '2', '54', '5401', '54', 0, 343, '', '');
INSERT INTO `sq_region` VALUES (5402, '日喀则市', '2', '54', '5402', '54', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (5403, '昌都市', '2', '54', '5403', '54', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (5404, '林芝市', '2', '54', '5404', '54', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (5405, '山南市', '2', '54', '5405', '54', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (5424, '那曲地区', '2', '54', '5424', '54', 0, 345, '', '');
INSERT INTO `sq_region` VALUES (5425, '阿里地区', '2', '54', '5425', '54', 0, 341, '', '');
INSERT INTO `sq_region` VALUES (6101, '西安市', '2', '61', '6101', '61', 0, 279, '', '');
INSERT INTO `sq_region` VALUES (6102, '铜川市', '2', '61', '6102', '61', 0, 277, '', '');
INSERT INTO `sq_region` VALUES (6103, '宝鸡市', '2', '61', '6103', '61', 0, 274, '', '');
INSERT INTO `sq_region` VALUES (6104, '咸阳市', '2', '61', '6104', '61', 0, 280, '', '');
INSERT INTO `sq_region` VALUES (6105, '渭南市', '2', '61', '6105', '61', 0, 278, '', '');
INSERT INTO `sq_region` VALUES (6106, '延安市', '2', '61', '6106', '61', 0, 281, '', '');
INSERT INTO `sq_region` VALUES (6107, '汉中市', '2', '61', '6107', '61', 0, 275, '', '');
INSERT INTO `sq_region` VALUES (6108, '榆林市', '2', '61', '6108', '61', 0, 282, '', '');
INSERT INTO `sq_region` VALUES (6109, '安康市', '2', '61', '6109', '61', 0, 273, '', '');
INSERT INTO `sq_region` VALUES (6110, '商洛市', '2', '61', '6110', '61', 0, 276, '', '');
INSERT INTO `sq_region` VALUES (6201, '兰州市', '2', '62', '6201', '62', 0, 51, '', '');
INSERT INTO `sq_region` VALUES (6202, '嘉峪关市', '2', '62', '6202', '62', 0, 48, '', '');
INSERT INTO `sq_region` VALUES (6203, '金昌市', '2', '62', '6203', '62', 0, 49, '', '');
INSERT INTO `sq_region` VALUES (6204, '白银市', '2', '62', '6204', '62', 0, 45, '', '');
INSERT INTO `sq_region` VALUES (6205, '天水市', '2', '62', '6205', '62', 0, 56, '', '');
INSERT INTO `sq_region` VALUES (6206, '武威市', '2', '62', '6206', '62', 0, 57, '', '');
INSERT INTO `sq_region` VALUES (6207, '张掖市', '2', '62', '6207', '62', 0, 58, '', '');
INSERT INTO `sq_region` VALUES (6208, '平凉市', '2', '62', '6208', '62', 0, 54, '', '');
INSERT INTO `sq_region` VALUES (6209, '酒泉市', '2', '62', '6209', '62', 0, 50, '', '');
INSERT INTO `sq_region` VALUES (6210, '庆阳市', '2', '62', '6210', '62', 0, 55, '', '');
INSERT INTO `sq_region` VALUES (6211, '定西市', '2', '62', '6211', '62', 0, 46, '', '');
INSERT INTO `sq_region` VALUES (6212, '陇南市', '2', '62', '6212', '62', 0, 53, '', '');
INSERT INTO `sq_region` VALUES (6229, '临夏回族自治州', '2', '62', '6229', '62', 0, 52, '', '');
INSERT INTO `sq_region` VALUES (6230, '甘南藏族自治州', '2', '62', '6230', '62', 0, 47, '', '');
INSERT INTO `sq_region` VALUES (6301, '西宁市', '2', '63', '6301', '63', 0, 243, '', '');
INSERT INTO `sq_region` VALUES (6302, '海东市', '2', '63', '6302', '63', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (6322, '海北藏族自治州', '2', '63', '6322', '63', 0, 238, '', '');
INSERT INTO `sq_region` VALUES (6323, '黄南藏族自治州', '2', '63', '6323', '63', 0, 242, '', '');
INSERT INTO `sq_region` VALUES (6325, '海南藏族自治州', '2', '63', '6325', '63', 0, 240, '', '');
INSERT INTO `sq_region` VALUES (6326, '果洛藏族自治州', '2', '63', '6326', '63', 0, 237, '', '');
INSERT INTO `sq_region` VALUES (6327, '玉树藏族自治州', '2', '63', '6327', '63', 0, 244, '', '');
INSERT INTO `sq_region` VALUES (6328, '海西蒙古族藏族自治州', '2', '63', '6328', '63', 0, 241, '', '');
INSERT INTO `sq_region` VALUES (6401, '银川市', '2', '64', '6401', '64', 0, 235, '', '');
INSERT INTO `sq_region` VALUES (6402, '石嘴山市', '2', '64', '6402', '64', 0, 233, '', '');
INSERT INTO `sq_region` VALUES (6403, '吴忠市', '2', '64', '6403', '64', 0, 234, '', '');
INSERT INTO `sq_region` VALUES (6404, '固原市', '2', '64', '6404', '64', 0, 232, '', '');
INSERT INTO `sq_region` VALUES (6405, '中卫市', '2', '64', '6405', '64', 0, 236, '', '');
INSERT INTO `sq_region` VALUES (6501, '乌鲁木齐市', '2', '65', '6501', '65', 0, 361, '', '');
INSERT INTO `sq_region` VALUES (6502, '克拉玛依市', '2', '65', '6502', '65', 0, 356, '', '');
INSERT INTO `sq_region` VALUES (6504, '吐鲁番市', '2', '65', '6504', '65', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (6505, '哈密市', '2', '65', '6505', '65', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (6523, '昌吉回族自治州', '2', '65', '6523', '65', 0, 352, '', '');
INSERT INTO `sq_region` VALUES (6527, '博尔塔拉蒙古自治州', '2', '65', '6527', '65', 0, 351, '', '');
INSERT INTO `sq_region` VALUES (6528, '巴音郭楞蒙古自治州', '2', '65', '6528', '65', 0, 350, '', '');
INSERT INTO `sq_region` VALUES (6529, '阿克苏地区', '2', '65', '6529', '65', 0, 348, '', '');
INSERT INTO `sq_region` VALUES (6530, '克孜勒苏柯尔克孜自治州', '2', '65', '6530', '65', 0, 357, '', '');
INSERT INTO `sq_region` VALUES (6531, '喀什地区', '2', '65', '6531', '65', 0, 355, '', '');
INSERT INTO `sq_region` VALUES (6532, '和田地区', '2', '65', '6532', '65', 0, 354, '', '');
INSERT INTO `sq_region` VALUES (6540, '伊犁哈萨克自治州', '2', '65', '6540', '65', 0, 362, '', '');
INSERT INTO `sq_region` VALUES (6542, '塔城地区', '2', '65', '6542', '65', 0, 359, '', '');
INSERT INTO `sq_region` VALUES (6543, '阿勒泰地区', '2', '65', '6543', '65', 0, 349, '', '');
INSERT INTO `sq_region` VALUES (6590, '自治区直辖县级行政区划', '2', '65', '6590', '65', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (110101, '东城区', '3', '11', '1101', '1101', 0, 562, '', '');
INSERT INTO `sq_region` VALUES (110102, '西城区', '3', '11', '1101', '1101', 0, 551, '', '');
INSERT INTO `sq_region` VALUES (110105, '朝阳区', '3', '11', '1101', '1101', 0, 565, '', '');
INSERT INTO `sq_region` VALUES (110106, '丰台区', '3', '11', '1101', '1101', 0, 560, '', '');
INSERT INTO `sq_region` VALUES (110107, '石景山区', '3', '11', '1101', '1101', 0, 554, '', '');
INSERT INTO `sq_region` VALUES (110108, '海淀区', '3', '11', '1101', '1101', 0, 559, '', '');
INSERT INTO `sq_region` VALUES (110109, '门头沟区', '3', '11', '1101', '1101', 0, 557, '', '');
INSERT INTO `sq_region` VALUES (110111, '房山区', '3', '11', '1101', '1101', 0, 561, '', '');
INSERT INTO `sq_region` VALUES (110112, '通州区', '3', '11', '1101', '1101', 0, 552, '', '');
INSERT INTO `sq_region` VALUES (110113, '顺义区', '3', '11', '1101', '1101', 0, 553, '', '');
INSERT INTO `sq_region` VALUES (110114, '昌平区', '3', '11', '1101', '1101', 0, 566, '', '');
INSERT INTO `sq_region` VALUES (110115, '大兴区', '3', '11', '1101', '1101', 0, 563, '', '');
INSERT INTO `sq_region` VALUES (110116, '怀柔区', '3', '11', '1101', '1101', 0, 558, '', '');
INSERT INTO `sq_region` VALUES (110117, '平谷区', '3', '11', '1101', '1101', 0, 555, '', '');
INSERT INTO `sq_region` VALUES (110118, '密云区', '3', '11', '1101', '1101', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (110119, '延庆区', '3', '11', '1101', '1101', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (120101, '和平区', '3', '12', '1201', '1201', 0, 543, '', '');
INSERT INTO `sq_region` VALUES (120102, '河东区', '3', '12', '1201', '1201', 0, 541, '', '');
INSERT INTO `sq_region` VALUES (120103, '河西区', '3', '12', '1201', '1201', 0, 540, '', '');
INSERT INTO `sq_region` VALUES (120104, '南开区', '3', '12', '1201', '1201', 0, 534, '', '');
INSERT INTO `sq_region` VALUES (120105, '河北区', '3', '12', '1201', '1201', 0, 542, '', '');
INSERT INTO `sq_region` VALUES (120106, '红桥区', '3', '12', '1201', '1201', 0, 539, '', '');
INSERT INTO `sq_region` VALUES (120110, '东丽区', '3', '12', '1201', '1201', 0, 545, '', '');
INSERT INTO `sq_region` VALUES (120111, '西青区', '3', '12', '1201', '1201', 0, 531, '', '');
INSERT INTO `sq_region` VALUES (120112, '津南区', '3', '12', '1201', '1201', 0, 537, '', '');
INSERT INTO `sq_region` VALUES (120113, '北辰区', '3', '12', '1201', '1201', 0, 547, '', '');
INSERT INTO `sq_region` VALUES (120114, '武清区', '3', '12', '1201', '1201', 0, 532, '', '');
INSERT INTO `sq_region` VALUES (120115, '宝坻区', '3', '12', '1201', '1201', 0, 548, '', '');
INSERT INTO `sq_region` VALUES (120116, '滨海新区', '3', '12', '1201', '1201', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (120117, '宁河区', '3', '12', '1201', '1201', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (120118, '静海区', '3', '12', '1201', '1201', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (120119, '蓟州区', '3', '12', '1201', '1201', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130101, '市辖区', '3', '13', '1301', '1301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130102, '长安区', '3', '13', '1301', '1301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130104, '桥西区', '3', '13', '1301', '1301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130105, '新华区', '3', '13', '1301', '1301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130107, '井陉矿区', '3', '13', '1301', '1301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130108, '裕华区', '3', '13', '1301', '1301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130109, '藁城区', '3', '13', '1301', '1301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130110, '鹿泉区', '3', '13', '1301', '1301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130111, '栾城区', '3', '13', '1301', '1301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130121, '井陉县', '3', '13', '1301', '1301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130123, '正定县', '3', '13', '1301', '1301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130125, '行唐县', '3', '13', '1301', '1301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130126, '灵寿县', '3', '13', '1301', '1301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130127, '高邑县', '3', '13', '1301', '1301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130128, '深泽县', '3', '13', '1301', '1301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130129, '赞皇县', '3', '13', '1301', '1301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130130, '无极县', '3', '13', '1301', '1301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130131, '平山县', '3', '13', '1301', '1301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130132, '元氏县', '3', '13', '1301', '1301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130133, '赵县', '3', '13', '1301', '1301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130183, '晋州市', '3', '13', '1301', '1301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130184, '新乐市', '3', '13', '1301', '1301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130201, '市辖区', '3', '13', '1302', '1302', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130202, '路南区', '3', '13', '1302', '1302', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130203, '路北区', '3', '13', '1302', '1302', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130204, '古冶区', '3', '13', '1302', '1302', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130205, '开平区', '3', '13', '1302', '1302', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130207, '丰南区', '3', '13', '1302', '1302', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130208, '丰润区', '3', '13', '1302', '1302', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130209, '曹妃甸区', '3', '13', '1302', '1302', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130223, '滦县', '3', '13', '1302', '1302', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130224, '滦南县', '3', '13', '1302', '1302', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130225, '乐亭县', '3', '13', '1302', '1302', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130227, '迁西县', '3', '13', '1302', '1302', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130229, '玉田县', '3', '13', '1302', '1302', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130281, '遵化市', '3', '13', '1302', '1302', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130283, '迁安市', '3', '13', '1302', '1302', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130301, '市辖区', '3', '13', '1303', '1303', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130302, '海港区', '3', '13', '1303', '1303', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130303, '山海关区', '3', '13', '1303', '1303', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130304, '北戴河区', '3', '13', '1303', '1303', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130306, '抚宁区', '3', '13', '1303', '1303', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130321, '青龙满族自治县', '3', '13', '1303', '1303', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130322, '昌黎县', '3', '13', '1303', '1303', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130324, '卢龙县', '3', '13', '1303', '1303', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130401, '市辖区', '3', '13', '1304', '1304', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130402, '邯山区', '3', '13', '1304', '1304', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130403, '丛台区', '3', '13', '1304', '1304', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130404, '复兴区', '3', '13', '1304', '1304', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130406, '峰峰矿区', '3', '13', '1304', '1304', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130421, '邯郸县', '3', '13', '1304', '1304', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130423, '临漳县', '3', '13', '1304', '1304', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130424, '成安县', '3', '13', '1304', '1304', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130425, '大名县', '3', '13', '1304', '1304', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130426, '涉县', '3', '13', '1304', '1304', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130427, '磁县', '3', '13', '1304', '1304', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130428, '肥乡县', '3', '13', '1304', '1304', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130429, '永年县', '3', '13', '1304', '1304', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130430, '邱县', '3', '13', '1304', '1304', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130431, '鸡泽县', '3', '13', '1304', '1304', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130432, '广平县', '3', '13', '1304', '1304', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130433, '馆陶县', '3', '13', '1304', '1304', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130434, '魏县', '3', '13', '1304', '1304', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130435, '曲周县', '3', '13', '1304', '1304', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130481, '武安市', '3', '13', '1304', '1304', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130501, '市辖区', '3', '13', '1305', '1305', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130502, '桥东区', '3', '13', '1305', '1305', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130503, '桥西区', '3', '13', '1305', '1305', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130521, '邢台县', '3', '13', '1305', '1305', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130522, '临城县', '3', '13', '1305', '1305', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130523, '内丘县', '3', '13', '1305', '1305', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130524, '柏乡县', '3', '13', '1305', '1305', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130525, '隆尧县', '3', '13', '1305', '1305', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130526, '任县', '3', '13', '1305', '1305', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130527, '南和县', '3', '13', '1305', '1305', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130528, '宁晋县', '3', '13', '1305', '1305', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130529, '巨鹿县', '3', '13', '1305', '1305', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130530, '新河县', '3', '13', '1305', '1305', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130531, '广宗县', '3', '13', '1305', '1305', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130532, '平乡县', '3', '13', '1305', '1305', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130533, '威县', '3', '13', '1305', '1305', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130534, '清河县', '3', '13', '1305', '1305', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130535, '临西县', '3', '13', '1305', '1305', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130581, '南宫市', '3', '13', '1305', '1305', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130582, '沙河市', '3', '13', '1305', '1305', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130601, '市辖区', '3', '13', '1306', '1306', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130602, '竞秀区', '3', '13', '1306', '1306', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130606, '莲池区', '3', '13', '1306', '1306', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130607, '满城区', '3', '13', '1306', '1306', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130608, '清苑区', '3', '13', '1306', '1306', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130609, '徐水区', '3', '13', '1306', '1306', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130623, '涞水县', '3', '13', '1306', '1306', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130624, '阜平县', '3', '13', '1306', '1306', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130626, '定兴县', '3', '13', '1306', '1306', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130627, '唐县', '3', '13', '1306', '1306', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130628, '高阳县', '3', '13', '1306', '1306', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130629, '容城县', '3', '13', '1306', '1306', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130630, '涞源县', '3', '13', '1306', '1306', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130631, '望都县', '3', '13', '1306', '1306', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130632, '安新县', '3', '13', '1306', '1306', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130633, '易县', '3', '13', '1306', '1306', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130634, '曲阳县', '3', '13', '1306', '1306', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130635, '蠡县', '3', '13', '1306', '1306', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130636, '顺平县', '3', '13', '1306', '1306', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130637, '博野县', '3', '13', '1306', '1306', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130638, '雄县', '3', '13', '1306', '1306', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130681, '涿州市', '3', '13', '1306', '1306', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130683, '安国市', '3', '13', '1306', '1306', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130684, '高碑店市', '3', '13', '1306', '1306', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130701, '市辖区', '3', '13', '1307', '1307', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130702, '桥东区', '3', '13', '1307', '1307', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130703, '桥西区', '3', '13', '1307', '1307', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130705, '宣化区', '3', '13', '1307', '1307', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130706, '下花园区', '3', '13', '1307', '1307', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130708, '万全区', '3', '13', '1307', '1307', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130709, '崇礼区', '3', '13', '1307', '1307', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130722, '张北县', '3', '13', '1307', '1307', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130723, '康保县', '3', '13', '1307', '1307', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130724, '沽源县', '3', '13', '1307', '1307', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130725, '尚义县', '3', '13', '1307', '1307', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130726, '蔚县', '3', '13', '1307', '1307', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130727, '阳原县', '3', '13', '1307', '1307', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130728, '怀安县', '3', '13', '1307', '1307', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130730, '怀来县', '3', '13', '1307', '1307', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130731, '涿鹿县', '3', '13', '1307', '1307', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130732, '赤城县', '3', '13', '1307', '1307', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130801, '市辖区', '3', '13', '1308', '1308', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130802, '双桥区', '3', '13', '1308', '1308', 0, 492, '', '');
INSERT INTO `sq_region` VALUES (130803, '双滦区', '3', '13', '1308', '1308', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130804, '鹰手营子矿区', '3', '13', '1308', '1308', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130821, '承德县', '3', '13', '1308', '1308', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130822, '兴隆县', '3', '13', '1308', '1308', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130823, '平泉县', '3', '13', '1308', '1308', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130824, '滦平县', '3', '13', '1308', '1308', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130825, '隆化县', '3', '13', '1308', '1308', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130826, '丰宁满族自治县', '3', '13', '1308', '1308', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130827, '宽城满族自治县', '3', '13', '1308', '1308', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130828, '围场满族蒙古族自治县', '3', '13', '1308', '1308', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130901, '市辖区', '3', '13', '1309', '1309', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130902, '新华区', '3', '13', '1309', '1309', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130903, '运河区', '3', '13', '1309', '1309', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130921, '沧县', '3', '13', '1309', '1309', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130922, '青县', '3', '13', '1309', '1309', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130923, '东光县', '3', '13', '1309', '1309', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130924, '海兴县', '3', '13', '1309', '1309', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130925, '盐山县', '3', '13', '1309', '1309', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130926, '肃宁县', '3', '13', '1309', '1309', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130927, '南皮县', '3', '13', '1309', '1309', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130928, '吴桥县', '3', '13', '1309', '1309', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130929, '献县', '3', '13', '1309', '1309', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130930, '孟村回族自治县', '3', '13', '1309', '1309', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130981, '泊头市', '3', '13', '1309', '1309', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130982, '任丘市', '3', '13', '1309', '1309', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130983, '黄骅市', '3', '13', '1309', '1309', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (130984, '河间市', '3', '13', '1309', '1309', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (131001, '市辖区', '3', '13', '1310', '1310', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (131002, '安次区', '3', '13', '1310', '1310', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (131003, '广阳区', '3', '13', '1310', '1310', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (131022, '固安县', '3', '13', '1310', '1310', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (131023, '永清县', '3', '13', '1310', '1310', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (131024, '香河县', '3', '13', '1310', '1310', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (131025, '大城县', '3', '13', '1310', '1310', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (131026, '文安县', '3', '13', '1310', '1310', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (131028, '大厂回族自治县', '3', '13', '1310', '1310', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (131081, '霸州市', '3', '13', '1310', '1310', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (131082, '三河市', '3', '13', '1310', '1310', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (131101, '市辖区', '3', '13', '1311', '1311', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (131102, '桃城区', '3', '13', '1311', '1311', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (131103, '冀州区', '3', '13', '1311', '1311', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (131121, '枣强县', '3', '13', '1311', '1311', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (131122, '武邑县', '3', '13', '1311', '1311', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (131123, '武强县', '3', '13', '1311', '1311', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (131124, '饶阳县', '3', '13', '1311', '1311', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (131125, '安平县', '3', '13', '1311', '1311', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (131126, '故城县', '3', '13', '1311', '1311', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (131127, '景县', '3', '13', '1311', '1311', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (131128, '阜城县', '3', '13', '1311', '1311', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (131182, '深州市', '3', '13', '1311', '1311', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (139001, '定州市', '3', '13', '1390', '1390', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (139002, '辛集市', '3', '13', '1390', '1390', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140101, '市辖区', '3', '14', '1401', '1401', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140105, '小店区', '3', '14', '1401', '1401', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140106, '迎泽区', '3', '14', '1401', '1401', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140107, '杏花岭区', '3', '14', '1401', '1401', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140108, '尖草坪区', '3', '14', '1401', '1401', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140109, '万柏林区', '3', '14', '1401', '1401', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140110, '晋源区', '3', '14', '1401', '1401', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140121, '清徐县', '3', '14', '1401', '1401', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140122, '阳曲县', '3', '14', '1401', '1401', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140123, '娄烦县', '3', '14', '1401', '1401', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140181, '古交市', '3', '14', '1401', '1401', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140201, '市辖区', '3', '14', '1402', '1402', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140202, '城区', '3', '14', '1402', '1402', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140203, '矿区', '3', '14', '1402', '1402', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140211, '南郊区', '3', '14', '1402', '1402', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140212, '新荣区', '3', '14', '1402', '1402', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140221, '阳高县', '3', '14', '1402', '1402', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140222, '天镇县', '3', '14', '1402', '1402', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140223, '广灵县', '3', '14', '1402', '1402', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140224, '灵丘县', '3', '14', '1402', '1402', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140225, '浑源县', '3', '14', '1402', '1402', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140226, '左云县', '3', '14', '1402', '1402', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140227, '大同县', '3', '14', '1402', '1402', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140301, '市辖区', '3', '14', '1403', '1403', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140302, '城区', '3', '14', '1403', '1403', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140303, '矿区', '3', '14', '1403', '1403', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140311, '郊区', '3', '14', '1403', '1403', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140321, '平定县', '3', '14', '1403', '1403', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140322, '盂县', '3', '14', '1403', '1403', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140401, '市辖区', '3', '14', '1404', '1404', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140402, '城区', '3', '14', '1404', '1404', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140411, '郊区', '3', '14', '1404', '1404', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140421, '长治县', '3', '14', '1404', '1404', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140423, '襄垣县', '3', '14', '1404', '1404', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140424, '屯留县', '3', '14', '1404', '1404', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140425, '平顺县', '3', '14', '1404', '1404', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140426, '黎城县', '3', '14', '1404', '1404', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140427, '壶关县', '3', '14', '1404', '1404', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140428, '长子县', '3', '14', '1404', '1404', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140429, '武乡县', '3', '14', '1404', '1404', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140430, '沁县', '3', '14', '1404', '1404', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140431, '沁源县', '3', '14', '1404', '1404', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140481, '潞城市', '3', '14', '1404', '1404', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140501, '市辖区', '3', '14', '1405', '1405', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140502, '城区', '3', '14', '1405', '1405', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140521, '沁水县', '3', '14', '1405', '1405', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140522, '阳城县', '3', '14', '1405', '1405', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140524, '陵川县', '3', '14', '1405', '1405', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140525, '泽州县', '3', '14', '1405', '1405', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140581, '高平市', '3', '14', '1405', '1405', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140601, '市辖区', '3', '14', '1406', '1406', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140602, '朔城区', '3', '14', '1406', '1406', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140603, '平鲁区', '3', '14', '1406', '1406', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140621, '山阴县', '3', '14', '1406', '1406', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140622, '应县', '3', '14', '1406', '1406', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140623, '右玉县', '3', '14', '1406', '1406', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140624, '怀仁县', '3', '14', '1406', '1406', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140701, '市辖区', '3', '14', '1407', '1407', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140702, '榆次区', '3', '14', '1407', '1407', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140721, '榆社县', '3', '14', '1407', '1407', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140722, '左权县', '3', '14', '1407', '1407', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140723, '和顺县', '3', '14', '1407', '1407', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140724, '昔阳县', '3', '14', '1407', '1407', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140725, '寿阳县', '3', '14', '1407', '1407', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140726, '太谷县', '3', '14', '1407', '1407', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140727, '祁县', '3', '14', '1407', '1407', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140728, '平遥县', '3', '14', '1407', '1407', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140729, '灵石县', '3', '14', '1407', '1407', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140781, '介休市', '3', '14', '1407', '1407', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140801, '市辖区', '3', '14', '1408', '1408', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140802, '盐湖区', '3', '14', '1408', '1408', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140821, '临猗县', '3', '14', '1408', '1408', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140822, '万荣县', '3', '14', '1408', '1408', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140823, '闻喜县', '3', '14', '1408', '1408', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140824, '稷山县', '3', '14', '1408', '1408', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140825, '新绛县', '3', '14', '1408', '1408', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140826, '绛县', '3', '14', '1408', '1408', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140827, '垣曲县', '3', '14', '1408', '1408', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140828, '夏县', '3', '14', '1408', '1408', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140829, '平陆县', '3', '14', '1408', '1408', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140830, '芮城县', '3', '14', '1408', '1408', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140881, '永济市', '3', '14', '1408', '1408', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140882, '河津市', '3', '14', '1408', '1408', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140901, '市辖区', '3', '14', '1409', '1409', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140902, '忻府区', '3', '14', '1409', '1409', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140921, '定襄县', '3', '14', '1409', '1409', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140922, '五台县', '3', '14', '1409', '1409', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140923, '代县', '3', '14', '1409', '1409', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140924, '繁峙县', '3', '14', '1409', '1409', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140925, '宁武县', '3', '14', '1409', '1409', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140926, '静乐县', '3', '14', '1409', '1409', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140927, '神池县', '3', '14', '1409', '1409', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140928, '五寨县', '3', '14', '1409', '1409', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140929, '岢岚县', '3', '14', '1409', '1409', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140930, '河曲县', '3', '14', '1409', '1409', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140931, '保德县', '3', '14', '1409', '1409', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140932, '偏关县', '3', '14', '1409', '1409', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (140981, '原平市', '3', '14', '1409', '1409', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (141001, '市辖区', '3', '14', '1410', '1410', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (141002, '尧都区', '3', '14', '1410', '1410', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (141021, '曲沃县', '3', '14', '1410', '1410', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (141022, '翼城县', '3', '14', '1410', '1410', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (141023, '襄汾县', '3', '14', '1410', '1410', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (141024, '洪洞县', '3', '14', '1410', '1410', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (141025, '古县', '3', '14', '1410', '1410', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (141026, '安泽县', '3', '14', '1410', '1410', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (141027, '浮山县', '3', '14', '1410', '1410', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (141028, '吉县', '3', '14', '1410', '1410', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (141029, '乡宁县', '3', '14', '1410', '1410', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (141030, '大宁县', '3', '14', '1410', '1410', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (141031, '隰县', '3', '14', '1410', '1410', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (141032, '永和县', '3', '14', '1410', '1410', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (141033, '蒲县', '3', '14', '1410', '1410', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (141034, '汾西县', '3', '14', '1410', '1410', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (141081, '侯马市', '3', '14', '1410', '1410', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (141082, '霍州市', '3', '14', '1410', '1410', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (141101, '市辖区', '3', '14', '1411', '1411', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (141102, '离石区', '3', '14', '1411', '1411', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (141121, '文水县', '3', '14', '1411', '1411', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (141122, '交城县', '3', '14', '1411', '1411', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (141123, '兴县', '3', '14', '1411', '1411', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (141124, '临县', '3', '14', '1411', '1411', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (141125, '柳林县', '3', '14', '1411', '1411', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (141126, '石楼县', '3', '14', '1411', '1411', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (141127, '岚县', '3', '14', '1411', '1411', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (141128, '方山县', '3', '14', '1411', '1411', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (141129, '中阳县', '3', '14', '1411', '1411', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (141130, '交口县', '3', '14', '1411', '1411', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (141181, '孝义市', '3', '14', '1411', '1411', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (141182, '汾阳市', '3', '14', '1411', '1411', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150101, '市辖区', '3', '15', '1501', '1501', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150102, '新城区', '3', '15', '1501', '1501', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150103, '回民区', '3', '15', '1501', '1501', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150104, '玉泉区', '3', '15', '1501', '1501', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150105, '赛罕区', '3', '15', '1501', '1501', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150121, '土默特左旗', '3', '15', '1501', '1501', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150122, '托克托县', '3', '15', '1501', '1501', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150123, '和林格尔县', '3', '15', '1501', '1501', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150124, '清水河县', '3', '15', '1501', '1501', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150125, '武川县', '3', '15', '1501', '1501', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150201, '市辖区', '3', '15', '1502', '1502', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150202, '东河区', '3', '15', '1502', '1502', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150203, '昆都仑区', '3', '15', '1502', '1502', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150204, '青山区', '3', '15', '1502', '1502', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150205, '石拐区', '3', '15', '1502', '1502', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150206, '白云鄂博矿区', '3', '15', '1502', '1502', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150207, '九原区', '3', '15', '1502', '1502', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150221, '土默特右旗', '3', '15', '1502', '1502', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150222, '固阳县', '3', '15', '1502', '1502', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150223, '达尔罕茂明安联合旗', '3', '15', '1502', '1502', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150301, '市辖区', '3', '15', '1503', '1503', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150302, '海勃湾区', '3', '15', '1503', '1503', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150303, '海南区', '3', '15', '1503', '1503', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150304, '乌达区', '3', '15', '1503', '1503', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150401, '市辖区', '3', '15', '1504', '1504', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150402, '红山区', '3', '15', '1504', '1504', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150403, '元宝山区', '3', '15', '1504', '1504', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150404, '松山区', '3', '15', '1504', '1504', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150421, '阿鲁科尔沁旗', '3', '15', '1504', '1504', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150422, '巴林左旗', '3', '15', '1504', '1504', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150423, '巴林右旗', '3', '15', '1504', '1504', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150424, '林西县', '3', '15', '1504', '1504', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150425, '克什克腾旗', '3', '15', '1504', '1504', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150426, '翁牛特旗', '3', '15', '1504', '1504', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150428, '喀喇沁旗', '3', '15', '1504', '1504', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150429, '宁城县', '3', '15', '1504', '1504', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150430, '敖汉旗', '3', '15', '1504', '1504', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150501, '市辖区', '3', '15', '1505', '1505', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150502, '科尔沁区', '3', '15', '1505', '1505', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150521, '科尔沁左翼中旗', '3', '15', '1505', '1505', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150522, '科尔沁左翼后旗', '3', '15', '1505', '1505', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150523, '开鲁县', '3', '15', '1505', '1505', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150524, '库伦旗', '3', '15', '1505', '1505', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150525, '奈曼旗', '3', '15', '1505', '1505', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150526, '扎鲁特旗', '3', '15', '1505', '1505', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150581, '霍林郭勒市', '3', '15', '1505', '1505', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150601, '市辖区', '3', '15', '1506', '1506', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150602, '东胜区', '3', '15', '1506', '1506', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150603, '康巴什区', '3', '15', '1506', '1506', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150621, '达拉特旗', '3', '15', '1506', '1506', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150622, '准格尔旗', '3', '15', '1506', '1506', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150623, '鄂托克前旗', '3', '15', '1506', '1506', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150624, '鄂托克旗', '3', '15', '1506', '1506', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150625, '杭锦旗', '3', '15', '1506', '1506', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150626, '乌审旗', '3', '15', '1506', '1506', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150627, '伊金霍洛旗', '3', '15', '1506', '1506', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150701, '市辖区', '3', '15', '1507', '1507', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150702, '海拉尔区', '3', '15', '1507', '1507', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150703, '扎赉诺尔区', '3', '15', '1507', '1507', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150721, '阿荣旗', '3', '15', '1507', '1507', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150722, '莫力达瓦达斡尔族自治旗', '3', '15', '1507', '1507', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150723, '鄂伦春自治旗', '3', '15', '1507', '1507', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150724, '鄂温克族自治旗', '3', '15', '1507', '1507', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150725, '陈巴尔虎旗', '3', '15', '1507', '1507', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150726, '新巴尔虎左旗', '3', '15', '1507', '1507', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150727, '新巴尔虎右旗', '3', '15', '1507', '1507', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150781, '满洲里市', '3', '15', '1507', '1507', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150782, '牙克石市', '3', '15', '1507', '1507', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150783, '扎兰屯市', '3', '15', '1507', '1507', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150784, '额尔古纳市', '3', '15', '1507', '1507', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150785, '根河市', '3', '15', '1507', '1507', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150801, '市辖区', '3', '15', '1508', '1508', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150802, '临河区', '3', '15', '1508', '1508', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150821, '五原县', '3', '15', '1508', '1508', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150822, '磴口县', '3', '15', '1508', '1508', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150823, '乌拉特前旗', '3', '15', '1508', '1508', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150824, '乌拉特中旗', '3', '15', '1508', '1508', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150825, '乌拉特后旗', '3', '15', '1508', '1508', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150826, '杭锦后旗', '3', '15', '1508', '1508', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150901, '市辖区', '3', '15', '1509', '1509', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150902, '集宁区', '3', '15', '1509', '1509', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150921, '卓资县', '3', '15', '1509', '1509', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150922, '化德县', '3', '15', '1509', '1509', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150923, '商都县', '3', '15', '1509', '1509', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150924, '兴和县', '3', '15', '1509', '1509', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150925, '凉城县', '3', '15', '1509', '1509', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150926, '察哈尔右翼前旗', '3', '15', '1509', '1509', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150927, '察哈尔右翼中旗', '3', '15', '1509', '1509', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150928, '察哈尔右翼后旗', '3', '15', '1509', '1509', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150929, '四子王旗', '3', '15', '1509', '1509', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (150981, '丰镇市', '3', '15', '1509', '1509', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (152201, '乌兰浩特市', '3', '15', '1522', '1522', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (152202, '阿尔山市', '3', '15', '1522', '1522', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (152221, '科尔沁右翼前旗', '3', '15', '1522', '1522', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (152222, '科尔沁右翼中旗', '3', '15', '1522', '1522', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (152223, '扎赉特旗', '3', '15', '1522', '1522', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (152224, '突泉县', '3', '15', '1522', '1522', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (152501, '二连浩特市', '3', '15', '1525', '1525', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (152502, '锡林浩特市', '3', '15', '1525', '1525', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (152522, '阿巴嘎旗', '3', '15', '1525', '1525', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (152523, '苏尼特左旗', '3', '15', '1525', '1525', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (152524, '苏尼特右旗', '3', '15', '1525', '1525', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (152525, '东乌珠穆沁旗', '3', '15', '1525', '1525', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (152526, '西乌珠穆沁旗', '3', '15', '1525', '1525', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (152527, '太仆寺旗', '3', '15', '1525', '1525', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (152528, '镶黄旗', '3', '15', '1525', '1525', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (152529, '正镶白旗', '3', '15', '1525', '1525', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (152530, '正蓝旗', '3', '15', '1525', '1525', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (152531, '多伦县', '3', '15', '1525', '1525', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (152921, '阿拉善左旗', '3', '15', '1529', '1529', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (152922, '阿拉善右旗', '3', '15', '1529', '1529', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (152923, '额济纳旗', '3', '15', '1529', '1529', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (210101, '市辖区', '3', '21', '2101', '2101', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (210102, '和平区', '3', '21', '2101', '2101', 0, 543, '', '');
INSERT INTO `sq_region` VALUES (210103, '沈河区', '3', '21', '2101', '2101', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (210104, '大东区', '3', '21', '2101', '2101', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (210105, '皇姑区', '3', '21', '2101', '2101', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (210106, '铁西区', '3', '21', '2101', '2101', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (210111, '苏家屯区', '3', '21', '2101', '2101', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (210112, '浑南区', '3', '21', '2101', '2101', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (210113, '沈北新区', '3', '21', '2101', '2101', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (210114, '于洪区', '3', '21', '2101', '2101', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (210115, '辽中区', '3', '21', '2101', '2101', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (210123, '康平县', '3', '21', '2101', '2101', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (210124, '法库县', '3', '21', '2101', '2101', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (210181, '新民市', '3', '21', '2101', '2101', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (210201, '市辖区', '3', '21', '2102', '2102', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (210202, '中山区', '3', '21', '2102', '2102', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (210203, '西岗区', '3', '21', '2102', '2102', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (210204, '沙河口区', '3', '21', '2102', '2102', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (210211, '甘井子区', '3', '21', '2102', '2102', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (210212, '旅顺口区', '3', '21', '2102', '2102', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (210213, '金州区', '3', '21', '2102', '2102', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (210214, '普兰店区', '3', '21', '2102', '2102', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (210224, '长海县', '3', '21', '2102', '2102', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (210281, '瓦房店市', '3', '21', '2102', '2102', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (210283, '庄河市', '3', '21', '2102', '2102', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (210301, '市辖区', '3', '21', '2103', '2103', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (210302, '铁东区', '3', '21', '2103', '2103', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (210303, '铁西区', '3', '21', '2103', '2103', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (210304, '立山区', '3', '21', '2103', '2103', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (210311, '千山区', '3', '21', '2103', '2103', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (210321, '台安县', '3', '21', '2103', '2103', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (210323, '岫岩满族自治县', '3', '21', '2103', '2103', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (210381, '海城市', '3', '21', '2103', '2103', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (210401, '市辖区', '3', '21', '2104', '2104', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (210402, '新抚区', '3', '21', '2104', '2104', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (210403, '东洲区', '3', '21', '2104', '2104', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (210404, '望花区', '3', '21', '2104', '2104', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (210411, '顺城区', '3', '21', '2104', '2104', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (210421, '抚顺县', '3', '21', '2104', '2104', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (210422, '新宾满族自治县', '3', '21', '2104', '2104', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (210423, '清原满族自治县', '3', '21', '2104', '2104', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (210501, '市辖区', '3', '21', '2105', '2105', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (210502, '平山区', '3', '21', '2105', '2105', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (210503, '溪湖区', '3', '21', '2105', '2105', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (210504, '明山区', '3', '21', '2105', '2105', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (210505, '南芬区', '3', '21', '2105', '2105', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (210521, '本溪满族自治县', '3', '21', '2105', '2105', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (210522, '桓仁满族自治县', '3', '21', '2105', '2105', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (210601, '市辖区', '3', '21', '2106', '2106', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (210602, '元宝区', '3', '21', '2106', '2106', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (210603, '振兴区', '3', '21', '2106', '2106', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (210604, '振安区', '3', '21', '2106', '2106', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (210624, '宽甸满族自治县', '3', '21', '2106', '2106', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (210681, '东港市', '3', '21', '2106', '2106', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (210682, '凤城市', '3', '21', '2106', '2106', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (210701, '市辖区', '3', '21', '2107', '2107', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (210702, '古塔区', '3', '21', '2107', '2107', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (210703, '凌河区', '3', '21', '2107', '2107', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (210711, '太和区', '3', '21', '2107', '2107', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (210726, '黑山县', '3', '21', '2107', '2107', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (210727, '义县', '3', '21', '2107', '2107', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (210781, '凌海市', '3', '21', '2107', '2107', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (210782, '北镇市', '3', '21', '2107', '2107', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (210801, '市辖区', '3', '21', '2108', '2108', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (210802, '站前区', '3', '21', '2108', '2108', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (210803, '西市区', '3', '21', '2108', '2108', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (210804, '鲅鱼圈区', '3', '21', '2108', '2108', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (210811, '老边区', '3', '21', '2108', '2108', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (210881, '盖州市', '3', '21', '2108', '2108', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (210882, '大石桥市', '3', '21', '2108', '2108', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (210901, '市辖区', '3', '21', '2109', '2109', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (210902, '海州区', '3', '21', '2109', '2109', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (210903, '新邱区', '3', '21', '2109', '2109', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (210904, '太平区', '3', '21', '2109', '2109', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (210905, '清河门区', '3', '21', '2109', '2109', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (210911, '细河区', '3', '21', '2109', '2109', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (210921, '阜新蒙古族自治县', '3', '21', '2109', '2109', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (210922, '彰武县', '3', '21', '2109', '2109', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (211001, '市辖区', '3', '21', '2110', '2110', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (211002, '白塔区', '3', '21', '2110', '2110', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (211003, '文圣区', '3', '21', '2110', '2110', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (211004, '宏伟区', '3', '21', '2110', '2110', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (211005, '弓长岭区', '3', '21', '2110', '2110', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (211011, '太子河区', '3', '21', '2110', '2110', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (211021, '辽阳县', '3', '21', '2110', '2110', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (211081, '灯塔市', '3', '21', '2110', '2110', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (211101, '市辖区', '3', '21', '2111', '2111', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (211102, '双台子区', '3', '21', '2111', '2111', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (211103, '兴隆台区', '3', '21', '2111', '2111', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (211104, '大洼区', '3', '21', '2111', '2111', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (211122, '盘山县', '3', '21', '2111', '2111', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (211201, '市辖区', '3', '21', '2112', '2112', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (211202, '银州区', '3', '21', '2112', '2112', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (211204, '清河区', '3', '21', '2112', '2112', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (211221, '铁岭县', '3', '21', '2112', '2112', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (211223, '西丰县', '3', '21', '2112', '2112', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (211224, '昌图县', '3', '21', '2112', '2112', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (211281, '调兵山市', '3', '21', '2112', '2112', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (211282, '开原市', '3', '21', '2112', '2112', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (211301, '市辖区', '3', '21', '2113', '2113', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (211302, '双塔区', '3', '21', '2113', '2113', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (211303, '龙城区', '3', '21', '2113', '2113', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (211321, '朝阳县', '3', '21', '2113', '2113', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (211322, '建平县', '3', '21', '2113', '2113', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (211324, '喀喇沁左翼蒙古族自治县', '3', '21', '2113', '2113', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (211381, '北票市', '3', '21', '2113', '2113', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (211382, '凌源市', '3', '21', '2113', '2113', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (211401, '市辖区', '3', '21', '2114', '2114', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (211402, '连山区', '3', '21', '2114', '2114', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (211403, '龙港区', '3', '21', '2114', '2114', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (211404, '南票区', '3', '21', '2114', '2114', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (211421, '绥中县', '3', '21', '2114', '2114', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (211422, '建昌县', '3', '21', '2114', '2114', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (211481, '兴城市', '3', '21', '2114', '2114', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (220101, '市辖区', '3', '22', '2201', '2201', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (220102, '南关区', '3', '22', '2201', '2201', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (220103, '宽城区', '3', '22', '2201', '2201', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (220104, '朝阳区', '3', '22', '2201', '2201', 0, 565, '', '');
INSERT INTO `sq_region` VALUES (220105, '二道区', '3', '22', '2201', '2201', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (220106, '绿园区', '3', '22', '2201', '2201', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (220112, '双阳区', '3', '22', '2201', '2201', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (220113, '九台区', '3', '22', '2201', '2201', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (220122, '农安县', '3', '22', '2201', '2201', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (220182, '榆树市', '3', '22', '2201', '2201', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (220183, '德惠市', '3', '22', '2201', '2201', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (220201, '市辖区', '3', '22', '2202', '2202', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (220202, '昌邑区', '3', '22', '2202', '2202', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (220203, '龙潭区', '3', '22', '2202', '2202', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (220204, '船营区', '3', '22', '2202', '2202', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (220211, '丰满区', '3', '22', '2202', '2202', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (220221, '永吉县', '3', '22', '2202', '2202', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (220281, '蛟河市', '3', '22', '2202', '2202', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (220282, '桦甸市', '3', '22', '2202', '2202', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (220283, '舒兰市', '3', '22', '2202', '2202', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (220284, '磐石市', '3', '22', '2202', '2202', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (220301, '市辖区', '3', '22', '2203', '2203', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (220302, '铁西区', '3', '22', '2203', '2203', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (220303, '铁东区', '3', '22', '2203', '2203', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (220322, '梨树县', '3', '22', '2203', '2203', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (220323, '伊通满族自治县', '3', '22', '2203', '2203', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (220381, '公主岭市', '3', '22', '2203', '2203', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (220382, '双辽市', '3', '22', '2203', '2203', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (220401, '市辖区', '3', '22', '2204', '2204', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (220402, '龙山区', '3', '22', '2204', '2204', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (220403, '西安区', '3', '22', '2204', '2204', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (220421, '东丰县', '3', '22', '2204', '2204', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (220422, '东辽县', '3', '22', '2204', '2204', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (220501, '市辖区', '3', '22', '2205', '2205', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (220502, '东昌区', '3', '22', '2205', '2205', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (220503, '二道江区', '3', '22', '2205', '2205', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (220521, '通化县', '3', '22', '2205', '2205', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (220523, '辉南县', '3', '22', '2205', '2205', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (220524, '柳河县', '3', '22', '2205', '2205', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (220581, '梅河口市', '3', '22', '2205', '2205', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (220582, '集安市', '3', '22', '2205', '2205', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (220601, '市辖区', '3', '22', '2206', '2206', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (220602, '浑江区', '3', '22', '2206', '2206', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (220605, '江源区', '3', '22', '2206', '2206', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (220621, '抚松县', '3', '22', '2206', '2206', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (220622, '靖宇县', '3', '22', '2206', '2206', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (220623, '长白朝鲜族自治县', '3', '22', '2206', '2206', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (220681, '临江市', '3', '22', '2206', '2206', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (220701, '市辖区', '3', '22', '2207', '2207', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (220702, '宁江区', '3', '22', '2207', '2207', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (220721, '前郭尔罗斯蒙古族自治县', '3', '22', '2207', '2207', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (220722, '长岭县', '3', '22', '2207', '2207', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (220723, '乾安县', '3', '22', '2207', '2207', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (220781, '扶余市', '3', '22', '2207', '2207', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (220801, '市辖区', '3', '22', '2208', '2208', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (220802, '洮北区', '3', '22', '2208', '2208', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (220821, '镇赉县', '3', '22', '2208', '2208', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (220822, '通榆县', '3', '22', '2208', '2208', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (220881, '洮南市', '3', '22', '2208', '2208', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (220882, '大安市', '3', '22', '2208', '2208', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (222401, '延吉市', '3', '22', '2224', '2224', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (222402, '图们市', '3', '22', '2224', '2224', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (222403, '敦化市', '3', '22', '2224', '2224', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (222404, '珲春市', '3', '22', '2224', '2224', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (222405, '龙井市', '3', '22', '2224', '2224', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (222406, '和龙市', '3', '22', '2224', '2224', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (222424, '汪清县', '3', '22', '2224', '2224', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (222426, '安图县', '3', '22', '2224', '2224', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230101, '市辖区', '3', '23', '2301', '2301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230102, '道里区', '3', '23', '2301', '2301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230103, '南岗区', '3', '23', '2301', '2301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230104, '道外区', '3', '23', '2301', '2301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230108, '平房区', '3', '23', '2301', '2301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230109, '松北区', '3', '23', '2301', '2301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230110, '香坊区', '3', '23', '2301', '2301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230111, '呼兰区', '3', '23', '2301', '2301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230112, '阿城区', '3', '23', '2301', '2301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230113, '双城区', '3', '23', '2301', '2301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230123, '依兰县', '3', '23', '2301', '2301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230124, '方正县', '3', '23', '2301', '2301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230125, '宾县', '3', '23', '2301', '2301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230126, '巴彦县', '3', '23', '2301', '2301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230127, '木兰县', '3', '23', '2301', '2301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230128, '通河县', '3', '23', '2301', '2301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230129, '延寿县', '3', '23', '2301', '2301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230183, '尚志市', '3', '23', '2301', '2301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230184, '五常市', '3', '23', '2301', '2301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230201, '市辖区', '3', '23', '2302', '2302', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230202, '龙沙区', '3', '23', '2302', '2302', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230203, '建华区', '3', '23', '2302', '2302', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230204, '铁锋区', '3', '23', '2302', '2302', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230205, '昂昂溪区', '3', '23', '2302', '2302', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230206, '富拉尔基区', '3', '23', '2302', '2302', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230207, '碾子山区', '3', '23', '2302', '2302', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230208, '梅里斯达斡尔族区', '3', '23', '2302', '2302', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230221, '龙江县', '3', '23', '2302', '2302', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230223, '依安县', '3', '23', '2302', '2302', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230224, '泰来县', '3', '23', '2302', '2302', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230225, '甘南县', '3', '23', '2302', '2302', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230227, '富裕县', '3', '23', '2302', '2302', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230229, '克山县', '3', '23', '2302', '2302', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230230, '克东县', '3', '23', '2302', '2302', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230231, '拜泉县', '3', '23', '2302', '2302', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230281, '讷河市', '3', '23', '2302', '2302', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230301, '市辖区', '3', '23', '2303', '2303', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230302, '鸡冠区', '3', '23', '2303', '2303', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230303, '恒山区', '3', '23', '2303', '2303', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230304, '滴道区', '3', '23', '2303', '2303', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230305, '梨树区', '3', '23', '2303', '2303', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230306, '城子河区', '3', '23', '2303', '2303', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230307, '麻山区', '3', '23', '2303', '2303', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230321, '鸡东县', '3', '23', '2303', '2303', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230381, '虎林市', '3', '23', '2303', '2303', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230382, '密山市', '3', '23', '2303', '2303', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230401, '市辖区', '3', '23', '2304', '2304', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230402, '向阳区', '3', '23', '2304', '2304', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230403, '工农区', '3', '23', '2304', '2304', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230404, '南山区', '3', '23', '2304', '2304', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230405, '兴安区', '3', '23', '2304', '2304', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230406, '东山区', '3', '23', '2304', '2304', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230407, '兴山区', '3', '23', '2304', '2304', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230421, '萝北县', '3', '23', '2304', '2304', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230422, '绥滨县', '3', '23', '2304', '2304', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230501, '市辖区', '3', '23', '2305', '2305', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230502, '尖山区', '3', '23', '2305', '2305', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230503, '岭东区', '3', '23', '2305', '2305', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230505, '四方台区', '3', '23', '2305', '2305', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230506, '宝山区', '3', '23', '2305', '2305', 0, 490, '', '');
INSERT INTO `sq_region` VALUES (230521, '集贤县', '3', '23', '2305', '2305', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230522, '友谊县', '3', '23', '2305', '2305', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230523, '宝清县', '3', '23', '2305', '2305', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230524, '饶河县', '3', '23', '2305', '2305', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230601, '市辖区', '3', '23', '2306', '2306', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230602, '萨尔图区', '3', '23', '2306', '2306', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230603, '龙凤区', '3', '23', '2306', '2306', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230604, '让胡路区', '3', '23', '2306', '2306', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230605, '红岗区', '3', '23', '2306', '2306', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230606, '大同区', '3', '23', '2306', '2306', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230621, '肇州县', '3', '23', '2306', '2306', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230622, '肇源县', '3', '23', '2306', '2306', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230623, '林甸县', '3', '23', '2306', '2306', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230624, '杜尔伯特蒙古族自治县', '3', '23', '2306', '2306', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230701, '市辖区', '3', '23', '2307', '2307', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230702, '伊春区', '3', '23', '2307', '2307', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230703, '南岔区', '3', '23', '2307', '2307', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230704, '友好区', '3', '23', '2307', '2307', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230705, '西林区', '3', '23', '2307', '2307', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230706, '翠峦区', '3', '23', '2307', '2307', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230707, '新青区', '3', '23', '2307', '2307', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230708, '美溪区', '3', '23', '2307', '2307', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230709, '金山屯区', '3', '23', '2307', '2307', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230710, '五营区', '3', '23', '2307', '2307', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230711, '乌马河区', '3', '23', '2307', '2307', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230712, '汤旺河区', '3', '23', '2307', '2307', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230713, '带岭区', '3', '23', '2307', '2307', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230714, '乌伊岭区', '3', '23', '2307', '2307', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230715, '红星区', '3', '23', '2307', '2307', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230716, '上甘岭区', '3', '23', '2307', '2307', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230722, '嘉荫县', '3', '23', '2307', '2307', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230781, '铁力市', '3', '23', '2307', '2307', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230801, '市辖区', '3', '23', '2308', '2308', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230803, '向阳区', '3', '23', '2308', '2308', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230804, '前进区', '3', '23', '2308', '2308', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230805, '东风区', '3', '23', '2308', '2308', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230811, '郊区', '3', '23', '2308', '2308', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230822, '桦南县', '3', '23', '2308', '2308', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230826, '桦川县', '3', '23', '2308', '2308', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230828, '汤原县', '3', '23', '2308', '2308', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230881, '同江市', '3', '23', '2308', '2308', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230882, '富锦市', '3', '23', '2308', '2308', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230883, '抚远市', '3', '23', '2308', '2308', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230901, '市辖区', '3', '23', '2309', '2309', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230902, '新兴区', '3', '23', '2309', '2309', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230903, '桃山区', '3', '23', '2309', '2309', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230904, '茄子河区', '3', '23', '2309', '2309', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (230921, '勃利县', '3', '23', '2309', '2309', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (231001, '市辖区', '3', '23', '2310', '2310', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (231002, '东安区', '3', '23', '2310', '2310', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (231003, '阳明区', '3', '23', '2310', '2310', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (231004, '爱民区', '3', '23', '2310', '2310', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (231005, '西安区', '3', '23', '2310', '2310', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (231025, '林口县', '3', '23', '2310', '2310', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (231081, '绥芬河市', '3', '23', '2310', '2310', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (231083, '海林市', '3', '23', '2310', '2310', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (231084, '宁安市', '3', '23', '2310', '2310', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (231085, '穆棱市', '3', '23', '2310', '2310', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (231086, '东宁市', '3', '23', '2310', '2310', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (231101, '市辖区', '3', '23', '2311', '2311', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (231102, '爱辉区', '3', '23', '2311', '2311', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (231121, '嫩江县', '3', '23', '2311', '2311', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (231123, '逊克县', '3', '23', '2311', '2311', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (231124, '孙吴县', '3', '23', '2311', '2311', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (231181, '北安市', '3', '23', '2311', '2311', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (231182, '五大连池市', '3', '23', '2311', '2311', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (231201, '市辖区', '3', '23', '2312', '2312', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (231202, '北林区', '3', '23', '2312', '2312', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (231221, '望奎县', '3', '23', '2312', '2312', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (231222, '兰西县', '3', '23', '2312', '2312', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (231223, '青冈县', '3', '23', '2312', '2312', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (231224, '庆安县', '3', '23', '2312', '2312', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (231225, '明水县', '3', '23', '2312', '2312', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (231226, '绥棱县', '3', '23', '2312', '2312', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (231281, '安达市', '3', '23', '2312', '2312', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (231282, '肇东市', '3', '23', '2312', '2312', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (231283, '海伦市', '3', '23', '2312', '2312', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (232721, '呼玛县', '3', '23', '2327', '2327', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (232722, '塔河县', '3', '23', '2327', '2327', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (232723, '漠河县', '3', '23', '2327', '2327', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (310101, '黄浦区', '3', '31', '3101', '3101', 0, 485, '', '');
INSERT INTO `sq_region` VALUES (310104, '徐汇区', '3', '31', '3101', '3101', 0, 475, '', '');
INSERT INTO `sq_region` VALUES (310105, '长宁区', '3', '31', '3101', '3101', 0, 489, '', '');
INSERT INTO `sq_region` VALUES (310106, '静安区', '3', '31', '3101', '3101', 0, 482, '', '');
INSERT INTO `sq_region` VALUES (310107, '普陀区', '3', '31', '3101', '3101', 0, 479, '', '');
INSERT INTO `sq_region` VALUES (310109, '虹口区', '3', '31', '3101', '3101', 0, 486, '', '');
INSERT INTO `sq_region` VALUES (310110, '杨浦区', '3', '31', '3101', '3101', 0, 474, '', '');
INSERT INTO `sq_region` VALUES (310112, '闵行区', '3', '31', '3101', '3101', 0, 472, '', '');
INSERT INTO `sq_region` VALUES (310113, '宝山区', '3', '31', '3101', '3101', 0, 490, '', '');
INSERT INTO `sq_region` VALUES (310114, '嘉定区', '3', '31', '3101', '3101', 0, 484, '', '');
INSERT INTO `sq_region` VALUES (310115, '浦东新区', '3', '31', '3101', '3101', 0, 478, '', '');
INSERT INTO `sq_region` VALUES (310116, '金山区', '3', '31', '3101', '3101', 0, 483, '', '');
INSERT INTO `sq_region` VALUES (310117, '松江区', '3', '31', '3101', '3101', 0, 476, '', '');
INSERT INTO `sq_region` VALUES (310118, '青浦区', '3', '31', '3101', '3101', 0, 477, '', '');
INSERT INTO `sq_region` VALUES (310120, '奉贤区', '3', '31', '3101', '3101', 0, 487, '', '');
INSERT INTO `sq_region` VALUES (310151, '崇明区', '3', '31', '3101', '3101', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (320101, '市辖区', '3', '32', '3201', '3201', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (320102, '玄武区', '3', '32', '3201', '3201', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (320104, '秦淮区', '3', '32', '3201', '3201', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (320105, '建邺区', '3', '32', '3201', '3201', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (320106, '鼓楼区', '3', '32', '3201', '3201', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (320111, '浦口区', '3', '32', '3201', '3201', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (320113, '栖霞区', '3', '32', '3201', '3201', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (320114, '雨花台区', '3', '32', '3201', '3201', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (320115, '江宁区', '3', '32', '3201', '3201', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (320116, '六合区', '3', '32', '3201', '3201', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (320117, '溧水区', '3', '32', '3201', '3201', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (320118, '高淳区', '3', '32', '3201', '3201', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (320201, '市辖区', '3', '32', '3202', '3202', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (320205, '锡山区', '3', '32', '3202', '3202', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (320206, '惠山区', '3', '32', '3202', '3202', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (320211, '滨湖区', '3', '32', '3202', '3202', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (320213, '梁溪区', '3', '32', '3202', '3202', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (320214, '新吴区', '3', '32', '3202', '3202', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (320281, '江阴市', '3', '32', '3202', '3202', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (320282, '宜兴市', '3', '32', '3202', '3202', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (320301, '市辖区', '3', '32', '3203', '3203', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (320302, '鼓楼区', '3', '32', '3203', '3203', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (320303, '云龙区', '3', '32', '3203', '3203', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (320305, '贾汪区', '3', '32', '3203', '3203', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (320311, '泉山区', '3', '32', '3203', '3203', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (320312, '铜山区', '3', '32', '3203', '3203', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (320321, '丰县', '3', '32', '3203', '3203', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (320322, '沛县', '3', '32', '3203', '3203', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (320324, '睢宁县', '3', '32', '3203', '3203', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (320381, '新沂市', '3', '32', '3203', '3203', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (320382, '邳州市', '3', '32', '3203', '3203', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (320401, '市辖区', '3', '32', '3204', '3204', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (320402, '天宁区', '3', '32', '3204', '3204', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (320404, '钟楼区', '3', '32', '3204', '3204', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (320411, '新北区', '3', '32', '3204', '3204', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (320412, '武进区', '3', '32', '3204', '3204', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (320413, '金坛区', '3', '32', '3204', '3204', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (320481, '溧阳市', '3', '32', '3204', '3204', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (320501, '市辖区', '3', '32', '3205', '3205', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (320505, '虎丘区', '3', '32', '3205', '3205', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (320506, '吴中区', '3', '32', '3205', '3205', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (320507, '相城区', '3', '32', '3205', '3205', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (320508, '姑苏区', '3', '32', '3205', '3205', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (320509, '吴江区', '3', '32', '3205', '3205', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (320581, '常熟市', '3', '32', '3205', '3205', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (320582, '张家港市', '3', '32', '3205', '3205', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (320583, '昆山市', '3', '32', '3205', '3205', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (320585, '太仓市', '3', '32', '3205', '3205', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (320601, '市辖区', '3', '32', '3206', '3206', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (320602, '崇川区', '3', '32', '3206', '3206', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (320611, '港闸区', '3', '32', '3206', '3206', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (320612, '通州区', '3', '32', '3206', '3206', 0, 552, '', '');
INSERT INTO `sq_region` VALUES (320621, '海安县', '3', '32', '3206', '3206', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (320623, '如东县', '3', '32', '3206', '3206', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (320681, '启东市', '3', '32', '3206', '3206', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (320682, '如皋市', '3', '32', '3206', '3206', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (320684, '海门市', '3', '32', '3206', '3206', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (320701, '市辖区', '3', '32', '3207', '3207', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (320703, '连云区', '3', '32', '3207', '3207', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (320706, '海州区', '3', '32', '3207', '3207', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (320707, '赣榆区', '3', '32', '3207', '3207', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (320722, '东海县', '3', '32', '3207', '3207', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (320723, '灌云县', '3', '32', '3207', '3207', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (320724, '灌南县', '3', '32', '3207', '3207', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (320801, '市辖区', '3', '32', '3208', '3208', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (320803, '淮安区', '3', '32', '3208', '3208', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (320804, '淮阴区', '3', '32', '3208', '3208', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (320812, '清江浦区', '3', '32', '3208', '3208', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (320813, '洪泽区', '3', '32', '3208', '3208', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (320826, '涟水县', '3', '32', '3208', '3208', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (320830, '盱眙县', '3', '32', '3208', '3208', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (320831, '金湖县', '3', '32', '3208', '3208', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (320901, '市辖区', '3', '32', '3209', '3209', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (320902, '亭湖区', '3', '32', '3209', '3209', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (320903, '盐都区', '3', '32', '3209', '3209', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (320904, '大丰区', '3', '32', '3209', '3209', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (320921, '响水县', '3', '32', '3209', '3209', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (320922, '滨海县', '3', '32', '3209', '3209', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (320923, '阜宁县', '3', '32', '3209', '3209', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (320924, '射阳县', '3', '32', '3209', '3209', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (320925, '建湖县', '3', '32', '3209', '3209', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (320981, '东台市', '3', '32', '3209', '3209', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (321001, '市辖区', '3', '32', '3210', '3210', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (321002, '广陵区', '3', '32', '3210', '3210', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (321003, '邗江区', '3', '32', '3210', '3210', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (321012, '江都区', '3', '32', '3210', '3210', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (321023, '宝应县', '3', '32', '3210', '3210', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (321081, '仪征市', '3', '32', '3210', '3210', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (321084, '高邮市', '3', '32', '3210', '3210', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (321101, '市辖区', '3', '32', '3211', '3211', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (321102, '京口区', '3', '32', '3211', '3211', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (321111, '润州区', '3', '32', '3211', '3211', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (321112, '丹徒区', '3', '32', '3211', '3211', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (321181, '丹阳市', '3', '32', '3211', '3211', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (321182, '扬中市', '3', '32', '3211', '3211', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (321183, '句容市', '3', '32', '3211', '3211', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (321201, '市辖区', '3', '32', '3212', '3212', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (321202, '海陵区', '3', '32', '3212', '3212', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (321203, '高港区', '3', '32', '3212', '3212', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (321204, '姜堰区', '3', '32', '3212', '3212', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (321281, '兴化市', '3', '32', '3212', '3212', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (321282, '靖江市', '3', '32', '3212', '3212', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (321283, '泰兴市', '3', '32', '3212', '3212', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (321301, '市辖区', '3', '32', '3213', '3213', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (321302, '宿城区', '3', '32', '3213', '3213', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (321311, '宿豫区', '3', '32', '3213', '3213', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (321322, '沭阳县', '3', '32', '3213', '3213', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (321323, '泗阳县', '3', '32', '3213', '3213', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (321324, '泗洪县', '3', '32', '3213', '3213', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (330101, '市辖区', '3', '33', '3301', '3301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (330102, '上城区', '3', '33', '3301', '3301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (330103, '下城区', '3', '33', '3301', '3301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (330104, '江干区', '3', '33', '3301', '3301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (330105, '拱墅区', '3', '33', '3301', '3301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (330106, '西湖区', '3', '33', '3301', '3301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (330108, '滨江区', '3', '33', '3301', '3301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (330109, '萧山区', '3', '33', '3301', '3301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (330110, '余杭区', '3', '33', '3301', '3301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (330111, '富阳区', '3', '33', '3301', '3301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (330122, '桐庐县', '3', '33', '3301', '3301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (330127, '淳安县', '3', '33', '3301', '3301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (330182, '建德市', '3', '33', '3301', '3301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (330185, '临安市', '3', '33', '3301', '3301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (330201, '市辖区', '3', '33', '3302', '3302', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (330203, '海曙区', '3', '33', '3302', '3302', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (330204, '江东区', '3', '33', '3302', '3302', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (330205, '江北区', '3', '33', '3302', '3302', 0, 504, '', '');
INSERT INTO `sq_region` VALUES (330206, '北仑区', '3', '33', '3302', '3302', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (330211, '镇海区', '3', '33', '3302', '3302', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (330212, '鄞州区', '3', '33', '3302', '3302', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (330225, '象山县', '3', '33', '3302', '3302', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (330226, '宁海县', '3', '33', '3302', '3302', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (330281, '余姚市', '3', '33', '3302', '3302', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (330282, '慈溪市', '3', '33', '3302', '3302', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (330283, '奉化市', '3', '33', '3302', '3302', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (330301, '市辖区', '3', '33', '3303', '3303', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (330302, '鹿城区', '3', '33', '3303', '3303', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (330303, '龙湾区', '3', '33', '3303', '3303', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (330304, '瓯海区', '3', '33', '3303', '3303', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (330305, '洞头区', '3', '33', '3303', '3303', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (330324, '永嘉县', '3', '33', '3303', '3303', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (330326, '平阳县', '3', '33', '3303', '3303', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (330327, '苍南县', '3', '33', '3303', '3303', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (330328, '文成县', '3', '33', '3303', '3303', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (330329, '泰顺县', '3', '33', '3303', '3303', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (330381, '瑞安市', '3', '33', '3303', '3303', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (330382, '乐清市', '3', '33', '3303', '3303', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (330401, '市辖区', '3', '33', '3304', '3304', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (330402, '南湖区', '3', '33', '3304', '3304', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (330411, '秀洲区', '3', '33', '3304', '3304', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (330421, '嘉善县', '3', '33', '3304', '3304', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (330424, '海盐县', '3', '33', '3304', '3304', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (330481, '海宁市', '3', '33', '3304', '3304', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (330482, '平湖市', '3', '33', '3304', '3304', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (330483, '桐乡市', '3', '33', '3304', '3304', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (330501, '市辖区', '3', '33', '3305', '3305', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (330502, '吴兴区', '3', '33', '3305', '3305', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (330503, '南浔区', '3', '33', '3305', '3305', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (330521, '德清县', '3', '33', '3305', '3305', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (330522, '长兴县', '3', '33', '3305', '3305', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (330523, '安吉县', '3', '33', '3305', '3305', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (330601, '市辖区', '3', '33', '3306', '3306', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (330602, '越城区', '3', '33', '3306', '3306', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (330603, '柯桥区', '3', '33', '3306', '3306', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (330604, '上虞区', '3', '33', '3306', '3306', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (330624, '新昌县', '3', '33', '3306', '3306', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (330681, '诸暨市', '3', '33', '3306', '3306', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (330683, '嵊州市', '3', '33', '3306', '3306', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (330701, '市辖区', '3', '33', '3307', '3307', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (330702, '婺城区', '3', '33', '3307', '3307', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (330703, '金东区', '3', '33', '3307', '3307', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (330723, '武义县', '3', '33', '3307', '3307', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (330726, '浦江县', '3', '33', '3307', '3307', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (330727, '磐安县', '3', '33', '3307', '3307', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (330781, '兰溪市', '3', '33', '3307', '3307', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (330782, '义乌市', '3', '33', '3307', '3307', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (330783, '东阳市', '3', '33', '3307', '3307', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (330784, '永康市', '3', '33', '3307', '3307', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (330801, '市辖区', '3', '33', '3308', '3308', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (330802, '柯城区', '3', '33', '3308', '3308', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (330803, '衢江区', '3', '33', '3308', '3308', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (330822, '常山县', '3', '33', '3308', '3308', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (330824, '开化县', '3', '33', '3308', '3308', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (330825, '龙游县', '3', '33', '3308', '3308', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (330881, '江山市', '3', '33', '3308', '3308', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (330901, '市辖区', '3', '33', '3309', '3309', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (330902, '定海区', '3', '33', '3309', '3309', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (330903, '普陀区', '3', '33', '3309', '3309', 0, 479, '', '');
INSERT INTO `sq_region` VALUES (330921, '岱山县', '3', '33', '3309', '3309', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (330922, '嵊泗县', '3', '33', '3309', '3309', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (331001, '市辖区', '3', '33', '3310', '3310', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (331002, '椒江区', '3', '33', '3310', '3310', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (331003, '黄岩区', '3', '33', '3310', '3310', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (331004, '路桥区', '3', '33', '3310', '3310', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (331021, '玉环县', '3', '33', '3310', '3310', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (331022, '三门县', '3', '33', '3310', '3310', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (331023, '天台县', '3', '33', '3310', '3310', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (331024, '仙居县', '3', '33', '3310', '3310', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (331081, '温岭市', '3', '33', '3310', '3310', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (331082, '临海市', '3', '33', '3310', '3310', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (331101, '市辖区', '3', '33', '3311', '3311', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (331102, '莲都区', '3', '33', '3311', '3311', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (331121, '青田县', '3', '33', '3311', '3311', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (331122, '缙云县', '3', '33', '3311', '3311', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (331123, '遂昌县', '3', '33', '3311', '3311', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (331124, '松阳县', '3', '33', '3311', '3311', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (331125, '云和县', '3', '33', '3311', '3311', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (331126, '庆元县', '3', '33', '3311', '3311', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (331127, '景宁畲族自治县', '3', '33', '3311', '3311', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (331181, '龙泉市', '3', '33', '3311', '3311', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (340101, '市辖区', '3', '34', '3401', '3401', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (340102, '瑶海区', '3', '34', '3401', '3401', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (340103, '庐阳区', '3', '34', '3401', '3401', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (340104, '蜀山区', '3', '34', '3401', '3401', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (340111, '包河区', '3', '34', '3401', '3401', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (340121, '长丰县', '3', '34', '3401', '3401', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (340122, '肥东县', '3', '34', '3401', '3401', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (340123, '肥西县', '3', '34', '3401', '3401', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (340124, '庐江县', '3', '34', '3401', '3401', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (340181, '巢湖市', '3', '34', '3401', '3401', 0, 3, '', '');
INSERT INTO `sq_region` VALUES (340201, '市辖区', '3', '34', '3402', '3402', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (340202, '镜湖区', '3', '34', '3402', '3402', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (340203, '弋江区', '3', '34', '3402', '3402', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (340207, '鸠江区', '3', '34', '3402', '3402', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (340208, '三山区', '3', '34', '3402', '3402', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (340221, '芜湖县', '3', '34', '3402', '3402', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (340222, '繁昌县', '3', '34', '3402', '3402', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (340223, '南陵县', '3', '34', '3402', '3402', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (340225, '无为县', '3', '34', '3402', '3402', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (340301, '市辖区', '3', '34', '3403', '3403', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (340302, '龙子湖区', '3', '34', '3403', '3403', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (340303, '蚌山区', '3', '34', '3403', '3403', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (340304, '禹会区', '3', '34', '3403', '3403', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (340311, '淮上区', '3', '34', '3403', '3403', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (340321, '怀远县', '3', '34', '3403', '3403', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (340322, '五河县', '3', '34', '3403', '3403', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (340323, '固镇县', '3', '34', '3403', '3403', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (340401, '市辖区', '3', '34', '3404', '3404', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (340402, '大通区', '3', '34', '3404', '3404', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (340403, '田家庵区', '3', '34', '3404', '3404', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (340404, '谢家集区', '3', '34', '3404', '3404', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (340405, '八公山区', '3', '34', '3404', '3404', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (340406, '潘集区', '3', '34', '3404', '3404', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (340421, '凤台县', '3', '34', '3404', '3404', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (340422, '寿县', '3', '34', '3404', '3404', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (340501, '市辖区', '3', '34', '3405', '3405', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (340503, '花山区', '3', '34', '3405', '3405', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (340504, '雨山区', '3', '34', '3405', '3405', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (340506, '博望区', '3', '34', '3405', '3405', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (340521, '当涂县', '3', '34', '3405', '3405', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (340522, '含山县', '3', '34', '3405', '3405', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (340523, '和县', '3', '34', '3405', '3405', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (340601, '市辖区', '3', '34', '3406', '3406', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (340602, '杜集区', '3', '34', '3406', '3406', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (340603, '相山区', '3', '34', '3406', '3406', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (340604, '烈山区', '3', '34', '3406', '3406', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (340621, '濉溪县', '3', '34', '3406', '3406', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (340701, '市辖区', '3', '34', '3407', '3407', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (340705, '铜官区', '3', '34', '3407', '3407', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (340706, '义安区', '3', '34', '3407', '3407', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (340711, '郊区', '3', '34', '3407', '3407', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (340722, '枞阳县', '3', '34', '3407', '3407', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (340801, '市辖区', '3', '34', '3408', '3408', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (340802, '迎江区', '3', '34', '3408', '3408', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (340803, '大观区', '3', '34', '3408', '3408', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (340811, '宜秀区', '3', '34', '3408', '3408', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (340822, '怀宁县', '3', '34', '3408', '3408', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (340824, '潜山县', '3', '34', '3408', '3408', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (340825, '太湖县', '3', '34', '3408', '3408', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (340826, '宿松县', '3', '34', '3408', '3408', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (340827, '望江县', '3', '34', '3408', '3408', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (340828, '岳西县', '3', '34', '3408', '3408', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (340881, '桐城市', '3', '34', '3408', '3408', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (341001, '市辖区', '3', '34', '3410', '3410', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (341002, '屯溪区', '3', '34', '3410', '3410', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (341003, '黄山区', '3', '34', '3410', '3410', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (341004, '徽州区', '3', '34', '3410', '3410', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (341021, '歙县', '3', '34', '3410', '3410', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (341022, '休宁县', '3', '34', '3410', '3410', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (341023, '黟县', '3', '34', '3410', '3410', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (341024, '祁门县', '3', '34', '3410', '3410', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (341101, '市辖区', '3', '34', '3411', '3411', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (341102, '琅琊区', '3', '34', '3411', '3411', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (341103, '南谯区', '3', '34', '3411', '3411', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (341122, '来安县', '3', '34', '3411', '3411', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (341124, '全椒县', '3', '34', '3411', '3411', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (341125, '定远县', '3', '34', '3411', '3411', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (341126, '凤阳县', '3', '34', '3411', '3411', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (341181, '天长市', '3', '34', '3411', '3411', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (341182, '明光市', '3', '34', '3411', '3411', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (341201, '市辖区', '3', '34', '3412', '3412', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (341202, '颍州区', '3', '34', '3412', '3412', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (341203, '颍东区', '3', '34', '3412', '3412', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (341204, '颍泉区', '3', '34', '3412', '3412', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (341221, '临泉县', '3', '34', '3412', '3412', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (341222, '太和县', '3', '34', '3412', '3412', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (341225, '阜南县', '3', '34', '3412', '3412', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (341226, '颍上县', '3', '34', '3412', '3412', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (341282, '界首市', '3', '34', '3412', '3412', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (341301, '市辖区', '3', '34', '3413', '3413', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (341302, '埇桥区', '3', '34', '3413', '3413', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (341321, '砀山县', '3', '34', '3413', '3413', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (341322, '萧县', '3', '34', '3413', '3413', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (341323, '灵璧县', '3', '34', '3413', '3413', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (341324, '泗县', '3', '34', '3413', '3413', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (341501, '市辖区', '3', '34', '3415', '3415', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (341502, '金安区', '3', '34', '3415', '3415', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (341503, '裕安区', '3', '34', '3415', '3415', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (341504, '叶集区', '3', '34', '3415', '3415', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (341522, '霍邱县', '3', '34', '3415', '3415', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (341523, '舒城县', '3', '34', '3415', '3415', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (341524, '金寨县', '3', '34', '3415', '3415', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (341525, '霍山县', '3', '34', '3415', '3415', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (341601, '市辖区', '3', '34', '3416', '3416', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (341602, '谯城区', '3', '34', '3416', '3416', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (341621, '涡阳县', '3', '34', '3416', '3416', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (341622, '蒙城县', '3', '34', '3416', '3416', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (341623, '利辛县', '3', '34', '3416', '3416', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (341701, '市辖区', '3', '34', '3417', '3417', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (341702, '贵池区', '3', '34', '3417', '3417', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (341721, '东至县', '3', '34', '3417', '3417', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (341722, '石台县', '3', '34', '3417', '3417', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (341723, '青阳县', '3', '34', '3417', '3417', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (341801, '市辖区', '3', '34', '3418', '3418', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (341802, '宣州区', '3', '34', '3418', '3418', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (341821, '郎溪县', '3', '34', '3418', '3418', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (341822, '广德县', '3', '34', '3418', '3418', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (341823, '泾县', '3', '34', '3418', '3418', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (341824, '绩溪县', '3', '34', '3418', '3418', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (341825, '旌德县', '3', '34', '3418', '3418', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (341881, '宁国市', '3', '34', '3418', '3418', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350101, '市辖区', '3', '35', '3501', '3501', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350102, '鼓楼区', '3', '35', '3501', '3501', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350103, '台江区', '3', '35', '3501', '3501', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350104, '仓山区', '3', '35', '3501', '3501', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350105, '马尾区', '3', '35', '3501', '3501', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350111, '晋安区', '3', '35', '3501', '3501', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350121, '闽侯县', '3', '35', '3501', '3501', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350122, '连江县', '3', '35', '3501', '3501', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350123, '罗源县', '3', '35', '3501', '3501', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350124, '闽清县', '3', '35', '3501', '3501', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350125, '永泰县', '3', '35', '3501', '3501', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350128, '平潭县', '3', '35', '3501', '3501', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350181, '福清市', '3', '35', '3501', '3501', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350182, '长乐市', '3', '35', '3501', '3501', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350201, '市辖区', '3', '35', '3502', '3502', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350203, '思明区', '3', '35', '3502', '3502', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350205, '海沧区', '3', '35', '3502', '3502', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350206, '湖里区', '3', '35', '3502', '3502', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350211, '集美区', '3', '35', '3502', '3502', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350212, '同安区', '3', '35', '3502', '3502', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350213, '翔安区', '3', '35', '3502', '3502', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350301, '市辖区', '3', '35', '3503', '3503', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350302, '城厢区', '3', '35', '3503', '3503', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350303, '涵江区', '3', '35', '3503', '3503', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350304, '荔城区', '3', '35', '3503', '3503', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350305, '秀屿区', '3', '35', '3503', '3503', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350322, '仙游县', '3', '35', '3503', '3503', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350401, '市辖区', '3', '35', '3504', '3504', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350402, '梅列区', '3', '35', '3504', '3504', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350403, '三元区', '3', '35', '3504', '3504', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350421, '明溪县', '3', '35', '3504', '3504', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350423, '清流县', '3', '35', '3504', '3504', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350424, '宁化县', '3', '35', '3504', '3504', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350425, '大田县', '3', '35', '3504', '3504', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350426, '尤溪县', '3', '35', '3504', '3504', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350427, '沙县', '3', '35', '3504', '3504', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350428, '将乐县', '3', '35', '3504', '3504', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350429, '泰宁县', '3', '35', '3504', '3504', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350430, '建宁县', '3', '35', '3504', '3504', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350481, '永安市', '3', '35', '3504', '3504', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350501, '市辖区', '3', '35', '3505', '3505', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350502, '鲤城区', '3', '35', '3505', '3505', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350503, '丰泽区', '3', '35', '3505', '3505', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350504, '洛江区', '3', '35', '3505', '3505', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350505, '泉港区', '3', '35', '3505', '3505', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350521, '惠安县', '3', '35', '3505', '3505', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350524, '安溪县', '3', '35', '3505', '3505', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350525, '永春县', '3', '35', '3505', '3505', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350526, '德化县', '3', '35', '3505', '3505', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350527, '金门县', '3', '35', '3505', '3505', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350581, '石狮市', '3', '35', '3505', '3505', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350582, '晋江市', '3', '35', '3505', '3505', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350583, '南安市', '3', '35', '3505', '3505', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350601, '市辖区', '3', '35', '3506', '3506', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350602, '芗城区', '3', '35', '3506', '3506', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350603, '龙文区', '3', '35', '3506', '3506', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350622, '云霄县', '3', '35', '3506', '3506', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350623, '漳浦县', '3', '35', '3506', '3506', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350624, '诏安县', '3', '35', '3506', '3506', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350625, '长泰县', '3', '35', '3506', '3506', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350626, '东山县', '3', '35', '3506', '3506', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350627, '南靖县', '3', '35', '3506', '3506', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350628, '平和县', '3', '35', '3506', '3506', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350629, '华安县', '3', '35', '3506', '3506', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350681, '龙海市', '3', '35', '3506', '3506', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350701, '市辖区', '3', '35', '3507', '3507', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350702, '延平区', '3', '35', '3507', '3507', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350703, '建阳区', '3', '35', '3507', '3507', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350721, '顺昌县', '3', '35', '3507', '3507', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350722, '浦城县', '3', '35', '3507', '3507', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350723, '光泽县', '3', '35', '3507', '3507', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350724, '松溪县', '3', '35', '3507', '3507', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350725, '政和县', '3', '35', '3507', '3507', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350781, '邵武市', '3', '35', '3507', '3507', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350782, '武夷山市', '3', '35', '3507', '3507', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350783, '建瓯市', '3', '35', '3507', '3507', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350801, '市辖区', '3', '35', '3508', '3508', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350802, '新罗区', '3', '35', '3508', '3508', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350803, '永定区', '3', '35', '3508', '3508', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350821, '长汀县', '3', '35', '3508', '3508', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350823, '上杭县', '3', '35', '3508', '3508', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350824, '武平县', '3', '35', '3508', '3508', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350825, '连城县', '3', '35', '3508', '3508', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350881, '漳平市', '3', '35', '3508', '3508', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350901, '市辖区', '3', '35', '3509', '3509', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350902, '蕉城区', '3', '35', '3509', '3509', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350921, '霞浦县', '3', '35', '3509', '3509', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350922, '古田县', '3', '35', '3509', '3509', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350923, '屏南县', '3', '35', '3509', '3509', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350924, '寿宁县', '3', '35', '3509', '3509', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350925, '周宁县', '3', '35', '3509', '3509', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350926, '柘荣县', '3', '35', '3509', '3509', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350981, '福安市', '3', '35', '3509', '3509', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (350982, '福鼎市', '3', '35', '3509', '3509', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360101, '市辖区', '3', '36', '3601', '3601', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360102, '东湖区', '3', '36', '3601', '3601', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360103, '西湖区', '3', '36', '3601', '3601', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360104, '青云谱区', '3', '36', '3601', '3601', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360105, '湾里区', '3', '36', '3601', '3601', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360111, '青山湖区', '3', '36', '3601', '3601', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360112, '新建区', '3', '36', '3601', '3601', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360121, '南昌县', '3', '36', '3601', '3601', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360123, '安义县', '3', '36', '3601', '3601', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360124, '进贤县', '3', '36', '3601', '3601', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360201, '市辖区', '3', '36', '3602', '3602', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360202, '昌江区', '3', '36', '3602', '3602', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360203, '珠山区', '3', '36', '3602', '3602', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360222, '浮梁县', '3', '36', '3602', '3602', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360281, '乐平市', '3', '36', '3602', '3602', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360301, '市辖区', '3', '36', '3603', '3603', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360302, '安源区', '3', '36', '3603', '3603', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360313, '湘东区', '3', '36', '3603', '3603', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360321, '莲花县', '3', '36', '3603', '3603', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360322, '上栗县', '3', '36', '3603', '3603', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360323, '芦溪县', '3', '36', '3603', '3603', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360401, '市辖区', '3', '36', '3604', '3604', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360402, '濂溪区', '3', '36', '3604', '3604', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360403, '浔阳区', '3', '36', '3604', '3604', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360421, '九江县', '3', '36', '3604', '3604', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360423, '武宁县', '3', '36', '3604', '3604', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360424, '修水县', '3', '36', '3604', '3604', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360425, '永修县', '3', '36', '3604', '3604', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360426, '德安县', '3', '36', '3604', '3604', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360428, '都昌县', '3', '36', '3604', '3604', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360429, '湖口县', '3', '36', '3604', '3604', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360430, '彭泽县', '3', '36', '3604', '3604', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360481, '瑞昌市', '3', '36', '3604', '3604', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360482, '共青城市', '3', '36', '3604', '3604', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360483, '庐山市', '3', '36', '3604', '3604', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360501, '市辖区', '3', '36', '3605', '3605', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360502, '渝水区', '3', '36', '3605', '3605', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360521, '分宜县', '3', '36', '3605', '3605', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360601, '市辖区', '3', '36', '3606', '3606', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360602, '月湖区', '3', '36', '3606', '3606', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360622, '余江县', '3', '36', '3606', '3606', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360681, '贵溪市', '3', '36', '3606', '3606', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360701, '市辖区', '3', '36', '3607', '3607', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360702, '章贡区', '3', '36', '3607', '3607', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360703, '南康区', '3', '36', '3607', '3607', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360721, '赣县', '3', '36', '3607', '3607', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360722, '信丰县', '3', '36', '3607', '3607', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360723, '大余县', '3', '36', '3607', '3607', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360724, '上犹县', '3', '36', '3607', '3607', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360725, '崇义县', '3', '36', '3607', '3607', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360726, '安远县', '3', '36', '3607', '3607', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360727, '龙南县', '3', '36', '3607', '3607', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360728, '定南县', '3', '36', '3607', '3607', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360729, '全南县', '3', '36', '3607', '3607', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360730, '宁都县', '3', '36', '3607', '3607', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360731, '于都县', '3', '36', '3607', '3607', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360732, '兴国县', '3', '36', '3607', '3607', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360733, '会昌县', '3', '36', '3607', '3607', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360734, '寻乌县', '3', '36', '3607', '3607', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360735, '石城县', '3', '36', '3607', '3607', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360781, '瑞金市', '3', '36', '3607', '3607', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360801, '市辖区', '3', '36', '3608', '3608', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360802, '吉州区', '3', '36', '3608', '3608', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360803, '青原区', '3', '36', '3608', '3608', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360821, '吉安县', '3', '36', '3608', '3608', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360822, '吉水县', '3', '36', '3608', '3608', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360823, '峡江县', '3', '36', '3608', '3608', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360824, '新干县', '3', '36', '3608', '3608', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360825, '永丰县', '3', '36', '3608', '3608', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360826, '泰和县', '3', '36', '3608', '3608', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360827, '遂川县', '3', '36', '3608', '3608', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360828, '万安县', '3', '36', '3608', '3608', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360829, '安福县', '3', '36', '3608', '3608', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360830, '永新县', '3', '36', '3608', '3608', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360881, '井冈山市', '3', '36', '3608', '3608', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360901, '市辖区', '3', '36', '3609', '3609', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360902, '袁州区', '3', '36', '3609', '3609', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360921, '奉新县', '3', '36', '3609', '3609', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360922, '万载县', '3', '36', '3609', '3609', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360923, '上高县', '3', '36', '3609', '3609', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360924, '宜丰县', '3', '36', '3609', '3609', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360925, '靖安县', '3', '36', '3609', '3609', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360926, '铜鼓县', '3', '36', '3609', '3609', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360981, '丰城市', '3', '36', '3609', '3609', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360982, '樟树市', '3', '36', '3609', '3609', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (360983, '高安市', '3', '36', '3609', '3609', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (361001, '市辖区', '3', '36', '3610', '3610', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (361002, '临川区', '3', '36', '3610', '3610', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (361021, '南城县', '3', '36', '3610', '3610', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (361022, '黎川县', '3', '36', '3610', '3610', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (361023, '南丰县', '3', '36', '3610', '3610', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (361024, '崇仁县', '3', '36', '3610', '3610', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (361025, '乐安县', '3', '36', '3610', '3610', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (361026, '宜黄县', '3', '36', '3610', '3610', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (361027, '金溪县', '3', '36', '3610', '3610', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (361028, '资溪县', '3', '36', '3610', '3610', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (361029, '东乡县', '3', '36', '3610', '3610', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (361030, '广昌县', '3', '36', '3610', '3610', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (361101, '市辖区', '3', '36', '3611', '3611', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (361102, '信州区', '3', '36', '3611', '3611', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (361103, '广丰区', '3', '36', '3611', '3611', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (361121, '上饶县', '3', '36', '3611', '3611', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (361123, '玉山县', '3', '36', '3611', '3611', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (361124, '铅山县', '3', '36', '3611', '3611', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (361125, '横峰县', '3', '36', '3611', '3611', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (361126, '弋阳县', '3', '36', '3611', '3611', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (361127, '余干县', '3', '36', '3611', '3611', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (361128, '鄱阳县', '3', '36', '3611', '3611', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (361129, '万年县', '3', '36', '3611', '3611', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (361130, '婺源县', '3', '36', '3611', '3611', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (361181, '德兴市', '3', '36', '3611', '3611', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370101, '市辖区', '3', '37', '3701', '3701', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370102, '历下区', '3', '37', '3701', '3701', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370103, '市中区', '3', '37', '3701', '3701', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370104, '槐荫区', '3', '37', '3701', '3701', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370105, '天桥区', '3', '37', '3701', '3701', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370112, '历城区', '3', '37', '3701', '3701', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370113, '长清区', '3', '37', '3701', '3701', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370124, '平阴县', '3', '37', '3701', '3701', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370125, '济阳县', '3', '37', '3701', '3701', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370126, '商河县', '3', '37', '3701', '3701', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370181, '章丘市', '3', '37', '3701', '3701', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370201, '市辖区', '3', '37', '3702', '3702', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370202, '市南区', '3', '37', '3702', '3702', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370203, '市北区', '3', '37', '3702', '3702', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370211, '黄岛区', '3', '37', '3702', '3702', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370212, '崂山区', '3', '37', '3702', '3702', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370213, '李沧区', '3', '37', '3702', '3702', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370214, '城阳区', '3', '37', '3702', '3702', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370281, '胶州市', '3', '37', '3702', '3702', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370282, '即墨市', '3', '37', '3702', '3702', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370283, '平度市', '3', '37', '3702', '3702', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370285, '莱西市', '3', '37', '3702', '3702', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370301, '市辖区', '3', '37', '3703', '3703', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370302, '淄川区', '3', '37', '3703', '3703', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370303, '张店区', '3', '37', '3703', '3703', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370304, '博山区', '3', '37', '3703', '3703', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370305, '临淄区', '3', '37', '3703', '3703', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370306, '周村区', '3', '37', '3703', '3703', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370321, '桓台县', '3', '37', '3703', '3703', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370322, '高青县', '3', '37', '3703', '3703', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370323, '沂源县', '3', '37', '3703', '3703', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370401, '市辖区', '3', '37', '3704', '3704', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370402, '市中区', '3', '37', '3704', '3704', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370403, '薛城区', '3', '37', '3704', '3704', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370404, '峄城区', '3', '37', '3704', '3704', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370405, '台儿庄区', '3', '37', '3704', '3704', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370406, '山亭区', '3', '37', '3704', '3704', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370481, '滕州市', '3', '37', '3704', '3704', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370501, '市辖区', '3', '37', '3705', '3705', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370502, '东营区', '3', '37', '3705', '3705', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370503, '河口区', '3', '37', '3705', '3705', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370505, '垦利区', '3', '37', '3705', '3705', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370522, '利津县', '3', '37', '3705', '3705', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370523, '广饶县', '3', '37', '3705', '3705', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370601, '市辖区', '3', '37', '3706', '3706', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370602, '芝罘区', '3', '37', '3706', '3706', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370611, '福山区', '3', '37', '3706', '3706', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370612, '牟平区', '3', '37', '3706', '3706', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370613, '莱山区', '3', '37', '3706', '3706', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370634, '长岛县', '3', '37', '3706', '3706', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370681, '龙口市', '3', '37', '3706', '3706', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370682, '莱阳市', '3', '37', '3706', '3706', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370683, '莱州市', '3', '37', '3706', '3706', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370684, '蓬莱市', '3', '37', '3706', '3706', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370685, '招远市', '3', '37', '3706', '3706', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370686, '栖霞市', '3', '37', '3706', '3706', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370687, '海阳市', '3', '37', '3706', '3706', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370701, '市辖区', '3', '37', '3707', '3707', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370702, '潍城区', '3', '37', '3707', '3707', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370703, '寒亭区', '3', '37', '3707', '3707', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370704, '坊子区', '3', '37', '3707', '3707', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370705, '奎文区', '3', '37', '3707', '3707', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370724, '临朐县', '3', '37', '3707', '3707', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370725, '昌乐县', '3', '37', '3707', '3707', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370781, '青州市', '3', '37', '3707', '3707', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370782, '诸城市', '3', '37', '3707', '3707', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370783, '寿光市', '3', '37', '3707', '3707', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370784, '安丘市', '3', '37', '3707', '3707', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370785, '高密市', '3', '37', '3707', '3707', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370786, '昌邑市', '3', '37', '3707', '3707', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370801, '市辖区', '3', '37', '3708', '3708', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370811, '任城区', '3', '37', '3708', '3708', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370812, '兖州区', '3', '37', '3708', '3708', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370826, '微山县', '3', '37', '3708', '3708', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370827, '鱼台县', '3', '37', '3708', '3708', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370828, '金乡县', '3', '37', '3708', '3708', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370829, '嘉祥县', '3', '37', '3708', '3708', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370830, '汶上县', '3', '37', '3708', '3708', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370831, '泗水县', '3', '37', '3708', '3708', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370832, '梁山县', '3', '37', '3708', '3708', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370881, '曲阜市', '3', '37', '3708', '3708', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370883, '邹城市', '3', '37', '3708', '3708', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370901, '市辖区', '3', '37', '3709', '3709', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370902, '泰山区', '3', '37', '3709', '3709', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370911, '岱岳区', '3', '37', '3709', '3709', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370921, '宁阳县', '3', '37', '3709', '3709', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370923, '东平县', '3', '37', '3709', '3709', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370982, '新泰市', '3', '37', '3709', '3709', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (370983, '肥城市', '3', '37', '3709', '3709', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (371001, '市辖区', '3', '37', '3710', '3710', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (371002, '环翠区', '3', '37', '3710', '3710', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (371003, '文登区', '3', '37', '3710', '3710', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (371082, '荣成市', '3', '37', '3710', '3710', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (371083, '乳山市', '3', '37', '3710', '3710', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (371101, '市辖区', '3', '37', '3711', '3711', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (371102, '东港区', '3', '37', '3711', '3711', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (371103, '岚山区', '3', '37', '3711', '3711', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (371121, '五莲县', '3', '37', '3711', '3711', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (371122, '莒县', '3', '37', '3711', '3711', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (371201, '市辖区', '3', '37', '3712', '3712', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (371202, '莱城区', '3', '37', '3712', '3712', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (371203, '钢城区', '3', '37', '3712', '3712', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (371301, '市辖区', '3', '37', '3713', '3713', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (371302, '兰山区', '3', '37', '3713', '3713', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (371311, '罗庄区', '3', '37', '3713', '3713', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (371312, '河东区', '3', '37', '3713', '3713', 0, 541, '', '');
INSERT INTO `sq_region` VALUES (371321, '沂南县', '3', '37', '3713', '3713', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (371322, '郯城县', '3', '37', '3713', '3713', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (371323, '沂水县', '3', '37', '3713', '3713', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (371324, '兰陵县', '3', '37', '3713', '3713', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (371325, '费县', '3', '37', '3713', '3713', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (371326, '平邑县', '3', '37', '3713', '3713', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (371327, '莒南县', '3', '37', '3713', '3713', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (371328, '蒙阴县', '3', '37', '3713', '3713', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (371329, '临沭县', '3', '37', '3713', '3713', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (371401, '市辖区', '3', '37', '3714', '3714', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (371402, '德城区', '3', '37', '3714', '3714', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (371403, '陵城区', '3', '37', '3714', '3714', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (371422, '宁津县', '3', '37', '3714', '3714', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (371423, '庆云县', '3', '37', '3714', '3714', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (371424, '临邑县', '3', '37', '3714', '3714', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (371425, '齐河县', '3', '37', '3714', '3714', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (371426, '平原县', '3', '37', '3714', '3714', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (371427, '夏津县', '3', '37', '3714', '3714', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (371428, '武城县', '3', '37', '3714', '3714', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (371481, '乐陵市', '3', '37', '3714', '3714', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (371482, '禹城市', '3', '37', '3714', '3714', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (371501, '市辖区', '3', '37', '3715', '3715', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (371502, '东昌府区', '3', '37', '3715', '3715', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (371521, '阳谷县', '3', '37', '3715', '3715', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (371522, '莘县', '3', '37', '3715', '3715', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (371523, '茌平县', '3', '37', '3715', '3715', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (371524, '东阿县', '3', '37', '3715', '3715', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (371525, '冠县', '3', '37', '3715', '3715', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (371526, '高唐县', '3', '37', '3715', '3715', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (371581, '临清市', '3', '37', '3715', '3715', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (371601, '市辖区', '3', '37', '3716', '3716', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (371602, '滨城区', '3', '37', '3716', '3716', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (371603, '沾化区', '3', '37', '3716', '3716', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (371621, '惠民县', '3', '37', '3716', '3716', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (371622, '阳信县', '3', '37', '3716', '3716', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (371623, '无棣县', '3', '37', '3716', '3716', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (371625, '博兴县', '3', '37', '3716', '3716', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (371626, '邹平县', '3', '37', '3716', '3716', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (371701, '市辖区', '3', '37', '3717', '3717', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (371702, '牡丹区', '3', '37', '3717', '3717', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (371703, '定陶区', '3', '37', '3717', '3717', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (371721, '曹县', '3', '37', '3717', '3717', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (371722, '单县', '3', '37', '3717', '3717', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (371723, '成武县', '3', '37', '3717', '3717', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (371724, '巨野县', '3', '37', '3717', '3717', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (371725, '郓城县', '3', '37', '3717', '3717', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (371726, '鄄城县', '3', '37', '3717', '3717', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (371728, '东明县', '3', '37', '3717', '3717', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410101, '市辖区', '3', '41', '4101', '4101', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410102, '中原区', '3', '41', '4101', '4101', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410103, '二七区', '3', '41', '4101', '4101', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410104, '管城回族区', '3', '41', '4101', '4101', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410105, '金水区', '3', '41', '4101', '4101', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410106, '上街区', '3', '41', '4101', '4101', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410108, '惠济区', '3', '41', '4101', '4101', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410122, '中牟县', '3', '41', '4101', '4101', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410181, '巩义市', '3', '41', '4101', '4101', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410182, '荥阳市', '3', '41', '4101', '4101', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410183, '新密市', '3', '41', '4101', '4101', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410184, '新郑市', '3', '41', '4101', '4101', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410185, '登封市', '3', '41', '4101', '4101', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410201, '市辖区', '3', '41', '4102', '4102', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410202, '龙亭区', '3', '41', '4102', '4102', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410203, '顺河回族区', '3', '41', '4102', '4102', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410204, '鼓楼区', '3', '41', '4102', '4102', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410205, '禹王台区', '3', '41', '4102', '4102', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410211, '金明区', '3', '41', '4102', '4102', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410212, '祥符区', '3', '41', '4102', '4102', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410221, '杞县', '3', '41', '4102', '4102', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410222, '通许县', '3', '41', '4102', '4102', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410223, '尉氏县', '3', '41', '4102', '4102', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410225, '兰考县', '3', '41', '4102', '4102', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410301, '市辖区', '3', '41', '4103', '4103', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410302, '老城区', '3', '41', '4103', '4103', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410303, '西工区', '3', '41', '4103', '4103', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410304, '瀍河回族区', '3', '41', '4103', '4103', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410305, '涧西区', '3', '41', '4103', '4103', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410306, '吉利区', '3', '41', '4103', '4103', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410311, '洛龙区', '3', '41', '4103', '4103', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410322, '孟津县', '3', '41', '4103', '4103', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410323, '新安县', '3', '41', '4103', '4103', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410324, '栾川县', '3', '41', '4103', '4103', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410325, '嵩县', '3', '41', '4103', '4103', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410326, '汝阳县', '3', '41', '4103', '4103', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410327, '宜阳县', '3', '41', '4103', '4103', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410328, '洛宁县', '3', '41', '4103', '4103', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410329, '伊川县', '3', '41', '4103', '4103', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410381, '偃师市', '3', '41', '4103', '4103', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410401, '市辖区', '3', '41', '4104', '4104', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410402, '新华区', '3', '41', '4104', '4104', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410403, '卫东区', '3', '41', '4104', '4104', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410404, '石龙区', '3', '41', '4104', '4104', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410411, '湛河区', '3', '41', '4104', '4104', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410421, '宝丰县', '3', '41', '4104', '4104', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410422, '叶县', '3', '41', '4104', '4104', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410423, '鲁山县', '3', '41', '4104', '4104', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410425, '郏县', '3', '41', '4104', '4104', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410481, '舞钢市', '3', '41', '4104', '4104', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410482, '汝州市', '3', '41', '4104', '4104', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410501, '市辖区', '3', '41', '4105', '4105', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410502, '文峰区', '3', '41', '4105', '4105', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410503, '北关区', '3', '41', '4105', '4105', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410505, '殷都区', '3', '41', '4105', '4105', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410506, '龙安区', '3', '41', '4105', '4105', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410522, '安阳县', '3', '41', '4105', '4105', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410523, '汤阴县', '3', '41', '4105', '4105', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410526, '滑县', '3', '41', '4105', '4105', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410527, '内黄县', '3', '41', '4105', '4105', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410581, '林州市', '3', '41', '4105', '4105', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410601, '市辖区', '3', '41', '4106', '4106', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410602, '鹤山区', '3', '41', '4106', '4106', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410603, '山城区', '3', '41', '4106', '4106', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410611, '淇滨区', '3', '41', '4106', '4106', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410621, '浚县', '3', '41', '4106', '4106', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410622, '淇县', '3', '41', '4106', '4106', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410701, '市辖区', '3', '41', '4107', '4107', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410702, '红旗区', '3', '41', '4107', '4107', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410703, '卫滨区', '3', '41', '4107', '4107', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410704, '凤泉区', '3', '41', '4107', '4107', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410711, '牧野区', '3', '41', '4107', '4107', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410721, '新乡县', '3', '41', '4107', '4107', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410724, '获嘉县', '3', '41', '4107', '4107', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410725, '原阳县', '3', '41', '4107', '4107', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410726, '延津县', '3', '41', '4107', '4107', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410727, '封丘县', '3', '41', '4107', '4107', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410728, '长垣县', '3', '41', '4107', '4107', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410781, '卫辉市', '3', '41', '4107', '4107', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410782, '辉县市', '3', '41', '4107', '4107', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410801, '市辖区', '3', '41', '4108', '4108', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410802, '解放区', '3', '41', '4108', '4108', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410803, '中站区', '3', '41', '4108', '4108', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410804, '马村区', '3', '41', '4108', '4108', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410811, '山阳区', '3', '41', '4108', '4108', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410821, '修武县', '3', '41', '4108', '4108', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410822, '博爱县', '3', '41', '4108', '4108', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410823, '武陟县', '3', '41', '4108', '4108', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410825, '温县', '3', '41', '4108', '4108', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410882, '沁阳市', '3', '41', '4108', '4108', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410883, '孟州市', '3', '41', '4108', '4108', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410901, '市辖区', '3', '41', '4109', '4109', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410902, '华龙区', '3', '41', '4109', '4109', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410922, '清丰县', '3', '41', '4109', '4109', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410923, '南乐县', '3', '41', '4109', '4109', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410926, '范县', '3', '41', '4109', '4109', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410927, '台前县', '3', '41', '4109', '4109', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (410928, '濮阳县', '3', '41', '4109', '4109', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (411001, '市辖区', '3', '41', '4110', '4110', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (411002, '魏都区', '3', '41', '4110', '4110', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (411023, '许昌县', '3', '41', '4110', '4110', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (411024, '鄢陵县', '3', '41', '4110', '4110', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (411025, '襄城县', '3', '41', '4110', '4110', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (411081, '禹州市', '3', '41', '4110', '4110', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (411082, '长葛市', '3', '41', '4110', '4110', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (411101, '市辖区', '3', '41', '4111', '4111', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (411102, '源汇区', '3', '41', '4111', '4111', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (411103, '郾城区', '3', '41', '4111', '4111', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (411104, '召陵区', '3', '41', '4111', '4111', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (411121, '舞阳县', '3', '41', '4111', '4111', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (411122, '临颍县', '3', '41', '4111', '4111', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (411201, '市辖区', '3', '41', '4112', '4112', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (411202, '湖滨区', '3', '41', '4112', '4112', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (411203, '陕州区', '3', '41', '4112', '4112', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (411221, '渑池县', '3', '41', '4112', '4112', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (411224, '卢氏县', '3', '41', '4112', '4112', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (411281, '义马市', '3', '41', '4112', '4112', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (411282, '灵宝市', '3', '41', '4112', '4112', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (411301, '市辖区', '3', '41', '4113', '4113', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (411302, '宛城区', '3', '41', '4113', '4113', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (411303, '卧龙区', '3', '41', '4113', '4113', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (411321, '南召县', '3', '41', '4113', '4113', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (411322, '方城县', '3', '41', '4113', '4113', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (411323, '西峡县', '3', '41', '4113', '4113', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (411324, '镇平县', '3', '41', '4113', '4113', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (411325, '内乡县', '3', '41', '4113', '4113', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (411326, '淅川县', '3', '41', '4113', '4113', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (411327, '社旗县', '3', '41', '4113', '4113', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (411328, '唐河县', '3', '41', '4113', '4113', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (411329, '新野县', '3', '41', '4113', '4113', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (411330, '桐柏县', '3', '41', '4113', '4113', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (411381, '邓州市', '3', '41', '4113', '4113', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (411401, '市辖区', '3', '41', '4114', '4114', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (411402, '梁园区', '3', '41', '4114', '4114', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (411403, '睢阳区', '3', '41', '4114', '4114', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (411421, '民权县', '3', '41', '4114', '4114', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (411422, '睢县', '3', '41', '4114', '4114', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (411423, '宁陵县', '3', '41', '4114', '4114', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (411424, '柘城县', '3', '41', '4114', '4114', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (411425, '虞城县', '3', '41', '4114', '4114', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (411426, '夏邑县', '3', '41', '4114', '4114', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (411481, '永城市', '3', '41', '4114', '4114', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (411501, '市辖区', '3', '41', '4115', '4115', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (411502, '浉河区', '3', '41', '4115', '4115', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (411503, '平桥区', '3', '41', '4115', '4115', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (411521, '罗山县', '3', '41', '4115', '4115', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (411522, '光山县', '3', '41', '4115', '4115', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (411523, '新县', '3', '41', '4115', '4115', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (411524, '商城县', '3', '41', '4115', '4115', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (411525, '固始县', '3', '41', '4115', '4115', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (411526, '潢川县', '3', '41', '4115', '4115', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (411527, '淮滨县', '3', '41', '4115', '4115', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (411528, '息县', '3', '41', '4115', '4115', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (411601, '市辖区', '3', '41', '4116', '4116', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (411602, '川汇区', '3', '41', '4116', '4116', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (411621, '扶沟县', '3', '41', '4116', '4116', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (411622, '西华县', '3', '41', '4116', '4116', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (411623, '商水县', '3', '41', '4116', '4116', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (411624, '沈丘县', '3', '41', '4116', '4116', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (411625, '郸城县', '3', '41', '4116', '4116', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (411626, '淮阳县', '3', '41', '4116', '4116', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (411627, '太康县', '3', '41', '4116', '4116', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (411628, '鹿邑县', '3', '41', '4116', '4116', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (411681, '项城市', '3', '41', '4116', '4116', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (411701, '市辖区', '3', '41', '4117', '4117', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (411702, '驿城区', '3', '41', '4117', '4117', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (411721, '西平县', '3', '41', '4117', '4117', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (411722, '上蔡县', '3', '41', '4117', '4117', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (411723, '平舆县', '3', '41', '4117', '4117', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (411724, '正阳县', '3', '41', '4117', '4117', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (411725, '确山县', '3', '41', '4117', '4117', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (411726, '泌阳县', '3', '41', '4117', '4117', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (411727, '汝南县', '3', '41', '4117', '4117', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (411728, '遂平县', '3', '41', '4117', '4117', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (411729, '新蔡县', '3', '41', '4117', '4117', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (419001, '济源市', '3', '41', '4190', '4190', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (420101, '市辖区', '3', '42', '4201', '4201', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (420102, '江岸区', '3', '42', '4201', '4201', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (420103, '江汉区', '3', '42', '4201', '4201', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (420104, '硚口区', '3', '42', '4201', '4201', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (420105, '汉阳区', '3', '42', '4201', '4201', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (420106, '武昌区', '3', '42', '4201', '4201', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (420107, '青山区', '3', '42', '4201', '4201', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (420111, '洪山区', '3', '42', '4201', '4201', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (420112, '东西湖区', '3', '42', '4201', '4201', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (420113, '汉南区', '3', '42', '4201', '4201', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (420114, '蔡甸区', '3', '42', '4201', '4201', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (420115, '江夏区', '3', '42', '4201', '4201', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (420116, '黄陂区', '3', '42', '4201', '4201', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (420117, '新洲区', '3', '42', '4201', '4201', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (420201, '市辖区', '3', '42', '4202', '4202', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (420202, '黄石港区', '3', '42', '4202', '4202', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (420203, '西塞山区', '3', '42', '4202', '4202', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (420204, '下陆区', '3', '42', '4202', '4202', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (420205, '铁山区', '3', '42', '4202', '4202', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (420222, '阳新县', '3', '42', '4202', '4202', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (420281, '大冶市', '3', '42', '4202', '4202', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (420301, '市辖区', '3', '42', '4203', '4203', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (420302, '茅箭区', '3', '42', '4203', '4203', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (420303, '张湾区', '3', '42', '4203', '4203', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (420304, '郧阳区', '3', '42', '4203', '4203', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (420322, '郧西县', '3', '42', '4203', '4203', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (420323, '竹山县', '3', '42', '4203', '4203', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (420324, '竹溪县', '3', '42', '4203', '4203', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (420325, '房县', '3', '42', '4203', '4203', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (420381, '丹江口市', '3', '42', '4203', '4203', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (420501, '市辖区', '3', '42', '4205', '4205', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (420502, '西陵区', '3', '42', '4205', '4205', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (420503, '伍家岗区', '3', '42', '4205', '4205', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (420504, '点军区', '3', '42', '4205', '4205', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (420505, '猇亭区', '3', '42', '4205', '4205', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (420506, '夷陵区', '3', '42', '4205', '4205', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (420525, '远安县', '3', '42', '4205', '4205', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (420526, '兴山县', '3', '42', '4205', '4205', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (420527, '秭归县', '3', '42', '4205', '4205', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (420528, '长阳土家族自治县', '3', '42', '4205', '4205', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (420529, '五峰土家族自治县', '3', '42', '4205', '4205', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (420581, '宜都市', '3', '42', '4205', '4205', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (420582, '当阳市', '3', '42', '4205', '4205', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (420583, '枝江市', '3', '42', '4205', '4205', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (420601, '市辖区', '3', '42', '4206', '4206', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (420602, '襄城区', '3', '42', '4206', '4206', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (420606, '樊城区', '3', '42', '4206', '4206', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (420607, '襄州区', '3', '42', '4206', '4206', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (420624, '南漳县', '3', '42', '4206', '4206', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (420625, '谷城县', '3', '42', '4206', '4206', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (420626, '保康县', '3', '42', '4206', '4206', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (420682, '老河口市', '3', '42', '4206', '4206', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (420683, '枣阳市', '3', '42', '4206', '4206', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (420684, '宜城市', '3', '42', '4206', '4206', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (420701, '市辖区', '3', '42', '4207', '4207', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (420702, '梁子湖区', '3', '42', '4207', '4207', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (420703, '华容区', '3', '42', '4207', '4207', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (420704, '鄂城区', '3', '42', '4207', '4207', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (420801, '市辖区', '3', '42', '4208', '4208', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (420802, '东宝区', '3', '42', '4208', '4208', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (420804, '掇刀区', '3', '42', '4208', '4208', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (420821, '京山县', '3', '42', '4208', '4208', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (420822, '沙洋县', '3', '42', '4208', '4208', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (420881, '钟祥市', '3', '42', '4208', '4208', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (420901, '市辖区', '3', '42', '4209', '4209', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (420902, '孝南区', '3', '42', '4209', '4209', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (420921, '孝昌县', '3', '42', '4209', '4209', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (420922, '大悟县', '3', '42', '4209', '4209', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (420923, '云梦县', '3', '42', '4209', '4209', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (420981, '应城市', '3', '42', '4209', '4209', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (420982, '安陆市', '3', '42', '4209', '4209', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (420984, '汉川市', '3', '42', '4209', '4209', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (421001, '市辖区', '3', '42', '4210', '4210', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (421002, '沙市区', '3', '42', '4210', '4210', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (421003, '荆州区', '3', '42', '4210', '4210', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (421022, '公安县', '3', '42', '4210', '4210', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (421023, '监利县', '3', '42', '4210', '4210', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (421024, '江陵县', '3', '42', '4210', '4210', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (421081, '石首市', '3', '42', '4210', '4210', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (421083, '洪湖市', '3', '42', '4210', '4210', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (421087, '松滋市', '3', '42', '4210', '4210', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (421101, '市辖区', '3', '42', '4211', '4211', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (421102, '黄州区', '3', '42', '4211', '4211', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (421121, '团风县', '3', '42', '4211', '4211', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (421122, '红安县', '3', '42', '4211', '4211', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (421123, '罗田县', '3', '42', '4211', '4211', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (421124, '英山县', '3', '42', '4211', '4211', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (421125, '浠水县', '3', '42', '4211', '4211', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (421126, '蕲春县', '3', '42', '4211', '4211', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (421127, '黄梅县', '3', '42', '4211', '4211', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (421181, '麻城市', '3', '42', '4211', '4211', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (421182, '武穴市', '3', '42', '4211', '4211', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (421201, '市辖区', '3', '42', '4212', '4212', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (421202, '咸安区', '3', '42', '4212', '4212', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (421221, '嘉鱼县', '3', '42', '4212', '4212', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (421222, '通城县', '3', '42', '4212', '4212', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (421223, '崇阳县', '3', '42', '4212', '4212', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (421224, '通山县', '3', '42', '4212', '4212', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (421281, '赤壁市', '3', '42', '4212', '4212', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (421301, '市辖区', '3', '42', '4213', '4213', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (421303, '曾都区', '3', '42', '4213', '4213', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (421321, '随县', '3', '42', '4213', '4213', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (421381, '广水市', '3', '42', '4213', '4213', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (422801, '恩施市', '3', '42', '4228', '4228', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (422802, '利川市', '3', '42', '4228', '4228', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (422822, '建始县', '3', '42', '4228', '4228', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (422823, '巴东县', '3', '42', '4228', '4228', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (422825, '宣恩县', '3', '42', '4228', '4228', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (422826, '咸丰县', '3', '42', '4228', '4228', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (422827, '来凤县', '3', '42', '4228', '4228', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (422828, '鹤峰县', '3', '42', '4228', '4228', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (429004, '仙桃市', '3', '42', '4290', '4290', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (429005, '潜江市', '3', '42', '4290', '4290', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (429006, '天门市', '3', '42', '4290', '4290', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (429021, '神农架林区', '3', '42', '4290', '4290', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430101, '市辖区', '3', '43', '4301', '4301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430102, '芙蓉区', '3', '43', '4301', '4301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430103, '天心区', '3', '43', '4301', '4301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430104, '岳麓区', '3', '43', '4301', '4301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430105, '开福区', '3', '43', '4301', '4301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430111, '雨花区', '3', '43', '4301', '4301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430112, '望城区', '3', '43', '4301', '4301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430121, '长沙县', '3', '43', '4301', '4301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430124, '宁乡县', '3', '43', '4301', '4301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430181, '浏阳市', '3', '43', '4301', '4301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430201, '市辖区', '3', '43', '4302', '4302', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430202, '荷塘区', '3', '43', '4302', '4302', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430203, '芦淞区', '3', '43', '4302', '4302', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430204, '石峰区', '3', '43', '4302', '4302', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430211, '天元区', '3', '43', '4302', '4302', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430221, '株洲县', '3', '43', '4302', '4302', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430223, '攸县', '3', '43', '4302', '4302', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430224, '茶陵县', '3', '43', '4302', '4302', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430225, '炎陵县', '3', '43', '4302', '4302', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430281, '醴陵市', '3', '43', '4302', '4302', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430301, '市辖区', '3', '43', '4303', '4303', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430302, '雨湖区', '3', '43', '4303', '4303', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430304, '岳塘区', '3', '43', '4303', '4303', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430321, '湘潭县', '3', '43', '4303', '4303', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430381, '湘乡市', '3', '43', '4303', '4303', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430382, '韶山市', '3', '43', '4303', '4303', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430401, '市辖区', '3', '43', '4304', '4304', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430405, '珠晖区', '3', '43', '4304', '4304', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430406, '雁峰区', '3', '43', '4304', '4304', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430407, '石鼓区', '3', '43', '4304', '4304', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430408, '蒸湘区', '3', '43', '4304', '4304', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430412, '南岳区', '3', '43', '4304', '4304', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430421, '衡阳县', '3', '43', '4304', '4304', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430422, '衡南县', '3', '43', '4304', '4304', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430423, '衡山县', '3', '43', '4304', '4304', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430424, '衡东县', '3', '43', '4304', '4304', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430426, '祁东县', '3', '43', '4304', '4304', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430481, '耒阳市', '3', '43', '4304', '4304', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430482, '常宁市', '3', '43', '4304', '4304', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430501, '市辖区', '3', '43', '4305', '4305', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430502, '双清区', '3', '43', '4305', '4305', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430503, '大祥区', '3', '43', '4305', '4305', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430511, '北塔区', '3', '43', '4305', '4305', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430521, '邵东县', '3', '43', '4305', '4305', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430522, '新邵县', '3', '43', '4305', '4305', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430523, '邵阳县', '3', '43', '4305', '4305', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430524, '隆回县', '3', '43', '4305', '4305', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430525, '洞口县', '3', '43', '4305', '4305', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430527, '绥宁县', '3', '43', '4305', '4305', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430528, '新宁县', '3', '43', '4305', '4305', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430529, '城步苗族自治县', '3', '43', '4305', '4305', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430581, '武冈市', '3', '43', '4305', '4305', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430601, '市辖区', '3', '43', '4306', '4306', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430602, '岳阳楼区', '3', '43', '4306', '4306', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430603, '云溪区', '3', '43', '4306', '4306', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430611, '君山区', '3', '43', '4306', '4306', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430621, '岳阳县', '3', '43', '4306', '4306', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430623, '华容县', '3', '43', '4306', '4306', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430624, '湘阴县', '3', '43', '4306', '4306', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430626, '平江县', '3', '43', '4306', '4306', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430681, '汨罗市', '3', '43', '4306', '4306', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430682, '临湘市', '3', '43', '4306', '4306', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430701, '市辖区', '3', '43', '4307', '4307', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430702, '武陵区', '3', '43', '4307', '4307', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430703, '鼎城区', '3', '43', '4307', '4307', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430721, '安乡县', '3', '43', '4307', '4307', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430722, '汉寿县', '3', '43', '4307', '4307', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430723, '澧县', '3', '43', '4307', '4307', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430724, '临澧县', '3', '43', '4307', '4307', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430725, '桃源县', '3', '43', '4307', '4307', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430726, '石门县', '3', '43', '4307', '4307', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430781, '津市市', '3', '43', '4307', '4307', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430801, '市辖区', '3', '43', '4308', '4308', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430802, '永定区', '3', '43', '4308', '4308', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430811, '武陵源区', '3', '43', '4308', '4308', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430821, '慈利县', '3', '43', '4308', '4308', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430822, '桑植县', '3', '43', '4308', '4308', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430901, '市辖区', '3', '43', '4309', '4309', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430902, '资阳区', '3', '43', '4309', '4309', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430903, '赫山区', '3', '43', '4309', '4309', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430921, '南县', '3', '43', '4309', '4309', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430922, '桃江县', '3', '43', '4309', '4309', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430923, '安化县', '3', '43', '4309', '4309', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (430981, '沅江市', '3', '43', '4309', '4309', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (431001, '市辖区', '3', '43', '4310', '4310', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (431002, '北湖区', '3', '43', '4310', '4310', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (431003, '苏仙区', '3', '43', '4310', '4310', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (431021, '桂阳县', '3', '43', '4310', '4310', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (431022, '宜章县', '3', '43', '4310', '4310', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (431023, '永兴县', '3', '43', '4310', '4310', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (431024, '嘉禾县', '3', '43', '4310', '4310', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (431025, '临武县', '3', '43', '4310', '4310', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (431026, '汝城县', '3', '43', '4310', '4310', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (431027, '桂东县', '3', '43', '4310', '4310', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (431028, '安仁县', '3', '43', '4310', '4310', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (431081, '资兴市', '3', '43', '4310', '4310', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (431101, '市辖区', '3', '43', '4311', '4311', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (431102, '零陵区', '3', '43', '4311', '4311', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (431103, '冷水滩区', '3', '43', '4311', '4311', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (431121, '祁阳县', '3', '43', '4311', '4311', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (431122, '东安县', '3', '43', '4311', '4311', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (431123, '双牌县', '3', '43', '4311', '4311', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (431124, '道县', '3', '43', '4311', '4311', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (431125, '江永县', '3', '43', '4311', '4311', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (431126, '宁远县', '3', '43', '4311', '4311', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (431127, '蓝山县', '3', '43', '4311', '4311', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (431128, '新田县', '3', '43', '4311', '4311', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (431129, '江华瑶族自治县', '3', '43', '4311', '4311', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (431201, '市辖区', '3', '43', '4312', '4312', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (431202, '鹤城区', '3', '43', '4312', '4312', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (431221, '中方县', '3', '43', '4312', '4312', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (431222, '沅陵县', '3', '43', '4312', '4312', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (431223, '辰溪县', '3', '43', '4312', '4312', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (431224, '溆浦县', '3', '43', '4312', '4312', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (431225, '会同县', '3', '43', '4312', '4312', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (431226, '麻阳苗族自治县', '3', '43', '4312', '4312', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (431227, '新晃侗族自治县', '3', '43', '4312', '4312', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (431228, '芷江侗族自治县', '3', '43', '4312', '4312', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (431229, '靖州苗族侗族自治县', '3', '43', '4312', '4312', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (431230, '通道侗族自治县', '3', '43', '4312', '4312', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (431281, '洪江市', '3', '43', '4312', '4312', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (431301, '市辖区', '3', '43', '4313', '4313', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (431302, '娄星区', '3', '43', '4313', '4313', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (431321, '双峰县', '3', '43', '4313', '4313', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (431322, '新化县', '3', '43', '4313', '4313', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (431381, '冷水江市', '3', '43', '4313', '4313', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (431382, '涟源市', '3', '43', '4313', '4313', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (433101, '吉首市', '3', '43', '4331', '4331', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (433122, '泸溪县', '3', '43', '4331', '4331', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (433123, '凤凰县', '3', '43', '4331', '4331', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (433124, '花垣县', '3', '43', '4331', '4331', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (433125, '保靖县', '3', '43', '4331', '4331', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (433126, '古丈县', '3', '43', '4331', '4331', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (433127, '永顺县', '3', '43', '4331', '4331', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (433130, '龙山县', '3', '43', '4331', '4331', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (440101, '市辖区', '3', '44', '4401', '4401', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (440103, '荔湾区', '3', '44', '4401', '4401', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (440104, '越秀区', '3', '44', '4401', '4401', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (440105, '海珠区', '3', '44', '4401', '4401', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (440106, '天河区', '3', '44', '4401', '4401', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (440111, '白云区', '3', '44', '4401', '4401', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (440112, '黄埔区', '3', '44', '4401', '4401', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (440113, '番禺区', '3', '44', '4401', '4401', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (440114, '花都区', '3', '44', '4401', '4401', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (440115, '南沙区', '3', '44', '4401', '4401', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (440117, '从化区', '3', '44', '4401', '4401', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (440118, '增城区', '3', '44', '4401', '4401', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (440201, '市辖区', '3', '44', '4402', '4402', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (440203, '武江区', '3', '44', '4402', '4402', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (440204, '浈江区', '3', '44', '4402', '4402', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (440205, '曲江区', '3', '44', '4402', '4402', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (440222, '始兴县', '3', '44', '4402', '4402', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (440224, '仁化县', '3', '44', '4402', '4402', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (440229, '翁源县', '3', '44', '4402', '4402', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (440232, '乳源瑶族自治县', '3', '44', '4402', '4402', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (440233, '新丰县', '3', '44', '4402', '4402', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (440281, '乐昌市', '3', '44', '4402', '4402', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (440282, '南雄市', '3', '44', '4402', '4402', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (440301, '市辖区', '3', '44', '4403', '4403', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (440303, '罗湖区', '3', '44', '4403', '4403', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (440304, '福田区', '3', '44', '4403', '4403', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (440305, '南山区', '3', '44', '4403', '4403', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (440306, '宝安区', '3', '44', '4403', '4403', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (440307, '龙岗区', '3', '44', '4403', '4403', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (440308, '盐田区', '3', '44', '4403', '4403', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (440401, '市辖区', '3', '44', '4404', '4404', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (440402, '香洲区', '3', '44', '4404', '4404', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (440403, '斗门区', '3', '44', '4404', '4404', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (440404, '金湾区', '3', '44', '4404', '4404', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (440501, '市辖区', '3', '44', '4405', '4405', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (440507, '龙湖区', '3', '44', '4405', '4405', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (440511, '金平区', '3', '44', '4405', '4405', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (440512, '濠江区', '3', '44', '4405', '4405', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (440513, '潮阳区', '3', '44', '4405', '4405', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (440514, '潮南区', '3', '44', '4405', '4405', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (440515, '澄海区', '3', '44', '4405', '4405', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (440523, '南澳县', '3', '44', '4405', '4405', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (440601, '市辖区', '3', '44', '4406', '4406', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (440604, '禅城区', '3', '44', '4406', '4406', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (440605, '南海区', '3', '44', '4406', '4406', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (440606, '顺德区', '3', '44', '4406', '4406', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (440607, '三水区', '3', '44', '4406', '4406', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (440608, '高明区', '3', '44', '4406', '4406', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (440701, '市辖区', '3', '44', '4407', '4407', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (440703, '蓬江区', '3', '44', '4407', '4407', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (440704, '江海区', '3', '44', '4407', '4407', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (440705, '新会区', '3', '44', '4407', '4407', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (440781, '台山市', '3', '44', '4407', '4407', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (440783, '开平市', '3', '44', '4407', '4407', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (440784, '鹤山市', '3', '44', '4407', '4407', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (440785, '恩平市', '3', '44', '4407', '4407', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (440801, '市辖区', '3', '44', '4408', '4408', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (440802, '赤坎区', '3', '44', '4408', '4408', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (440803, '霞山区', '3', '44', '4408', '4408', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (440804, '坡头区', '3', '44', '4408', '4408', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (440811, '麻章区', '3', '44', '4408', '4408', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (440823, '遂溪县', '3', '44', '4408', '4408', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (440825, '徐闻县', '3', '44', '4408', '4408', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (440881, '廉江市', '3', '44', '4408', '4408', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (440882, '雷州市', '3', '44', '4408', '4408', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (440883, '吴川市', '3', '44', '4408', '4408', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (440901, '市辖区', '3', '44', '4409', '4409', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (440902, '茂南区', '3', '44', '4409', '4409', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (440904, '电白区', '3', '44', '4409', '4409', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (440981, '高州市', '3', '44', '4409', '4409', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (440982, '化州市', '3', '44', '4409', '4409', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (440983, '信宜市', '3', '44', '4409', '4409', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (441201, '市辖区', '3', '44', '4412', '4412', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (441202, '端州区', '3', '44', '4412', '4412', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (441203, '鼎湖区', '3', '44', '4412', '4412', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (441204, '高要区', '3', '44', '4412', '4412', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (441223, '广宁县', '3', '44', '4412', '4412', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (441224, '怀集县', '3', '44', '4412', '4412', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (441225, '封开县', '3', '44', '4412', '4412', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (441226, '德庆县', '3', '44', '4412', '4412', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (441284, '四会市', '3', '44', '4412', '4412', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (441301, '市辖区', '3', '44', '4413', '4413', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (441302, '惠城区', '3', '44', '4413', '4413', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (441303, '惠阳区', '3', '44', '4413', '4413', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (441322, '博罗县', '3', '44', '4413', '4413', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (441323, '惠东县', '3', '44', '4413', '4413', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (441324, '龙门县', '3', '44', '4413', '4413', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (441401, '市辖区', '3', '44', '4414', '4414', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (441402, '梅江区', '3', '44', '4414', '4414', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (441403, '梅县区', '3', '44', '4414', '4414', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (441422, '大埔县', '3', '44', '4414', '4414', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (441423, '丰顺县', '3', '44', '4414', '4414', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (441424, '五华县', '3', '44', '4414', '4414', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (441426, '平远县', '3', '44', '4414', '4414', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (441427, '蕉岭县', '3', '44', '4414', '4414', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (441481, '兴宁市', '3', '44', '4414', '4414', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (441501, '市辖区', '3', '44', '4415', '4415', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (441502, '城区', '3', '44', '4415', '4415', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (441521, '海丰县', '3', '44', '4415', '4415', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (441523, '陆河县', '3', '44', '4415', '4415', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (441581, '陆丰市', '3', '44', '4415', '4415', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (441601, '市辖区', '3', '44', '4416', '4416', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (441602, '源城区', '3', '44', '4416', '4416', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (441621, '紫金县', '3', '44', '4416', '4416', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (441622, '龙川县', '3', '44', '4416', '4416', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (441623, '连平县', '3', '44', '4416', '4416', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (441624, '和平县', '3', '44', '4416', '4416', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (441625, '东源县', '3', '44', '4416', '4416', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (441701, '市辖区', '3', '44', '4417', '4417', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (441702, '江城区', '3', '44', '4417', '4417', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (441704, '阳东区', '3', '44', '4417', '4417', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (441721, '阳西县', '3', '44', '4417', '4417', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (441781, '阳春市', '3', '44', '4417', '4417', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (441801, '市辖区', '3', '44', '4418', '4418', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (441802, '清城区', '3', '44', '4418', '4418', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (441803, '清新区', '3', '44', '4418', '4418', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (441821, '佛冈县', '3', '44', '4418', '4418', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (441823, '阳山县', '3', '44', '4418', '4418', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (441825, '连山壮族瑶族自治县', '3', '44', '4418', '4418', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (441826, '连南瑶族自治县', '3', '44', '4418', '4418', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (441881, '英德市', '3', '44', '4418', '4418', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (441882, '连州市', '3', '44', '4418', '4418', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (445101, '市辖区', '3', '44', '4451', '4451', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (445102, '湘桥区', '3', '44', '4451', '4451', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (445103, '潮安区', '3', '44', '4451', '4451', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (445122, '饶平县', '3', '44', '4451', '4451', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (445201, '市辖区', '3', '44', '4452', '4452', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (445202, '榕城区', '3', '44', '4452', '4452', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (445203, '揭东区', '3', '44', '4452', '4452', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (445222, '揭西县', '3', '44', '4452', '4452', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (445224, '惠来县', '3', '44', '4452', '4452', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (445281, '普宁市', '3', '44', '4452', '4452', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (445301, '市辖区', '3', '44', '4453', '4453', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (445302, '云城区', '3', '44', '4453', '4453', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (445303, '云安区', '3', '44', '4453', '4453', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (445321, '新兴县', '3', '44', '4453', '4453', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (445322, '郁南县', '3', '44', '4453', '4453', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (445381, '罗定市', '3', '44', '4453', '4453', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (450101, '市辖区', '3', '45', '4501', '4501', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (450102, '兴宁区', '3', '45', '4501', '4501', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (450103, '青秀区', '3', '45', '4501', '4501', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (450105, '江南区', '3', '45', '4501', '4501', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (450107, '西乡塘区', '3', '45', '4501', '4501', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (450108, '良庆区', '3', '45', '4501', '4501', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (450109, '邕宁区', '3', '45', '4501', '4501', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (450110, '武鸣区', '3', '45', '4501', '4501', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (450123, '隆安县', '3', '45', '4501', '4501', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (450124, '马山县', '3', '45', '4501', '4501', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (450125, '上林县', '3', '45', '4501', '4501', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (450126, '宾阳县', '3', '45', '4501', '4501', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (450127, '横县', '3', '45', '4501', '4501', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (450201, '市辖区', '3', '45', '4502', '4502', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (450202, '城中区', '3', '45', '4502', '4502', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (450203, '鱼峰区', '3', '45', '4502', '4502', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (450204, '柳南区', '3', '45', '4502', '4502', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (450205, '柳北区', '3', '45', '4502', '4502', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (450206, '柳江区', '3', '45', '4502', '4502', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (450222, '柳城县', '3', '45', '4502', '4502', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (450223, '鹿寨县', '3', '45', '4502', '4502', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (450224, '融安县', '3', '45', '4502', '4502', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (450225, '融水苗族自治县', '3', '45', '4502', '4502', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (450226, '三江侗族自治县', '3', '45', '4502', '4502', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (450301, '市辖区', '3', '45', '4503', '4503', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (450302, '秀峰区', '3', '45', '4503', '4503', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (450303, '叠彩区', '3', '45', '4503', '4503', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (450304, '象山区', '3', '45', '4503', '4503', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (450305, '七星区', '3', '45', '4503', '4503', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (450311, '雁山区', '3', '45', '4503', '4503', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (450312, '临桂区', '3', '45', '4503', '4503', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (450321, '阳朔县', '3', '45', '4503', '4503', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (450323, '灵川县', '3', '45', '4503', '4503', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (450324, '全州县', '3', '45', '4503', '4503', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (450325, '兴安县', '3', '45', '4503', '4503', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (450326, '永福县', '3', '45', '4503', '4503', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (450327, '灌阳县', '3', '45', '4503', '4503', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (450328, '龙胜各族自治县', '3', '45', '4503', '4503', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (450329, '资源县', '3', '45', '4503', '4503', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (450330, '平乐县', '3', '45', '4503', '4503', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (450331, '荔浦县', '3', '45', '4503', '4503', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (450332, '恭城瑶族自治县', '3', '45', '4503', '4503', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (450401, '市辖区', '3', '45', '4504', '4504', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (450403, '万秀区', '3', '45', '4504', '4504', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (450405, '长洲区', '3', '45', '4504', '4504', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (450406, '龙圩区', '3', '45', '4504', '4504', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (450421, '苍梧县', '3', '45', '4504', '4504', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (450422, '藤县', '3', '45', '4504', '4504', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (450423, '蒙山县', '3', '45', '4504', '4504', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (450481, '岑溪市', '3', '45', '4504', '4504', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (450501, '市辖区', '3', '45', '4505', '4505', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (450502, '海城区', '3', '45', '4505', '4505', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (450503, '银海区', '3', '45', '4505', '4505', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (450512, '铁山港区', '3', '45', '4505', '4505', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (450521, '合浦县', '3', '45', '4505', '4505', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (450601, '市辖区', '3', '45', '4506', '4506', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (450602, '港口区', '3', '45', '4506', '4506', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (450603, '防城区', '3', '45', '4506', '4506', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (450621, '上思县', '3', '45', '4506', '4506', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (450681, '东兴市', '3', '45', '4506', '4506', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (450701, '市辖区', '3', '45', '4507', '4507', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (450702, '钦南区', '3', '45', '4507', '4507', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (450703, '钦北区', '3', '45', '4507', '4507', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (450721, '灵山县', '3', '45', '4507', '4507', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (450722, '浦北县', '3', '45', '4507', '4507', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (450801, '市辖区', '3', '45', '4508', '4508', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (450802, '港北区', '3', '45', '4508', '4508', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (450803, '港南区', '3', '45', '4508', '4508', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (450804, '覃塘区', '3', '45', '4508', '4508', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (450821, '平南县', '3', '45', '4508', '4508', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (450881, '桂平市', '3', '45', '4508', '4508', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (450901, '市辖区', '3', '45', '4509', '4509', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (450902, '玉州区', '3', '45', '4509', '4509', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (450903, '福绵区', '3', '45', '4509', '4509', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (450921, '容县', '3', '45', '4509', '4509', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (450922, '陆川县', '3', '45', '4509', '4509', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (450923, '博白县', '3', '45', '4509', '4509', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (450924, '兴业县', '3', '45', '4509', '4509', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (450981, '北流市', '3', '45', '4509', '4509', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (451001, '市辖区', '3', '45', '4510', '4510', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (451002, '右江区', '3', '45', '4510', '4510', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (451021, '田阳县', '3', '45', '4510', '4510', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (451022, '田东县', '3', '45', '4510', '4510', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (451023, '平果县', '3', '45', '4510', '4510', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (451024, '德保县', '3', '45', '4510', '4510', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (451026, '那坡县', '3', '45', '4510', '4510', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (451027, '凌云县', '3', '45', '4510', '4510', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (451028, '乐业县', '3', '45', '4510', '4510', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (451029, '田林县', '3', '45', '4510', '4510', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (451030, '西林县', '3', '45', '4510', '4510', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (451031, '隆林各族自治县', '3', '45', '4510', '4510', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (451081, '靖西市', '3', '45', '4510', '4510', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (451101, '市辖区', '3', '45', '4511', '4511', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (451102, '八步区', '3', '45', '4511', '4511', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (451103, '平桂区', '3', '45', '4511', '4511', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (451121, '昭平县', '3', '45', '4511', '4511', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (451122, '钟山县', '3', '45', '4511', '4511', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (451123, '富川瑶族自治县', '3', '45', '4511', '4511', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (451201, '市辖区', '3', '45', '4512', '4512', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (451202, '金城江区', '3', '45', '4512', '4512', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (451221, '南丹县', '3', '45', '4512', '4512', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (451222, '天峨县', '3', '45', '4512', '4512', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (451223, '凤山县', '3', '45', '4512', '4512', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (451224, '东兰县', '3', '45', '4512', '4512', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (451225, '罗城仫佬族自治县', '3', '45', '4512', '4512', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (451226, '环江毛南族自治县', '3', '45', '4512', '4512', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (451227, '巴马瑶族自治县', '3', '45', '4512', '4512', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (451228, '都安瑶族自治县', '3', '45', '4512', '4512', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (451229, '大化瑶族自治县', '3', '45', '4512', '4512', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (451281, '宜州市', '3', '45', '4512', '4512', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (451301, '市辖区', '3', '45', '4513', '4513', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (451302, '兴宾区', '3', '45', '4513', '4513', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (451321, '忻城县', '3', '45', '4513', '4513', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (451322, '象州县', '3', '45', '4513', '4513', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (451323, '武宣县', '3', '45', '4513', '4513', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (451324, '金秀瑶族自治县', '3', '45', '4513', '4513', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (451381, '合山市', '3', '45', '4513', '4513', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (451401, '市辖区', '3', '45', '4514', '4514', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (451402, '江州区', '3', '45', '4514', '4514', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (451421, '扶绥县', '3', '45', '4514', '4514', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (451422, '宁明县', '3', '45', '4514', '4514', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (451423, '龙州县', '3', '45', '4514', '4514', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (451424, '大新县', '3', '45', '4514', '4514', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (451425, '天等县', '3', '45', '4514', '4514', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (451481, '凭祥市', '3', '45', '4514', '4514', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (460101, '市辖区', '3', '46', '4601', '4601', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (460105, '秀英区', '3', '46', '4601', '4601', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (460106, '龙华区', '3', '46', '4601', '4601', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (460107, '琼山区', '3', '46', '4601', '4601', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (460108, '美兰区', '3', '46', '4601', '4601', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (460201, '市辖区', '3', '46', '4602', '4602', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (460202, '海棠区', '3', '46', '4602', '4602', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (460203, '吉阳区', '3', '46', '4602', '4602', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (460204, '天涯区', '3', '46', '4602', '4602', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (460205, '崖州区', '3', '46', '4602', '4602', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (469001, '五指山市', '3', '46', '4690', '4690', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (469002, '琼海市', '3', '46', '4690', '4690', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (469005, '文昌市', '3', '46', '4690', '4690', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (469006, '万宁市', '3', '46', '4690', '4690', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (469007, '东方市', '3', '46', '4690', '4690', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (469021, '定安县', '3', '46', '4690', '4690', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (469022, '屯昌县', '3', '46', '4690', '4690', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (469023, '澄迈县', '3', '46', '4690', '4690', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (469024, '临高县', '3', '46', '4690', '4690', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (469025, '白沙黎族自治县', '3', '46', '4690', '4690', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (469026, '昌江黎族自治县', '3', '46', '4690', '4690', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (469027, '乐东黎族自治县', '3', '46', '4690', '4690', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (469028, '陵水黎族自治县', '3', '46', '4690', '4690', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (469029, '保亭黎族苗族自治县', '3', '46', '4690', '4690', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (469030, '琼中黎族苗族自治县', '3', '46', '4690', '4690', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (500101, '万州区', '3', '50', '5001', '5001', 0, 517, '', '');
INSERT INTO `sq_region` VALUES (500102, '涪陵区', '3', '50', '5001', '5001', 0, 506, '', '');
INSERT INTO `sq_region` VALUES (500103, '渝中区', '3', '50', '5001', '5001', 0, 525, '', '');
INSERT INTO `sq_region` VALUES (500104, '大渡口区', '3', '50', '5001', '5001', 0, 511, '', '');
INSERT INTO `sq_region` VALUES (500105, '江北区', '3', '50', '5001', '5001', 0, 504, '', '');
INSERT INTO `sq_region` VALUES (500106, '沙坪坝区', '3', '50', '5001', '5001', 0, 494, '', '');
INSERT INTO `sq_region` VALUES (500107, '九龙坡区', '3', '50', '5001', '5001', 0, 502, '', '');
INSERT INTO `sq_region` VALUES (500108, '南岸区', '3', '50', '5001', '5001', 0, 499, '', '');
INSERT INTO `sq_region` VALUES (500109, '北碚区', '3', '50', '5001', '5001', 0, 514, '', '');
INSERT INTO `sq_region` VALUES (500110, '綦江区', '3', '50', '5001', '5001', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (500111, '大足区', '3', '50', '5001', '5001', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (500112, '渝北区', '3', '50', '5001', '5001', 0, 524, '', '');
INSERT INTO `sq_region` VALUES (500113, '巴南区', '3', '50', '5001', '5001', 0, 515, '', '');
INSERT INTO `sq_region` VALUES (500114, '黔江区', '3', '50', '5001', '5001', 0, 496, '', '');
INSERT INTO `sq_region` VALUES (500115, '长寿区', '3', '50', '5001', '5001', 0, 513, '', '');
INSERT INTO `sq_region` VALUES (500116, '江津区', '3', '50', '5001', '5001', 0, 503, '', '');
INSERT INTO `sq_region` VALUES (500117, '合川区', '3', '50', '5001', '5001', 0, 505, '', '');
INSERT INTO `sq_region` VALUES (500118, '永川区', '3', '50', '5001', '5001', 0, 522, '', '');
INSERT INTO `sq_region` VALUES (500119, '南川区', '3', '50', '5001', '5001', 0, 498, '', '');
INSERT INTO `sq_region` VALUES (500120, '璧山区', '3', '50', '5001', '5001', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (500151, '铜梁区', '3', '50', '5001', '5001', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (500152, '潼南区', '3', '50', '5001', '5001', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (500153, '荣昌区', '3', '50', '5001', '5001', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (500154, '开州区', '3', '50', '5001', '5001', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (500228, '梁平县', '3', '50', '5002', '5002', 0, 500, '', '');
INSERT INTO `sq_region` VALUES (500229, '城口县', '3', '50', '5002', '5002', 0, 512, '', '');
INSERT INTO `sq_region` VALUES (500230, '丰都县', '3', '50', '5002', '5002', 0, 508, '', '');
INSERT INTO `sq_region` VALUES (500231, '垫江县', '3', '50', '5002', '5002', 0, 510, '', '');
INSERT INTO `sq_region` VALUES (500232, '武隆县', '3', '50', '5002', '5002', 0, 520, '', '');
INSERT INTO `sq_region` VALUES (500233, '忠县', '3', '50', '5002', '5002', 0, 527, '', '');
INSERT INTO `sq_region` VALUES (500235, '云阳县', '3', '50', '5002', '5002', 0, 526, '', '');
INSERT INTO `sq_region` VALUES (500236, '奉节县', '3', '50', '5002', '5002', 0, 507, '', '');
INSERT INTO `sq_region` VALUES (500237, '巫山县', '3', '50', '5002', '5002', 0, 518, '', '');
INSERT INTO `sq_region` VALUES (500238, '巫溪县', '3', '50', '5002', '5002', 0, 519, '', '');
INSERT INTO `sq_region` VALUES (500240, '石柱土家族自治县', '3', '50', '5002', '5002', 0, 493, '', '');
INSERT INTO `sq_region` VALUES (500241, '秀山土家族苗族自治县', '3', '50', '5002', '5002', 0, 521, '', '');
INSERT INTO `sq_region` VALUES (500242, '酉阳土家族苗族自治县', '3', '50', '5002', '5002', 0, 523, '', '');
INSERT INTO `sq_region` VALUES (500243, '彭水苗族土家族自治县', '3', '50', '5002', '5002', 0, 497, '', '');
INSERT INTO `sq_region` VALUES (510101, '市辖区', '3', '51', '5101', '5101', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (510104, '锦江区', '3', '51', '5101', '5101', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (510105, '青羊区', '3', '51', '5101', '5101', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (510106, '金牛区', '3', '51', '5101', '5101', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (510107, '武侯区', '3', '51', '5101', '5101', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (510108, '成华区', '3', '51', '5101', '5101', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (510112, '龙泉驿区', '3', '51', '5101', '5101', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (510113, '青白江区', '3', '51', '5101', '5101', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (510114, '新都区', '3', '51', '5101', '5101', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (510115, '温江区', '3', '51', '5101', '5101', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (510116, '双流区', '3', '51', '5101', '5101', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (510121, '金堂县', '3', '51', '5101', '5101', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (510124, '郫县', '3', '51', '5101', '5101', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (510129, '大邑县', '3', '51', '5101', '5101', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (510131, '蒲江县', '3', '51', '5101', '5101', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (510132, '新津县', '3', '51', '5101', '5101', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (510181, '都江堰市', '3', '51', '5101', '5101', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (510182, '彭州市', '3', '51', '5101', '5101', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (510183, '邛崃市', '3', '51', '5101', '5101', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (510184, '崇州市', '3', '51', '5101', '5101', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (510185, '简阳市', '3', '51', '5101', '5101', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (510301, '市辖区', '3', '51', '5103', '5103', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (510302, '自流井区', '3', '51', '5103', '5103', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (510303, '贡井区', '3', '51', '5103', '5103', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (510304, '大安区', '3', '51', '5103', '5103', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (510311, '沿滩区', '3', '51', '5103', '5103', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (510321, '荣县', '3', '51', '5103', '5103', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (510322, '富顺县', '3', '51', '5103', '5103', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (510401, '市辖区', '3', '51', '5104', '5104', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (510402, '东区', '3', '51', '5104', '5104', 0, 432, '', '');
INSERT INTO `sq_region` VALUES (510403, '西区', '3', '51', '5104', '5104', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (510411, '仁和区', '3', '51', '5104', '5104', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (510421, '米易县', '3', '51', '5104', '5104', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (510422, '盐边县', '3', '51', '5104', '5104', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (510501, '市辖区', '3', '51', '5105', '5105', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (510502, '江阳区', '3', '51', '5105', '5105', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (510503, '纳溪区', '3', '51', '5105', '5105', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (510504, '龙马潭区', '3', '51', '5105', '5105', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (510521, '泸县', '3', '51', '5105', '5105', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (510522, '合江县', '3', '51', '5105', '5105', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (510524, '叙永县', '3', '51', '5105', '5105', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (510525, '古蔺县', '3', '51', '5105', '5105', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (510601, '市辖区', '3', '51', '5106', '5106', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (510603, '旌阳区', '3', '51', '5106', '5106', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (510623, '中江县', '3', '51', '5106', '5106', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (510626, '罗江县', '3', '51', '5106', '5106', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (510681, '广汉市', '3', '51', '5106', '5106', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (510682, '什邡市', '3', '51', '5106', '5106', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (510683, '绵竹市', '3', '51', '5106', '5106', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (510701, '市辖区', '3', '51', '5107', '5107', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (510703, '涪城区', '3', '51', '5107', '5107', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (510704, '游仙区', '3', '51', '5107', '5107', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (510705, '安州区', '3', '51', '5107', '5107', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (510722, '三台县', '3', '51', '5107', '5107', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (510723, '盐亭县', '3', '51', '5107', '5107', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (510725, '梓潼县', '3', '51', '5107', '5107', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (510726, '北川羌族自治县', '3', '51', '5107', '5107', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (510727, '平武县', '3', '51', '5107', '5107', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (510781, '江油市', '3', '51', '5107', '5107', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (510801, '市辖区', '3', '51', '5108', '5108', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (510802, '利州区', '3', '51', '5108', '5108', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (510811, '昭化区', '3', '51', '5108', '5108', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (510812, '朝天区', '3', '51', '5108', '5108', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (510821, '旺苍县', '3', '51', '5108', '5108', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (510822, '青川县', '3', '51', '5108', '5108', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (510823, '剑阁县', '3', '51', '5108', '5108', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (510824, '苍溪县', '3', '51', '5108', '5108', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (510901, '市辖区', '3', '51', '5109', '5109', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (510903, '船山区', '3', '51', '5109', '5109', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (510904, '安居区', '3', '51', '5109', '5109', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (510921, '蓬溪县', '3', '51', '5109', '5109', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (510922, '射洪县', '3', '51', '5109', '5109', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (510923, '大英县', '3', '51', '5109', '5109', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (511001, '市辖区', '3', '51', '5110', '5110', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (511002, '市中区', '3', '51', '5110', '5110', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (511011, '东兴区', '3', '51', '5110', '5110', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (511024, '威远县', '3', '51', '5110', '5110', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (511025, '资中县', '3', '51', '5110', '5110', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (511028, '隆昌县', '3', '51', '5110', '5110', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (511101, '市辖区', '3', '51', '5111', '5111', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (511102, '市中区', '3', '51', '5111', '5111', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (511111, '沙湾区', '3', '51', '5111', '5111', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (511112, '五通桥区', '3', '51', '5111', '5111', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (511113, '金口河区', '3', '51', '5111', '5111', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (511123, '犍为县', '3', '51', '5111', '5111', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (511124, '井研县', '3', '51', '5111', '5111', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (511126, '夹江县', '3', '51', '5111', '5111', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (511129, '沐川县', '3', '51', '5111', '5111', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (511132, '峨边彝族自治县', '3', '51', '5111', '5111', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (511133, '马边彝族自治县', '3', '51', '5111', '5111', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (511181, '峨眉山市', '3', '51', '5111', '5111', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (511301, '市辖区', '3', '51', '5113', '5113', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (511302, '顺庆区', '3', '51', '5113', '5113', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (511303, '高坪区', '3', '51', '5113', '5113', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (511304, '嘉陵区', '3', '51', '5113', '5113', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (511321, '南部县', '3', '51', '5113', '5113', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (511322, '营山县', '3', '51', '5113', '5113', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (511323, '蓬安县', '3', '51', '5113', '5113', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (511324, '仪陇县', '3', '51', '5113', '5113', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (511325, '西充县', '3', '51', '5113', '5113', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (511381, '阆中市', '3', '51', '5113', '5113', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (511401, '市辖区', '3', '51', '5114', '5114', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (511402, '东坡区', '3', '51', '5114', '5114', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (511403, '彭山区', '3', '51', '5114', '5114', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (511421, '仁寿县', '3', '51', '5114', '5114', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (511423, '洪雅县', '3', '51', '5114', '5114', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (511424, '丹棱县', '3', '51', '5114', '5114', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (511425, '青神县', '3', '51', '5114', '5114', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (511501, '市辖区', '3', '51', '5115', '5115', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (511502, '翠屏区', '3', '51', '5115', '5115', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (511503, '南溪区', '3', '51', '5115', '5115', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (511521, '宜宾县', '3', '51', '5115', '5115', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (511523, '江安县', '3', '51', '5115', '5115', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (511524, '长宁县', '3', '51', '5115', '5115', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (511525, '高县', '3', '51', '5115', '5115', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (511526, '珙县', '3', '51', '5115', '5115', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (511527, '筠连县', '3', '51', '5115', '5115', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (511528, '兴文县', '3', '51', '5115', '5115', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (511529, '屏山县', '3', '51', '5115', '5115', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (511601, '市辖区', '3', '51', '5116', '5116', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (511602, '广安区', '3', '51', '5116', '5116', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (511603, '前锋区', '3', '51', '5116', '5116', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (511621, '岳池县', '3', '51', '5116', '5116', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (511622, '武胜县', '3', '51', '5116', '5116', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (511623, '邻水县', '3', '51', '5116', '5116', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (511681, '华蓥市', '3', '51', '5116', '5116', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (511701, '市辖区', '3', '51', '5117', '5117', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (511702, '通川区', '3', '51', '5117', '5117', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (511703, '达川区', '3', '51', '5117', '5117', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (511722, '宣汉县', '3', '51', '5117', '5117', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (511723, '开江县', '3', '51', '5117', '5117', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (511724, '大竹县', '3', '51', '5117', '5117', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (511725, '渠县', '3', '51', '5117', '5117', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (511781, '万源市', '3', '51', '5117', '5117', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (511801, '市辖区', '3', '51', '5118', '5118', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (511802, '雨城区', '3', '51', '5118', '5118', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (511803, '名山区', '3', '51', '5118', '5118', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (511822, '荥经县', '3', '51', '5118', '5118', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (511823, '汉源县', '3', '51', '5118', '5118', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (511824, '石棉县', '3', '51', '5118', '5118', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (511825, '天全县', '3', '51', '5118', '5118', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (511826, '芦山县', '3', '51', '5118', '5118', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (511827, '宝兴县', '3', '51', '5118', '5118', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (511901, '市辖区', '3', '51', '5119', '5119', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (511902, '巴州区', '3', '51', '5119', '5119', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (511903, '恩阳区', '3', '51', '5119', '5119', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (511921, '通江县', '3', '51', '5119', '5119', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (511922, '南江县', '3', '51', '5119', '5119', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (511923, '平昌县', '3', '51', '5119', '5119', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (512001, '市辖区', '3', '51', '5120', '5120', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (512002, '雁江区', '3', '51', '5120', '5120', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (512021, '安岳县', '3', '51', '5120', '5120', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (512022, '乐至县', '3', '51', '5120', '5120', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (513201, '马尔康市', '3', '51', '5132', '5132', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (513221, '汶川县', '3', '51', '5132', '5132', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (513222, '理县', '3', '51', '5132', '5132', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (513223, '茂县', '3', '51', '5132', '5132', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (513224, '松潘县', '3', '51', '5132', '5132', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (513225, '九寨沟县', '3', '51', '5132', '5132', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (513226, '金川县', '3', '51', '5132', '5132', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (513227, '小金县', '3', '51', '5132', '5132', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (513228, '黑水县', '3', '51', '5132', '5132', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (513230, '壤塘县', '3', '51', '5132', '5132', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (513231, '阿坝县', '3', '51', '5132', '5132', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (513232, '若尔盖县', '3', '51', '5132', '5132', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (513233, '红原县', '3', '51', '5132', '5132', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (513301, '康定市', '3', '51', '5133', '5133', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (513322, '泸定县', '3', '51', '5133', '5133', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (513323, '丹巴县', '3', '51', '5133', '5133', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (513324, '九龙县', '3', '51', '5133', '5133', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (513325, '雅江县', '3', '51', '5133', '5133', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (513326, '道孚县', '3', '51', '5133', '5133', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (513327, '炉霍县', '3', '51', '5133', '5133', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (513328, '甘孜县', '3', '51', '5133', '5133', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (513329, '新龙县', '3', '51', '5133', '5133', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (513330, '德格县', '3', '51', '5133', '5133', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (513331, '白玉县', '3', '51', '5133', '5133', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (513332, '石渠县', '3', '51', '5133', '5133', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (513333, '色达县', '3', '51', '5133', '5133', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (513334, '理塘县', '3', '51', '5133', '5133', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (513335, '巴塘县', '3', '51', '5133', '5133', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (513336, '乡城县', '3', '51', '5133', '5133', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (513337, '稻城县', '3', '51', '5133', '5133', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (513338, '得荣县', '3', '51', '5133', '5133', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (513401, '西昌市', '3', '51', '5134', '5134', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (513422, '木里藏族自治县', '3', '51', '5134', '5134', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (513423, '盐源县', '3', '51', '5134', '5134', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (513424, '德昌县', '3', '51', '5134', '5134', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (513425, '会理县', '3', '51', '5134', '5134', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (513426, '会东县', '3', '51', '5134', '5134', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (513427, '宁南县', '3', '51', '5134', '5134', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (513428, '普格县', '3', '51', '5134', '5134', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (513429, '布拖县', '3', '51', '5134', '5134', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (513430, '金阳县', '3', '51', '5134', '5134', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (513431, '昭觉县', '3', '51', '5134', '5134', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (513432, '喜德县', '3', '51', '5134', '5134', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (513433, '冕宁县', '3', '51', '5134', '5134', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (513434, '越西县', '3', '51', '5134', '5134', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (513435, '甘洛县', '3', '51', '5134', '5134', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (513436, '美姑县', '3', '51', '5134', '5134', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (513437, '雷波县', '3', '51', '5134', '5134', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (520101, '市辖区', '3', '52', '5201', '5201', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (520102, '南明区', '3', '52', '5201', '5201', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (520103, '云岩区', '3', '52', '5201', '5201', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (520111, '花溪区', '3', '52', '5201', '5201', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (520112, '乌当区', '3', '52', '5201', '5201', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (520113, '白云区', '3', '52', '5201', '5201', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (520115, '观山湖区', '3', '52', '5201', '5201', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (520121, '开阳县', '3', '52', '5201', '5201', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (520122, '息烽县', '3', '52', '5201', '5201', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (520123, '修文县', '3', '52', '5201', '5201', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (520181, '清镇市', '3', '52', '5201', '5201', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (520201, '钟山区', '3', '52', '5202', '5202', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (520203, '六枝特区', '3', '52', '5202', '5202', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (520221, '水城县', '3', '52', '5202', '5202', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (520222, '盘县', '3', '52', '5202', '5202', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (520301, '市辖区', '3', '52', '5203', '5203', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (520302, '红花岗区', '3', '52', '5203', '5203', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (520303, '汇川区', '3', '52', '5203', '5203', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (520304, '播州区', '3', '52', '5203', '5203', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (520322, '桐梓县', '3', '52', '5203', '5203', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (520323, '绥阳县', '3', '52', '5203', '5203', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (520324, '正安县', '3', '52', '5203', '5203', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (520325, '道真仡佬族苗族自治县', '3', '52', '5203', '5203', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (520326, '务川仡佬族苗族自治县', '3', '52', '5203', '5203', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (520327, '凤冈县', '3', '52', '5203', '5203', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (520328, '湄潭县', '3', '52', '5203', '5203', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (520329, '余庆县', '3', '52', '5203', '5203', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (520330, '习水县', '3', '52', '5203', '5203', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (520381, '赤水市', '3', '52', '5203', '5203', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (520382, '仁怀市', '3', '52', '5203', '5203', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (520401, '市辖区', '3', '52', '5204', '5204', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (520402, '西秀区', '3', '52', '5204', '5204', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (520403, '平坝区', '3', '52', '5204', '5204', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (520422, '普定县', '3', '52', '5204', '5204', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (520423, '镇宁布依族苗族自治县', '3', '52', '5204', '5204', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (520424, '关岭布依族苗族自治县', '3', '52', '5204', '5204', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (520425, '紫云苗族布依族自治县', '3', '52', '5204', '5204', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (520501, '市辖区', '3', '52', '5205', '5205', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (520502, '七星关区', '3', '52', '5205', '5205', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (520521, '大方县', '3', '52', '5205', '5205', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (520522, '黔西县', '3', '52', '5205', '5205', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (520523, '金沙县', '3', '52', '5205', '5205', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (520524, '织金县', '3', '52', '5205', '5205', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (520525, '纳雍县', '3', '52', '5205', '5205', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (520526, '威宁彝族回族苗族自治县', '3', '52', '5205', '5205', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (520527, '赫章县', '3', '52', '5205', '5205', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (520601, '市辖区', '3', '52', '5206', '5206', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (520602, '碧江区', '3', '52', '5206', '5206', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (520603, '万山区', '3', '52', '5206', '5206', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (520621, '江口县', '3', '52', '5206', '5206', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (520622, '玉屏侗族自治县', '3', '52', '5206', '5206', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (520623, '石阡县', '3', '52', '5206', '5206', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (520624, '思南县', '3', '52', '5206', '5206', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (520625, '印江土家族苗族自治县', '3', '52', '5206', '5206', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (520626, '德江县', '3', '52', '5206', '5206', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (520627, '沿河土家族自治县', '3', '52', '5206', '5206', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (520628, '松桃苗族自治县', '3', '52', '5206', '5206', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (522301, '兴义市', '3', '52', '5223', '5223', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (522322, '兴仁县', '3', '52', '5223', '5223', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (522323, '普安县', '3', '52', '5223', '5223', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (522324, '晴隆县', '3', '52', '5223', '5223', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (522325, '贞丰县', '3', '52', '5223', '5223', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (522326, '望谟县', '3', '52', '5223', '5223', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (522327, '册亨县', '3', '52', '5223', '5223', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (522328, '安龙县', '3', '52', '5223', '5223', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (522601, '凯里市', '3', '52', '5226', '5226', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (522622, '黄平县', '3', '52', '5226', '5226', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (522623, '施秉县', '3', '52', '5226', '5226', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (522624, '三穗县', '3', '52', '5226', '5226', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (522625, '镇远县', '3', '52', '5226', '5226', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (522626, '岑巩县', '3', '52', '5226', '5226', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (522627, '天柱县', '3', '52', '5226', '5226', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (522628, '锦屏县', '3', '52', '5226', '5226', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (522629, '剑河县', '3', '52', '5226', '5226', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (522630, '台江县', '3', '52', '5226', '5226', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (522631, '黎平县', '3', '52', '5226', '5226', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (522632, '榕江县', '3', '52', '5226', '5226', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (522633, '从江县', '3', '52', '5226', '5226', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (522634, '雷山县', '3', '52', '5226', '5226', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (522635, '麻江县', '3', '52', '5226', '5226', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (522636, '丹寨县', '3', '52', '5226', '5226', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (522701, '都匀市', '3', '52', '5227', '5227', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (522702, '福泉市', '3', '52', '5227', '5227', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (522722, '荔波县', '3', '52', '5227', '5227', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (522723, '贵定县', '3', '52', '5227', '5227', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (522725, '瓮安县', '3', '52', '5227', '5227', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (522726, '独山县', '3', '52', '5227', '5227', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (522727, '平塘县', '3', '52', '5227', '5227', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (522728, '罗甸县', '3', '52', '5227', '5227', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (522729, '长顺县', '3', '52', '5227', '5227', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (522730, '龙里县', '3', '52', '5227', '5227', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (522731, '惠水县', '3', '52', '5227', '5227', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (522732, '三都水族自治县', '3', '52', '5227', '5227', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (530101, '市辖区', '3', '53', '5301', '5301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (530102, '五华区', '3', '53', '5301', '5301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (530103, '盘龙区', '3', '53', '5301', '5301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (530111, '官渡区', '3', '53', '5301', '5301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (530112, '西山区', '3', '53', '5301', '5301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (530113, '东川区', '3', '53', '5301', '5301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (530114, '呈贡区', '3', '53', '5301', '5301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (530122, '晋宁县', '3', '53', '5301', '5301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (530124, '富民县', '3', '53', '5301', '5301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (530125, '宜良县', '3', '53', '5301', '5301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (530126, '石林彝族自治县', '3', '53', '5301', '5301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (530127, '嵩明县', '3', '53', '5301', '5301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (530128, '禄劝彝族苗族自治县', '3', '53', '5301', '5301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (530129, '寻甸回族彝族自治县', '3', '53', '5301', '5301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (530181, '安宁市', '3', '53', '5301', '5301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (530301, '市辖区', '3', '53', '5303', '5303', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (530302, '麒麟区', '3', '53', '5303', '5303', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (530303, '沾益区', '3', '53', '5303', '5303', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (530321, '马龙县', '3', '53', '5303', '5303', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (530322, '陆良县', '3', '53', '5303', '5303', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (530323, '师宗县', '3', '53', '5303', '5303', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (530324, '罗平县', '3', '53', '5303', '5303', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (530325, '富源县', '3', '53', '5303', '5303', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (530326, '会泽县', '3', '53', '5303', '5303', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (530381, '宣威市', '3', '53', '5303', '5303', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (530401, '市辖区', '3', '53', '5304', '5304', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (530402, '红塔区', '3', '53', '5304', '5304', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (530403, '江川区', '3', '53', '5304', '5304', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (530422, '澄江县', '3', '53', '5304', '5304', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (530423, '通海县', '3', '53', '5304', '5304', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (530424, '华宁县', '3', '53', '5304', '5304', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (530425, '易门县', '3', '53', '5304', '5304', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (530426, '峨山彝族自治县', '3', '53', '5304', '5304', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (530427, '新平彝族傣族自治县', '3', '53', '5304', '5304', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (530428, '元江哈尼族彝族傣族自治县', '3', '53', '5304', '5304', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (530501, '市辖区', '3', '53', '5305', '5305', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (530502, '隆阳区', '3', '53', '5305', '5305', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (530521, '施甸县', '3', '53', '5305', '5305', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (530523, '龙陵县', '3', '53', '5305', '5305', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (530524, '昌宁县', '3', '53', '5305', '5305', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (530581, '腾冲市', '3', '53', '5305', '5305', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (530601, '市辖区', '3', '53', '5306', '5306', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (530602, '昭阳区', '3', '53', '5306', '5306', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (530621, '鲁甸县', '3', '53', '5306', '5306', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (530622, '巧家县', '3', '53', '5306', '5306', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (530623, '盐津县', '3', '53', '5306', '5306', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (530624, '大关县', '3', '53', '5306', '5306', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (530625, '永善县', '3', '53', '5306', '5306', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (530626, '绥江县', '3', '53', '5306', '5306', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (530627, '镇雄县', '3', '53', '5306', '5306', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (530628, '彝良县', '3', '53', '5306', '5306', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (530629, '威信县', '3', '53', '5306', '5306', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (530630, '水富县', '3', '53', '5306', '5306', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (530701, '市辖区', '3', '53', '5307', '5307', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (530702, '古城区', '3', '53', '5307', '5307', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (530721, '玉龙纳西族自治县', '3', '53', '5307', '5307', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (530722, '永胜县', '3', '53', '5307', '5307', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (530723, '华坪县', '3', '53', '5307', '5307', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (530724, '宁蒗彝族自治县', '3', '53', '5307', '5307', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (530801, '市辖区', '3', '53', '5308', '5308', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (530802, '思茅区', '3', '53', '5308', '5308', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (530821, '宁洱哈尼族彝族自治县', '3', '53', '5308', '5308', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (530822, '墨江哈尼族自治县', '3', '53', '5308', '5308', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (530823, '景东彝族自治县', '3', '53', '5308', '5308', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (530824, '景谷傣族彝族自治县', '3', '53', '5308', '5308', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (530825, '镇沅彝族哈尼族拉祜族自治县', '3', '53', '5308', '5308', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (530826, '江城哈尼族彝族自治县', '3', '53', '5308', '5308', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (530827, '孟连傣族拉祜族佤族自治县', '3', '53', '5308', '5308', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (530828, '澜沧拉祜族自治县', '3', '53', '5308', '5308', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (530829, '西盟佤族自治县', '3', '53', '5308', '5308', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (530901, '市辖区', '3', '53', '5309', '5309', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (530902, '临翔区', '3', '53', '5309', '5309', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (530921, '凤庆县', '3', '53', '5309', '5309', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (530922, '云县', '3', '53', '5309', '5309', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (530923, '永德县', '3', '53', '5309', '5309', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (530924, '镇康县', '3', '53', '5309', '5309', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (530925, '双江拉祜族佤族布朗族傣族自治县', '3', '53', '5309', '5309', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (530926, '耿马傣族佤族自治县', '3', '53', '5309', '5309', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (530927, '沧源佤族自治县', '3', '53', '5309', '5309', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (532301, '楚雄市', '3', '53', '5323', '5323', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (532322, '双柏县', '3', '53', '5323', '5323', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (532323, '牟定县', '3', '53', '5323', '5323', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (532324, '南华县', '3', '53', '5323', '5323', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (532325, '姚安县', '3', '53', '5323', '5323', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (532326, '大姚县', '3', '53', '5323', '5323', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (532327, '永仁县', '3', '53', '5323', '5323', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (532328, '元谋县', '3', '53', '5323', '5323', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (532329, '武定县', '3', '53', '5323', '5323', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (532331, '禄丰县', '3', '53', '5323', '5323', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (532501, '个旧市', '3', '53', '5325', '5325', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (532502, '开远市', '3', '53', '5325', '5325', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (532503, '蒙自市', '3', '53', '5325', '5325', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (532504, '弥勒市', '3', '53', '5325', '5325', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (532523, '屏边苗族自治县', '3', '53', '5325', '5325', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (532524, '建水县', '3', '53', '5325', '5325', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (532525, '石屏县', '3', '53', '5325', '5325', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (532527, '泸西县', '3', '53', '5325', '5325', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (532528, '元阳县', '3', '53', '5325', '5325', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (532529, '红河县', '3', '53', '5325', '5325', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (532530, '金平苗族瑶族傣族自治县', '3', '53', '5325', '5325', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (532531, '绿春县', '3', '53', '5325', '5325', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (532532, '河口瑶族自治县', '3', '53', '5325', '5325', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (532601, '文山市', '3', '53', '5326', '5326', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (532622, '砚山县', '3', '53', '5326', '5326', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (532623, '西畴县', '3', '53', '5326', '5326', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (532624, '麻栗坡县', '3', '53', '5326', '5326', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (532625, '马关县', '3', '53', '5326', '5326', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (532626, '丘北县', '3', '53', '5326', '5326', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (532627, '广南县', '3', '53', '5326', '5326', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (532628, '富宁县', '3', '53', '5326', '5326', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (532801, '景洪市', '3', '53', '5328', '5328', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (532822, '勐海县', '3', '53', '5328', '5328', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (532823, '勐腊县', '3', '53', '5328', '5328', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (532901, '大理市', '3', '53', '5329', '5329', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (532922, '漾濞彝族自治县', '3', '53', '5329', '5329', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (532923, '祥云县', '3', '53', '5329', '5329', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (532924, '宾川县', '3', '53', '5329', '5329', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (532925, '弥渡县', '3', '53', '5329', '5329', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (532926, '南涧彝族自治县', '3', '53', '5329', '5329', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (532927, '巍山彝族回族自治县', '3', '53', '5329', '5329', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (532928, '永平县', '3', '53', '5329', '5329', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (532929, '云龙县', '3', '53', '5329', '5329', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (532930, '洱源县', '3', '53', '5329', '5329', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (532931, '剑川县', '3', '53', '5329', '5329', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (532932, '鹤庆县', '3', '53', '5329', '5329', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (533102, '瑞丽市', '3', '53', '5331', '5331', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (533103, '芒市', '3', '53', '5331', '5331', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (533122, '梁河县', '3', '53', '5331', '5331', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (533123, '盈江县', '3', '53', '5331', '5331', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (533124, '陇川县', '3', '53', '5331', '5331', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (533301, '泸水市', '3', '53', '5333', '5333', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (533323, '福贡县', '3', '53', '5333', '5333', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (533324, '贡山独龙族怒族自治县', '3', '53', '5333', '5333', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (533325, '兰坪白族普米族自治县', '3', '53', '5333', '5333', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (533401, '香格里拉市', '3', '53', '5334', '5334', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (533422, '德钦县', '3', '53', '5334', '5334', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (533423, '维西傈僳族自治县', '3', '53', '5334', '5334', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (540101, '市辖区', '3', '54', '5401', '5401', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (540102, '城关区', '3', '54', '5401', '5401', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (540103, '堆龙德庆区', '3', '54', '5401', '5401', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (540121, '林周县', '3', '54', '5401', '5401', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (540122, '当雄县', '3', '54', '5401', '5401', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (540123, '尼木县', '3', '54', '5401', '5401', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (540124, '曲水县', '3', '54', '5401', '5401', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (540126, '达孜县', '3', '54', '5401', '5401', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (540127, '墨竹工卡县', '3', '54', '5401', '5401', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (540202, '桑珠孜区', '3', '54', '5402', '5402', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (540221, '南木林县', '3', '54', '5402', '5402', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (540222, '江孜县', '3', '54', '5402', '5402', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (540223, '定日县', '3', '54', '5402', '5402', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (540224, '萨迦县', '3', '54', '5402', '5402', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (540225, '拉孜县', '3', '54', '5402', '5402', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (540226, '昂仁县', '3', '54', '5402', '5402', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (540227, '谢通门县', '3', '54', '5402', '5402', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (540228, '白朗县', '3', '54', '5402', '5402', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (540229, '仁布县', '3', '54', '5402', '5402', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (540230, '康马县', '3', '54', '5402', '5402', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (540231, '定结县', '3', '54', '5402', '5402', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (540232, '仲巴县', '3', '54', '5402', '5402', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (540233, '亚东县', '3', '54', '5402', '5402', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (540234, '吉隆县', '3', '54', '5402', '5402', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (540235, '聂拉木县', '3', '54', '5402', '5402', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (540236, '萨嘎县', '3', '54', '5402', '5402', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (540237, '岗巴县', '3', '54', '5402', '5402', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (540302, '卡若区', '3', '54', '5403', '5403', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (540321, '江达县', '3', '54', '5403', '5403', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (540322, '贡觉县', '3', '54', '5403', '5403', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (540323, '类乌齐县', '3', '54', '5403', '5403', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (540324, '丁青县', '3', '54', '5403', '5403', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (540325, '察雅县', '3', '54', '5403', '5403', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (540326, '八宿县', '3', '54', '5403', '5403', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (540327, '左贡县', '3', '54', '5403', '5403', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (540328, '芒康县', '3', '54', '5403', '5403', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (540329, '洛隆县', '3', '54', '5403', '5403', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (540330, '边坝县', '3', '54', '5403', '5403', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (540402, '巴宜区', '3', '54', '5404', '5404', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (540421, '工布江达县', '3', '54', '5404', '5404', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (540422, '米林县', '3', '54', '5404', '5404', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (540423, '墨脱县', '3', '54', '5404', '5404', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (540424, '波密县', '3', '54', '5404', '5404', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (540425, '察隅县', '3', '54', '5404', '5404', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (540426, '朗县', '3', '54', '5404', '5404', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (540501, '市辖区', '3', '54', '5405', '5405', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (540502, '乃东区', '3', '54', '5405', '5405', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (540521, '扎囊县', '3', '54', '5405', '5405', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (540522, '贡嘎县', '3', '54', '5405', '5405', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (540523, '桑日县', '3', '54', '5405', '5405', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (540524, '琼结县', '3', '54', '5405', '5405', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (540525, '曲松县', '3', '54', '5405', '5405', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (540526, '措美县', '3', '54', '5405', '5405', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (540527, '洛扎县', '3', '54', '5405', '5405', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (540528, '加查县', '3', '54', '5405', '5405', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (540529, '隆子县', '3', '54', '5405', '5405', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (540530, '错那县', '3', '54', '5405', '5405', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (540531, '浪卡子县', '3', '54', '5405', '5405', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (542421, '那曲县', '3', '54', '5424', '5424', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (542422, '嘉黎县', '3', '54', '5424', '5424', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (542423, '比如县', '3', '54', '5424', '5424', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (542424, '聂荣县', '3', '54', '5424', '5424', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (542425, '安多县', '3', '54', '5424', '5424', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (542426, '申扎县', '3', '54', '5424', '5424', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (542427, '索县', '3', '54', '5424', '5424', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (542428, '班戈县', '3', '54', '5424', '5424', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (542429, '巴青县', '3', '54', '5424', '5424', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (542430, '尼玛县', '3', '54', '5424', '5424', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (542431, '双湖县', '3', '54', '5424', '5424', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (542521, '普兰县', '3', '54', '5425', '5425', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (542522, '札达县', '3', '54', '5425', '5425', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (542523, '噶尔县', '3', '54', '5425', '5425', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (542524, '日土县', '3', '54', '5425', '5425', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (542525, '革吉县', '3', '54', '5425', '5425', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (542526, '改则县', '3', '54', '5425', '5425', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (542527, '措勤县', '3', '54', '5425', '5425', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610101, '市辖区', '3', '61', '6101', '6101', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610102, '新城区', '3', '61', '6101', '6101', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610103, '碑林区', '3', '61', '6101', '6101', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610104, '莲湖区', '3', '61', '6101', '6101', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610111, '灞桥区', '3', '61', '6101', '6101', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610112, '未央区', '3', '61', '6101', '6101', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610113, '雁塔区', '3', '61', '6101', '6101', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610114, '阎良区', '3', '61', '6101', '6101', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610115, '临潼区', '3', '61', '6101', '6101', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610116, '长安区', '3', '61', '6101', '6101', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610117, '高陵区', '3', '61', '6101', '6101', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610122, '蓝田县', '3', '61', '6101', '6101', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610124, '周至县', '3', '61', '6101', '6101', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610125, '户县', '3', '61', '6101', '6101', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610201, '市辖区', '3', '61', '6102', '6102', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610202, '王益区', '3', '61', '6102', '6102', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610203, '印台区', '3', '61', '6102', '6102', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610204, '耀州区', '3', '61', '6102', '6102', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610222, '宜君县', '3', '61', '6102', '6102', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610301, '市辖区', '3', '61', '6103', '6103', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610302, '渭滨区', '3', '61', '6103', '6103', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610303, '金台区', '3', '61', '6103', '6103', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610304, '陈仓区', '3', '61', '6103', '6103', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610322, '凤翔县', '3', '61', '6103', '6103', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610323, '岐山县', '3', '61', '6103', '6103', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610324, '扶风县', '3', '61', '6103', '6103', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610326, '眉县', '3', '61', '6103', '6103', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610327, '陇县', '3', '61', '6103', '6103', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610328, '千阳县', '3', '61', '6103', '6103', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610329, '麟游县', '3', '61', '6103', '6103', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610330, '凤县', '3', '61', '6103', '6103', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610331, '太白县', '3', '61', '6103', '6103', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610401, '市辖区', '3', '61', '6104', '6104', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610402, '秦都区', '3', '61', '6104', '6104', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610403, '杨陵区', '3', '61', '6104', '6104', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610404, '渭城区', '3', '61', '6104', '6104', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610422, '三原县', '3', '61', '6104', '6104', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610423, '泾阳县', '3', '61', '6104', '6104', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610424, '乾县', '3', '61', '6104', '6104', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610425, '礼泉县', '3', '61', '6104', '6104', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610426, '永寿县', '3', '61', '6104', '6104', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610427, '彬县', '3', '61', '6104', '6104', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610428, '长武县', '3', '61', '6104', '6104', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610429, '旬邑县', '3', '61', '6104', '6104', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610430, '淳化县', '3', '61', '6104', '6104', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610431, '武功县', '3', '61', '6104', '6104', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610481, '兴平市', '3', '61', '6104', '6104', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610501, '市辖区', '3', '61', '6105', '6105', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610502, '临渭区', '3', '61', '6105', '6105', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610503, '华州区', '3', '61', '6105', '6105', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610522, '潼关县', '3', '61', '6105', '6105', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610523, '大荔县', '3', '61', '6105', '6105', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610524, '合阳县', '3', '61', '6105', '6105', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610525, '澄城县', '3', '61', '6105', '6105', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610526, '蒲城县', '3', '61', '6105', '6105', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610527, '白水县', '3', '61', '6105', '6105', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610528, '富平县', '3', '61', '6105', '6105', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610581, '韩城市', '3', '61', '6105', '6105', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610582, '华阴市', '3', '61', '6105', '6105', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610601, '市辖区', '3', '61', '6106', '6106', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610602, '宝塔区', '3', '61', '6106', '6106', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610603, '安塞区', '3', '61', '6106', '6106', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610621, '延长县', '3', '61', '6106', '6106', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610622, '延川县', '3', '61', '6106', '6106', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610623, '子长县', '3', '61', '6106', '6106', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610625, '志丹县', '3', '61', '6106', '6106', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610626, '吴起县', '3', '61', '6106', '6106', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610627, '甘泉县', '3', '61', '6106', '6106', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610628, '富县', '3', '61', '6106', '6106', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610629, '洛川县', '3', '61', '6106', '6106', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610630, '宜川县', '3', '61', '6106', '6106', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610631, '黄龙县', '3', '61', '6106', '6106', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610632, '黄陵县', '3', '61', '6106', '6106', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610701, '市辖区', '3', '61', '6107', '6107', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610702, '汉台区', '3', '61', '6107', '6107', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610721, '南郑县', '3', '61', '6107', '6107', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610722, '城固县', '3', '61', '6107', '6107', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610723, '洋县', '3', '61', '6107', '6107', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610724, '西乡县', '3', '61', '6107', '6107', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610725, '勉县', '3', '61', '6107', '6107', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610726, '宁强县', '3', '61', '6107', '6107', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610727, '略阳县', '3', '61', '6107', '6107', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610728, '镇巴县', '3', '61', '6107', '6107', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610729, '留坝县', '3', '61', '6107', '6107', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610730, '佛坪县', '3', '61', '6107', '6107', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610801, '市辖区', '3', '61', '6108', '6108', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610802, '榆阳区', '3', '61', '6108', '6108', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610803, '横山区', '3', '61', '6108', '6108', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610821, '神木县', '3', '61', '6108', '6108', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610822, '府谷县', '3', '61', '6108', '6108', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610824, '靖边县', '3', '61', '6108', '6108', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610825, '定边县', '3', '61', '6108', '6108', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610826, '绥德县', '3', '61', '6108', '6108', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610827, '米脂县', '3', '61', '6108', '6108', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610828, '佳县', '3', '61', '6108', '6108', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610829, '吴堡县', '3', '61', '6108', '6108', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610830, '清涧县', '3', '61', '6108', '6108', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610831, '子洲县', '3', '61', '6108', '6108', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610901, '市辖区', '3', '61', '6109', '6109', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610902, '汉滨区', '3', '61', '6109', '6109', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610921, '汉阴县', '3', '61', '6109', '6109', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610922, '石泉县', '3', '61', '6109', '6109', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610923, '宁陕县', '3', '61', '6109', '6109', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610924, '紫阳县', '3', '61', '6109', '6109', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610925, '岚皋县', '3', '61', '6109', '6109', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610926, '平利县', '3', '61', '6109', '6109', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610927, '镇坪县', '3', '61', '6109', '6109', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610928, '旬阳县', '3', '61', '6109', '6109', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (610929, '白河县', '3', '61', '6109', '6109', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (611001, '市辖区', '3', '61', '6110', '6110', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (611002, '商州区', '3', '61', '6110', '6110', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (611021, '洛南县', '3', '61', '6110', '6110', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (611022, '丹凤县', '3', '61', '6110', '6110', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (611023, '商南县', '3', '61', '6110', '6110', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (611024, '山阳县', '3', '61', '6110', '6110', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (611025, '镇安县', '3', '61', '6110', '6110', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (611026, '柞水县', '3', '61', '6110', '6110', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (620101, '市辖区', '3', '62', '6201', '6201', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (620102, '城关区', '3', '62', '6201', '6201', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (620103, '七里河区', '3', '62', '6201', '6201', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (620104, '西固区', '3', '62', '6201', '6201', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (620105, '安宁区', '3', '62', '6201', '6201', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (620111, '红古区', '3', '62', '6201', '6201', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (620121, '永登县', '3', '62', '6201', '6201', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (620122, '皋兰县', '3', '62', '6201', '6201', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (620123, '榆中县', '3', '62', '6201', '6201', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (620201, '市辖区', '3', '62', '6202', '6202', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (620301, '市辖区', '3', '62', '6203', '6203', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (620302, '金川区', '3', '62', '6203', '6203', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (620321, '永昌县', '3', '62', '6203', '6203', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (620401, '市辖区', '3', '62', '6204', '6204', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (620402, '白银区', '3', '62', '6204', '6204', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (620403, '平川区', '3', '62', '6204', '6204', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (620421, '靖远县', '3', '62', '6204', '6204', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (620422, '会宁县', '3', '62', '6204', '6204', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (620423, '景泰县', '3', '62', '6204', '6204', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (620501, '市辖区', '3', '62', '6205', '6205', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (620502, '秦州区', '3', '62', '6205', '6205', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (620503, '麦积区', '3', '62', '6205', '6205', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (620521, '清水县', '3', '62', '6205', '6205', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (620522, '秦安县', '3', '62', '6205', '6205', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (620523, '甘谷县', '3', '62', '6205', '6205', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (620524, '武山县', '3', '62', '6205', '6205', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (620525, '张家川回族自治县', '3', '62', '6205', '6205', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (620601, '市辖区', '3', '62', '6206', '6206', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (620602, '凉州区', '3', '62', '6206', '6206', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (620621, '民勤县', '3', '62', '6206', '6206', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (620622, '古浪县', '3', '62', '6206', '6206', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (620623, '天祝藏族自治县', '3', '62', '6206', '6206', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (620701, '市辖区', '3', '62', '6207', '6207', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (620702, '甘州区', '3', '62', '6207', '6207', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (620721, '肃南裕固族自治县', '3', '62', '6207', '6207', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (620722, '民乐县', '3', '62', '6207', '6207', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (620723, '临泽县', '3', '62', '6207', '6207', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (620724, '高台县', '3', '62', '6207', '6207', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (620725, '山丹县', '3', '62', '6207', '6207', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (620801, '市辖区', '3', '62', '6208', '6208', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (620802, '崆峒区', '3', '62', '6208', '6208', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (620821, '泾川县', '3', '62', '6208', '6208', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (620822, '灵台县', '3', '62', '6208', '6208', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (620823, '崇信县', '3', '62', '6208', '6208', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (620824, '华亭县', '3', '62', '6208', '6208', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (620825, '庄浪县', '3', '62', '6208', '6208', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (620826, '静宁县', '3', '62', '6208', '6208', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (620901, '市辖区', '3', '62', '6209', '6209', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (620902, '肃州区', '3', '62', '6209', '6209', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (620921, '金塔县', '3', '62', '6209', '6209', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (620922, '瓜州县', '3', '62', '6209', '6209', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (620923, '肃北蒙古族自治县', '3', '62', '6209', '6209', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (620924, '阿克塞哈萨克族自治县', '3', '62', '6209', '6209', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (620981, '玉门市', '3', '62', '6209', '6209', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (620982, '敦煌市', '3', '62', '6209', '6209', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (621001, '市辖区', '3', '62', '6210', '6210', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (621002, '西峰区', '3', '62', '6210', '6210', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (621021, '庆城县', '3', '62', '6210', '6210', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (621022, '环县', '3', '62', '6210', '6210', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (621023, '华池县', '3', '62', '6210', '6210', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (621024, '合水县', '3', '62', '6210', '6210', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (621025, '正宁县', '3', '62', '6210', '6210', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (621026, '宁县', '3', '62', '6210', '6210', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (621027, '镇原县', '3', '62', '6210', '6210', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (621101, '市辖区', '3', '62', '6211', '6211', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (621102, '安定区', '3', '62', '6211', '6211', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (621121, '通渭县', '3', '62', '6211', '6211', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (621122, '陇西县', '3', '62', '6211', '6211', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (621123, '渭源县', '3', '62', '6211', '6211', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (621124, '临洮县', '3', '62', '6211', '6211', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (621125, '漳县', '3', '62', '6211', '6211', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (621126, '岷县', '3', '62', '6211', '6211', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (621201, '市辖区', '3', '62', '6212', '6212', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (621202, '武都区', '3', '62', '6212', '6212', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (621221, '成县', '3', '62', '6212', '6212', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (621222, '文县', '3', '62', '6212', '6212', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (621223, '宕昌县', '3', '62', '6212', '6212', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (621224, '康县', '3', '62', '6212', '6212', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (621225, '西和县', '3', '62', '6212', '6212', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (621226, '礼县', '3', '62', '6212', '6212', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (621227, '徽县', '3', '62', '6212', '6212', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (621228, '两当县', '3', '62', '6212', '6212', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (622901, '临夏市', '3', '62', '6229', '6229', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (622921, '临夏县', '3', '62', '6229', '6229', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (622922, '康乐县', '3', '62', '6229', '6229', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (622923, '永靖县', '3', '62', '6229', '6229', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (622924, '广河县', '3', '62', '6229', '6229', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (622925, '和政县', '3', '62', '6229', '6229', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (622926, '东乡族自治县', '3', '62', '6229', '6229', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (622927, '积石山保安族东乡族撒拉族自治县', '3', '62', '6229', '6229', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (623001, '合作市', '3', '62', '6230', '6230', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (623021, '临潭县', '3', '62', '6230', '6230', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (623022, '卓尼县', '3', '62', '6230', '6230', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (623023, '舟曲县', '3', '62', '6230', '6230', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (623024, '迭部县', '3', '62', '6230', '6230', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (623025, '玛曲县', '3', '62', '6230', '6230', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (623026, '碌曲县', '3', '62', '6230', '6230', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (623027, '夏河县', '3', '62', '6230', '6230', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (630101, '市辖区', '3', '63', '6301', '6301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (630102, '城东区', '3', '63', '6301', '6301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (630103, '城中区', '3', '63', '6301', '6301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (630104, '城西区', '3', '63', '6301', '6301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (630105, '城北区', '3', '63', '6301', '6301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (630121, '大通回族土族自治县', '3', '63', '6301', '6301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (630122, '湟中县', '3', '63', '6301', '6301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (630123, '湟源县', '3', '63', '6301', '6301', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (630202, '乐都区', '3', '63', '6302', '6302', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (630203, '平安区', '3', '63', '6302', '6302', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (630222, '民和回族土族自治县', '3', '63', '6302', '6302', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (630223, '互助土族自治县', '3', '63', '6302', '6302', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (630224, '化隆回族自治县', '3', '63', '6302', '6302', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (630225, '循化撒拉族自治县', '3', '63', '6302', '6302', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (632221, '门源回族自治县', '3', '63', '6322', '6322', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (632222, '祁连县', '3', '63', '6322', '6322', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (632223, '海晏县', '3', '63', '6322', '6322', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (632224, '刚察县', '3', '63', '6322', '6322', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (632321, '同仁县', '3', '63', '6323', '6323', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (632322, '尖扎县', '3', '63', '6323', '6323', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (632323, '泽库县', '3', '63', '6323', '6323', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (632324, '河南蒙古族自治县', '3', '63', '6323', '6323', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (632521, '共和县', '3', '63', '6325', '6325', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (632522, '同德县', '3', '63', '6325', '6325', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (632523, '贵德县', '3', '63', '6325', '6325', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (632524, '兴海县', '3', '63', '6325', '6325', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (632525, '贵南县', '3', '63', '6325', '6325', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (632621, '玛沁县', '3', '63', '6326', '6326', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (632622, '班玛县', '3', '63', '6326', '6326', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (632623, '甘德县', '3', '63', '6326', '6326', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (632624, '达日县', '3', '63', '6326', '6326', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (632625, '久治县', '3', '63', '6326', '6326', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (632626, '玛多县', '3', '63', '6326', '6326', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (632701, '玉树市', '3', '63', '6327', '6327', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (632722, '杂多县', '3', '63', '6327', '6327', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (632723, '称多县', '3', '63', '6327', '6327', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (632724, '治多县', '3', '63', '6327', '6327', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (632725, '囊谦县', '3', '63', '6327', '6327', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (632726, '曲麻莱县', '3', '63', '6327', '6327', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (632801, '格尔木市', '3', '63', '6328', '6328', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (632802, '德令哈市', '3', '63', '6328', '6328', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (632821, '乌兰县', '3', '63', '6328', '6328', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (632822, '都兰县', '3', '63', '6328', '6328', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (632823, '天峻县', '3', '63', '6328', '6328', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (640101, '市辖区', '3', '64', '6401', '6401', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (640104, '兴庆区', '3', '64', '6401', '6401', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (640105, '西夏区', '3', '64', '6401', '6401', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (640106, '金凤区', '3', '64', '6401', '6401', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (640121, '永宁县', '3', '64', '6401', '6401', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (640122, '贺兰县', '3', '64', '6401', '6401', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (640181, '灵武市', '3', '64', '6401', '6401', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (640201, '市辖区', '3', '64', '6402', '6402', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (640202, '大武口区', '3', '64', '6402', '6402', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (640205, '惠农区', '3', '64', '6402', '6402', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (640221, '平罗县', '3', '64', '6402', '6402', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (640301, '市辖区', '3', '64', '6403', '6403', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (640302, '利通区', '3', '64', '6403', '6403', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (640303, '红寺堡区', '3', '64', '6403', '6403', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (640323, '盐池县', '3', '64', '6403', '6403', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (640324, '同心县', '3', '64', '6403', '6403', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (640381, '青铜峡市', '3', '64', '6403', '6403', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (640401, '市辖区', '3', '64', '6404', '6404', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (640402, '原州区', '3', '64', '6404', '6404', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (640422, '西吉县', '3', '64', '6404', '6404', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (640423, '隆德县', '3', '64', '6404', '6404', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (640424, '泾源县', '3', '64', '6404', '6404', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (640425, '彭阳县', '3', '64', '6404', '6404', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (640501, '市辖区', '3', '64', '6405', '6405', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (640502, '沙坡头区', '3', '64', '6405', '6405', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (640521, '中宁县', '3', '64', '6405', '6405', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (640522, '海原县', '3', '64', '6405', '6405', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (650101, '市辖区', '3', '65', '6501', '6501', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (650102, '天山区', '3', '65', '6501', '6501', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (650103, '沙依巴克区', '3', '65', '6501', '6501', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (650104, '新市区', '3', '65', '6501', '6501', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (650105, '水磨沟区', '3', '65', '6501', '6501', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (650106, '头屯河区', '3', '65', '6501', '6501', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (650107, '达坂城区', '3', '65', '6501', '6501', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (650109, '米东区', '3', '65', '6501', '6501', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (650121, '乌鲁木齐县', '3', '65', '6501', '6501', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (650201, '市辖区', '3', '65', '6502', '6502', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (650202, '独山子区', '3', '65', '6502', '6502', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (650203, '克拉玛依区', '3', '65', '6502', '6502', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (650204, '白碱滩区', '3', '65', '6502', '6502', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (650205, '乌尔禾区', '3', '65', '6502', '6502', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (650402, '高昌区', '3', '65', '6504', '6504', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (650421, '鄯善县', '3', '65', '6504', '6504', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (650422, '托克逊县', '3', '65', '6504', '6504', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (650502, '伊州区', '3', '65', '6505', '6505', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (650521, '巴里坤哈萨克自治县', '3', '65', '6505', '6505', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (650522, '伊吾县', '3', '65', '6505', '6505', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (652301, '昌吉市', '3', '65', '6523', '6523', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (652302, '阜康市', '3', '65', '6523', '6523', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (652323, '呼图壁县', '3', '65', '6523', '6523', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (652324, '玛纳斯县', '3', '65', '6523', '6523', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (652325, '奇台县', '3', '65', '6523', '6523', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (652327, '吉木萨尔县', '3', '65', '6523', '6523', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (652328, '木垒哈萨克自治县', '3', '65', '6523', '6523', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (652701, '博乐市', '3', '65', '6527', '6527', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (652702, '阿拉山口市', '3', '65', '6527', '6527', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (652722, '精河县', '3', '65', '6527', '6527', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (652723, '温泉县', '3', '65', '6527', '6527', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (652801, '库尔勒市', '3', '65', '6528', '6528', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (652822, '轮台县', '3', '65', '6528', '6528', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (652823, '尉犁县', '3', '65', '6528', '6528', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (652824, '若羌县', '3', '65', '6528', '6528', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (652825, '且末县', '3', '65', '6528', '6528', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (652826, '焉耆回族自治县', '3', '65', '6528', '6528', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (652827, '和静县', '3', '65', '6528', '6528', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (652828, '和硕县', '3', '65', '6528', '6528', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (652829, '博湖县', '3', '65', '6528', '6528', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (652901, '阿克苏市', '3', '65', '6529', '6529', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (652922, '温宿县', '3', '65', '6529', '6529', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (652923, '库车县', '3', '65', '6529', '6529', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (652924, '沙雅县', '3', '65', '6529', '6529', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (652925, '新和县', '3', '65', '6529', '6529', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (652926, '拜城县', '3', '65', '6529', '6529', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (652927, '乌什县', '3', '65', '6529', '6529', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (652928, '阿瓦提县', '3', '65', '6529', '6529', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (652929, '柯坪县', '3', '65', '6529', '6529', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (653001, '阿图什市', '3', '65', '6530', '6530', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (653022, '阿克陶县', '3', '65', '6530', '6530', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (653023, '阿合奇县', '3', '65', '6530', '6530', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (653024, '乌恰县', '3', '65', '6530', '6530', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (653101, '喀什市', '3', '65', '6531', '6531', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (653121, '疏附县', '3', '65', '6531', '6531', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (653122, '疏勒县', '3', '65', '6531', '6531', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (653123, '英吉沙县', '3', '65', '6531', '6531', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (653124, '泽普县', '3', '65', '6531', '6531', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (653125, '莎车县', '3', '65', '6531', '6531', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (653126, '叶城县', '3', '65', '6531', '6531', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (653127, '麦盖提县', '3', '65', '6531', '6531', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (653128, '岳普湖县', '3', '65', '6531', '6531', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (653129, '伽师县', '3', '65', '6531', '6531', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (653130, '巴楚县', '3', '65', '6531', '6531', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (653131, '塔什库尔干塔吉克自治县', '3', '65', '6531', '6531', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (653201, '和田市', '3', '65', '6532', '6532', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (653221, '和田县', '3', '65', '6532', '6532', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (653222, '墨玉县', '3', '65', '6532', '6532', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (653223, '皮山县', '3', '65', '6532', '6532', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (653224, '洛浦县', '3', '65', '6532', '6532', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (653225, '策勒县', '3', '65', '6532', '6532', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (653226, '于田县', '3', '65', '6532', '6532', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (653227, '民丰县', '3', '65', '6532', '6532', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (654002, '伊宁市', '3', '65', '6540', '6540', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (654003, '奎屯市', '3', '65', '6540', '6540', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (654004, '霍尔果斯市', '3', '65', '6540', '6540', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (654021, '伊宁县', '3', '65', '6540', '6540', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (654022, '察布查尔锡伯自治县', '3', '65', '6540', '6540', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (654023, '霍城县', '3', '65', '6540', '6540', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (654024, '巩留县', '3', '65', '6540', '6540', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (654025, '新源县', '3', '65', '6540', '6540', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (654026, '昭苏县', '3', '65', '6540', '6540', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (654027, '特克斯县', '3', '65', '6540', '6540', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (654028, '尼勒克县', '3', '65', '6540', '6540', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (654201, '塔城市', '3', '65', '6542', '6542', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (654202, '乌苏市', '3', '65', '6542', '6542', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (654221, '额敏县', '3', '65', '6542', '6542', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (654223, '沙湾县', '3', '65', '6542', '6542', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (654224, '托里县', '3', '65', '6542', '6542', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (654225, '裕民县', '3', '65', '6542', '6542', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (654226, '和布克赛尔蒙古自治县', '3', '65', '6542', '6542', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (654301, '阿勒泰市', '3', '65', '6543', '6543', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (654321, '布尔津县', '3', '65', '6543', '6543', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (654322, '富蕴县', '3', '65', '6543', '6543', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (654323, '福海县', '3', '65', '6543', '6543', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (654324, '哈巴河县', '3', '65', '6543', '6543', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (654325, '青河县', '3', '65', '6543', '6543', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (654326, '吉木乃县', '3', '65', '6543', '6543', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (659001, '石河子市', '3', '65', '6590', '6590', 0, 358, '', '');
INSERT INTO `sq_region` VALUES (659002, '阿拉尔市', '3', '65', '6590', '6590', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (659003, '图木舒克市', '3', '65', '6590', '6590', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (659004, '五家渠市', '3', '65', '6590', '6590', 0, NULL, '', '');
INSERT INTO `sq_region` VALUES (659006, '铁门关市', '3', '65', '6590', '6590', 0, NULL, '', '');

-- ----------------------------
-- Table structure for sq_shop
-- ----------------------------
DROP TABLE IF EXISTS `sq_shop`;
CREATE TABLE `sq_shop`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '店铺名称',
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图片',
  `province_id` int(11) NULL DEFAULT NULL,
  `city_id` int(11) NULL DEFAULT NULL,
  `district_id` int(11) NULL DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '详细地址',
  `mobile` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系方式',
  `user_id` int(11) NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `is_delete` tinyint(1) NULL DEFAULT 2 COMMENT '1是 2否',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '店铺管理' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of sq_shop
-- ----------------------------
INSERT INTO `sq_shop` VALUES (12, '爱亲母婴店', '/files/20210819/-1629343760-35617.png', 37, 3702, 370202, '香港中路2230号', '15859652362', NULL, '2021-08-19 09:47:35', '2021-08-19 11:30:16', 2);

-- ----------------------------
-- Table structure for sq_spokesman
-- ----------------------------
DROP TABLE IF EXISTS `sq_spokesman`;
CREATE TABLE `sq_spokesman`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `video` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '最后修改时间',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '注册时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '明星推介' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of sq_spokesman
-- ----------------------------
INSERT INTO `sq_spokesman` VALUES (1, '/files/20210412/-1618190003-30251.jpg', '万哺乐HN001鼠李糖乳杆菌滴剂加拿大进口12ml', '<p><img src=\"http://yun.com/files/rich_text/-1623037182-3187.png\" style=\"max-width:100%;\"/><br/></p>', '<p><video src=\"http://yun.com/files/video/-1623037637-28220.mp4\" controls=\"controls\" style=\"max-width:100%\"></video></p>', '2021-06-07 11:47:20', '2020-12-01 10:31:10');

-- ----------------------------
-- Table structure for sq_standard_height
-- ----------------------------
DROP TABLE IF EXISTS `sq_standard_height`;
CREATE TABLE `sq_standard_height`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `age` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '年龄',
  `month` int(11) NULL DEFAULT NULL,
  `sex` int(11) NULL DEFAULT NULL COMMENT '1男 2女',
  `percent_3` decimal(5, 1) NULL DEFAULT NULL COMMENT '百分位',
  `percent_10` decimal(5, 1) NULL DEFAULT NULL COMMENT '百分位',
  `percent_25` decimal(5, 1) NULL DEFAULT NULL COMMENT '百分位',
  `percent_50` decimal(5, 1) NULL DEFAULT NULL COMMENT '百分位',
  `percent_75` decimal(5, 1) NULL DEFAULT NULL COMMENT '百分位',
  `percent_90` decimal(5, 1) NULL DEFAULT NULL COMMENT '百分位',
  `percent_97` decimal(5, 1) NULL DEFAULT NULL COMMENT '百分位',
  `avg_rate` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '平均成长速度',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 120 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '标准身高表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of sq_standard_height
-- ----------------------------
INSERT INTO `sq_standard_height` VALUES (1, '0', 0, 1, 47.1, 48.1, 49.2, 50.4, 51.6, 52.7, 53.8, NULL);
INSERT INTO `sq_standard_height` VALUES (30, '0.17', 2, 1, 54.6, 55.9, 57.2, 58.7, 60.3, 61.7, 63.0, NULL);
INSERT INTO `sq_standard_height` VALUES (31, '0.33', 4, 1, 60.3, 61.7, 63.0, 64.6, 66.2, 67.6, 69.0, NULL);
INSERT INTO `sq_standard_height` VALUES (32, '0.5', 6, 1, 64.0, 65.4, 66.8, 68.4, 70.0, 71.5, 73.0, NULL);
INSERT INTO `sq_standard_height` VALUES (33, '0.75', 9, 1, 67.9, 69.4, 70.9, 72.6, 74.4, 75.9, 77.5, NULL);
INSERT INTO `sq_standard_height` VALUES (34, '1', 12, 1, 71.5, 73.1, 74.7, 76.5, 78.4, 80.1, 81.8, '24-28');
INSERT INTO `sq_standard_height` VALUES (35, '1.25', 15, 1, 74.4, 76.1, 77.8, 79.8, 81.8, 83.6, 85.4, NULL);
INSERT INTO `sq_standard_height` VALUES (36, '1.5', 18, 1, 76.9, 78.7, 80.6, 82.7, 84.8, 86.7, 88.7, NULL);
INSERT INTO `sq_standard_height` VALUES (37, '1.75', 21, 1, 79.5, 81.4, 83.4, 85.6, 87.9, 90.0, 92.0, NULL);
INSERT INTO `sq_standard_height` VALUES (38, '2', 24, 1, 82.1, 84.1, 86.2, 88.5, 90.9, 93.1, 95.3, '12-15');
INSERT INTO `sq_standard_height` VALUES (39, '3', 36, 1, 89.7, 91.9, 94.2, 96.8, 99.4, 101.8, 104.1, '8.4');
INSERT INTO `sq_standard_height` VALUES (40, '4', 48, 1, 96.7, 99.1, 101.4, 104.1, 106.9, 109.3, 111.8, '7.4');
INSERT INTO `sq_standard_height` VALUES (41, '5', 60, 1, 103.3, 105.8, 108.4, 111.3, 114.2, 116.9, 119.6, '6.9');
INSERT INTO `sq_standard_height` VALUES (42, '6', 72, 1, 109.1, 111.8, 114.6, 117.7, 120.9, 123.7, 126.6, '6.9');
INSERT INTO `sq_standard_height` VALUES (43, '7', 84, 1, 114.6, 117.6, 120.0, 124.0, 127.4, 130.5, 133.7, '6.1');
INSERT INTO `sq_standard_height` VALUES (44, '8', 96, 1, 119.9, 123.1, 126.3, 130.0, 133.7, 137.1, 140.4, '5.6');
INSERT INTO `sq_standard_height` VALUES (45, '9', 108, 1, 124.6, 128.0, 131.4, 135.4, 139.3, 142.9, 146.5, '5.6');
INSERT INTO `sq_standard_height` VALUES (46, '10', 120, 1, 128.7, 132.3, 136.0, 140.2, 144.4, 148.2, 152.0, '5.1');
INSERT INTO `sq_standard_height` VALUES (47, '11', 132, 1, 132.9, 136.8, 140.8, 145.3, 149.9, 154.0, 158.1, '5.1');
INSERT INTO `sq_standard_height` VALUES (48, '12', 144, 1, 138.1, 142.5, 147.0, 151.9, 157.0, 161.5, 166.0, '5.1');
INSERT INTO `sq_standard_height` VALUES (49, '13', 156, 1, 145.0, 149.6, 154.3, 159.5, 164.8, 169.5, 174.2, '6.6');
INSERT INTO `sq_standard_height` VALUES (50, '14', 168, 1, 152.3, 156.7, 161.0, 165.9, 170.7, 175.1, 179.4, '9.2');
INSERT INTO `sq_standard_height` VALUES (51, '15', 180, 1, 157.5, 161.4, 165.4, 169.8, 174.2, 178.2, 182.0, '6.5');
INSERT INTO `sq_standard_height` VALUES (52, '16', 192, 1, 159.9, 163.6, 167.4, 171.6, 175.8, 179.5, 183.2, '3.1');
INSERT INTO `sq_standard_height` VALUES (53, '17', 204, 1, 160.9, 164.5, 168.2, 172.3, 176.4, 180.1, 183.7, '1.5');
INSERT INTO `sq_standard_height` VALUES (54, '18', 216, 1, 161.3, 164.9, 168.6, 172.7, 176.7, 180.4, 183.9, '1.0');
INSERT INTO `sq_standard_height` VALUES (55, '0', 0, 2, 46.6, 47.5, 48.6, 49.7, 50.9, 51.9, 53.0, NULL);
INSERT INTO `sq_standard_height` VALUES (56, '0.17', 2, 2, 53.4, 54.7, 56.0, 57.4, 58.9, 60.2, 61.6, NULL);
INSERT INTO `sq_standard_height` VALUES (57, '0.33', 4, 2, 59.1, 60.3, 61.7, 63.1, 64.6, 66.0, 67.4, NULL);
INSERT INTO `sq_standard_height` VALUES (58, '0.5', 6, 2, 62.5, 63.9, 65.2, 66.8, 68.4, 69.8, 71.2, NULL);
INSERT INTO `sq_standard_height` VALUES (59, '0.75', 9, 2, 66.4, 67.8, 69.3, 71.0, 72.8, 74.3, 75.9, NULL);
INSERT INTO `sq_standard_height` VALUES (60, '1', 12, 2, 70.0, 71.6, 73.2, 75.0, 76.8, 78.5, 80.2, '24-28');
INSERT INTO `sq_standard_height` VALUES (70, '1.25', 15, 2, 73.2, 74.9, 76.6, 78.5, 80.4, 82.2, 84.0, NULL);
INSERT INTO `sq_standard_height` VALUES (71, '1.5', 18, 2, 76.0, 77.7, 79.5, 81.5, 83.6, 85.5, 87.4, NULL);
INSERT INTO `sq_standard_height` VALUES (72, '1.75', 21, 2, 78.5, 80.4, 82.3, 84.4, 86.6, 88.6, 90.7, NULL);
INSERT INTO `sq_standard_height` VALUES (73, '2', 24, 2, 80.9, 82.9, 84.9, 87.2, 89.6, 91.7, 93.9, '12-15');
INSERT INTO `sq_standard_height` VALUES (74, '3', 36, 2, 88.6, 90.8, 93.1, 95.6, 98.2, 100.5, 102.9, '8.4');
INSERT INTO `sq_standard_height` VALUES (75, '4', 48, 2, 95.8, 98.1, 100.4, 103.1, 105.7, 108.2, 110.6, '7.4');
INSERT INTO `sq_standard_height` VALUES (76, '5', 60, 2, 102.3, 104.8, 107.3, 110.2, 113.1, 115.7, 118.4, '6.9');
INSERT INTO `sq_standard_height` VALUES (77, '6', 72, 2, 108.1, 110.8, 113.5, 116.6, 119.7, 122.5, 125.4, '6.9');
INSERT INTO `sq_standard_height` VALUES (78, '7', 84, 2, 113.3, 116.2, 119.2, 122.5, 125.9, 129.0, 132.1, '6.1');
INSERT INTO `sq_standard_height` VALUES (79, '8', 96, 2, 118.5, 121.6, 124.9, 128.5, 132.1, 135.4, 138.7, '5.8');
INSERT INTO `sq_standard_height` VALUES (80, '9', 108, 2, 123.3, 126.7, 130.2, 134.1, 138.0, 141.6, 145.1, '5.6');
INSERT INTO `sq_standard_height` VALUES (81, '10', 120, 2, 128.3, 132.1, 135.9, 140.1, 144.4, 148.2, 152.0, '5.8');
INSERT INTO `sq_standard_height` VALUES (82, '11', 132, 2, 134.2, 138.2, 142.2, 146.6, 151.1, 155.2, 159.2, '6.9');
INSERT INTO `sq_standard_height` VALUES (83, '12', 144, 2, 140.2, 144.1, 148.0, 152.4, 156.7, 160.7, 164.5, '8.3');
INSERT INTO `sq_standard_height` VALUES (84, '13', 156, 2, 145.0, 148.6, 152.2, 156.3, 160.3, 164.0, 167.6, '5.8');
INSERT INTO `sq_standard_height` VALUES (85, '14', 168, 2, 147.9, 151.3, 154.8, 158.6, 162.4, 165.9, 169.3, '3.0');
INSERT INTO `sq_standard_height` VALUES (86, '15', 180, 2, 149.5, 152.8, 156.1, 159.8, 163.5, 166.8, 170.1, '0.8');
INSERT INTO `sq_standard_height` VALUES (87, '16', 192, 2, 149.8, 153.1, 156.4, 160.1, 163.8, 167.1, 170.4, '0.1');
INSERT INTO `sq_standard_height` VALUES (88, '17', 204, 2, 150.1, 153.4, 156.7, 160.3, 164.0, 167.3, 170.5, '0.1');
INSERT INTO `sq_standard_height` VALUES (89, '18', 216, 2, 150.4, 153.7, 157.0, 160.6, 164.2, 167.5, 170.7, '0.1');
INSERT INTO `sq_standard_height` VALUES (90, '2.5', 30, 1, 86.4, 88.6, 90.8, 93.3, 95.9, 98.2, 100.5, NULL);
INSERT INTO `sq_standard_height` VALUES (91, '3.5', 42, 1, 93.4, 95.7, 98.0, 100.6, 103.2, 105.7, 108.1, NULL);
INSERT INTO `sq_standard_height` VALUES (92, '4.5', 54, 1, 100.0, 102.4, 104.9, 107.7, 110.5, 113.1, 115.7, NULL);
INSERT INTO `sq_standard_height` VALUES (93, '5.5', 66, 1, 106.4, 109.0, 111.7, 114.7, 117.7, 120.5, 123.3, NULL);
INSERT INTO `sq_standard_height` VALUES (94, '6.5', 78, 1, 111.7, 114.5, 117.4, 120.7, 123.9, 126.9, 129.9, NULL);
INSERT INTO `sq_standard_height` VALUES (95, '7.5', 90, 1, 117.4, 120.5, 123.6, 127.1, 130.7, 133.9, 137.2, NULL);
INSERT INTO `sq_standard_height` VALUES (96, '8.5', 102, 1, 122.3, 125.6, 129.0, 132.7, 136.6, 140.1, 143.6, NULL);
INSERT INTO `sq_standard_height` VALUES (97, '9.5', 114, 1, 126.7, 130.3, 133.9, 137.9, 142.0, 145.7, 149.4, NULL);
INSERT INTO `sq_standard_height` VALUES (98, '10.5', 126, 1, 130.7, 134.5, 138.3, 142.6, 147.0, 150.9, 154.9, NULL);
INSERT INTO `sq_standard_height` VALUES (99, '11.5', 138, 1, 135.3, 139.5, 143.7, 148.4, 153.1, 157.4, 161.7, NULL);
INSERT INTO `sq_standard_height` VALUES (100, '12.5', 150, 1, 141.1, 145.7, 150.4, 155.6, 160.8, 165.5, 170.2, NULL);
INSERT INTO `sq_standard_height` VALUES (101, '13.5', 162, 1, 148.8, 153.3, 157.9, 163.0, 168.1, 172.7, 177.2, NULL);
INSERT INTO `sq_standard_height` VALUES (102, '14.5', 174, 1, 155.3, 159.4, 163.6, 168.2, 172.8, 176.9, 181.0, NULL);
INSERT INTO `sq_standard_height` VALUES (103, '15.5', 186, 1, 159.1, 162.9, 166.7, 171.0, 175.2, 179.1, 182.8, NULL);
INSERT INTO `sq_standard_height` VALUES (104, '16.5', 198, 1, 160.5, 164.2, 167.9, 172.1, 176.2, 179.9, 183.5, NULL);
INSERT INTO `sq_standard_height` VALUES (105, '2.5', 30, 2, 85.2, 87.4, 89.6, 92.1, 94.6, 97.0, 99.3, NULL);
INSERT INTO `sq_standard_height` VALUES (106, '3.5', 42, 2, 92.4, 94.6, 96.8, 99.4, 102.0, 104.4, 106.8, NULL);
INSERT INTO `sq_standard_height` VALUES (107, '4.5', 54, 2, 99.2, 101.5, 104.0, 106.7, 109.5, 112.1, 114.7, NULL);
INSERT INTO `sq_standard_height` VALUES (108, '5.5', 66, 2, 105.4, 108.0, 110.6, 113.5, 116.5, 119.3, 122.0, NULL);
INSERT INTO `sq_standard_height` VALUES (109, '6.5', 78, 2, 110.6, 113.4, 116.2, 119.4, 122.7, 125.6, 128.6, NULL);
INSERT INTO `sq_standard_height` VALUES (110, '7.5', 90, 2, 116.0, 119.0, 122.1, 125.6, 129.1, 132.3, 135.5, NULL);
INSERT INTO `sq_standard_height` VALUES (111, '8.5', 102, 2, 121.0, 124.2, 127.6, 131.3, 135.1, 138.5, 141.9, NULL);
INSERT INTO `sq_standard_height` VALUES (112, '9.5', 114, 2, 125.7, 129.3, 132.9, 137.0, 141.1, 144.8, 148.5, NULL);
INSERT INTO `sq_standard_height` VALUES (113, '10.5', 126, 2, 131.1, 135.0, 138.9, 143.3, 147.7, 151.6, 155.6, NULL);
INSERT INTO `sq_standard_height` VALUES (114, '11.5', 138, 2, 137.2, 141.2, 145.2, 149.7, 154.1, 158.2, 162.1, NULL);
INSERT INTO `sq_standard_height` VALUES (115, '12.5', 150, 2, 142.9, 146.6, 150.4, 154.6, 158.8, 162.6, 166.3, NULL);
INSERT INTO `sq_standard_height` VALUES (116, '13.5', 162, 2, 146.7, 150.2, 153.7, 157.6, 161.6, 165.1, 168.6, NULL);
INSERT INTO `sq_standard_height` VALUES (117, '14.5', 174, 2, 148.9, 152.2, 155.6, 159.4, 163.1, 166.5, 169.8, NULL);
INSERT INTO `sq_standard_height` VALUES (118, '15.5', 186, 2, 149.9, 153.1, 156.5, 160.1, 163.8, 167.1, 170.3, NULL);
INSERT INTO `sq_standard_height` VALUES (119, '16.5', 198, 2, 149.9, 153.2, 156.5, 160.2, 163.8, 167.1, 170.4, NULL);

-- ----------------------------
-- Table structure for sq_standard_weight
-- ----------------------------
DROP TABLE IF EXISTS `sq_standard_weight`;
CREATE TABLE `sq_standard_weight`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `age` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '年龄',
  `month` int(11) NULL DEFAULT NULL,
  `sex` int(11) NULL DEFAULT NULL COMMENT '1男 2女',
  `percent_3` decimal(5, 2) NULL DEFAULT NULL COMMENT '百分位',
  `percent_10` decimal(5, 2) NULL DEFAULT NULL COMMENT '百分位',
  `percent_25` decimal(5, 2) NULL DEFAULT NULL COMMENT '百分位',
  `percent_50` decimal(5, 2) NULL DEFAULT NULL COMMENT '百分位',
  `percent_75` decimal(5, 2) NULL DEFAULT NULL COMMENT '百分位',
  `percent_90` decimal(5, 2) NULL DEFAULT NULL COMMENT '百分位',
  `percent_97` decimal(5, 2) NULL DEFAULT NULL COMMENT '百分位',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 120 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '标准身高表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of sq_standard_weight
-- ----------------------------
INSERT INTO `sq_standard_weight` VALUES (1, '0', 0, 1, 2.62, 2.83, 3.06, 3.32, 3.59, 3.85, 4.12);
INSERT INTO `sq_standard_weight` VALUES (30, '0.17', 2, 1, 4.53, 4.88, 5.25, 5.68, 6.15, 6.59, 7.05);
INSERT INTO `sq_standard_weight` VALUES (31, '0.33', 4, 1, 5.99, 6.43, 6.90, 7.45, 8.04, 8.61, 9.20);
INSERT INTO `sq_standard_weight` VALUES (32, '0.5', 6, 1, 6.80, 7.28, 7.80, 8.41, 9.07, 9.70, 10.37);
INSERT INTO `sq_standard_weight` VALUES (33, '0.75', 9, 1, 7.56, 8.09, 8.66, 9.33, 10.06, 10.75, 11.49);
INSERT INTO `sq_standard_weight` VALUES (34, '1', 12, 1, 8.16, 8.72, 9.33, 10.05, 10.83, 11.58, 12.37);
INSERT INTO `sq_standard_weight` VALUES (35, '1.25', 15, 1, 8.68, 9.27, 9.91, 10.68, 11.51, 12.30, 13.15);
INSERT INTO `sq_standard_weight` VALUES (36, '1.5', 18, 1, 9.19, 9.81, 10.48, 11.29, 12.16, 13.01, 13.90);
INSERT INTO `sq_standard_weight` VALUES (37, '1.75', 21, 1, 9.71, 10.37, 11.08, 11.93, 12.86, 13.75, 14.70);
INSERT INTO `sq_standard_weight` VALUES (38, '2', 24, 1, 10.22, 10.90, 11.65, 12.54, 13.51, 14.46, 15.46);
INSERT INTO `sq_standard_weight` VALUES (39, '3', 36, 1, 11.94, 12.74, 13.61, 14.65, 15.80, 16.92, 18.12);
INSERT INTO `sq_standard_weight` VALUES (40, '4', 48, 1, 13.52, 14.43, 15.43, 16.64, 17.98, 19.29, 20.71);
INSERT INTO `sq_standard_weight` VALUES (41, '5', 60, 1, 15.26, 16.33, 17.52, 18.98, 20.61, 22.23, 24.00);
INSERT INTO `sq_standard_weight` VALUES (42, '6', 72, 1, 16.80, 18.06, 19.49, 21.26, 23.26, 25.29, 27.55);
INSERT INTO `sq_standard_weight` VALUES (43, '7', 84, 1, 18.48, 20.04, 21.81, 24.06, 26.66, 29.35, 32.41);
INSERT INTO `sq_standard_weight` VALUES (44, '8', 96, 1, 20.32, 22.24, 24.46, 27.33, 30.71, 34.31, 38.49);
INSERT INTO `sq_standard_weight` VALUES (45, '9', 108, 1, 22.04, 24.31, 26.98, 30.46, 34.61, 39.08, 44.35);
INSERT INTO `sq_standard_weight` VALUES (46, '10', 120, 1, 23.89, 26.55, 29.66, 33.74, 38.61, 43.85, 50.01);
INSERT INTO `sq_standard_weight` VALUES (47, '11', 132, 1, 26.21, 29.33, 32.97, 37.69, 43.27, 49.20, 56.07);
INSERT INTO `sq_standard_weight` VALUES (48, '12', 144, 1, 29.01, 32.77, 37.03, 42.49, 48.86, 55.50, 63.04);
INSERT INTO `sq_standard_weight` VALUES (49, '13', 156, 1, 32.82, 37.04, 41.90, 48.08, 55.21, 62.57, 70.83);
INSERT INTO `sq_standard_weight` VALUES (50, '14', 168, 1, 37.36, 41.80, 46.90, 53.37, 60.83, 68.53, 77.20);
INSERT INTO `sq_standard_weight` VALUES (51, '15', 180, 1, 41.43, 45.77, 50.75, 57.08, 64.40, 72.00, 80.60);
INSERT INTO `sq_standard_weight` VALUES (52, '16', 192, 1, 44.28, 48.74, 53.26, 59.35, 66.40, 73.73, 82.05);
INSERT INTO `sq_standard_weight` VALUES (53, '17', 204, 1, 46.04, 50.11, 54.77, 60.68, 67.51, 74.62, 82.70);
INSERT INTO `sq_standard_weight` VALUES (54, '18', 216, 1, 47.01, 51.02, 55.60, 61.40, 68.11, 75.08, 83.00);
INSERT INTO `sq_standard_weight` VALUES (55, '0', 0, 2, 2.57, 2.76, 2.96, 3.21, 3.49, 3.75, 4.04);
INSERT INTO `sq_standard_weight` VALUES (56, '0.17', 2, 2, 4.21, 4.50, 4.82, 5.21, 5.64, 6.06, 6.51);
INSERT INTO `sq_standard_weight` VALUES (57, '0.33', 4, 2, 5.55, 5.93, 6.34, 6.83, 7.37, 7.90, 8.47);
INSERT INTO `sq_standard_weight` VALUES (58, '0.5', 6, 2, 6.34, 6.76, 7.21, 7.77, 8.37, 8.96, 9.59);
INSERT INTO `sq_standard_weight` VALUES (59, '0.75', 9, 2, 7.11, 7.58, 8.08, 8.69, 9.36, 10.01, 10.71);
INSERT INTO `sq_standard_weight` VALUES (60, '1', 12, 2, 7.70, 8.20, 8.74, 9.40, 10.12, 10.82, 11.57);
INSERT INTO `sq_standard_weight` VALUES (70, '1.25', 15, 2, 8.22, 8.75, 9.33, 10.02, 10.79, 11.53, 12.33);
INSERT INTO `sq_standard_weight` VALUES (71, '1.5', 18, 2, 8.73, 9.29, 9.91, 10.65, 11.46, 12.25, 13.11);
INSERT INTO `sq_standard_weight` VALUES (72, '1.75', 21, 2, 9.26, 9.86, 10.51, 11.30, 12.17, 13.01, 13.93);
INSERT INTO `sq_standard_weight` VALUES (73, '2', 24, 2, 9.76, 10.39, 11.08, 11.92, 12.84, 13.74, 14.71);
INSERT INTO `sq_standard_weight` VALUES (74, '3', 36, 2, 11.50, 12.27, 13.11, 14.13, 15.25, 16.36, 17.55);
INSERT INTO `sq_standard_weight` VALUES (75, '4', 48, 2, 13.10, 13.99, 14.97, 16.17, 17.50, 18.81, 20.24);
INSERT INTO `sq_standard_weight` VALUES (76, '5', 60, 2, 14.64, 15.68, 16.84, 18.26, 19.83, 21.41, 23.14);
INSERT INTO `sq_standard_weight` VALUES (77, '6', 72, 2, 16.10, 17.32, 18.68, 20.37, 22.27, 24.19, 26.30);
INSERT INTO `sq_standard_weight` VALUES (78, '7', 84, 2, 17.58, 19.01, 20.62, 22.64, 24.94, 27.28, 29.89);
INSERT INTO `sq_standard_weight` VALUES (79, '8', 96, 2, 19.20, 20.89, 22.81, 25.25, 28.05, 30.95, 34.23);
INSERT INTO `sq_standard_weight` VALUES (80, '9', 108, 2, 20.93, 22.93, 25.23, 28.19, 31.63, 35.26, 39.41);
INSERT INTO `sq_standard_weight` VALUES (81, '10', 120, 2, 22.98, 25.36, 28.15, 31.76, 36.05, 40.63, 45.97);
INSERT INTO `sq_standard_weight` VALUES (82, '11', 132, 2, 25.74, 28.53, 31.81, 36.10, 41.24, 46.78, 53.33);
INSERT INTO `sq_standard_weight` VALUES (83, '12', 144, 2, 29.33, 32.42, 36.04, 40.77, 46.42, 52.49, 59.64);
INSERT INTO `sq_standard_weight` VALUES (84, '13', 156, 2, 33.09, 36.29, 40.00, 44.79, 50.45, 56.46, 63.45);
INSERT INTO `sq_standard_weight` VALUES (85, '14', 168, 2, 36.38, 39.55, 43.15, 47.83, 53.23, 58.88, 65.36);
INSERT INTO `sq_standard_weight` VALUES (86, '15', 180, 2, 38.73, 41.83, 45.36, 49.82, 54.96, 60.28, 66.30);
INSERT INTO `sq_standard_weight` VALUES (87, '16', 192, 2, 39.96, 43.01, 46.47, 50.81, 55.79, 60.91, 66.69);
INSERT INTO `sq_standard_weight` VALUES (88, '17', 204, 2, 40.44, 43.47, 46.90, 51.20, 56.11, 61.15, 66.82);
INSERT INTO `sq_standard_weight` VALUES (89, '18', 216, 2, 40.71, 43.73, 47.14, 51.41, 56.28, 61.28, 66.89);
INSERT INTO `sq_standard_weight` VALUES (90, '2.5', 30, 1, 11.11, 11.85, 12.66, 13.64, 14.70, 15.73, 16.83);
INSERT INTO `sq_standard_weight` VALUES (91, '3.5', 42, 1, 12.73, 13.58, 14.51, 15.63, 16.86, 18.08, 19.38);
INSERT INTO `sq_standard_weight` VALUES (92, '4.5', 54, 1, 14.37, 15.35, 16.43, 17.75, 19.22, 20.67, 22.24);
INSERT INTO `sq_standard_weight` VALUES (93, '5.5', 66, 1, 16.09, 11.26, 18.56, 20.18, 21.98, 23.81, 25.81);
INSERT INTO `sq_standard_weight` VALUES (94, '6.5', 78, 1, 17.53, 18.92, 20.49, 22.45, 24.70, 27.00, 29.57);
INSERT INTO `sq_standard_weight` VALUES (95, '7.5', 90, 1, 19.43, 21.17, 23.16, 25.72, 28.70, 31.84, 35.45);
INSERT INTO `sq_standard_weight` VALUES (96, '8.5', 102, 1, 21.18, 23.28, 25.73, 28.91, 32.69, 36.74, 41.49);
INSERT INTO `sq_standard_weight` VALUES (97, '9.5', 114, 1, 22.95, 25.42, 28.31, 32.09, 36.61, 41.49, 47.24);
INSERT INTO `sq_standard_weight` VALUES (98, '10.5', 126, 1, 24.96, 27.83, 31.20, 35.58, 40.81, 46.40, 52.93);
INSERT INTO `sq_standard_weight` VALUES (99, '11.5', 138, 1, 27.59, 30.97, 34.91, 39.98, 45.94, 52.21, 59.40);
INSERT INTO `sq_standard_weight` VALUES (100, '12.5', 150, 1, 30.74, 34.71, 39.29, 45.13, 51.89, 58.90, 66.81);
INSERT INTO `sq_standard_weight` VALUES (101, '13.5', 162, 1, 35.03, 39.42, 44.45, 50.85, 58.21, 65.80, 74.33);
INSERT INTO `sq_standard_weight` VALUES (102, '14.5', 174, 1, 39.53, 43.94, 49.00, 55.43, 62.86, 70.55, 79.24);
INSERT INTO `sq_standard_weight` VALUES (103, '15.5', 186, 1, 43.05, 47.31, 52.19, 58.39, 65.57, 73.03, 81.49);
INSERT INTO `sq_standard_weight` VALUES (104, '16.5', 198, 1, 45.30, 49.42, 54.13, 60.12, 67.05, 74.25, 82.44);
INSERT INTO `sq_standard_weight` VALUES (105, '2.5', 30, 2, 10.65, 11.35, 12.12, 13.05, 14.07, 15.08, 16.16);
INSERT INTO `sq_standard_weight` VALUES (106, '3.5', 42, 2, 12.32, 13.14, 14.05, 15.16, 16.38, 17.59, 18.89);
INSERT INTO `sq_standard_weight` VALUES (107, '4.5', 54, 2, 13.89, 14.85, 15.92, 17.22, 18.66, 20.10, 21.67);
INSERT INTO `sq_standard_weight` VALUES (108, '5.5', 66, 2, 15.39, 16.52, 17.78, 19.33, 21.06, 22.81, 24.72);
INSERT INTO `sq_standard_weight` VALUES (109, '6.5', 78, 2, 16.80, 18.12, 19.60, 21.44, 23.51, 25.62, 27.96);
INSERT INTO `sq_standard_weight` VALUES (110, '7.5', 90, 2, 18.39, 19.95, 21.71, 23.93, 26.48, 29.08, 32.01);
INSERT INTO `sq_standard_weight` VALUES (111, '8.5', 102, 2, 20.05, 21.88, 23.99, 26.67, 29.77, 33.00, 36.69);
INSERT INTO `sq_standard_weight` VALUES (112, '9.5', 114, 2, 21.89, 24.08, 26.61, 29.87, 33.72, 37.79, 42.51);
INSERT INTO `sq_standard_weight` VALUES (113, '10.5', 126, 2, 24.22, 26.80, 29.84, 33.80, 38.53, 43.61, 49.59);
INSERT INTO `sq_standard_weight` VALUES (114, '11.5', 138, 2, 27.43, 30.39, 33.86, 38.40, 43.85, 49.73, 56.67);
INSERT INTO `sq_standard_weight` VALUES (115, '12.5', 150, 2, 31.22, 34.39, 38.09, 42.89, 48.60, 54.71, 61.86);
INSERT INTO `sq_standard_weight` VALUES (116, '13.5', 162, 2, 34.82, 38.01, 41.69, 46.42, 51.97, 57.81, 64.55);
INSERT INTO `sq_standard_weight` VALUES (117, '14.5', 174, 2, 37.71, 40.84, 44.43, 48.97, 54.23, 59.70, 65.93);
INSERT INTO `sq_standard_weight` VALUES (118, '15.5', 186, 2, 39.51, 42.58, 46.06, 50.45, 55.49, 60.69, 66.55);
INSERT INTO `sq_standard_weight` VALUES (119, '16.5', 198, 2, 40.29, 43.32, 46.76, 51.07, 56.01, 61.07, 66.78);

-- ----------------------------
-- Table structure for sq_sys_file
-- ----------------------------
DROP TABLE IF EXISTS `sq_sys_file`;
CREATE TABLE `sq_sys_file`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文件名称',
  `save_name` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '保存名称',
  `save_path` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `exts` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文件类型',
  `size` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '大小',
  `user_id` int(11) NULL DEFAULT 0 COMMENT '上传用户ID',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '上传时间',
  `update_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 554 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '上传文件记录表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of sq_sys_file
-- ----------------------------
INSERT INTO `sq_sys_file` VALUES (1, '代理人修改后.png', '-1600658669-23841.png', '/files/20200921/-1600658669-23841.png', 'png', '446806', NULL, '2020-09-21 11:24:29', '2020-09-21 11:24:29');
INSERT INTO `sq_sys_file` VALUES (2, '代理人修改后.png', '-1600658722-7419.png', '/files/20200921/-1600658722-7419.png', 'png', '446806', NULL, '2020-09-21 11:25:22', '2020-09-21 11:25:22');
INSERT INTO `sq_sys_file` VALUES (3, '微信图片_20200915140910.jpg', '-1600659119-6501.jpg', '/files/20200921/-1600659119-6501.jpg', 'jpg', '67011', NULL, '2020-09-21 11:31:59', '2020-09-21 11:31:59');
INSERT INTO `sq_sys_file` VALUES (4, '微信图片_20200915140910.jpg', '-1600659248-75071.jpg', '/files/20200921/-1600659248-75071.jpg', 'jpg', '67011', NULL, '2020-09-21 11:34:08', '2020-09-21 11:34:08');
INSERT INTO `sq_sys_file` VALUES (5, '微信图片_20200915140910.jpg', '-1600659293-60773.jpg', '/files/20200921/-1600659293-60773.jpg', 'jpg', '67011', NULL, '2020-09-21 11:34:53', '2020-09-21 11:34:53');
INSERT INTO `sq_sys_file` VALUES (6, '微信图片_20200917104800.jpg', '-1600659445-80552.jpg', '/files/20200921/-1600659445-80552.jpg', 'jpg', '645988', NULL, '2020-09-21 11:37:26', '2020-09-21 11:37:26');
INSERT INTO `sq_sys_file` VALUES (7, 'cate_bg.png', '-1600659663-94245.png', '/files/20200921/-1600659663-94245.png', 'png', '70495', NULL, '2020-09-21 11:41:03', '2020-09-21 11:41:03');
INSERT INTO `sq_sys_file` VALUES (8, '333.jpg', '-1600670042-87641.jpg', '/files/20200921/-1600670042-87641.jpg', 'jpg', '19353', NULL, '2020-09-21 14:34:02', '2020-09-21 14:34:02');
INSERT INTO `sq_sys_file` VALUES (9, '微信图片_20200915140910.jpg', '-1600670170-11948.jpg', '/files/20200921/-1600670170-11948.jpg', 'jpg', '67011', NULL, '2020-09-21 14:36:10', '2020-09-21 14:36:10');
INSERT INTO `sq_sys_file` VALUES (10, '微信图片_20200917104800.jpg', '-1600670179-28785.jpg', '/files/20200921/-1600670179-28785.jpg', 'jpg', '645988', NULL, '2020-09-21 14:36:19', '2020-09-21 14:36:19');
INSERT INTO `sq_sys_file` VALUES (11, '微信图片_202009171048001.jpg', '-1600670212-486.jpg', '/files/20200921/-1600670212-486.jpg', 'jpg', '1378401', NULL, '2020-09-21 14:36:52', '2020-09-21 14:36:52');
INSERT INTO `sq_sys_file` VALUES (12, '微信图片_20200917104800.jpg', '-1600670217-25739.jpg', '/files/20200921/-1600670217-25739.jpg', 'jpg', '645988', NULL, '2020-09-21 14:36:57', '2020-09-21 14:36:57');
INSERT INTO `sq_sys_file` VALUES (13, '微信图片_20200915140910.jpg', '-1600670230-57966.jpg', '/files/20200921/-1600670230-57966.jpg', 'jpg', '67011', NULL, '2020-09-21 14:37:10', '2020-09-21 14:37:10');
INSERT INTO `sq_sys_file` VALUES (14, 'cate_bg.png', '-1600670240-165.png', '/files/20200921/-1600670240-165.png', 'png', '70495', NULL, '2020-09-21 14:37:20', '2020-09-21 14:37:20');
INSERT INTO `sq_sys_file` VALUES (15, 'cate_bg.png', '-1600675636-21445.png', '/files/20200921/-1600675636-21445.png', 'png', '70495', NULL, '2020-09-21 16:07:16', '2020-09-21 16:07:16');
INSERT INTO `sq_sys_file` VALUES (16, '微信图片_20200915140910.jpg', '-1600675658-73612.jpg', '/files/20200921/-1600675658-73612.jpg', 'jpg', '67011', NULL, '2020-09-21 16:07:38', '2020-09-21 16:07:38');
INSERT INTO `sq_sys_file` VALUES (17, '代理人修改后.png', '-1600675733-2656.png', '/files/20200921/-1600675733-2656.png', 'png', '446806', NULL, '2020-09-21 16:08:53', '2020-09-21 16:08:53');
INSERT INTO `sq_sys_file` VALUES (18, '代理人修改后.png', '-1600676109-74985.png', '/files/20200921/-1600676109-74985.png', 'png', '446806', NULL, '2020-09-21 16:15:09', '2020-09-21 16:15:09');
INSERT INTO `sq_sys_file` VALUES (19, '代理人修改后.png', '-1600676122-31659.png', '/files/20200921/-1600676122-31659.png', 'png', '446806', NULL, '2020-09-21 16:15:22', '2020-09-21 16:15:22');
INSERT INTO `sq_sys_file` VALUES (20, 'cate_bg.png', '-1600676316-60679.png', '/files/20200921/-1600676316-60679.png', 'png', '70495', NULL, '2020-09-21 16:18:36', '2020-09-21 16:18:36');
INSERT INTO `sq_sys_file` VALUES (21, '代理人修改后.png', '-1600677786-28238.png', '/files/20200921/-1600677786-28238.png', 'png', '446806', NULL, '2020-09-21 16:43:06', '2020-09-21 16:43:06');
INSERT INTO `sq_sys_file` VALUES (22, 'my_bg.png', '-1600677859-83118.png', '/files/20200921/-1600677859-83118.png', 'png', '31764', NULL, '2020-09-21 16:44:19', '2020-09-21 16:44:19');
INSERT INTO `sq_sys_file` VALUES (23, 'my_bg.png', '-1600677931-98203.png', '/files/20200921/-1600677931-98203.png', 'png', '31764', NULL, '2020-09-21 16:45:31', '2020-09-21 16:45:31');
INSERT INTO `sq_sys_file` VALUES (24, '代理人修改后.png', '-1600678177-47898.png', '/files/20200921/-1600678177-47898.png', 'png', '446806', NULL, '2020-09-21 16:49:37', '2020-09-21 16:49:37');
INSERT INTO `sq_sys_file` VALUES (25, '代理人修改后.png', '-1600678224-53193.png', '/files/20200921/-1600678224-53193.png', 'png', '446806', NULL, '2020-09-21 16:50:24', '2020-09-21 16:50:24');
INSERT INTO `sq_sys_file` VALUES (26, '代理人修改后.png', '-1600678273-6534.png', '/files/20200921/-1600678273-6534.png', 'png', '446806', NULL, '2020-09-21 16:51:13', '2020-09-21 16:51:13');
INSERT INTO `sq_sys_file` VALUES (27, 'my_bg.png', '-1600678382-73768.png', '/files/20200921/-1600678382-73768.png', 'png', '31764', NULL, '2020-09-21 16:53:02', '2020-09-21 16:53:02');
INSERT INTO `sq_sys_file` VALUES (28, 'my_bg.png', '-1600678449-99750.png', '/files/20200921/-1600678449-99750.png', 'png', '31764', NULL, '2020-09-21 16:54:09', '2020-09-21 16:54:09');
INSERT INTO `sq_sys_file` VALUES (29, 'timg.jpg', '-1600929529-60917.jpg', '/files/20200924/-1600929529-60917.jpg', 'jpg', '187516', NULL, '2020-09-24 14:38:49', '2020-09-24 14:38:49');
INSERT INTO `sq_sys_file` VALUES (30, 'timg.jpg', '-1600931328-35318.jpg', '/files/20200924/-1600931328-35318.jpg', 'jpg', '187516', NULL, '2020-09-24 15:08:48', '2020-09-24 15:08:48');
INSERT INTO `sq_sys_file` VALUES (31, 'u=494999854,985780807&fm=26&gp=0.jpg', '-1600931778-54453.jpg', '/files/20200924/-1600931778-54453.jpg', 'jpg', '42875', NULL, '2020-09-24 15:16:18', '2020-09-24 15:16:18');
INSERT INTO `sq_sys_file` VALUES (32, 'u=2326788584,2231322671&fm=26&gp=0.jpg', '-1600940555-77100.jpg', '/files/20200924/-1600940555-77100.jpg', 'jpg', '18285', NULL, '2020-09-24 17:42:35', '2020-09-24 17:42:35');
INSERT INTO `sq_sys_file` VALUES (33, 'article.jpg', '-1601016014-30805.jpg', '/files/20200925/-1601016014-30805.jpg', 'jpg', '331449', NULL, '2020-09-25 14:40:14', '2020-09-25 14:40:14');
INSERT INTO `sq_sys_file` VALUES (34, 'article.jpg', '-1601016849-2036.jpg', '/files/20200925/-1601016849-2036.jpg', 'jpg', '331449', NULL, '2020-09-25 14:54:09', '2020-09-25 14:54:09');
INSERT INTO `sq_sys_file` VALUES (35, 'article.jpg', '-1601016870-65711.jpg', '/files/20200925/-1601016870-65711.jpg', 'jpg', '331449', NULL, '2020-09-25 14:54:30', '2020-09-25 14:54:30');
INSERT INTO `sq_sys_file` VALUES (36, 'article.jpg', '-1601016898-71338.jpg', '/files/20200925/-1601016898-71338.jpg', 'jpg', '331449', NULL, '2020-09-25 14:54:58', '2020-09-25 14:54:58');
INSERT INTO `sq_sys_file` VALUES (37, 'baike-selected.png', '-1601016941-8790.png', '/files/20200925/-1601016941-8790.png', 'png', '2813', NULL, '2020-09-25 14:55:41', '2020-09-25 14:55:41');
INSERT INTO `sq_sys_file` VALUES (38, 'article.jpg', '-1601016948-50565.jpg', '/files/20200925/-1601016948-50565.jpg', 'jpg', '331449', NULL, '2020-09-25 14:55:48', '2020-09-25 14:55:48');
INSERT INTO `sq_sys_file` VALUES (39, 'article.jpg', '-1601017029-61429.jpg', '/files/20200925/-1601017029-61429.jpg', 'jpg', '331449', NULL, '2020-09-25 14:57:09', '2020-09-25 14:57:09');
INSERT INTO `sq_sys_file` VALUES (40, 'agent-bg.png', '-1601017125-74515.png', '/files/20200925/-1601017125-74515.png', 'png', '165655', NULL, '2020-09-25 14:58:45', '2020-09-25 14:58:45');
INSERT INTO `sq_sys_file` VALUES (41, 'share_bg.png', '-1601017141-85474.png', '/files/20200925/-1601017141-85474.png', 'png', '25029', NULL, '2020-09-25 14:59:01', '2020-09-25 14:59:01');
INSERT INTO `sq_sys_file` VALUES (42, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.15JYQcYODqbc8a75d2a7593f0c15b2811d57fcb6a538.png', '-1601190730-60053.png', '/files/20200927/-1601190730-60053.png', 'png', '2113', NULL, '2020-09-27 15:12:10', '2020-09-27 15:12:10');
INSERT INTO `sq_sys_file` VALUES (43, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.gDKOJUjpRFHef1cdf40a4f3e1e10c3021f323b21016c.png', '-1601190818-98527.png', '/files/20200927/-1601190818-98527.png', 'png', '2039', NULL, '2020-09-27 15:13:38', '2020-09-27 15:13:38');
INSERT INTO `sq_sys_file` VALUES (44, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.wLfb94g5j6BDfbf35be391be5e50c8aa1409f0b212e6.png', '-1601190868-45328.png', '/files/20200927/-1601190868-45328.png', 'png', '2401', NULL, '2020-09-27 15:14:28', '2020-09-27 15:14:28');
INSERT INTO `sq_sys_file` VALUES (45, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.oKXOz7eWwqGTfbf35be391be5e50c8aa1409f0b212e6.png', '-1601190942-96845.png', '/files/20200927/-1601190942-96845.png', 'png', '2401', NULL, '2020-09-27 15:15:42', '2020-09-27 15:15:42');
INSERT INTO `sq_sys_file` VALUES (46, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.5OpYqkRGbax7fbf35be391be5e50c8aa1409f0b212e6.png', '-1601191043-88263.png', '/files/20200927/-1601191043-88263.png', 'png', '2401', NULL, '2020-09-27 15:17:23', '2020-09-27 15:17:23');
INSERT INTO `sq_sys_file` VALUES (47, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.l8xEPRhHT11y7c557a3d94f992136ed825b993d08bf3.png', '-1601191283-13270.png', '/files/20200927/-1601191283-13270.png', 'png', '2000', NULL, '2020-09-27 15:21:23', '2020-09-27 15:21:23');
INSERT INTO `sq_sys_file` VALUES (48, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.pqE2N2vuhmIE04998ee80db13ebf7d965529584a7eb1.png', '-1601191547-42209.png', '/files/20200927/-1601191547-42209.png', 'png', '17591', NULL, '2020-09-27 15:25:47', '2020-09-27 15:25:47');
INSERT INTO `sq_sys_file` VALUES (49, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.JRS5mDnFGsGO04998ee80db13ebf7d965529584a7eb1.png', '-1601191605-8069.png', '/files/20200927/-1601191605-8069.png', 'png', '17591', NULL, '2020-09-27 15:26:45', '2020-09-27 15:26:45');
INSERT INTO `sq_sys_file` VALUES (50, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.kaSIUDOP2GhR04998ee80db13ebf7d965529584a7eb1.png', '-1601191749-93351.png', '/files/20200927/-1601191749-93351.png', 'png', '17591', NULL, '2020-09-27 15:29:09', '2020-09-27 15:29:09');
INSERT INTO `sq_sys_file` VALUES (51, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.vFqi8I7o0AMa04998ee80db13ebf7d965529584a7eb1.png', '-1601191818-74006.png', '/files/20200927/-1601191818-74006.png', 'png', '17591', NULL, '2020-09-27 15:30:18', '2020-09-27 15:30:18');
INSERT INTO `sq_sys_file` VALUES (52, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.fTnc2D1GOK6A09973eccc2665e9557603d283ebd7142.png', '-1601191821-42808.png', '/files/20200927/-1601191821-42808.png', 'png', '5746', NULL, '2020-09-27 15:30:21', '2020-09-27 15:30:21');
INSERT INTO `sq_sys_file` VALUES (53, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.r1hrplRmsRNr09973eccc2665e9557603d283ebd7142.png', '-1601194345-66059.png', '/files/20200927/-1601194345-66059.png', 'png', '5746', NULL, '2020-09-27 16:12:25', '2020-09-27 16:12:25');
INSERT INTO `sq_sys_file` VALUES (54, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.dM4MTMoplL2Ie024bcf3efe25032bb7157ba6ed9b3a0.png', '-1601194485-26233.png', '/files/20200927/-1601194485-26233.png', 'png', '2195', NULL, '2020-09-27 16:14:45', '2020-09-27 16:14:45');
INSERT INTO `sq_sys_file` VALUES (55, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.u4iuUhFN8rWefbf35be391be5e50c8aa1409f0b212e6.png', '-1601194522-82126.png', '/files/20200927/-1601194522-82126.png', 'png', '2401', NULL, '2020-09-27 16:15:22', '2020-09-27 16:15:22');
INSERT INTO `sq_sys_file` VALUES (56, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.WT3tzDBLsGbSc46efad0d5454342c5c2afea1a560c86.png', '-1601194605-98899.png', '/files/20200927/-1601194605-98899.png', 'png', '1831', NULL, '2020-09-27 16:16:45', '2020-09-27 16:16:45');
INSERT INTO `sq_sys_file` VALUES (57, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.dKC0DXNI5VyZ09973eccc2665e9557603d283ebd7142.png', '-1601195289-43784.png', '/files/20200927/-1601195289-43784.png', 'png', '5746', NULL, '2020-09-27 16:28:09', '2020-09-27 16:28:09');
INSERT INTO `sq_sys_file` VALUES (58, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.d5Yzms0fSQPK04998ee80db13ebf7d965529584a7eb1.png', '-1601196135-95426.png', '/files/20200927/-1601196135-95426.png', 'png', '17591', NULL, '2020-09-27 16:42:15', '2020-09-27 16:42:15');
INSERT INTO `sq_sys_file` VALUES (59, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.bDnj3XnDJxPLf1cdf40a4f3e1e10c3021f323b21016c.png', '-1601196331-94449.png', '/files/20200927/-1601196331-94449.png', 'png', '2039', NULL, '2020-09-27 16:45:31', '2020-09-27 16:45:31');
INSERT INTO `sq_sys_file` VALUES (60, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.soWPegc2N3HH04998ee80db13ebf7d965529584a7eb1.png', '-1601200628-98502.png', '/files/20200927/-1601200628-98502.png', 'png', '17591', NULL, '2020-09-27 17:57:08', '2020-09-27 17:57:08');
INSERT INTO `sq_sys_file` VALUES (61, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.c1I0nhRndo3z69eb74b3fc30540ca5781c8d0d88dc31.png', '-1601201144-78431.png', '/files/20200927/-1601201144-78431.png', 'png', '2299', NULL, '2020-09-27 18:05:44', '2020-09-27 18:05:44');
INSERT INTO `sq_sys_file` VALUES (62, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.tQ8IAUFxDBDBf6908085d667beabdc5dc5e0b7518441.png', '-1601257319-6846.png', '/files/20200928/-1601257319-6846.png', 'png', '1589', NULL, '2020-09-28 09:41:59', '2020-09-28 09:41:59');
INSERT INTO `sq_sys_file` VALUES (63, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.m4AS3CdYAgoef6908085d667beabdc5dc5e0b7518441.png', '-1601257500-34534.png', '/files/20200928/-1601257500-34534.png', 'png', '1589', NULL, '2020-09-28 09:45:00', '2020-09-28 09:45:00');
INSERT INTO `sq_sys_file` VALUES (64, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.5xuA2j7zG3XSf6908085d667beabdc5dc5e0b7518441.png', '-1601258411-50718.png', '/files/20200928/-1601258411-50718.png', 'png', '1589', NULL, '2020-09-28 10:00:11', '2020-09-28 10:00:11');
INSERT INTO `sq_sys_file` VALUES (65, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.MhVx47WcsK1h04998ee80db13ebf7d965529584a7eb1.png', '-1601258501-15711.png', '/files/20200928/-1601258501-15711.png', 'png', '17591', NULL, '2020-09-28 10:01:41', '2020-09-28 10:01:41');
INSERT INTO `sq_sys_file` VALUES (66, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.EXE5rMqtrc5704998ee80db13ebf7d965529584a7eb1.png', '-1601258964-37696.png', '/files/20200928/-1601258964-37696.png', 'png', '17591', NULL, '2020-09-28 10:09:24', '2020-09-28 10:09:24');
INSERT INTO `sq_sys_file` VALUES (67, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.toD4DxTfXrz2f6908085d667beabdc5dc5e0b7518441.png', '-1601259215-98426.png', '/files/20200928/-1601259215-98426.png', 'png', '1589', NULL, '2020-09-28 10:13:35', '2020-09-28 10:13:35');
INSERT INTO `sq_sys_file` VALUES (68, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.ZsyEuDEKeDqwe024bcf3efe25032bb7157ba6ed9b3a0.png', '-1601261625-98603.png', '/files/20200928/-1601261625-98603.png', 'png', '2195', NULL, '2020-09-28 10:53:45', '2020-09-28 10:53:45');
INSERT INTO `sq_sys_file` VALUES (69, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.ZxOWBrpfKEDLf1cdf40a4f3e1e10c3021f323b21016c.png', '-1601272517-50809.png', '/files/20200928/-1601272517-50809.png', 'png', '2039', NULL, '2020-09-28 13:55:17', '2020-09-28 13:55:17');
INSERT INTO `sq_sys_file` VALUES (70, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.uvJ6NPlgpETRfbf35be391be5e50c8aa1409f0b212e6.png', '-1601272640-19236.png', '/files/20200928/-1601272640-19236.png', 'png', '2401', NULL, '2020-09-28 13:57:20', '2020-09-28 13:57:20');
INSERT INTO `sq_sys_file` VALUES (71, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.fvILg4XAtRUyf6908085d667beabdc5dc5e0b7518441.png', '-1601272644-93659.png', '/files/20200928/-1601272644-93659.png', 'png', '1589', NULL, '2020-09-28 13:57:24', '2020-09-28 13:57:24');
INSERT INTO `sq_sys_file` VALUES (72, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.UZCsBCLjQ6hu04998ee80db13ebf7d965529584a7eb1.png', '-1601276040-84614.png', '/files/20200928/-1601276040-84614.png', 'png', '17591', NULL, '2020-09-28 14:54:00', '2020-09-28 14:54:00');
INSERT INTO `sq_sys_file` VALUES (73, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.L0b8G19knXonf6908085d667beabdc5dc5e0b7518441.png', '-1601276074-25617.png', '/files/20200928/-1601276074-25617.png', 'png', '1589', NULL, '2020-09-28 14:54:34', '2020-09-28 14:54:34');
INSERT INTO `sq_sys_file` VALUES (74, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.Q7nOIa36ZWmG7c557a3d94f992136ed825b993d08bf3.png', '-1601276122-50818.png', '/files/20200928/-1601276122-50818.png', 'png', '2000', NULL, '2020-09-28 14:55:22', '2020-09-28 14:55:22');
INSERT INTO `sq_sys_file` VALUES (75, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.86Sj8MRAxgpSe024bcf3efe25032bb7157ba6ed9b3a0.png', '-1601276178-46341.png', '/files/20200928/-1601276178-46341.png', 'png', '2195', NULL, '2020-09-28 14:56:18', '2020-09-28 14:56:18');
INSERT INTO `sq_sys_file` VALUES (76, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.ZnFVV9jXuw88fbf35be391be5e50c8aa1409f0b212e6.png', '-1601276426-83588.png', '/files/20200928/-1601276426-83588.png', 'png', '2401', 0, '2020-09-28 15:00:26', '2020-09-28 15:00:26');
INSERT INTO `sq_sys_file` VALUES (77, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.blKdElKzbNUwf1cdf40a4f3e1e10c3021f323b21016c.png', '-1601282429-16782.png', '/files/20200928/-1601282429-16782.png', 'png', '2039', 0, '2020-09-28 16:40:29', '2020-09-28 16:40:29');
INSERT INTO `sq_sys_file` VALUES (78, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.1UoKNQ2Zg32Kf1cdf40a4f3e1e10c3021f323b21016c.png', '-1601282555-74698.png', '/files/20200928/-1601282555-74698.png', 'png', '2039', 0, '2020-09-28 16:42:35', '2020-09-28 16:42:35');
INSERT INTO `sq_sys_file` VALUES (79, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.8vvZi0RSbWU2e024bcf3efe25032bb7157ba6ed9b3a0.png', '-1601282583-12400.png', '/files/20200928/-1601282583-12400.png', 'png', '2195', 0, '2020-09-28 16:43:03', '2020-09-28 16:43:03');
INSERT INTO `sq_sys_file` VALUES (80, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.vzK6gGotpoUA8a75d2a7593f0c15b2811d57fcb6a538.png', '-1601282648-33771.png', '/files/20200928/-1601282648-33771.png', 'png', '2113', 0, '2020-09-28 16:44:08', '2020-09-28 16:44:08');
INSERT INTO `sq_sys_file` VALUES (81, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.I9XvsWB3sWFUfbf35be391be5e50c8aa1409f0b212e6.png', '-1601282949-74390.png', '/files/20200928/-1601282949-74390.png', 'png', '2401', 0, '2020-09-28 16:49:09', '2020-09-28 16:49:09');
INSERT INTO `sq_sys_file` VALUES (82, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.CKvMANhizirce024bcf3efe25032bb7157ba6ed9b3a0.png', '-1601282957-63859.png', '/files/20200928/-1601282957-63859.png', 'png', '2195', 0, '2020-09-28 16:49:17', '2020-09-28 16:49:17');
INSERT INTO `sq_sys_file` VALUES (83, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.qCXLfMy6CDS47c557a3d94f992136ed825b993d08bf3.png', '-1601282958-34864.png', '/files/20200928/-1601282958-34864.png', 'png', '2000', 0, '2020-09-28 16:49:18', '2020-09-28 16:49:18');
INSERT INTO `sq_sys_file` VALUES (84, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.h9L2PSH9POyofbf35be391be5e50c8aa1409f0b212e6.png', '-1601282994-6324.png', '/files/20200928/-1601282994-6324.png', 'png', '2401', 0, '2020-09-28 16:49:54', '2020-09-28 16:49:54');
INSERT INTO `sq_sys_file` VALUES (85, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.dLTOJVeVNbusf1cdf40a4f3e1e10c3021f323b21016c.png', '-1601282995-2677.png', '/files/20200928/-1601282995-2677.png', 'png', '2039', 0, '2020-09-28 16:49:55', '2020-09-28 16:49:55');
INSERT INTO `sq_sys_file` VALUES (86, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.8RngLIo8BJjE8a75d2a7593f0c15b2811d57fcb6a538.png', '-1601282995-31632.png', '/files/20200928/-1601282995-31632.png', 'png', '2113', 0, '2020-09-28 16:49:55', '2020-09-28 16:49:55');
INSERT INTO `sq_sys_file` VALUES (87, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.XHIQARbIc1rXe024bcf3efe25032bb7157ba6ed9b3a0.png', '-1601282995-71668.png', '/files/20200928/-1601282995-71668.png', 'png', '2195', 0, '2020-09-28 16:49:55', '2020-09-28 16:49:55');
INSERT INTO `sq_sys_file` VALUES (88, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.LHmkPKVSsDnlf1cdf40a4f3e1e10c3021f323b21016c.png', '-1601283159-80823.png', '/files/20200928/-1601283159-80823.png', 'png', '2039', 0, '2020-09-28 16:52:39', '2020-09-28 16:52:39');
INSERT INTO `sq_sys_file` VALUES (89, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.ennxx7EDeBChf1cdf40a4f3e1e10c3021f323b21016c.png', '-1601283163-59540.png', '/files/20200928/-1601283163-59540.png', 'png', '2039', 0, '2020-09-28 16:52:43', '2020-09-28 16:52:43');
INSERT INTO `sq_sys_file` VALUES (90, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.74nCCReyCS5O8a75d2a7593f0c15b2811d57fcb6a538.png', '-1601283163-55359.png', '/files/20200928/-1601283163-55359.png', 'png', '2113', 0, '2020-09-28 16:52:43', '2020-09-28 16:52:43');
INSERT INTO `sq_sys_file` VALUES (91, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.althRhXEaxowf1cdf40a4f3e1e10c3021f323b21016c.png', '-1601283330-89527.png', '/files/20200928/-1601283330-89527.png', 'png', '2039', 0, '2020-09-28 16:55:30', '2020-09-28 16:55:30');
INSERT INTO `sq_sys_file` VALUES (92, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.imnGmJ5z9hYl8a75d2a7593f0c15b2811d57fcb6a538.png', '-1601283330-82831.png', '/files/20200928/-1601283330-82831.png', 'png', '2113', 0, '2020-09-28 16:55:30', '2020-09-28 16:55:30');
INSERT INTO `sq_sys_file` VALUES (93, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.o4xlqftxASb5e024bcf3efe25032bb7157ba6ed9b3a0.png', '-1601283330-94693.png', '/files/20200928/-1601283330-94693.png', 'png', '2195', 0, '2020-09-28 16:55:30', '2020-09-28 16:55:30');
INSERT INTO `sq_sys_file` VALUES (94, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.7wm1SugkxFnuf1cdf40a4f3e1e10c3021f323b21016c.png', '-1601283336-90980.png', '/files/20200928/-1601283336-90980.png', 'png', '2039', 0, '2020-09-28 16:55:36', '2020-09-28 16:55:36');
INSERT INTO `sq_sys_file` VALUES (95, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.Z5xdkloU9M0H8a75d2a7593f0c15b2811d57fcb6a538.png', '-1601283336-34311.png', '/files/20200928/-1601283336-34311.png', 'png', '2113', 0, '2020-09-28 16:55:36', '2020-09-28 16:55:36');
INSERT INTO `sq_sys_file` VALUES (96, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.cWcJHDXEWL1Vfbf35be391be5e50c8aa1409f0b212e6.png', '-1601283336-47980.png', '/files/20200928/-1601283336-47980.png', 'png', '2401', 0, '2020-09-28 16:55:36', '2020-09-28 16:55:36');
INSERT INTO `sq_sys_file` VALUES (97, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.JU61xu0DCJcNe024bcf3efe25032bb7157ba6ed9b3a0.png', '-1601283336-46274.png', '/files/20200928/-1601283336-46274.png', 'png', '2195', 0, '2020-09-28 16:55:36', '2020-09-28 16:55:36');
INSERT INTO `sq_sys_file` VALUES (98, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.in5qql1r9zGzfbf35be391be5e50c8aa1409f0b212e6.png', '-1601283380-96677.png', '/files/20200928/-1601283380-96677.png', 'png', '2401', 0, '2020-09-28 16:56:20', '2020-09-28 16:56:20');
INSERT INTO `sq_sys_file` VALUES (99, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.sAT9a9yAlBh5f1cdf40a4f3e1e10c3021f323b21016c.png', '-1601283380-69846.png', '/files/20200928/-1601283380-69846.png', 'png', '2039', 0, '2020-09-28 16:56:20', '2020-09-28 16:56:20');
INSERT INTO `sq_sys_file` VALUES (100, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.yViSOe6BfXLr8a75d2a7593f0c15b2811d57fcb6a538.png', '-1601283380-7697.png', '/files/20200928/-1601283380-7697.png', 'png', '2113', 0, '2020-09-28 16:56:20', '2020-09-28 16:56:20');
INSERT INTO `sq_sys_file` VALUES (101, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.RImveb2YhQuWe024bcf3efe25032bb7157ba6ed9b3a0.png', '-1601283380-5002.png', '/files/20200928/-1601283380-5002.png', 'png', '2195', 0, '2020-09-28 16:56:20', '2020-09-28 16:56:20');
INSERT INTO `sq_sys_file` VALUES (102, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.RJ190ATvR3aIf1cdf40a4f3e1e10c3021f323b21016c.png', '-1601283385-20688.png', '/files/20200928/-1601283385-20688.png', 'png', '2039', 0, '2020-09-28 16:56:26', '2020-09-28 16:56:26');
INSERT INTO `sq_sys_file` VALUES (103, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.24UiF6lTiv2K8a75d2a7593f0c15b2811d57fcb6a538.png', '-1601283386-75583.png', '/files/20200928/-1601283386-75583.png', 'png', '2113', 0, '2020-09-28 16:56:26', '2020-09-28 16:56:26');
INSERT INTO `sq_sys_file` VALUES (104, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.TqihMI22EKC5e024bcf3efe25032bb7157ba6ed9b3a0.png', '-1601283386-876.png', '/files/20200928/-1601283386-876.png', 'png', '2195', 0, '2020-09-28 16:56:26', '2020-09-28 16:56:26');
INSERT INTO `sq_sys_file` VALUES (105, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.opV14Ks5aFpCe346e25e0d25b2cd2feea2bfe4733903.png', '-1601283429-7010.png', '/files/20200928/-1601283429-7010.png', 'png', '1684', 0, '2020-09-28 16:57:09', '2020-09-28 16:57:09');
INSERT INTO `sq_sys_file` VALUES (106, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.dQJd7DjSKA6bfbf35be391be5e50c8aa1409f0b212e6.png', '-1601283618-99140.png', '/files/20200928/-1601283618-99140.png', 'png', '2401', 0, '2020-09-28 17:00:18', '2020-09-28 17:00:18');
INSERT INTO `sq_sys_file` VALUES (107, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.3Hx4wItxkWBJe024bcf3efe25032bb7157ba6ed9b3a0.png', '-1601283619-61396.png', '/files/20200928/-1601283619-61396.png', 'png', '2195', 0, '2020-09-28 17:00:19', '2020-09-28 17:00:19');
INSERT INTO `sq_sys_file` VALUES (108, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.St1zDpJGMfpdf1cdf40a4f3e1e10c3021f323b21016c.png', '-1601283619-59794.png', '/files/20200928/-1601283619-59794.png', 'png', '2039', 0, '2020-09-28 17:00:20', '2020-09-28 17:00:20');
INSERT INTO `sq_sys_file` VALUES (109, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.jU85DLJvnOR18a75d2a7593f0c15b2811d57fcb6a538.png', '-1601283620-7892.png', '/files/20200928/-1601283620-7892.png', 'png', '2113', 0, '2020-09-28 17:00:20', '2020-09-28 17:00:20');
INSERT INTO `sq_sys_file` VALUES (110, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.lQrjzlu4cWdg8a75d2a7593f0c15b2811d57fcb6a538.png', '-1601283624-65131.png', '/files/20200928/-1601283624-65131.png', 'png', '2113', 0, '2020-09-28 17:00:24', '2020-09-28 17:00:24');
INSERT INTO `sq_sys_file` VALUES (111, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.GS1x2LfPggVLe024bcf3efe25032bb7157ba6ed9b3a0.png', '-1601283624-7291.png', '/files/20200928/-1601283624-7291.png', 'png', '2195', 0, '2020-09-28 17:00:24', '2020-09-28 17:00:24');
INSERT INTO `sq_sys_file` VALUES (112, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.1UlBW6I6n5WT7c557a3d94f992136ed825b993d08bf3.png', '-1601283625-92148.png', '/files/20200928/-1601283625-92148.png', 'png', '2000', 0, '2020-09-28 17:00:25', '2020-09-28 17:00:25');
INSERT INTO `sq_sys_file` VALUES (113, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.mF7enC2YDWexf1cdf40a4f3e1e10c3021f323b21016c.png', '-1601285018-36133.png', '/files/20200928/-1601285018-36133.png', 'png', '2039', 0, '2020-09-28 17:23:38', '2020-09-28 17:23:38');
INSERT INTO `sq_sys_file` VALUES (114, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.BmQUpk0RPgIXe024bcf3efe25032bb7157ba6ed9b3a0.png', '-1601285060-63666.png', '/files/20200928/-1601285060-63666.png', 'png', '2195', 0, '2020-09-28 17:24:20', '2020-09-28 17:24:20');
INSERT INTO `sq_sys_file` VALUES (115, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.E3iAiTRIBZ1wf1cdf40a4f3e1e10c3021f323b21016c.png', '-1601285061-30958.png', '/files/20200928/-1601285061-30958.png', 'png', '2039', 0, '2020-09-28 17:24:21', '2020-09-28 17:24:21');
INSERT INTO `sq_sys_file` VALUES (116, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.RIOgBZM3PLve8a75d2a7593f0c15b2811d57fcb6a538.png', '-1601285061-72919.png', '/files/20200928/-1601285061-72919.png', 'png', '2113', 0, '2020-09-28 17:24:21', '2020-09-28 17:24:21');
INSERT INTO `sq_sys_file` VALUES (117, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.TFPcht6F6lzVfbf35be391be5e50c8aa1409f0b212e6.png', '-1601285066-56708.png', '/files/20200928/-1601285066-56708.png', 'png', '2401', 0, '2020-09-28 17:24:26', '2020-09-28 17:24:26');
INSERT INTO `sq_sys_file` VALUES (118, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.P93oGaadizOVf1cdf40a4f3e1e10c3021f323b21016c.png', '-1601285066-13077.png', '/files/20200928/-1601285066-13077.png', 'png', '2039', 0, '2020-09-28 17:24:26', '2020-09-28 17:24:26');
INSERT INTO `sq_sys_file` VALUES (119, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.gsgdu1r3nMco8a75d2a7593f0c15b2811d57fcb6a538.png', '-1601285067-25934.png', '/files/20200928/-1601285067-25934.png', 'png', '2113', 0, '2020-09-28 17:24:27', '2020-09-28 17:24:27');
INSERT INTO `sq_sys_file` VALUES (120, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.dQ7RRn4mCgggf1cdf40a4f3e1e10c3021f323b21016c.png', '-1601285088-14701.png', '/files/20200928/-1601285088-14701.png', 'png', '2039', 0, '2020-09-28 17:24:48', '2020-09-28 17:24:48');
INSERT INTO `sq_sys_file` VALUES (121, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.soWk1XILtZIif1cdf40a4f3e1e10c3021f323b21016c.png', '-1601285099-24640.png', '/files/20200928/-1601285099-24640.png', 'png', '2039', 0, '2020-09-28 17:24:59', '2020-09-28 17:24:59');
INSERT INTO `sq_sys_file` VALUES (122, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.5tOQPtV4S38tf1cdf40a4f3e1e10c3021f323b21016c.png', '-1601285103-54920.png', '/files/20200928/-1601285103-54920.png', 'png', '2039', 0, '2020-09-28 17:25:03', '2020-09-28 17:25:03');
INSERT INTO `sq_sys_file` VALUES (123, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.lMaSN0yYgziU8a75d2a7593f0c15b2811d57fcb6a538.png', '-1601285103-30649.png', '/files/20200928/-1601285103-30649.png', 'png', '2113', 0, '2020-09-28 17:25:03', '2020-09-28 17:25:03');
INSERT INTO `sq_sys_file` VALUES (124, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.hgGdHaS9QVVNfbf35be391be5e50c8aa1409f0b212e6.png', '-1601285272-76047.png', '/files/20200928/-1601285272-76047.png', 'png', '2401', 0, '2020-09-28 17:27:52', '2020-09-28 17:27:52');
INSERT INTO `sq_sys_file` VALUES (125, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.1XB3qAz2i7kX250b42edc51a8401531f9d3c426e826c.png', '-1601285445-41364.png', '/files/20200928/-1601285445-41364.png', 'png', '1617', 0, '2020-09-28 17:30:45', '2020-09-28 17:30:45');
INSERT INTO `sq_sys_file` VALUES (126, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.5AWzh48Uv2Dpfbf35be391be5e50c8aa1409f0b212e6.png', '-1601285480-29477.png', '/files/20200928/-1601285480-29477.png', 'png', '2401', 0, '2020-09-28 17:31:20', '2020-09-28 17:31:20');
INSERT INTO `sq_sys_file` VALUES (127, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.veFcDVJMnUmXe346e25e0d25b2cd2feea2bfe4733903.png', '-1601285552-59620.png', '/files/20200928/-1601285552-59620.png', 'png', '1684', 0, '2020-09-28 17:32:32', '2020-09-28 17:32:32');
INSERT INTO `sq_sys_file` VALUES (128, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.H8okYFZlhHE9e346e25e0d25b2cd2feea2bfe4733903.png', '-1601285573-80622.png', '/files/20200928/-1601285573-80622.png', 'png', '1684', 0, '2020-09-28 17:32:53', '2020-09-28 17:32:53');
INSERT INTO `sq_sys_file` VALUES (129, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.MrztJnyL95mf04998ee80db13ebf7d965529584a7eb1.png', '-1601285581-48707.png', '/files/20200928/-1601285581-48707.png', 'png', '17591', 0, '2020-09-28 17:33:01', '2020-09-28 17:33:01');
INSERT INTO `sq_sys_file` VALUES (130, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.FwoYMWQ2k7ju00bad2a0e12270e59e40ba236eb621c5.jpg', '-1601285587-5033.jpg', '/files/20200928/-1601285587-5033.jpg', 'jpg', '140799', 0, '2020-09-28 17:33:07', '2020-09-28 17:33:07');
INSERT INTO `sq_sys_file` VALUES (131, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.bAYMqKHN1awu16be585afec68d2c0d53896a0506bce6.png', '-1601285656-42746.png', '/files/20200928/-1601285656-42746.png', 'png', '1464', 0, '2020-09-28 17:34:16', '2020-09-28 17:34:16');
INSERT INTO `sq_sys_file` VALUES (132, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.AS7B0BYV4M1Q8a75d2a7593f0c15b2811d57fcb6a538.png', '-1601286769-20221.png', '/files/20200928/-1601286769-20221.png', 'png', '2113', 0, '2020-09-28 17:52:49', '2020-09-28 17:52:49');
INSERT INTO `sq_sys_file` VALUES (133, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.1Vk6IzMb1zkje024bcf3efe25032bb7157ba6ed9b3a0.png', '-1601286770-59723.png', '/files/20200928/-1601286770-59723.png', 'png', '2195', 0, '2020-09-28 17:52:50', '2020-09-28 17:52:50');
INSERT INTO `sq_sys_file` VALUES (134, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.dmUwwUNNrSmuf1cdf40a4f3e1e10c3021f323b21016c.png', '-1601342170-80582.png', '/files/20200929/-1601342170-80582.png', 'png', '2039', 0, '2020-09-29 09:16:10', '2020-09-29 09:16:10');
INSERT INTO `sq_sys_file` VALUES (135, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.WBbNqve5Xke88a75d2a7593f0c15b2811d57fcb6a538.png', '-1601342171-48981.png', '/files/20200929/-1601342171-48981.png', 'png', '2113', 0, '2020-09-29 09:16:11', '2020-09-29 09:16:11');
INSERT INTO `sq_sys_file` VALUES (136, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.TMj8v1oP8gBze024bcf3efe25032bb7157ba6ed9b3a0.png', '-1601342171-2421.png', '/files/20200929/-1601342171-2421.png', 'png', '2195', 0, '2020-09-29 09:16:11', '2020-09-29 09:16:11');
INSERT INTO `sq_sys_file` VALUES (137, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.A53qT1wauCr8250b42edc51a8401531f9d3c426e826c.png', '-1601342737-58100.png', '/files/20200929/-1601342737-58100.png', 'png', '1617', 0, '2020-09-29 09:25:38', '2020-09-29 09:25:38');
INSERT INTO `sq_sys_file` VALUES (138, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.PPPcBBX8xrcMe346e25e0d25b2cd2feea2bfe4733903.png', '-1601343100-91428.png', '/files/20200929/-1601343100-91428.png', 'png', '1684', 0, '2020-09-29 09:31:40', '2020-09-29 09:31:40');
INSERT INTO `sq_sys_file` VALUES (139, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.CZaPD26u8RKr16be585afec68d2c0d53896a0506bce6.png', '-1601343100-5439.png', '/files/20200929/-1601343100-5439.png', 'png', '1464', 0, '2020-09-29 09:31:40', '2020-09-29 09:31:40');
INSERT INTO `sq_sys_file` VALUES (140, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.mLFKauVZiHwv13f409e1fc37aef7815f2aa44b77e1ff.png', '-1601436416-54023.png', '/files/20200930/-1601436416-54023.png', 'png', '1758', 0, '2020-09-30 11:26:56', '2020-09-30 11:26:56');
INSERT INTO `sq_sys_file` VALUES (141, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.QIcel1iM1iUn8a75d2a7593f0c15b2811d57fcb6a538.png', '-1601436416-6501.png', '/files/20200930/-1601436416-6501.png', 'png', '2113', 0, '2020-09-30 11:26:57', '2020-09-30 11:26:57');
INSERT INTO `sq_sys_file` VALUES (142, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.wMuh8Nbc08n4c46efad0d5454342c5c2afea1a560c86.png', '-1601436417-97025.png', '/files/20200930/-1601436417-97025.png', 'png', '1831', 0, '2020-09-30 11:26:57', '2020-09-30 11:26:57');
INSERT INTO `sq_sys_file` VALUES (143, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.zkPtD7pUT3APf1cdf40a4f3e1e10c3021f323b21016c.png', '-1601436418-53766.png', '/files/20200930/-1601436418-53766.png', 'png', '2039', 0, '2020-09-30 11:26:58', '2020-09-30 11:26:58');
INSERT INTO `sq_sys_file` VALUES (144, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.SK8tsvQg7fscfbf35be391be5e50c8aa1409f0b212e6.png', '-1601436418-59430.png', '/files/20200930/-1601436418-59430.png', 'png', '2401', 0, '2020-09-30 11:26:58', '2020-09-30 11:26:58');
INSERT INTO `sq_sys_file` VALUES (145, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.vrhhLB4w0xq6f6908085d667beabdc5dc5e0b7518441.png', '-1601436418-74863.png', '/files/20200930/-1601436418-74863.png', 'png', '1589', 0, '2020-09-30 11:26:58', '2020-09-30 11:26:58');
INSERT INTO `sq_sys_file` VALUES (146, 'u=1877458196,1723995717&fm=26&gp=0.jpg', '-1601690681-37172.jpg', '/files/20201003/-1601690681-37172.jpg', 'jpg', '26898', NULL, '2020-10-03 10:04:41', '2020-10-03 10:04:41');
INSERT INTO `sq_sys_file` VALUES (147, 'u=3144071579,427022990&fm=15&gp=0.jpg', '-1601690690-54148.jpg', '/files/20201003/-1601690690-54148.jpg', 'jpg', '20336', NULL, '2020-10-03 10:04:50', '2020-10-03 10:04:50');
INSERT INTO `sq_sys_file` VALUES (148, 'u=3665382132,1844047519&fm=15&gp=0.jpg', '-1601690699-29835.jpg', '/files/20201003/-1601690699-29835.jpg', 'jpg', '167944', NULL, '2020-10-03 10:04:59', '2020-10-03 10:04:59');
INSERT INTO `sq_sys_file` VALUES (149, 'u=3144071579,427022990&fm=15&gp=0.jpg', '-1601690737-83760.jpg', '/files/20201003/-1601690737-83760.jpg', 'jpg', '20336', NULL, '2020-10-03 10:05:37', '2020-10-03 10:05:37');
INSERT INTO `sq_sys_file` VALUES (150, 'article.jpg', '-1601690805-874.jpg', '/files/20201003/-1601690805-874.jpg', 'jpg', '331449', NULL, '2020-10-03 10:06:45', '2020-10-03 10:06:45');
INSERT INTO `sq_sys_file` VALUES (151, 'u=3665382132,1844047519&fm=15&gp=0.jpg', '-1601690866-74265.jpg', '/files/20201003/-1601690866-74265.jpg', 'jpg', '167944', NULL, '2020-10-03 10:07:47', '2020-10-03 10:07:47');
INSERT INTO `sq_sys_file` VALUES (152, 'u=1877458196,1723995717&fm=26&gp=0.jpg', '-1601690901-64298.jpg', '/files/20201003/-1601690901-64298.jpg', 'jpg', '26898', NULL, '2020-10-03 10:08:21', '2020-10-03 10:08:21');
INSERT INTO `sq_sys_file` VALUES (153, 'article.jpg', '-1601690907-97044.jpg', '/files/20201003/-1601690907-97044.jpg', 'jpg', '331449', NULL, '2020-10-03 10:08:27', '2020-10-03 10:08:27');
INSERT INTO `sq_sys_file` VALUES (154, 'u=3144071579,427022990&fm=15&gp=0.jpg', '-1601690922-97881.jpg', '/files/20201003/-1601690922-97881.jpg', 'jpg', '20336', NULL, '2020-10-03 10:08:42', '2020-10-03 10:08:42');
INSERT INTO `sq_sys_file` VALUES (155, 'article.jpg', '-1601690927-3125.jpg', '/files/20201003/-1601690927-3125.jpg', 'jpg', '331449', NULL, '2020-10-03 10:08:47', '2020-10-03 10:08:47');
INSERT INTO `sq_sys_file` VALUES (156, 'u=3144071579,427022990&fm=15&gp=0.jpg', '-1601690943-58800.jpg', '/files/20201003/-1601690943-58800.jpg', 'jpg', '20336', NULL, '2020-10-03 10:09:03', '2020-10-03 10:09:03');
INSERT INTO `sq_sys_file` VALUES (157, 'article.jpg', '-1601690950-85331.jpg', '/files/20201003/-1601690950-85331.jpg', 'jpg', '331449', NULL, '2020-10-03 10:09:10', '2020-10-03 10:09:10');
INSERT INTO `sq_sys_file` VALUES (158, '2.jpg', '-1601698312-10300.jpg', '/files/20201003/-1601698312-10300.jpg', 'jpg', '15442', NULL, '2020-10-03 12:11:52', '2020-10-03 12:11:52');
INSERT INTO `sq_sys_file` VALUES (159, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.qnwOXNOMozF504979c4c3c810af306bc9cb2ff298891.jpg', '-1601698372-34935.jpg', '/files/20201003/-1601698372-34935.jpg', 'jpg', '26898', 0, '2020-10-03 12:12:53', '2020-10-03 12:12:53');
INSERT INTO `sq_sys_file` VALUES (160, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.Qh0z7HJ6whSr04979c4c3c810af306bc9cb2ff298891.jpg', '-1601784010-5258.jpg', '/files/20201004/-1601784010-5258.jpg', 'jpg', '26898', 0, '2020-10-04 12:00:10', '2020-10-04 12:00:10');
INSERT INTO `sq_sys_file` VALUES (161, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.hpHVMui4kEKIdc1b7828d99fe346462b83d0aab86640.jpg', '-1601784010-95256.jpg', '/files/20201004/-1601784010-95256.jpg', 'jpg', '15442', 0, '2020-10-04 12:00:10', '2020-10-04 12:00:10');
INSERT INTO `sq_sys_file` VALUES (162, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.UTpsIHndFOOX05ba4e7d33066624bc9e52e180ccd07c.jpg', '-1601784219-19209.jpg', '/files/20201004/-1601784219-19209.jpg', 'jpg', '42576', 0, '2020-10-04 12:03:39', '2020-10-04 12:03:39');
INSERT INTO `sq_sys_file` VALUES (163, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.2GiJLdHslUvU04979c4c3c810af306bc9cb2ff298891.jpg', '-1601784219-14048.jpg', '/files/20201004/-1601784219-14048.jpg', 'jpg', '26898', 0, '2020-10-04 12:03:39', '2020-10-04 12:03:39');
INSERT INTO `sq_sys_file` VALUES (164, '1.jpg', '-1601883145-17947.jpg', '/files/20201005/-1601883145-17947.jpg', 'jpg', '49045', NULL, '2020-10-05 15:32:26', '2020-10-05 15:32:26');
INSERT INTO `sq_sys_file` VALUES (165, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.Ol7UTyfhSVQY04979c4c3c810af306bc9cb2ff298891.jpg', '-1602060239-28356.jpg', '/files/20201007/-1602060239-28356.jpg', 'jpg', '26898', 0, '2020-10-07 16:43:59', '2020-10-07 16:43:59');
INSERT INTO `sq_sys_file` VALUES (166, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.6vxXfoMnieoJdc1b7828d99fe346462b83d0aab86640.jpg', '-1602060277-11357.jpg', '/files/20201007/-1602060277-11357.jpg', 'jpg', '15442', 0, '2020-10-07 16:44:37', '2020-10-07 16:44:37');
INSERT INTO `sq_sys_file` VALUES (167, '微信图片_20190821171438.jpg', '-1602211082-43845.jpg', '/files/20201009/-1602211082-43845.jpg', 'jpg', '289062', NULL, '2020-10-09 10:38:02', '2020-10-09 10:38:02');
INSERT INTO `sq_sys_file` VALUES (168, '微信图片_20190821171516.jpg', '-1602211089-76597.jpg', '/files/20201009/-1602211089-76597.jpg', 'jpg', '215010', NULL, '2020-10-09 10:38:09', '2020-10-09 10:38:09');
INSERT INTO `sq_sys_file` VALUES (169, '微信图片_20190821170722.jpg', '-1602211096-74314.jpg', '/files/20201009/-1602211096-74314.jpg', 'jpg', '86986', NULL, '2020-10-09 10:38:16', '2020-10-09 10:38:16');
INSERT INTO `sq_sys_file` VALUES (170, 'timg.jpg', '-1602211116-51191.jpg', '/files/20201009/-1602211116-51191.jpg', 'jpg', '187516', NULL, '2020-10-09 10:38:36', '2020-10-09 10:38:36');
INSERT INTO `sq_sys_file` VALUES (171, 'agent-bg.png', '-1602211127-39722.png', '/files/20201009/-1602211127-39722.png', 'png', '165655', NULL, '2020-10-09 10:38:47', '2020-10-09 10:38:47');
INSERT INTO `sq_sys_file` VALUES (172, 'my_bg.png', '-1602211138-9195.png', '/files/20201009/-1602211138-9195.png', 'png', '17591', NULL, '2020-10-09 10:38:58', '2020-10-09 10:38:58');
INSERT INTO `sq_sys_file` VALUES (173, 'agent-bg.png', '-1602211143-22562.png', '/files/20201009/-1602211143-22562.png', 'png', '165655', NULL, '2020-10-09 10:39:03', '2020-10-09 10:39:03');
INSERT INTO `sq_sys_file` VALUES (174, 'export.png', '-1602211154-72132.png', '/files/20201009/-1602211154-72132.png', 'png', '2113', NULL, '2020-10-09 10:39:14', '2020-10-09 10:39:14');
INSERT INTO `sq_sys_file` VALUES (175, 'agent-bg.png', '-1602211157-73304.png', '/files/20201009/-1602211157-73304.png', 'png', '165655', NULL, '2020-10-09 10:39:17', '2020-10-09 10:39:17');
INSERT INTO `sq_sys_file` VALUES (176, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.T0ecnZE0LDgu13f409e1fc37aef7815f2aa44b77e1ff.png', '-1602300003-75394.png', '/files/20201010/-1602300003-75394.png', 'png', '1758', 0, '2020-10-10 11:20:03', '2020-10-10 11:20:03');
INSERT INTO `sq_sys_file` VALUES (177, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.UfqrGRqYNp0cf1cdf40a4f3e1e10c3021f323b21016c.png', '-1602300003-63883.png', '/files/20201010/-1602300003-63883.png', 'png', '2039', 0, '2020-10-10 11:20:03', '2020-10-10 11:20:03');
INSERT INTO `sq_sys_file` VALUES (178, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.v8P8VL7OMJw4f6908085d667beabdc5dc5e0b7518441.png', '-1602300004-34122.png', '/files/20201010/-1602300004-34122.png', 'png', '1589', 0, '2020-10-10 11:20:04', '2020-10-10 11:20:04');
INSERT INTO `sq_sys_file` VALUES (179, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.hrVwyxMvB8lsc46efad0d5454342c5c2afea1a560c86.png', '-1602300004-95539.png', '/files/20201010/-1602300004-95539.png', 'png', '1831', 0, '2020-10-10 11:20:04', '2020-10-10 11:20:04');
INSERT INTO `sq_sys_file` VALUES (180, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.eVRh3fMWUFkU8a75d2a7593f0c15b2811d57fcb6a538.png', '-1602300004-5176.png', '/files/20201010/-1602300004-5176.png', 'png', '2113', 0, '2020-10-10 11:20:04', '2020-10-10 11:20:04');
INSERT INTO `sq_sys_file` VALUES (181, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.VxkfZ0O7BFvCfbf35be391be5e50c8aa1409f0b212e6.png', '-1602300004-2106.png', '/files/20201010/-1602300004-2106.png', 'png', '2401', 0, '2020-10-10 11:20:04', '2020-10-10 11:20:04');
INSERT INTO `sq_sys_file` VALUES (182, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.02cnXlAAlV1H13f409e1fc37aef7815f2aa44b77e1ff.png', '-1602300023-89975.png', '/files/20201010/-1602300023-89975.png', 'png', '1758', 0, '2020-10-10 11:20:23', '2020-10-10 11:20:23');
INSERT INTO `sq_sys_file` VALUES (183, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.fEJ8ZW11jLhPf6908085d667beabdc5dc5e0b7518441.png', '-1602300023-62354.png', '/files/20201010/-1602300023-62354.png', 'png', '1589', 0, '2020-10-10 11:20:23', '2020-10-10 11:20:23');
INSERT INTO `sq_sys_file` VALUES (184, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.BNrfNvLoseoic46efad0d5454342c5c2afea1a560c86.png', '-1602300023-37739.png', '/files/20201010/-1602300023-37739.png', 'png', '1831', 0, '2020-10-10 11:20:23', '2020-10-10 11:20:23');
INSERT INTO `sq_sys_file` VALUES (185, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.fvwjT0ZEolmVfbf35be391be5e50c8aa1409f0b212e6.png', '-1602300028-22144.png', '/files/20201010/-1602300028-22144.png', 'png', '2401', 0, '2020-10-10 11:20:28', '2020-10-10 11:20:28');
INSERT INTO `sq_sys_file` VALUES (186, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.FIdamZr3c0unf1cdf40a4f3e1e10c3021f323b21016c.png', '-1602300028-72684.png', '/files/20201010/-1602300028-72684.png', 'png', '2039', 0, '2020-10-10 11:20:28', '2020-10-10 11:20:28');
INSERT INTO `sq_sys_file` VALUES (187, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.ESuuodtVEEhk8a75d2a7593f0c15b2811d57fcb6a538.png', '-1602300028-26471.png', '/files/20201010/-1602300028-26471.png', 'png', '2113', 0, '2020-10-10 11:20:28', '2020-10-10 11:20:28');
INSERT INTO `sq_sys_file` VALUES (188, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.JV4C0CHrGkeK00bad2a0e12270e59e40ba236eb621c5.jpg', '-1602310710-5347.jpg', '/files/20201010/-1602310710-5347.jpg', 'jpg', '140799', 0, '2020-10-10 14:18:30', '2020-10-10 14:18:30');
INSERT INTO `sq_sys_file` VALUES (189, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.BrVkcoNiyKH4e5bad709bc04711efdf0c2ef51cf576d.png', '-1602310710-849.png', '/files/20201010/-1602310710-849.png', 'png', '25029', 0, '2020-10-10 14:18:30', '2020-10-10 14:18:30');
INSERT INTO `sq_sys_file` VALUES (190, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.60mdtffpmvQZa3bb3f2992cc24558a5c540ec2115bd9.jpg', '-1602310711-56287.jpg', '/files/20201010/-1602310711-56287.jpg', 'jpg', '106002', 0, '2020-10-10 14:18:31', '2020-10-10 14:18:31');
INSERT INTO `sq_sys_file` VALUES (191, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.dtD8Heq7t55k13f409e1fc37aef7815f2aa44b77e1ff.png', '-1602468399-63361.png', '/files/20201012/-1602468399-63361.png', 'png', '1758', 0, '2020-10-12 10:06:39', '2020-10-12 10:06:39');
INSERT INTO `sq_sys_file` VALUES (192, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.zYFfaQQ8BZ9Lc46efad0d5454342c5c2afea1a560c86.png', '-1602468456-60907.png', '/files/20201012/-1602468456-60907.png', 'png', '1831', 0, '2020-10-12 10:07:36', '2020-10-12 10:07:36');
INSERT INTO `sq_sys_file` VALUES (193, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.bFQBprtWGzDpf6908085d667beabdc5dc5e0b7518441.png', '-1602468456-11664.png', '/files/20201012/-1602468456-11664.png', 'png', '1589', 0, '2020-10-12 10:07:36', '2020-10-12 10:07:36');
INSERT INTO `sq_sys_file` VALUES (194, 'wxdf0830417ad278fe.o6zAJs2JMRqKKFaOG508xypKs25s.KUTMPuLqDsHGc46efad0d5454342c5c2afea1a560c86.png', '-1602668043-1188.png', '/files/20201014/-1602668043-1188.png', 'png', '1831', 0, '2020-10-14 17:34:03', '2020-10-14 17:34:03');
INSERT INTO `sq_sys_file` VALUES (195, '微信图片_20190821171516.jpg', '-1602746794-85743.jpg', '/files/20201015/-1602746794-85743.jpg', 'jpg', '215010', NULL, '2020-10-15 15:26:34', '2020-10-15 15:26:34');
INSERT INTO `sq_sys_file` VALUES (196, '微信图片_20190821171516.jpg', '-1602746845-67844.jpg', '/files/20201015/-1602746845-67844.jpg', 'jpg', '215010', NULL, '2020-10-15 15:27:25', '2020-10-15 15:27:25');
INSERT INTO `sq_sys_file` VALUES (197, 'timg.jpg', '-1602747101-99011.jpg', '/files/20201015/-1602747101-99011.jpg', 'jpg', '187516', NULL, '2020-10-15 15:31:41', '2020-10-15 15:31:41');
INSERT INTO `sq_sys_file` VALUES (198, '微信图片_20190821171018.jpg', '-1602747475-95338.jpg', '/files/20201015/-1602747475-95338.jpg', 'jpg', '177094', NULL, '2020-10-15 15:37:55', '2020-10-15 15:37:55');
INSERT INTO `sq_sys_file` VALUES (199, 'u=2326788584,2231322671&fm=26&gp=0.jpg', '-1602754174-38794.jpg', '/files/20201015/-1602754174-38794.jpg', 'jpg', '18285', NULL, '2020-10-15 17:29:34', '2020-10-15 17:29:34');
INSERT INTO `sq_sys_file` VALUES (200, '微信图片_20190821170722.jpg', '-1602754183-92496.jpg', '/files/20201015/-1602754183-92496.jpg', 'jpg', '86986', NULL, '2020-10-15 17:29:43', '2020-10-15 17:29:43');
INSERT INTO `sq_sys_file` VALUES (201, '微信图片_20190821171438.jpg', '-1602754428-80356.jpg', '/files/20201015/-1602754428-80356.jpg', 'jpg', '289062', NULL, '2020-10-15 17:33:48', '2020-10-15 17:33:48');
INSERT INTO `sq_sys_file` VALUES (202, 'timg (1).jpg', '-1602754437-5179.jpg', '/files/20201015/-1602754437-5179.jpg', 'jpg', '407454', NULL, '2020-10-15 17:33:57', '2020-10-15 17:33:57');
INSERT INTO `sq_sys_file` VALUES (203, 'timg.jpg', '-1602754525-76902.jpg', '/files/20201015/-1602754525-76902.jpg', 'jpg', '187516', NULL, '2020-10-15 17:35:26', '2020-10-15 17:35:26');
INSERT INTO `sq_sys_file` VALUES (204, '微信图片_20190821171516.jpg', '-1602754571-78370.jpg', '/files/20201015/-1602754571-78370.jpg', 'jpg', '215010', NULL, '2020-10-15 17:36:11', '2020-10-15 17:36:11');
INSERT INTO `sq_sys_file` VALUES (205, 'u=2326788584,2231322671&fm=26&gp=0.jpg', '-1603245622-4645.jpg', '/files/20201021/-1603245622-4645.jpg', 'jpg', '18285', NULL, '2020-10-21 10:00:22', '2020-10-21 10:00:22');
INSERT INTO `sq_sys_file` VALUES (206, '微信图片_20190821170722.jpg', '-1603245651-6473.jpg', '/files/20201021/-1603245651-6473.jpg', 'jpg', '86986', NULL, '2020-10-21 10:00:51', '2020-10-21 10:00:51');
INSERT INTO `sq_sys_file` VALUES (207, '预览图_千图网_编号33986588.png', '-1607048183-92728.png', '/files/rich_text/-1607048183-92728.png', 'png', '1115984', NULL, '2020-12-04 10:16:23', '2020-12-04 10:16:23');
INSERT INTO `sq_sys_file` VALUES (208, '微信图片_20190821171438.jpg', '-1607048269-63019.jpg', '/files/rich_text/-1607048269-63019.jpg', 'jpg', '289062', NULL, '2020-12-04 10:17:49', '2020-12-04 10:17:49');
INSERT INTO `sq_sys_file` VALUES (209, 'timg.jpg', '-1618190003-30251.jpg', '/files/20210412/-1618190003-30251.jpg', 'jpg', '187516', NULL, '2021-04-12 09:13:23', '2021-04-12 09:13:23');
INSERT INTO `sq_sys_file` VALUES (210, 'timg.jpg', '-1618192193-48951.jpg', '/files/20210412/-1618192193-48951.jpg', 'jpg', '187516', NULL, '2021-04-12 09:49:53', '2021-04-12 09:49:53');
INSERT INTO `sq_sys_file` VALUES (211, '微信图片_20190821171516.jpg', '-1618192204-15417.jpg', '/files/rich_text/-1618192204-15417.jpg', 'jpg', '215010', NULL, '2021-04-12 09:50:04', '2021-04-12 09:50:04');
INSERT INTO `sq_sys_file` VALUES (212, 'timg (1).jpg', '-1618192322-85858.jpg', '/files/20210412/-1618192322-85858.jpg', 'jpg', '407454', NULL, '2021-04-12 09:52:02', '2021-04-12 09:52:02');
INSERT INTO `sq_sys_file` VALUES (213, '微信图片_20190821171438.jpg', '-1618192329-42025.jpg', '/files/rich_text/-1618192329-42025.jpg', 'jpg', '289062', NULL, '2021-04-12 09:52:09', '2021-04-12 09:52:09');
INSERT INTO `sq_sys_file` VALUES (214, 'timg.jpg', '-1618198465-2998.jpg', '/files/20210412/-1618198465-2998.jpg', 'jpg', '187516', NULL, '2021-04-12 11:34:25', '2021-04-12 11:34:25');
INSERT INTO `sq_sys_file` VALUES (215, '微信图片_20190821171102.jpg', '-1618198859-67517.jpg', '/files/20210412/-1618198859-67517.jpg', 'jpg', '204607', NULL, '2021-04-12 11:40:59', '2021-04-12 11:40:59');
INSERT INTO `sq_sys_file` VALUES (216, 'u=2326788584,2231322671&fm=26&gp=0.jpg', '-1618207622-34524.jpg', '/files/20210412/-1618207622-34524.jpg', 'jpg', '18285', NULL, '2021-04-12 14:07:02', '2021-04-12 14:07:02');
INSERT INTO `sq_sys_file` VALUES (217, 'u=494999854,985780807&fm=26&gp=0.jpg', '-1618207651-92798.jpg', '/files/20210412/-1618207651-92798.jpg', 'jpg', '42875', NULL, '2021-04-12 14:07:31', '2021-04-12 14:07:31');
INSERT INTO `sq_sys_file` VALUES (218, '微信图片_20190821171516.jpg', '-1618215651-80495.jpg', '/files/rich_text/-1618215651-80495.jpg', 'jpg', '215010', NULL, '2021-04-12 16:20:51', '2021-04-12 16:20:51');
INSERT INTO `sq_sys_file` VALUES (219, 'timg.jpg', '-1618215729-1957.jpg', '/files/rich_text/-1618215729-1957.jpg', 'jpg', '187516', NULL, '2021-04-12 16:22:09', '2021-04-12 16:22:09');
INSERT INTO `sq_sys_file` VALUES (220, '微信图片_20190821171516.jpg', '-1618215775-88625.jpg', '/files/rich_text/-1618215775-88625.jpg', 'jpg', '215010', NULL, '2021-04-12 16:22:55', '2021-04-12 16:22:55');
INSERT INTO `sq_sys_file` VALUES (221, '微信图片_20190821171516.jpg', '-1618215808-46217.jpg', '/files/rich_text/-1618215808-46217.jpg', 'jpg', '215010', NULL, '2021-04-12 16:23:28', '2021-04-12 16:23:28');
INSERT INTO `sq_sys_file` VALUES (222, '微信图片_20190821171516.jpg', '-1618215928-5454.jpg', '/files/rich_text/-1618215928-5454.jpg', 'jpg', '215010', NULL, '2021-04-12 16:25:28', '2021-04-12 16:25:28');
INSERT INTO `sq_sys_file` VALUES (223, '微信图片_20190821171102.jpg', '-1618215958-39354.jpg', '/files/rich_text/-1618215958-39354.jpg', 'jpg', '204607', NULL, '2021-04-12 16:25:58', '2021-04-12 16:25:58');
INSERT INTO `sq_sys_file` VALUES (224, '微信图片_20190821171102.jpg', '-1618216025-97571.jpg', '/files/rich_text/-1618216025-97571.jpg', 'jpg', '204607', NULL, '2021-04-12 16:27:05', '2021-04-12 16:27:05');
INSERT INTO `sq_sys_file` VALUES (225, '222.mp4', '-1618216118-46772.mp4', '/files/rich_text/-1618216118-46772.mp4', 'mp4', '1504794', NULL, '2021-04-12 16:28:38', '2021-04-12 16:28:38');
INSERT INTO `sq_sys_file` VALUES (226, '222.mp4', '-1618216296-87365.mp4', '/files/rich_text/-1618216296-87365.mp4', 'mp4', '1504794', NULL, '2021-04-12 16:31:36', '2021-04-12 16:31:36');
INSERT INTO `sq_sys_file` VALUES (227, '111.png', '-1618216316-80322.png', '/files/video/-1618216316-80322.png', 'png', '34436', NULL, '2021-04-12 16:31:56', '2021-04-12 16:31:56');
INSERT INTO `sq_sys_file` VALUES (228, '222.mp4', '-1618216344-96105.mp4', '/files/video/-1618216344-96105.mp4', 'mp4', '1504794', NULL, '2021-04-12 16:32:25', '2021-04-12 16:32:25');
INSERT INTO `sq_sys_file` VALUES (229, '微信图片_20190821171516.jpg', '-1618371092-37975.jpg', '/files/20210414/-1618371092-37975.jpg', 'jpg', '215010', NULL, '2021-04-14 11:31:32', '2021-04-14 11:31:32');
INSERT INTO `sq_sys_file` VALUES (230, '微信图片_20190821171438.jpg', '-1618371182-39809.jpg', '/files/20210414/-1618371182-39809.jpg', 'jpg', '289062', NULL, '2021-04-14 11:33:02', '2021-04-14 11:33:02');
INSERT INTO `sq_sys_file` VALUES (231, '微信图片_20190821171516.jpg', '-1618371228-54066.jpg', '/files/20210414/-1618371228-54066.jpg', 'jpg', '215010', NULL, '2021-04-14 11:33:48', '2021-04-14 11:33:48');
INSERT INTO `sq_sys_file` VALUES (232, '微信图片_20190821171516.jpg', '-1618371268-88396.jpg', '/files/20210414/-1618371268-88396.jpg', 'jpg', '215010', NULL, '2021-04-14 11:34:28', '2021-04-14 11:34:28');
INSERT INTO `sq_sys_file` VALUES (233, '微信图片_20190821171438.jpg', '-1618371285-24785.jpg', '/files/20210414/-1618371285-24785.jpg', 'jpg', '289062', NULL, '2021-04-14 11:34:45', '2021-04-14 11:34:45');
INSERT INTO `sq_sys_file` VALUES (234, '微信图片_20190821171516.jpg', '-1618371334-86786.jpg', '/files/20210414/-1618371334-86786.jpg', 'jpg', '215010', NULL, '2021-04-14 11:35:34', '2021-04-14 11:35:34');
INSERT INTO `sq_sys_file` VALUES (235, '微信图片_20190821171438.jpg', '-1618371375-72435.jpg', '/files/20210414/-1618371375-72435.jpg', 'jpg', '289062', NULL, '2021-04-14 11:36:15', '2021-04-14 11:36:15');
INSERT INTO `sq_sys_file` VALUES (236, '微信图片_20190821171438.jpg', '-1618371516-73839.jpg', '/files/20210414/-1618371516-73839.jpg', 'jpg', '289062', NULL, '2021-04-14 11:38:36', '2021-04-14 11:38:36');
INSERT INTO `sq_sys_file` VALUES (237, '微信图片_20190821171102.jpg', '-1618371532-39923.jpg', '/files/20210414/-1618371532-39923.jpg', 'jpg', '204607', NULL, '2021-04-14 11:38:52', '2021-04-14 11:38:52');
INSERT INTO `sq_sys_file` VALUES (238, '微信图片_20190821171438.jpg', '-1618371566-18954.jpg', '/files/20210414/-1618371566-18954.jpg', 'jpg', '289062', NULL, '2021-04-14 11:39:26', '2021-04-14 11:39:26');
INSERT INTO `sq_sys_file` VALUES (239, '微信图片_20190821171438.jpg', '-1618372241-26866.jpg', '/files/20210414/-1618372241-26866.jpg', 'jpg', '289062', NULL, '2021-04-14 11:50:41', '2021-04-14 11:50:41');
INSERT INTO `sq_sys_file` VALUES (240, '微信图片_20190821171438.jpg', '-1618372269-95458.jpg', '/files/20210414/-1618372269-95458.jpg', 'jpg', '289062', NULL, '2021-04-14 11:51:09', '2021-04-14 11:51:09');
INSERT INTO `sq_sys_file` VALUES (241, '微信图片_20190821171438.jpg', '-1618372302-67987.jpg', '/files/20210414/-1618372302-67987.jpg', 'jpg', '289062', NULL, '2021-04-14 11:51:42', '2021-04-14 11:51:42');
INSERT INTO `sq_sys_file` VALUES (242, 'timg (1).jpg', '-1618383317-67465.jpg', '/files/20210414/-1618383317-67465.jpg', 'jpg', '407454', NULL, '2021-04-14 14:55:17', '2021-04-14 14:55:17');
INSERT INTO `sq_sys_file` VALUES (243, '微信图片_20190821171102.jpg', '-1618383634-3159.jpg', '/files/20210414/-1618383634-3159.jpg', 'jpg', '204607', NULL, '2021-04-14 15:00:34', '2021-04-14 15:00:34');
INSERT INTO `sq_sys_file` VALUES (244, '微信图片_20190821171438.jpg', '-1618383720-26526.jpg', '/files/20210414/-1618383720-26526.jpg', 'jpg', '289062', NULL, '2021-04-14 15:02:00', '2021-04-14 15:02:00');
INSERT INTO `sq_sys_file` VALUES (245, '微信图片_20190821171516.jpg', '-1618383744-60760.jpg', '/files/20210414/-1618383744-60760.jpg', 'jpg', '215010', NULL, '2021-04-14 15:02:24', '2021-04-14 15:02:24');
INSERT INTO `sq_sys_file` VALUES (246, '微信图片_20190821171438.jpg', '-1618384760-87162.jpg', '/files/20210414/-1618384760-87162.jpg', 'jpg', '289062', NULL, '2021-04-14 15:19:20', '2021-04-14 15:19:20');
INSERT INTO `sq_sys_file` VALUES (247, '微信图片_20190821171438.jpg', '-1618384868-77183.jpg', '/files/20210414/-1618384868-77183.jpg', 'jpg', '289062', NULL, '2021-04-14 15:21:08', '2021-04-14 15:21:08');
INSERT INTO `sq_sys_file` VALUES (248, '微信图片_20190821171438.jpg', '-1618384875-37197.jpg', '/files/20210414/-1618384875-37197.jpg', 'jpg', '289062', NULL, '2021-04-14 15:21:15', '2021-04-14 15:21:15');
INSERT INTO `sq_sys_file` VALUES (249, 'timg.jpg', '-1618385066-14205.jpg', '/files/20210414/-1618385066-14205.jpg', 'jpg', '187516', NULL, '2021-04-14 15:24:26', '2021-04-14 15:24:26');
INSERT INTO `sq_sys_file` VALUES (250, '微信图片_20190821171516.jpg', '-1618385605-5237.jpg', '/files/20210414/-1618385605-5237.jpg', 'jpg', '215010', NULL, '2021-04-14 15:33:25', '2021-04-14 15:33:25');
INSERT INTO `sq_sys_file` VALUES (251, '微信图片_20190821171438.jpg', '-1618385750-80130.jpg', '/files/20210414/-1618385750-80130.jpg', 'jpg', '289062', NULL, '2021-04-14 15:35:50', '2021-04-14 15:35:50');
INSERT INTO `sq_sys_file` VALUES (252, '微信图片_20190821171438.jpg', '-1618386616-83395.jpg', '/files/20210414/-1618386616-83395.jpg', 'jpg', '289062', NULL, '2021-04-14 15:50:16', '2021-04-14 15:50:16');
INSERT INTO `sq_sys_file` VALUES (253, 'timg.jpg', '-1618798196-27011.jpg', '/files/20210419/-1618798196-27011.jpg', 'jpg', '187516', NULL, '2021-04-19 10:09:56', '2021-04-19 10:09:56');
INSERT INTO `sq_sys_file` VALUES (254, 'timg.jpg', '-1618798419-80827.jpg', '/files/20210419/-1618798419-80827.jpg', 'jpg', '187516', NULL, '2021-04-19 10:13:39', '2021-04-19 10:13:39');
INSERT INTO `sq_sys_file` VALUES (255, 'timg.jpg', '-1618798620-36369.jpg', '/files/20210419/-1618798620-36369.jpg', 'jpg', '187516', NULL, '2021-04-19 10:17:00', '2021-04-19 10:17:00');
INSERT INTO `sq_sys_file` VALUES (256, '2.png', '-1621215512-34132.png', '/files/rich_text/-1621215512-34132.png', 'png', '40822', NULL, '2021-05-17 09:38:32', '2021-05-17 09:38:32');
INSERT INTO `sq_sys_file` VALUES (257, '3.png', '-1621215534-80775.png', '/files/rich_text/-1621215534-80775.png', 'png', '1783552', NULL, '2021-05-17 09:38:54', '2021-05-17 09:38:54');
INSERT INTO `sq_sys_file` VALUES (258, '4.png', '-1621215550-48514.png', '/files/rich_text/-1621215550-48514.png', 'png', '416739', NULL, '2021-05-17 09:39:10', '2021-05-17 09:39:10');
INSERT INTO `sq_sys_file` VALUES (259, '5.png', '-1621215597-57895.png', '/files/rich_text/-1621215597-57895.png', 'png', '1144813', NULL, '2021-05-17 09:39:57', '2021-05-17 09:39:57');
INSERT INTO `sq_sys_file` VALUES (260, '6.png', '-1621215607-407.png', '/files/rich_text/-1621215607-407.png', 'png', '606910', NULL, '2021-05-17 09:40:07', '2021-05-17 09:40:07');
INSERT INTO `sq_sys_file` VALUES (261, '6.png', '-1621216761-80303.png', '/files/rich_text/-1621216761-80303.png', 'png', '606910', NULL, '2021-05-17 09:59:21', '2021-05-17 09:59:21');
INSERT INTO `sq_sys_file` VALUES (262, 'ad0e8031c633a7d42e128608a7b7c1b8.mp4', '-1621218789-63842.mp4', '/files/video/-1621218789-63842.mp4', 'mp4', '1095907', NULL, '2021-05-17 10:33:09', '2021-05-17 10:33:09');
INSERT INTO `sq_sys_file` VALUES (263, 'ad0e8031c633a7d42e128608a7b7c1b8.mp4', '-1621218847-4755.mp4', '/files/video/-1621218847-4755.mp4', 'mp4', '1095907', NULL, '2021-05-17 10:34:07', '2021-05-17 10:34:07');
INSERT INTO `sq_sys_file` VALUES (264, '5.png', '-1621218884-58041.png', '/files/rich_text/-1621218884-58041.png', 'png', '1144813', NULL, '2021-05-17 10:34:44', '2021-05-17 10:34:44');
INSERT INTO `sq_sys_file` VALUES (265, '6.png', '-1621218887-91616.png', '/files/rich_text/-1621218887-91616.png', 'png', '606910', NULL, '2021-05-17 10:34:47', '2021-05-17 10:34:47');
INSERT INTO `sq_sys_file` VALUES (266, '9.png', '-1621222134-32848.png', '/files/20210517/-1621222134-32848.png', 'png', '449663', NULL, '2021-05-17 11:28:54', '2021-05-17 11:28:54');
INSERT INTO `sq_sys_file` VALUES (267, 'video-bg.png', '-1621235683-8283.png', '/files/20210517/-1621235683-8283.png', 'png', '78538', NULL, '2021-05-17 15:14:43', '2021-05-17 15:14:43');
INSERT INTO `sq_sys_file` VALUES (268, '2.png', '-1621300482-84122.png', '/files/rich_text/-1621300482-84122.png', 'png', '40822', NULL, '2021-05-18 09:14:42', '2021-05-18 09:14:42');
INSERT INTO `sq_sys_file` VALUES (269, '3.png', '-1621300495-95271.png', '/files/rich_text/-1621300495-95271.png', 'png', '1783552', NULL, '2021-05-18 09:14:55', '2021-05-18 09:14:55');
INSERT INTO `sq_sys_file` VALUES (270, '4.png', '-1621300498-83923.png', '/files/rich_text/-1621300498-83923.png', 'png', '416739', NULL, '2021-05-18 09:14:58', '2021-05-18 09:14:58');
INSERT INTO `sq_sys_file` VALUES (271, 'ad0e8031c633a7d42e128608a7b7c1b8.mp4', '-1621300535-66144.mp4', '/files/video/-1621300535-66144.mp4', 'mp4', '1095907', NULL, '2021-05-18 09:15:35', '2021-05-18 09:15:35');
INSERT INTO `sq_sys_file` VALUES (272, '5.png', '-1621300561-22473.png', '/files/rich_text/-1621300561-22473.png', 'png', '1144813', NULL, '2021-05-18 09:16:01', '2021-05-18 09:16:01');
INSERT INTO `sq_sys_file` VALUES (273, '6.png', '-1621300563-15282.png', '/files/rich_text/-1621300563-15282.png', 'png', '606910', NULL, '2021-05-18 09:16:03', '2021-05-18 09:16:03');
INSERT INTO `sq_sys_file` VALUES (274, '1.png', '-1623037134-38523.png', '/files/rich_text/-1623037134-38523.png', 'png', '1097878', NULL, '2021-06-07 11:38:54', '2021-06-07 11:38:54');
INSERT INTO `sq_sys_file` VALUES (275, '2.png', '-1623037138-71079.png', '/files/rich_text/-1623037138-71079.png', 'png', '1342347', NULL, '2021-06-07 11:38:58', '2021-06-07 11:38:58');
INSERT INTO `sq_sys_file` VALUES (276, '2.png', '-1623037182-3187.png', '/files/rich_text/-1623037182-3187.png', 'png', '1342347', NULL, '2021-06-07 11:39:42', '2021-06-07 11:39:42');
INSERT INTO `sq_sys_file` VALUES (277, 'e60a712bec4559210be8d20bef548aef.mp4', '-1623037578-71457.mp4', '/files/video/-1623037578-71457.mp4', 'mp4', '739780', NULL, '2021-06-07 11:46:18', '2021-06-07 11:46:18');
INSERT INTO `sq_sys_file` VALUES (278, 'e60a712bec4559210be8d20bef548aef.mp4', '-1623037637-28220.mp4', '/files/video/-1623037637-28220.mp4', 'mp4', '739780', NULL, '2021-06-07 11:47:17', '2021-06-07 11:47:17');
INSERT INTO `sq_sys_file` VALUES (279, 'timg (1).jpg', '-1627970514-19610.jpg', '/files/20210803/-1627970514-19610.jpg', 'jpg', '407454', NULL, '2021-08-03 14:01:54', '2021-08-03 14:01:54');
INSERT INTO `sq_sys_file` VALUES (280, 'cf2646d1b5978f5473b27cefba27762.png', '-1627970747-80281.png', '/files/20210803/-1627970747-80281.png', 'png', '449663', NULL, '2021-08-03 14:05:47', '2021-08-03 14:05:47');
INSERT INTO `sq_sys_file` VALUES (281, '微信图片_20190821171438.jpg', '-1627972127-28783.jpg', '/files/20210803/-1627972127-28783.jpg', 'jpg', '289062', NULL, '2021-08-03 14:28:47', '2021-08-03 14:28:47');
INSERT INTO `sq_sys_file` VALUES (282, '微信图片_20190821170722.jpg', '-1627972761-98224.jpg', '/files/20210803/-1627972761-98224.jpg', 'jpg', '86986', NULL, '2021-08-03 14:39:21', '2021-08-03 14:39:21');
INSERT INTO `sq_sys_file` VALUES (283, 'timg (1).jpg', '-1627974706-43599.jpg', '/files/20210803/-1627974706-43599.jpg', 'jpg', '407454', NULL, '2021-08-03 15:11:46', '2021-08-03 15:11:46');
INSERT INTO `sq_sys_file` VALUES (284, 'timg (1).jpg', '-1627974776-23096.jpg', '/files/20210803/-1627974776-23096.jpg', 'jpg', '407454', NULL, '2021-08-03 15:12:56', '2021-08-03 15:12:56');
INSERT INTO `sq_sys_file` VALUES (285, '微信图片_20190821171018.jpg', '-1627974815-6012.jpg', '/files/20210803/-1627974815-6012.jpg', 'jpg', '177094', NULL, '2021-08-03 15:13:35', '2021-08-03 15:13:35');
INSERT INTO `sq_sys_file` VALUES (286, 'u=2326788584,2231322671&fm=26&gp=0.jpg', '-1627974876-11771.jpg', '/files/20210803/-1627974876-11771.jpg', 'jpg', '18285', NULL, '2021-08-03 15:14:36', '2021-08-03 15:14:36');
INSERT INTO `sq_sys_file` VALUES (287, 'timg (1).jpg', '-1627975175-37748.jpg', '/files/20210803/-1627975175-37748.jpg', 'jpg', '407454', NULL, '2021-08-03 15:19:35', '2021-08-03 15:19:35');
INSERT INTO `sq_sys_file` VALUES (288, '微信图片_20190821170722.jpg', '-1627975330-15509.jpg', '/files/20210803/-1627975330-15509.jpg', 'jpg', '86986', NULL, '2021-08-03 15:22:10', '2021-08-03 15:22:10');
INSERT INTO `sq_sys_file` VALUES (289, 'timg (1).jpg', '-1627982370-28464.jpg', '/files/20210803/-1627982370-28464.jpg', 'jpg', '407454', NULL, '2021-08-03 17:19:30', '2021-08-03 17:19:30');
INSERT INTO `sq_sys_file` VALUES (290, 'timg (1).jpg', '-1627982479-35222.jpg', '/files/20210803/-1627982479-35222.jpg', 'jpg', '407454', NULL, '2021-08-03 17:21:19', '2021-08-03 17:21:19');
INSERT INTO `sq_sys_file` VALUES (291, 'timg (1).jpg', '-1627982515-95865.jpg', '/files/20210803/-1627982515-95865.jpg', 'jpg', '407454', NULL, '2021-08-03 17:21:55', '2021-08-03 17:21:55');
INSERT INTO `sq_sys_file` VALUES (292, '微信图片_20190821170722.jpg', '-1627982757-92957.jpg', '/files/20210803/-1627982757-92957.jpg', 'jpg', '86986', NULL, '2021-08-03 17:25:57', '2021-08-03 17:25:57');
INSERT INTO `sq_sys_file` VALUES (293, 'timg.jpg', '-1627984410-46574.jpg', '/files/20210803/-1627984410-46574.jpg', 'jpg', '187516', NULL, '2021-08-03 17:53:30', '2021-08-03 17:53:30');
INSERT INTO `sq_sys_file` VALUES (294, 'u=2326788584,2231322671&fm=26&gp=0.jpg', '-1627984431-44549.jpg', '/files/20210803/-1627984431-44549.jpg', 'jpg', '18285', NULL, '2021-08-03 17:53:51', '2021-08-03 17:53:51');
INSERT INTO `sq_sys_file` VALUES (295, '微信图片_20190821171516.jpg', '-1628039650-82615.jpg', '/files/20210804/-1628039650-82615.jpg', 'jpg', '215010', NULL, '2021-08-04 09:14:10', '2021-08-04 09:14:10');
INSERT INTO `sq_sys_file` VALUES (296, '微信图片_20190821171018.jpg', '-1628041335-35385.jpg', '/files/20210804/-1628041335-35385.jpg', 'jpg', '177094', NULL, '2021-08-04 09:42:15', '2021-08-04 09:42:15');
INSERT INTO `sq_sys_file` VALUES (297, 'cf2646d1b5978f5473b27cefba27762.png', '-1628041349-73185.png', '/files/20210804/-1628041349-73185.png', 'png', '449663', NULL, '2021-08-04 09:42:29', '2021-08-04 09:42:29');
INSERT INTO `sq_sys_file` VALUES (298, 'cf2646d1b5978f5473b27cefba27762.png', '-1628041402-15865.png', '/files/20210804/-1628041402-15865.png', 'png', '449663', NULL, '2021-08-04 09:43:22', '2021-08-04 09:43:22');
INSERT INTO `sq_sys_file` VALUES (299, '微信图片_20190821170722.jpg', '-1628042608-1768.jpg', '/files/20210804/-1628042608-1768.jpg', 'jpg', '86986', NULL, '2021-08-04 10:03:28', '2021-08-04 10:03:28');
INSERT INTO `sq_sys_file` VALUES (300, 'timg (1).jpg', '-1628042770-69960.jpg', '/files/20210804/-1628042770-69960.jpg', 'jpg', '407454', NULL, '2021-08-04 10:06:10', '2021-08-04 10:06:10');
INSERT INTO `sq_sys_file` VALUES (301, 'timg (1).jpg', '-1628043237-39203.jpg', '/files/20210804/-1628043237-39203.jpg', 'jpg', '407454', NULL, '2021-08-04 10:13:57', '2021-08-04 10:13:57');
INSERT INTO `sq_sys_file` VALUES (302, 'timg (1).jpg', '-1628043931-32725.jpg', '/files/20210804/-1628043931-32725.jpg', 'jpg', '407454', NULL, '2021-08-04 10:25:31', '2021-08-04 10:25:31');
INSERT INTO `sq_sys_file` VALUES (303, 'timg.jpg', '-1628044210-45777.jpg', '/files/20210804/-1628044210-45777.jpg', 'jpg', '187516', NULL, '2021-08-04 10:30:10', '2021-08-04 10:30:10');
INSERT INTO `sq_sys_file` VALUES (304, 'timg (1).jpg', '-1628044390-54507.jpg', '/files/20210804/-1628044390-54507.jpg', 'jpg', '407454', NULL, '2021-08-04 10:33:10', '2021-08-04 10:33:10');
INSERT INTO `sq_sys_file` VALUES (305, 'timg (1).jpg', '-1628044708-9196.jpg', '/files/20210804/-1628044708-9196.jpg', 'jpg', '407454', NULL, '2021-08-04 10:38:28', '2021-08-04 10:38:28');
INSERT INTO `sq_sys_file` VALUES (306, 'timg (1).jpg', '-1628044749-84000.jpg', '/files/20210804/-1628044749-84000.jpg', 'jpg', '407454', NULL, '2021-08-04 10:39:09', '2021-08-04 10:39:09');
INSERT INTO `sq_sys_file` VALUES (307, 'timg (1).jpg', '-1628045161-90108.jpg', '/files/20210804/-1628045161-90108.jpg', 'jpg', '407454', NULL, '2021-08-04 10:46:01', '2021-08-04 10:46:01');
INSERT INTO `sq_sys_file` VALUES (308, '微信图片_20190821171018.jpg', '-1628045408-23133.jpg', '/files/20210804/-1628045408-23133.jpg', 'jpg', '177094', NULL, '2021-08-04 10:50:08', '2021-08-04 10:50:08');
INSERT INTO `sq_sys_file` VALUES (309, 'timg (1).jpg', '-1628048640-6505.jpg', '/files/20210804/-1628048640-6505.jpg', 'jpg', '407454', NULL, '2021-08-04 11:44:00', '2021-08-04 11:44:00');
INSERT INTO `sq_sys_file` VALUES (310, 'timg.jpg', '-1628048932-1599.jpg', '/files/20210804/-1628048932-1599.jpg', 'jpg', '187516', NULL, '2021-08-04 11:48:52', '2021-08-04 11:48:52');
INSERT INTO `sq_sys_file` VALUES (311, '微信图片_20190821171018.jpg', '-1628049322-89546.jpg', '/files/20210804/-1628049322-89546.jpg', 'jpg', '177094', NULL, '2021-08-04 11:55:22', '2021-08-04 11:55:22');
INSERT INTO `sq_sys_file` VALUES (312, '微信图片_20190821171018.jpg', '-1628055576-65978.jpg', '/files/20210804/-1628055576-65978.jpg', 'jpg', '177094', NULL, '2021-08-04 13:39:36', '2021-08-04 13:39:36');
INSERT INTO `sq_sys_file` VALUES (313, '微信图片_20190821171018.jpg', '-1628055588-62208.jpg', '/files/20210804/-1628055588-62208.jpg', 'jpg', '177094', NULL, '2021-08-04 13:39:48', '2021-08-04 13:39:48');
INSERT INTO `sq_sys_file` VALUES (314, 'timg (1).jpg', '-1628055775-16326.jpg', '/files/20210804/-1628055775-16326.jpg', 'jpg', '407454', NULL, '2021-08-04 13:42:55', '2021-08-04 13:42:55');
INSERT INTO `sq_sys_file` VALUES (315, 'timg (1).jpg', '-1628056264-57112.jpg', '/files/20210804/-1628056264-57112.jpg', 'jpg', '407454', NULL, '2021-08-04 13:51:04', '2021-08-04 13:51:04');
INSERT INTO `sq_sys_file` VALUES (316, 'timg.jpg', '-1628056526-54690.jpg', '/files/20210804/-1628056526-54690.jpg', 'jpg', '187516', NULL, '2021-08-04 13:55:26', '2021-08-04 13:55:26');
INSERT INTO `sq_sys_file` VALUES (317, 'timg.jpg', '-1628057304-78161.jpg', '/files/20210804/-1628057304-78161.jpg', 'jpg', '187516', NULL, '2021-08-04 14:08:24', '2021-08-04 14:08:24');
INSERT INTO `sq_sys_file` VALUES (318, '微信图片_20190821171018.jpg', '-1628057645-23118.jpg', '/files/20210804/-1628057645-23118.jpg', 'jpg', '177094', NULL, '2021-08-04 14:14:05', '2021-08-04 14:14:05');
INSERT INTO `sq_sys_file` VALUES (319, '微信图片_20190821171438.jpg', '-1628059076-40439.jpg', '/files/20210804/-1628059076-40439.jpg', 'jpg', '289062', NULL, '2021-08-04 14:37:56', '2021-08-04 14:37:56');
INSERT INTO `sq_sys_file` VALUES (320, '微信图片_20190821171102.jpg', '-1628059112-1584.jpg', '/files/20210804/-1628059112-1584.jpg', 'jpg', '204607', NULL, '2021-08-04 14:38:32', '2021-08-04 14:38:32');
INSERT INTO `sq_sys_file` VALUES (321, '微信图片_20190821171018.jpg', '-1628059123-52022.jpg', '/files/20210804/-1628059123-52022.jpg', 'jpg', '177094', NULL, '2021-08-04 14:38:43', '2021-08-04 14:38:43');
INSERT INTO `sq_sys_file` VALUES (322, '微信图片_20190821171018.jpg', '-1628059276-7682.jpg', '/files/20210804/-1628059276-7682.jpg', 'jpg', '177094', NULL, '2021-08-04 14:41:16', '2021-08-04 14:41:16');
INSERT INTO `sq_sys_file` VALUES (323, '微信图片_20190821171018.jpg', '-1628059296-85287.jpg', '/files/20210804/-1628059296-85287.jpg', 'jpg', '177094', NULL, '2021-08-04 14:41:36', '2021-08-04 14:41:36');
INSERT INTO `sq_sys_file` VALUES (324, '微信图片_20190821171102.jpg', '-1628059419-17305.jpg', '/files/20210804/-1628059419-17305.jpg', 'jpg', '204607', NULL, '2021-08-04 14:43:39', '2021-08-04 14:43:39');
INSERT INTO `sq_sys_file` VALUES (325, 'timg (1).jpg', '-1628059442-61068.jpg', '/files/20210804/-1628059442-61068.jpg', 'jpg', '407454', NULL, '2021-08-04 14:44:02', '2021-08-04 14:44:02');
INSERT INTO `sq_sys_file` VALUES (326, 'timg (1).jpg', '-1628059492-46733.jpg', '/files/20210804/-1628059492-46733.jpg', 'jpg', '407454', NULL, '2021-08-04 14:44:52', '2021-08-04 14:44:52');
INSERT INTO `sq_sys_file` VALUES (327, '微信图片_20190821171018.jpg', '-1628059527-91164.jpg', '/files/20210804/-1628059527-91164.jpg', 'jpg', '177094', NULL, '2021-08-04 14:45:27', '2021-08-04 14:45:27');
INSERT INTO `sq_sys_file` VALUES (328, '微信图片_20190821171018.jpg', '-1628059600-83699.jpg', '/files/20210804/-1628059600-83699.jpg', 'jpg', '177094', NULL, '2021-08-04 14:46:40', '2021-08-04 14:46:40');
INSERT INTO `sq_sys_file` VALUES (329, '微信图片_20190821171018.jpg', '-1628060613-35612.jpg', '/files/20210804/-1628060613-35612.jpg', 'jpg', '177094', NULL, '2021-08-04 15:03:33', '2021-08-04 15:03:33');
INSERT INTO `sq_sys_file` VALUES (330, '微信图片_20190821171018.jpg', '-1628060656-75348.jpg', '/files/20210804/-1628060656-75348.jpg', 'jpg', '177094', NULL, '2021-08-04 15:04:16', '2021-08-04 15:04:16');
INSERT INTO `sq_sys_file` VALUES (331, '微信图片_20190821171018.jpg', '-1628060824-41889.jpg', '/files/20210804/-1628060824-41889.jpg', 'jpg', '177094', NULL, '2021-08-04 15:07:04', '2021-08-04 15:07:04');
INSERT INTO `sq_sys_file` VALUES (332, '微信图片_20190821171102.jpg', '-1628060851-85911.jpg', '/files/20210804/-1628060851-85911.jpg', 'jpg', '204607', NULL, '2021-08-04 15:07:31', '2021-08-04 15:07:31');
INSERT INTO `sq_sys_file` VALUES (333, '微信图片_20190821171018.jpg', '-1628060923-95375.jpg', '/files/20210804/-1628060923-95375.jpg', 'jpg', '177094', NULL, '2021-08-04 15:08:43', '2021-08-04 15:08:43');
INSERT INTO `sq_sys_file` VALUES (334, 'timg.jpg', '-1628061035-35046.jpg', '/files/20210804/-1628061035-35046.jpg', 'jpg', '187516', NULL, '2021-08-04 15:10:35', '2021-08-04 15:10:35');
INSERT INTO `sq_sys_file` VALUES (335, '微信图片_20190821171018.jpg', '-1628061050-30358.jpg', '/files/20210804/-1628061050-30358.jpg', 'jpg', '177094', NULL, '2021-08-04 15:10:50', '2021-08-04 15:10:50');
INSERT INTO `sq_sys_file` VALUES (336, '微信图片_20190821171018.jpg', '-1628061076-1989.jpg', '/files/20210804/-1628061076-1989.jpg', 'jpg', '177094', NULL, '2021-08-04 15:11:16', '2021-08-04 15:11:16');
INSERT INTO `sq_sys_file` VALUES (337, '微信图片_20190821171516.jpg', '-1628061152-73883.jpg', '/files/20210804/-1628061152-73883.jpg', 'jpg', '215010', NULL, '2021-08-04 15:12:32', '2021-08-04 15:12:32');
INSERT INTO `sq_sys_file` VALUES (338, 'timg (1).jpg', '-1628061264-99736.jpg', '/files/20210804/-1628061264-99736.jpg', 'jpg', '407454', NULL, '2021-08-04 15:14:24', '2021-08-04 15:14:24');
INSERT INTO `sq_sys_file` VALUES (339, '微信图片_20190821171516.jpg', '-1628061707-29291.jpg', '/files/20210804/-1628061707-29291.jpg', 'jpg', '215010', NULL, '2021-08-04 15:21:47', '2021-08-04 15:21:47');
INSERT INTO `sq_sys_file` VALUES (340, '微信图片_20190821171018.jpg', '-1628062947-63150.jpg', '/files/20210804/-1628062947-63150.jpg', 'jpg', '177094', NULL, '2021-08-04 15:42:27', '2021-08-04 15:42:27');
INSERT INTO `sq_sys_file` VALUES (343, '微信图片_20190821171102.jpg', '-1628063426-54876.jpg', '/files/20210804/-1628063426-54876.jpg', 'jpg', '204607', NULL, '2021-08-04 15:50:26', '2021-08-04 15:50:26');
INSERT INTO `sq_sys_file` VALUES (344, '微信图片_20190821171018.jpg', '-1628063783-24304.jpg', '/files/20210804/-1628063783-24304.jpg', 'jpg', '177094', NULL, '2021-08-04 15:56:23', '2021-08-04 15:56:23');
INSERT INTO `sq_sys_file` VALUES (345, '微信图片_20190821170722.jpg', '-1628063808-22035.jpg', '/files/20210804/-1628063808-22035.jpg', 'jpg', '86986', NULL, '2021-08-04 15:56:48', '2021-08-04 15:56:48');
INSERT INTO `sq_sys_file` VALUES (346, '微信图片_20190821171018.jpg', '-1628063936-27602.jpg', '/files/20210804/-1628063936-27602.jpg', 'jpg', '177094', NULL, '2021-08-04 15:58:56', '2021-08-04 15:58:56');
INSERT INTO `sq_sys_file` VALUES (347, '微信图片_20190821171018.jpg', '-1628063951-38568.jpg', '/files/20210804/-1628063951-38568.jpg', 'jpg', '177094', NULL, '2021-08-04 15:59:11', '2021-08-04 15:59:11');
INSERT INTO `sq_sys_file` VALUES (348, '微信图片_20190821171102.jpg', '-1628064023-35612.jpg', '/files/20210804/-1628064023-35612.jpg', 'jpg', '204607', NULL, '2021-08-04 16:00:23', '2021-08-04 16:00:23');
INSERT INTO `sq_sys_file` VALUES (349, '微信图片_20190821170722.jpg', '-1628064705-24675.jpg', '/files/20210804/-1628064705-24675.jpg', 'jpg', '86986', NULL, '2021-08-04 16:11:45', '2021-08-04 16:11:45');
INSERT INTO `sq_sys_file` VALUES (350, '微信图片_20190821170722.jpg', '-1628066184-47211.jpg', '/files/20210804/-1628066184-47211.jpg', 'jpg', '86986', NULL, '2021-08-04 16:36:24', '2021-08-04 16:36:24');
INSERT INTO `sq_sys_file` VALUES (351, 'timg.jpg', '-1628066634-98966.jpg', '/files/20210804/-1628066634-98966.jpg', 'jpg', '187516', NULL, '2021-08-04 16:43:54', '2021-08-04 16:43:54');
INSERT INTO `sq_sys_file` VALUES (352, 'timg.jpg', '-1628066993-13359.jpg', '/files/20210804/-1628066993-13359.jpg', 'jpg', '187516', NULL, '2021-08-04 16:49:53', '2021-08-04 16:49:53');
INSERT INTO `sq_sys_file` VALUES (353, 'timg.jpg', '-1628067032-79071.jpg', '/files/20210804/-1628067032-79071.jpg', 'jpg', '187516', NULL, '2021-08-04 16:50:32', '2021-08-04 16:50:32');
INSERT INTO `sq_sys_file` VALUES (354, '微信图片_20190821171018.jpg', '-1628067042-52209.jpg', '/files/20210804/-1628067042-52209.jpg', 'jpg', '177094', NULL, '2021-08-04 16:50:42', '2021-08-04 16:50:42');
INSERT INTO `sq_sys_file` VALUES (355, 'timg (1).jpg', '-1628127482-97423.jpg', '/files/20210805/-1628127482-97423.jpg', 'jpg', '407454', NULL, '2021-08-05 09:38:02', '2021-08-05 09:38:02');
INSERT INTO `sq_sys_file` VALUES (356, '微信图片_20190821171018.jpg', '-1628127537-56140.jpg', '/files/20210805/-1628127537-56140.jpg', 'jpg', '177094', NULL, '2021-08-05 09:38:57', '2021-08-05 09:38:57');
INSERT INTO `sq_sys_file` VALUES (357, 'timg (1).jpg', '-1628133183-5454.jpg', '/files/20210805/-1628133183-5454.jpg', 'jpg', '407454', NULL, '2021-08-05 11:13:03', '2021-08-05 11:13:03');
INSERT INTO `sq_sys_file` VALUES (358, 'timg (1).jpg', '-1628133313-32531.jpg', '/files/20210805/-1628133313-32531.jpg', 'jpg', '407454', NULL, '2021-08-05 11:15:13', '2021-08-05 11:15:13');
INSERT INTO `sq_sys_file` VALUES (359, 'timg (1).jpg', '-1628133325-29928.jpg', '/files/20210805/-1628133325-29928.jpg', 'jpg', '407454', NULL, '2021-08-05 11:15:25', '2021-08-05 11:15:25');
INSERT INTO `sq_sys_file` VALUES (360, 'timg (1).jpg', '-1628133332-97268.jpg', '/files/20210805/-1628133332-97268.jpg', 'jpg', '407454', NULL, '2021-08-05 11:15:32', '2021-08-05 11:15:32');
INSERT INTO `sq_sys_file` VALUES (361, '微信图片_20190821170722.jpg', '-1628133357-5644.jpg', '/files/20210805/-1628133357-5644.jpg', 'jpg', '86986', NULL, '2021-08-05 11:15:57', '2021-08-05 11:15:57');
INSERT INTO `sq_sys_file` VALUES (362, 'timg (1).jpg', '-1628143652-67849.jpg', '/files/20210805/-1628143652-67849.jpg', 'jpg', '407454', NULL, '2021-08-05 14:07:32', '2021-08-05 14:07:32');
INSERT INTO `sq_sys_file` VALUES (363, 'timg (1).jpg', '-1628143691-60063.jpg', '/files/20210805/-1628143691-60063.jpg', 'jpg', '407454', NULL, '2021-08-05 14:08:11', '2021-08-05 14:08:11');
INSERT INTO `sq_sys_file` VALUES (364, '微信图片_20190821171018.jpg', '-1628143741-11013.jpg', '/files/20210805/-1628143741-11013.jpg', 'jpg', '177094', NULL, '2021-08-05 14:09:01', '2021-08-05 14:09:01');
INSERT INTO `sq_sys_file` VALUES (365, '微信图片_20190821171018.jpg', '-1628143779-492.jpg', '/files/20210805/-1628143779-492.jpg', 'jpg', '177094', NULL, '2021-08-05 14:09:39', '2021-08-05 14:09:39');
INSERT INTO `sq_sys_file` VALUES (366, '微信图片_20190821171102.jpg', '-1628145065-23418.jpg', '/files/20210805/-1628145065-23418.jpg', 'jpg', '204607', NULL, '2021-08-05 14:31:05', '2021-08-05 14:31:05');
INSERT INTO `sq_sys_file` VALUES (367, '微信图片_20190821171102.jpg', '-1628145097-32014.jpg', '/files/20210805/-1628145097-32014.jpg', 'jpg', '204607', NULL, '2021-08-05 14:31:37', '2021-08-05 14:31:37');
INSERT INTO `sq_sys_file` VALUES (368, '微信图片_20190821171102.jpg', '-1628145118-9009.jpg', '/files/20210805/-1628145118-9009.jpg', 'jpg', '204607', NULL, '2021-08-05 14:31:58', '2021-08-05 14:31:58');
INSERT INTO `sq_sys_file` VALUES (369, '微信图片_20190821171102.jpg', '-1628145215-27453.jpg', '/files/20210805/-1628145215-27453.jpg', 'jpg', '204607', NULL, '2021-08-05 14:33:35', '2021-08-05 14:33:35');
INSERT INTO `sq_sys_file` VALUES (370, 'timg (1).jpg', '-1628145246-54510.jpg', '/files/20210805/-1628145246-54510.jpg', 'jpg', '407454', NULL, '2021-08-05 14:34:06', '2021-08-05 14:34:06');
INSERT INTO `sq_sys_file` VALUES (371, 'timg (1).jpg', '-1628145435-46276.jpg', '/files/20210805/-1628145435-46276.jpg', 'jpg', '407454', NULL, '2021-08-05 14:37:15', '2021-08-05 14:37:15');
INSERT INTO `sq_sys_file` VALUES (372, 'bg.jpg', '-1628473883-12409.jpg', '/files/20210809/-1628473883-12409.jpg', 'jpg', '188089', NULL, '2021-08-09 09:51:23', '2021-08-09 09:51:23');
INSERT INTO `sq_sys_file` VALUES (373, '微信图片_20190821171516.jpg', '-1628473951-15939.jpg', '/files/20210809/-1628473951-15939.jpg', 'jpg', '215010', NULL, '2021-08-09 09:52:31', '2021-08-09 09:52:31');
INSERT INTO `sq_sys_file` VALUES (374, '微信图片_20190821171516.jpg', '-1628473972-38606.jpg', '/files/20210809/-1628473972-38606.jpg', 'jpg', '215010', NULL, '2021-08-09 09:52:52', '2021-08-09 09:52:52');
INSERT INTO `sq_sys_file` VALUES (375, 'cf2646d1b5978f5473b27cefba27762.png', '-1628474071-24141.png', '/files/20210809/-1628474071-24141.png', 'png', '449663', NULL, '2021-08-09 09:54:31', '2021-08-09 09:54:31');
INSERT INTO `sq_sys_file` VALUES (376, '微信图片_20190821171516.jpg', '-1628474095-360.jpg', '/files/20210809/-1628474095-360.jpg', 'jpg', '215010', NULL, '2021-08-09 09:54:55', '2021-08-09 09:54:55');
INSERT INTO `sq_sys_file` VALUES (377, '微信图片_20190821171516.jpg', '-1628474115-15811.jpg', '/files/20210809/-1628474115-15811.jpg', 'jpg', '215010', NULL, '2021-08-09 09:55:15', '2021-08-09 09:55:15');
INSERT INTO `sq_sys_file` VALUES (378, '微信图片_20190821171516.jpg', '-1628474137-41800.jpg', '/files/20210809/-1628474137-41800.jpg', 'jpg', '215010', NULL, '2021-08-09 09:55:37', '2021-08-09 09:55:37');
INSERT INTO `sq_sys_file` VALUES (379, '微信图片_20190821171516.jpg', '-1628474166-20350.jpg', '/files/20210809/-1628474166-20350.jpg', 'jpg', '215010', NULL, '2021-08-09 09:56:06', '2021-08-09 09:56:06');
INSERT INTO `sq_sys_file` VALUES (380, '微信图片_20190821171516.jpg', '-1628474192-96828.jpg', '/files/20210809/-1628474192-96828.jpg', 'jpg', '215010', NULL, '2021-08-09 09:56:32', '2021-08-09 09:56:32');
INSERT INTO `sq_sys_file` VALUES (381, '微信图片_20190821171516.jpg', '-1628474272-87256.jpg', '/files/20210809/-1628474272-87256.jpg', 'jpg', '215010', NULL, '2021-08-09 09:57:52', '2021-08-09 09:57:52');
INSERT INTO `sq_sys_file` VALUES (382, '微信图片_20190821171516.jpg', '-1628474287-52588.jpg', '/files/20210809/-1628474287-52588.jpg', 'jpg', '215010', NULL, '2021-08-09 09:58:07', '2021-08-09 09:58:07');
INSERT INTO `sq_sys_file` VALUES (383, '微信图片_20190821171516.jpg', '-1628474302-39752.jpg', '/files/20210809/-1628474302-39752.jpg', 'jpg', '215010', NULL, '2021-08-09 09:58:22', '2021-08-09 09:58:22');
INSERT INTO `sq_sys_file` VALUES (384, '微信图片_20190821171516.jpg', '-1628474389-10919.jpg', '/files/20210809/-1628474389-10919.jpg', 'jpg', '215010', NULL, '2021-08-09 09:59:49', '2021-08-09 09:59:49');
INSERT INTO `sq_sys_file` VALUES (385, '1111.jpg', '-1628474412-37405.jpg', '/files/20210809/-1628474412-37405.jpg', 'jpg', '81449', NULL, '2021-08-09 10:00:12', '2021-08-09 10:00:12');
INSERT INTO `sq_sys_file` VALUES (386, '1111.jpg', '-1628474480-27171.jpg', '/files/20210809/-1628474480-27171.jpg', 'jpg', '81449', NULL, '2021-08-09 10:01:20', '2021-08-09 10:01:20');
INSERT INTO `sq_sys_file` VALUES (387, '222.jpg', '-1628474493-40349.jpg', '/files/20210809/-1628474493-40349.jpg', 'jpg', '76207', NULL, '2021-08-09 10:01:33', '2021-08-09 10:01:33');
INSERT INTO `sq_sys_file` VALUES (388, 'bg.jpg', '-1628474641-98565.jpg', '/files/20210809/-1628474641-98565.jpg', 'jpg', '188089', NULL, '2021-08-09 10:04:01', '2021-08-09 10:04:01');
INSERT INTO `sq_sys_file` VALUES (389, 'bg.jpg', '-1628474681-62329.jpg', '/files/20210809/-1628474681-62329.jpg', 'jpg', '188089', NULL, '2021-08-09 10:04:41', '2021-08-09 10:04:41');
INSERT INTO `sq_sys_file` VALUES (390, '1111.jpg', '-1628474696-12298.jpg', '/files/20210809/-1628474696-12298.jpg', 'jpg', '81449', NULL, '2021-08-09 10:04:56', '2021-08-09 10:04:56');
INSERT INTO `sq_sys_file` VALUES (391, '1111.jpg', '-1628474701-41400.jpg', '/files/20210809/-1628474701-41400.jpg', 'jpg', '81449', NULL, '2021-08-09 10:05:01', '2021-08-09 10:05:01');
INSERT INTO `sq_sys_file` VALUES (392, '微信图片_20190821171516.jpg', '-1628474748-98791.jpg', '/files/20210809/-1628474748-98791.jpg', 'jpg', '215010', NULL, '2021-08-09 10:05:48', '2021-08-09 10:05:48');
INSERT INTO `sq_sys_file` VALUES (393, '微信图片_20190821171516.jpg', '-1628474764-43793.jpg', '/files/20210809/-1628474764-43793.jpg', 'jpg', '215010', NULL, '2021-08-09 10:06:04', '2021-08-09 10:06:04');
INSERT INTO `sq_sys_file` VALUES (394, '微信图片_20190821171516.jpg', '-1628474796-7253.jpg', '/files/20210809/-1628474796-7253.jpg', 'jpg', '215010', NULL, '2021-08-09 10:06:36', '2021-08-09 10:06:36');
INSERT INTO `sq_sys_file` VALUES (395, '微信图片_20190821171516.jpg', '-1628474836-88604.jpg', '/files/20210809/-1628474836-88604.jpg', 'jpg', '215010', NULL, '2021-08-09 10:07:16', '2021-08-09 10:07:16');
INSERT INTO `sq_sys_file` VALUES (396, '微信图片_20190821171516.jpg', '-1628474876-94491.jpg', '/files/20210809/-1628474876-94491.jpg', 'jpg', '215010', NULL, '2021-08-09 10:07:57', '2021-08-09 10:07:57');
INSERT INTO `sq_sys_file` VALUES (397, 'timg.jpg', '-1628474906-83243.jpg', '/files/20210809/-1628474906-83243.jpg', 'jpg', '187516', NULL, '2021-08-09 10:08:26', '2021-08-09 10:08:26');
INSERT INTO `sq_sys_file` VALUES (398, '微信图片_20190821171516.jpg', '-1628475107-73086.jpg', '/files/20210809/-1628475107-73086.jpg', 'jpg', '215010', NULL, '2021-08-09 10:11:48', '2021-08-09 10:11:48');
INSERT INTO `sq_sys_file` VALUES (399, '微信图片_20190821171516.jpg', '-1628475201-1273.jpg', '/files/20210809/-1628475201-1273.jpg', 'jpg', '215010', NULL, '2021-08-09 10:13:21', '2021-08-09 10:13:21');
INSERT INTO `sq_sys_file` VALUES (401, '微信图片_20190821171516.jpg', '-1628475565-25699.jpg', '/files/20210809/-1628475565-25699.jpg', 'jpg', '215010', NULL, '2021-08-09 10:19:25', '2021-08-09 10:19:25');
INSERT INTO `sq_sys_file` VALUES (402, '微信图片_20190821171516.jpg', '-1628475582-896.jpg', '/files/20210809/-1628475582-896.jpg', 'jpg', '215010', NULL, '2021-08-09 10:19:42', '2021-08-09 10:19:42');
INSERT INTO `sq_sys_file` VALUES (403, '微信图片_20190821171516.jpg', '-1628475628-27682.jpg', '/files/20210809/-1628475628-27682.jpg', 'jpg', '215010', NULL, '2021-08-09 10:20:28', '2021-08-09 10:20:28');
INSERT INTO `sq_sys_file` VALUES (404, '微信图片_20190821171516.jpg', '-1628475658-11164.jpg', '/files/20210809/-1628475658-11164.jpg', 'jpg', '215010', NULL, '2021-08-09 10:20:58', '2021-08-09 10:20:58');
INSERT INTO `sq_sys_file` VALUES (405, '微信图片_20190821171516.jpg', '-1628475683-64224.jpg', '/files/20210809/-1628475683-64224.jpg', 'jpg', '215010', NULL, '2021-08-09 10:21:23', '2021-08-09 10:21:23');
INSERT INTO `sq_sys_file` VALUES (406, '微信图片_20190821171516.jpg', '-1628475714-23296.jpg', '/files/20210809/-1628475714-23296.jpg', 'jpg', '215010', NULL, '2021-08-09 10:21:54', '2021-08-09 10:21:54');
INSERT INTO `sq_sys_file` VALUES (407, 'u=494999854,985780807&fm=26&gp=0.jpg', '-1628475738-83681.jpg', '/files/20210809/-1628475738-83681.jpg', 'jpg', '42875', NULL, '2021-08-09 10:22:18', '2021-08-09 10:22:18');
INSERT INTO `sq_sys_file` VALUES (408, '微信图片_20190821171516.jpg', '-1628475780-46824.jpg', '/files/20210809/-1628475780-46824.jpg', 'jpg', '215010', NULL, '2021-08-09 10:23:00', '2021-08-09 10:23:00');
INSERT INTO `sq_sys_file` VALUES (409, '微信图片_20190821171516.jpg', '-1628478327-12722.jpg', '/files/20210809/-1628478327-12722.jpg', 'jpg', '215010', NULL, '2021-08-09 11:05:27', '2021-08-09 11:05:27');
INSERT INTO `sq_sys_file` VALUES (410, '微信图片_20190821171516.jpg', '-1628478349-23723.jpg', '/files/20210809/-1628478349-23723.jpg', 'jpg', '215010', NULL, '2021-08-09 11:05:49', '2021-08-09 11:05:49');
INSERT INTO `sq_sys_file` VALUES (411, '微信图片_20190821171516.jpg', '-1628478366-21036.jpg', '/files/20210809/-1628478366-21036.jpg', 'jpg', '215010', NULL, '2021-08-09 11:06:06', '2021-08-09 11:06:06');
INSERT INTO `sq_sys_file` VALUES (412, '微信图片_20190821171516.jpg', '-1628478392-76580.jpg', '/files/20210809/-1628478392-76580.jpg', 'jpg', '215010', NULL, '2021-08-09 11:06:32', '2021-08-09 11:06:32');
INSERT INTO `sq_sys_file` VALUES (413, '微信图片_20190821171516.jpg', '-1628478429-85005.jpg', '/files/20210809/-1628478429-85005.jpg', 'jpg', '215010', NULL, '2021-08-09 11:07:09', '2021-08-09 11:07:09');
INSERT INTO `sq_sys_file` VALUES (414, '微信图片_20190821171516.jpg', '-1628478475-42678.jpg', '/files/20210809/-1628478475-42678.jpg', 'jpg', '215010', NULL, '2021-08-09 11:07:55', '2021-08-09 11:07:55');
INSERT INTO `sq_sys_file` VALUES (415, '微信图片_20190821171516.jpg', '-1628478501-36042.jpg', '/files/20210809/-1628478501-36042.jpg', 'jpg', '215010', NULL, '2021-08-09 11:08:21', '2021-08-09 11:08:21');
INSERT INTO `sq_sys_file` VALUES (416, '微信图片_20190821171516.jpg', '-1628478512-7517.jpg', '/files/20210809/-1628478512-7517.jpg', 'jpg', '215010', NULL, '2021-08-09 11:08:32', '2021-08-09 11:08:32');
INSERT INTO `sq_sys_file` VALUES (417, '微信图片_20190821171516.jpg', '-1628478523-21120.jpg', '/files/20210809/-1628478523-21120.jpg', 'jpg', '215010', NULL, '2021-08-09 11:08:43', '2021-08-09 11:08:43');
INSERT INTO `sq_sys_file` VALUES (418, '微信图片_20190821171516.jpg', '-1628478667-93256.jpg', '/files/20210809/-1628478667-93256.jpg', 'jpg', '215010', NULL, '2021-08-09 11:11:07', '2021-08-09 11:11:07');
INSERT INTO `sq_sys_file` VALUES (419, '微信图片_20190821171516.jpg', '-1628478692-51571.jpg', '/files/20210809/-1628478692-51571.jpg', 'jpg', '215010', NULL, '2021-08-09 11:11:32', '2021-08-09 11:11:32');
INSERT INTO `sq_sys_file` VALUES (420, '微信图片_20190821171516.jpg', '-1628478715-20212.jpg', '/files/20210809/-1628478715-20212.jpg', 'jpg', '215010', NULL, '2021-08-09 11:11:55', '2021-08-09 11:11:55');
INSERT INTO `sq_sys_file` VALUES (421, '111.jpg', '-1628644905-74514.jpg', '/files/20210811/-1628644905-74514.jpg', 'jpg', '758185', NULL, '2021-08-11 09:21:45', '2021-08-11 09:21:45');
INSERT INTO `sq_sys_file` VALUES (422, '微信图片_20190821171102.jpg', '-1628644956-57164.jpg', '/files/20210811/-1628644956-57164.jpg', 'jpg', '204607', NULL, '2021-08-11 09:22:36', '2021-08-11 09:22:36');
INSERT INTO `sq_sys_file` VALUES (423, '微信图片_20190821171102.jpg', '-1628646502-37216.jpg', '/files/20210811/-1628646502-37216.jpg', 'jpg', '204607', NULL, '2021-08-11 09:48:22', '2021-08-11 09:48:22');
INSERT INTO `sq_sys_file` VALUES (424, '微信图片_20190821171516.jpg', '-1628646514-79664.jpg', '/files/20210811/-1628646514-79664.jpg', 'jpg', '215010', NULL, '2021-08-11 09:48:34', '2021-08-11 09:48:34');
INSERT INTO `sq_sys_file` VALUES (425, '微信图片_20190821171516.jpg', '-1628646540-91292.jpg', '/files/20210811/-1628646540-91292.jpg', 'jpg', '215010', NULL, '2021-08-11 09:49:00', '2021-08-11 09:49:00');
INSERT INTO `sq_sys_file` VALUES (426, '微信图片_20190821171516.jpg', '-1628646567-46705.jpg', '/files/20210811/-1628646567-46705.jpg', 'jpg', '215010', NULL, '2021-08-11 09:49:27', '2021-08-11 09:49:27');
INSERT INTO `sq_sys_file` VALUES (427, '微信图片_20190821171516.jpg', '-1628646595-13226.jpg', '/files/20210811/-1628646595-13226.jpg', 'jpg', '215010', NULL, '2021-08-11 09:49:55', '2021-08-11 09:49:55');
INSERT INTO `sq_sys_file` VALUES (428, '微信图片_20190821171516.jpg', '-1628646622-36.jpg', '/files/20210811/-1628646622-36.jpg', 'jpg', '215010', NULL, '2021-08-11 09:50:22', '2021-08-11 09:50:22');
INSERT INTO `sq_sys_file` VALUES (429, '微信图片_20190821171516.jpg', '-1628646643-31116.jpg', '/files/20210811/-1628646643-31116.jpg', 'jpg', '215010', NULL, '2021-08-11 09:50:44', '2021-08-11 09:50:44');
INSERT INTO `sq_sys_file` VALUES (430, 'timg.jpg', '-1628646658-40022.jpg', '/files/20210811/-1628646658-40022.jpg', 'jpg', '187516', NULL, '2021-08-11 09:50:58', '2021-08-11 09:50:58');
INSERT INTO `sq_sys_file` VALUES (431, '微信图片_20190821171516.jpg', '-1628647295-35352.jpg', '/files/20210811/-1628647295-35352.jpg', 'jpg', '215010', NULL, '2021-08-11 10:01:35', '2021-08-11 10:01:35');
INSERT INTO `sq_sys_file` VALUES (432, '金标点亮海报.jpg', '-1628664395-22332.jpg', '/files/20210811/-1628664395-22332.jpg', 'jpg', '1406141', NULL, '2021-08-11 14:46:35', '2021-08-11 14:46:35');
INSERT INTO `sq_sys_file` VALUES (433, '金标点亮海报.jpg', '-1628664408-30207.jpg', '/files/20210811/-1628664408-30207.jpg', 'jpg', '1406141', NULL, '2021-08-11 14:46:48', '2021-08-11 14:46:48');
INSERT INTO `sq_sys_file` VALUES (434, '微信图片_20190821171516.jpg', '-1628664426-56458.jpg', '/files/20210811/-1628664426-56458.jpg', 'jpg', '215010', NULL, '2021-08-11 14:47:06', '2021-08-11 14:47:06');
INSERT INTO `sq_sys_file` VALUES (435, '微信图片_20190821171516.jpg', '-1628664509-27761.jpg', '/files/20210811/-1628664509-27761.jpg', 'jpg', '215010', NULL, '2021-08-11 14:48:30', '2021-08-11 14:48:30');
INSERT INTO `sq_sys_file` VALUES (436, '微信图片_20190821171516.jpg', '-1628664535-10144.jpg', '/files/20210811/-1628664535-10144.jpg', 'jpg', '215010', NULL, '2021-08-11 14:48:55', '2021-08-11 14:48:55');
INSERT INTO `sq_sys_file` VALUES (437, '微信图片_20190821171516.jpg', '-1628664558-90437.jpg', '/files/20210811/-1628664558-90437.jpg', 'jpg', '215010', NULL, '2021-08-11 14:49:18', '2021-08-11 14:49:18');
INSERT INTO `sq_sys_file` VALUES (438, '微信图片_20190821171516.jpg', '-1628664576-8748.jpg', '/files/20210811/-1628664576-8748.jpg', 'jpg', '215010', NULL, '2021-08-11 14:49:36', '2021-08-11 14:49:36');
INSERT INTO `sq_sys_file` VALUES (439, '微信图片_20190821171516.jpg', '-1628664595-34912.jpg', '/files/20210811/-1628664595-34912.jpg', 'jpg', '215010', NULL, '2021-08-11 14:49:55', '2021-08-11 14:49:55');
INSERT INTO `sq_sys_file` VALUES (440, '微信图片_20190821171516.jpg', '-1628664616-39517.jpg', '/files/20210811/-1628664616-39517.jpg', 'jpg', '215010', NULL, '2021-08-11 14:50:16', '2021-08-11 14:50:16');
INSERT INTO `sq_sys_file` VALUES (441, '微信图片_20190821171516.jpg', '-1628664629-96213.jpg', '/files/20210811/-1628664629-96213.jpg', 'jpg', '215010', NULL, '2021-08-11 14:50:29', '2021-08-11 14:50:29');
INSERT INTO `sq_sys_file` VALUES (442, '微信图片_20190821171516.jpg', '-1628664687-81263.jpg', '/files/20210811/-1628664687-81263.jpg', 'jpg', '215010', NULL, '2021-08-11 14:51:27', '2021-08-11 14:51:27');
INSERT INTO `sq_sys_file` VALUES (443, '微信图片_20190821171516.jpg', '-1628664703-7578.jpg', '/files/20210811/-1628664703-7578.jpg', 'jpg', '215010', NULL, '2021-08-11 14:51:43', '2021-08-11 14:51:43');
INSERT INTO `sq_sys_file` VALUES (444, '微信图片_20190821171516.jpg', '-1628664717-31785.jpg', '/files/20210811/-1628664717-31785.jpg', 'jpg', '215010', NULL, '2021-08-11 14:51:57', '2021-08-11 14:51:57');
INSERT INTO `sq_sys_file` VALUES (445, '微信图片_20190821171516.jpg', '-1628664738-31254.jpg', '/files/20210811/-1628664738-31254.jpg', 'jpg', '215010', NULL, '2021-08-11 14:52:18', '2021-08-11 14:52:18');
INSERT INTO `sq_sys_file` VALUES (446, 'timg (1).jpg', '-1628755598-62439.jpg', '/files/20210812/-1628755598-62439.jpg', 'jpg', '407454', NULL, '2021-08-12 16:06:38', '2021-08-12 16:06:38');
INSERT INTO `sq_sys_file` VALUES (447, 'u=494999854,985780807&fm=26&gp=0.jpg', '-1628755813-86645.jpg', '/files/20210812/-1628755813-86645.jpg', 'jpg', '42875', NULL, '2021-08-12 16:10:13', '2021-08-12 16:10:13');
INSERT INTO `sq_sys_file` VALUES (448, 'u=494999854,985780807&fm=26&gp=0.jpg', '-1628755917-78413.jpg', '/files/20210812/-1628755917-78413.jpg', 'jpg', '42875', NULL, '2021-08-12 16:11:57', '2021-08-12 16:11:57');
INSERT INTO `sq_sys_file` VALUES (449, '微信图片_20190821171516.jpg', '-1628756590-40354.jpg', '/files/20210812/-1628756590-40354.jpg', 'jpg', '215010', NULL, '2021-08-12 16:23:10', '2021-08-12 16:23:10');
INSERT INTO `sq_sys_file` VALUES (450, '微信图片_20190821171516.jpg', '-1628756769-6408.jpg', '/files/20210812/-1628756769-6408.jpg', 'jpg', '215010', NULL, '2021-08-12 16:26:09', '2021-08-12 16:26:09');
INSERT INTO `sq_sys_file` VALUES (451, '微信图片_20190821171516.jpg', '-1628756786-22125.jpg', '/files/20210812/-1628756786-22125.jpg', 'jpg', '215010', NULL, '2021-08-12 16:26:26', '2021-08-12 16:26:26');
INSERT INTO `sq_sys_file` VALUES (452, '微信图片_20190821171516.jpg', '-1628756810-49624.jpg', '/files/20210812/-1628756810-49624.jpg', 'jpg', '215010', NULL, '2021-08-12 16:26:50', '2021-08-12 16:26:50');
INSERT INTO `sq_sys_file` VALUES (453, '微信图片_20190821171516.jpg', '-1628756824-80143.jpg', '/files/20210812/-1628756824-80143.jpg', 'jpg', '215010', NULL, '2021-08-12 16:27:04', '2021-08-12 16:27:04');
INSERT INTO `sq_sys_file` VALUES (454, 'u=494999854,985780807&fm=26&gp=0.jpg', '-1628756829-1677.jpg', '/files/20210812/-1628756829-1677.jpg', 'jpg', '42875', NULL, '2021-08-12 16:27:09', '2021-08-12 16:27:09');
INSERT INTO `sq_sys_file` VALUES (455, 'u=494999854,985780807&fm=26&gp=0.jpg', '-1628756847-22121.jpg', '/files/20210812/-1628756847-22121.jpg', 'jpg', '42875', NULL, '2021-08-12 16:27:27', '2021-08-12 16:27:27');
INSERT INTO `sq_sys_file` VALUES (456, '微信图片_20190821171516.jpg', '-1628756848-80467.jpg', '/files/20210812/-1628756848-80467.jpg', 'jpg', '215010', NULL, '2021-08-12 16:27:28', '2021-08-12 16:27:28');
INSERT INTO `sq_sys_file` VALUES (457, '微信图片_20190821171516.jpg', '-1628756860-98635.jpg', '/files/20210812/-1628756860-98635.jpg', 'jpg', '215010', NULL, '2021-08-12 16:27:40', '2021-08-12 16:27:40');
INSERT INTO `sq_sys_file` VALUES (458, '微信图片_20190821171438.jpg', '-1628756866-69202.jpg', '/files/20210812/-1628756866-69202.jpg', 'jpg', '289062', NULL, '2021-08-12 16:27:46', '2021-08-12 16:27:46');
INSERT INTO `sq_sys_file` VALUES (459, '微信图片_20190821171018.jpg', '-1628756873-38361.jpg', '/files/20210812/-1628756873-38361.jpg', 'jpg', '177094', NULL, '2021-08-12 16:27:53', '2021-08-12 16:27:53');
INSERT INTO `sq_sys_file` VALUES (460, 'timg.jpg', '-1628756882-70415.jpg', '/files/20210812/-1628756882-70415.jpg', 'jpg', '187516', NULL, '2021-08-12 16:28:02', '2021-08-12 16:28:02');
INSERT INTO `sq_sys_file` VALUES (461, '微信图片_20190821171516.jpg', '-1628758309-25093.jpg', '/files/20210812/-1628758309-25093.jpg', 'jpg', '215010', NULL, '2021-08-12 16:51:49', '2021-08-12 16:51:49');
INSERT INTO `sq_sys_file` VALUES (462, 'timg (1).jpg', '-1629093200-6698.jpg', '/files/20210816/-1629093200-6698.jpg', 'jpg', '407454', NULL, '2021-08-16 13:53:20', '2021-08-16 13:53:20');
INSERT INTO `sq_sys_file` VALUES (463, 'timg (1).jpg', '-1629093214-97741.jpg', '/files/20210816/-1629093214-97741.jpg', 'jpg', '407454', NULL, '2021-08-16 13:53:34', '2021-08-16 13:53:34');
INSERT INTO `sq_sys_file` VALUES (464, '微信图片_20190821171516.jpg', '-1629093441-17582.jpg', '/files/20210816/-1629093441-17582.jpg', 'jpg', '215010', NULL, '2021-08-16 13:57:21', '2021-08-16 13:57:21');
INSERT INTO `sq_sys_file` VALUES (465, 'timg.jpg', '-1629093612-99512.jpg', '/files/20210816/-1629093612-99512.jpg', 'jpg', '187516', NULL, '2021-08-16 14:00:12', '2021-08-16 14:00:12');
INSERT INTO `sq_sys_file` VALUES (466, '微信图片_20190821171516.jpg', '-1629093630-99718.jpg', '/files/20210816/-1629093630-99718.jpg', 'jpg', '215010', NULL, '2021-08-16 14:00:30', '2021-08-16 14:00:30');
INSERT INTO `sq_sys_file` VALUES (467, 'timg.jpg', '-1629093637-63023.jpg', '/files/20210816/-1629093637-63023.jpg', 'jpg', '187516', NULL, '2021-08-16 14:00:37', '2021-08-16 14:00:37');
INSERT INTO `sq_sys_file` VALUES (477, '微信图片_20190821171516.jpg', '-1629094758-30929.jpg', '/files/20210816/-1629094758-30929.jpg', 'jpg', '215010', NULL, '2021-08-16 14:19:18', '2021-08-16 14:19:18');
INSERT INTO `sq_sys_file` VALUES (478, '微信图片_20190821171516.jpg', '-1629094802-86823.jpg', '/files/20210816/-1629094802-86823.jpg', 'jpg', '215010', NULL, '2021-08-16 14:20:02', '2021-08-16 14:20:02');
INSERT INTO `sq_sys_file` VALUES (479, '微信图片_20190821171516.jpg', '-1629094902-99188.jpg', '/files/20210816/-1629094902-99188.jpg', 'jpg', '215010', NULL, '2021-08-16 14:21:42', '2021-08-16 14:21:42');
INSERT INTO `sq_sys_file` VALUES (480, '微信图片_20190821171516.jpg', '-1629094973-69966.jpg', '/files/20210816/-1629094973-69966.jpg', 'jpg', '215010', NULL, '2021-08-16 14:22:53', '2021-08-16 14:22:53');
INSERT INTO `sq_sys_file` VALUES (481, '微信图片_20190821171516.jpg', '-1629095062-27921.jpg', '/files/20210816/-1629095062-27921.jpg', 'jpg', '215010', NULL, '2021-08-16 14:24:22', '2021-08-16 14:24:22');
INSERT INTO `sq_sys_file` VALUES (482, '微信图片_20190821171516.jpg', '-1629095111-23844.jpg', '/files/20210816/-1629095111-23844.jpg', 'jpg', '215010', NULL, '2021-08-16 14:25:11', '2021-08-16 14:25:11');
INSERT INTO `sq_sys_file` VALUES (484, '微信图片_20190821171516.jpg', '-1629097338-93514.jpg', '/files/20210816/-1629097338-93514.jpg', 'jpg', '215010', NULL, '2021-08-16 15:02:18', '2021-08-16 15:02:18');
INSERT INTO `sq_sys_file` VALUES (485, '微信图片_20190821171438.jpg', '-1629097435-53329.jpg', '/files/20210816/-1629097435-53329.jpg', 'jpg', '289062', NULL, '2021-08-16 15:03:55', '2021-08-16 15:03:55');
INSERT INTO `sq_sys_file` VALUES (486, '微信图片_20190821171516.jpg', '-1629097469-70618.jpg', '/files/20210816/-1629097469-70618.jpg', 'jpg', '215010', NULL, '2021-08-16 15:04:29', '2021-08-16 15:04:29');
INSERT INTO `sq_sys_file` VALUES (487, '微信图片_20190821171516.jpg', '-1629097546-69093.jpg', '/files/20210816/-1629097546-69093.jpg', 'jpg', '215010', NULL, '2021-08-16 15:05:46', '2021-08-16 15:05:46');
INSERT INTO `sq_sys_file` VALUES (488, '微信图片_20190821171516.jpg', '-1629097602-74729.jpg', '/files/20210816/-1629097602-74729.jpg', 'jpg', '215010', NULL, '2021-08-16 15:06:42', '2021-08-16 15:06:42');
INSERT INTO `sq_sys_file` VALUES (489, '微信图片_20190821171516.jpg', '-1629097628-22909.jpg', '/files/20210816/-1629097628-22909.jpg', 'jpg', '215010', NULL, '2021-08-16 15:07:08', '2021-08-16 15:07:08');
INSERT INTO `sq_sys_file` VALUES (490, '微信图片_20190821171516.jpg', '-1629097712-87340.jpg', '/files/20210816/-1629097712-87340.jpg', 'jpg', '215010', NULL, '2021-08-16 15:08:32', '2021-08-16 15:08:32');
INSERT INTO `sq_sys_file` VALUES (491, '微信图片_20190821171516.jpg', '-1629097761-72977.jpg', '/files/20210816/-1629097761-72977.jpg', 'jpg', '215010', NULL, '2021-08-16 15:09:21', '2021-08-16 15:09:21');
INSERT INTO `sq_sys_file` VALUES (492, '微信图片_20190821171516.jpg', '-1629097825-51564.jpg', '/files/20210816/-1629097825-51564.jpg', 'jpg', '215010', NULL, '2021-08-16 15:10:25', '2021-08-16 15:10:25');
INSERT INTO `sq_sys_file` VALUES (493, '微信图片_20190821171516.jpg', '-1629098359-94211.jpg', '/files/20210816/-1629098359-94211.jpg', 'jpg', '215010', NULL, '2021-08-16 15:19:19', '2021-08-16 15:19:19');
INSERT INTO `sq_sys_file` VALUES (494, '微信图片_20190821171438.jpg', '-1629098365-90834.jpg', '/files/20210816/-1629098365-90834.jpg', 'jpg', '289062', NULL, '2021-08-16 15:19:25', '2021-08-16 15:19:25');
INSERT INTO `sq_sys_file` VALUES (495, '微信图片_20190821171438.jpg', '-1629098458-69388.jpg', '/files/20210816/-1629098458-69388.jpg', 'jpg', '289062', NULL, '2021-08-16 15:20:58', '2021-08-16 15:20:58');
INSERT INTO `sq_sys_file` VALUES (496, '微信图片_20190821171438.jpg', '-1629098692-96920.jpg', '/files/20210816/-1629098692-96920.jpg', 'jpg', '289062', NULL, '2021-08-16 15:24:52', '2021-08-16 15:24:52');
INSERT INTO `sq_sys_file` VALUES (497, 'timg.jpg', '-1629098817-70186.jpg', '/files/20210816/-1629098817-70186.jpg', 'jpg', '187516', NULL, '2021-08-16 15:26:57', '2021-08-16 15:26:57');
INSERT INTO `sq_sys_file` VALUES (498, '微信图片_20190821171516.jpg', '-1629098824-20565.jpg', '/files/20210816/-1629098824-20565.jpg', 'jpg', '215010', NULL, '2021-08-16 15:27:04', '2021-08-16 15:27:04');
INSERT INTO `sq_sys_file` VALUES (499, 'timg.jpg', '-1629098995-44556.jpg', '/files/20210816/-1629098995-44556.jpg', 'jpg', '187516', NULL, '2021-08-16 15:29:55', '2021-08-16 15:29:55');
INSERT INTO `sq_sys_file` VALUES (500, '微信图片_20190821171516.jpg', '-1629099000-63881.jpg', '/files/20210816/-1629099000-63881.jpg', 'jpg', '215010', NULL, '2021-08-16 15:30:00', '2021-08-16 15:30:00');
INSERT INTO `sq_sys_file` VALUES (501, '微信图片_20190821171516.jpg', '-1629099219-15832.jpg', '/files/20210816/-1629099219-15832.jpg', 'jpg', '215010', NULL, '2021-08-16 15:33:39', '2021-08-16 15:33:39');
INSERT INTO `sq_sys_file` VALUES (502, 'timg.jpg', '-1629099224-73093.jpg', '/files/20210816/-1629099224-73093.jpg', 'jpg', '187516', NULL, '2021-08-16 15:33:44', '2021-08-16 15:33:44');
INSERT INTO `sq_sys_file` VALUES (503, 'timg.jpg', '-1629099529-18734.jpg', '/files/20210816/-1629099529-18734.jpg', 'jpg', '187516', NULL, '2021-08-16 15:38:49', '2021-08-16 15:38:49');
INSERT INTO `sq_sys_file` VALUES (504, '微信图片_20190821171516.jpg', '-1629099593-52847.jpg', '/files/20210816/-1629099593-52847.jpg', 'jpg', '215010', NULL, '2021-08-16 15:39:53', '2021-08-16 15:39:53');
INSERT INTO `sq_sys_file` VALUES (505, '微信图片_20190821171516.jpg', '-1629099634-60743.jpg', '/files/20210816/-1629099634-60743.jpg', 'jpg', '215010', NULL, '2021-08-16 15:40:34', '2021-08-16 15:40:34');
INSERT INTO `sq_sys_file` VALUES (506, '微信图片_20190821171438.jpg', '-1629099650-59021.jpg', '/files/20210816/-1629099650-59021.jpg', 'jpg', '289062', NULL, '2021-08-16 15:40:50', '2021-08-16 15:40:50');
INSERT INTO `sq_sys_file` VALUES (507, '微信图片_20190821171516.jpg', '-1629099758-91837.jpg', '/files/20210816/-1629099758-91837.jpg', 'jpg', '215010', NULL, '2021-08-16 15:42:38', '2021-08-16 15:42:38');
INSERT INTO `sq_sys_file` VALUES (508, '微信图片_20190821171516.jpg', '-1629100185-1580.jpg', '/files/20210816/-1629100185-1580.jpg', 'jpg', '215010', NULL, '2021-08-16 15:49:45', '2021-08-16 15:49:45');
INSERT INTO `sq_sys_file` VALUES (509, '微信图片_20190821171102.jpg', '-1629101301-56773.jpg', '/files/20210816/-1629101301-56773.jpg', 'jpg', '204607', NULL, '2021-08-16 16:08:21', '2021-08-16 16:08:21');
INSERT INTO `sq_sys_file` VALUES (510, '微信图片_20190821171516.jpg', '-1629101319-99393.jpg', '/files/20210816/-1629101319-99393.jpg', 'jpg', '215010', NULL, '2021-08-16 16:08:39', '2021-08-16 16:08:39');
INSERT INTO `sq_sys_file` VALUES (511, '微信图片_20190821171516.jpg', '-1629101334-3304.jpg', '/files/20210816/-1629101334-3304.jpg', 'jpg', '215010', NULL, '2021-08-16 16:08:54', '2021-08-16 16:08:54');
INSERT INTO `sq_sys_file` VALUES (530, '微信图片_20190821171516.jpg', '-1629105654-72019.jpg', '/files/20210816/-1629105654-72019.jpg', 'jpg', '215010', NULL, '2021-08-16 17:20:54', '2021-08-16 17:20:54');
INSERT INTO `sq_sys_file` VALUES (531, '微信图片_20190821171516.jpg', '-1629105708-1833.jpg', '/files/20210816/-1629105708-1833.jpg', 'jpg', '215010', NULL, '2021-08-16 17:21:48', '2021-08-16 17:21:48');
INSERT INTO `sq_sys_file` VALUES (532, 'timg.jpg', '-1629105723-76810.jpg', '/files/20210816/-1629105723-76810.jpg', 'jpg', '187516', NULL, '2021-08-16 17:22:03', '2021-08-16 17:22:03');
INSERT INTO `sq_sys_file` VALUES (533, '微信图片_20190821171018.jpg', '-1629337649-76413.jpg', '/files/20210819/-1629337649-76413.jpg', 'jpg', '177094', NULL, '2021-08-19 09:47:29', '2021-08-19 09:47:29');
INSERT INTO `sq_sys_file` VALUES (534, '1629343707(1).png', '-1629343760-35617.png', '/files/20210819/-1629343760-35617.png', 'png', '40506', NULL, '2021-08-19 11:29:20', '2021-08-19 11:29:20');
INSERT INTO `sq_sys_file` VALUES (535, 'file_1629344882000.jpg', '-1629344882-43556.jpg', '/files/20210819/-1629344882-43556.jpg', 'jpg', '5317', NULL, '2021-08-19 11:48:02', '2021-08-19 11:48:02');
INSERT INTO `sq_sys_file` VALUES (536, 'file_1629359480000.jpg', '-1629359480-10810.jpg', '/files/20210819/-1629359480-10810.jpg', 'jpg', '105061', NULL, '2021-08-19 15:51:20', '2021-08-19 15:51:20');
INSERT INTO `sq_sys_file` VALUES (537, 'file_1629359540000.jpg', '-1629359540-43842.jpg', '/files/20210819/-1629359540-43842.jpg', 'jpg', '105061', NULL, '2021-08-19 15:52:20', '2021-08-19 15:52:20');
INSERT INTO `sq_sys_file` VALUES (538, 'file_1629359549000.jpg', '-1629359549-30478.jpg', '/files/20210819/-1629359549-30478.jpg', 'jpg', '105061', NULL, '2021-08-19 15:52:29', '2021-08-19 15:52:29');
INSERT INTO `sq_sys_file` VALUES (539, 'file_1629360755000.jpg', '-1629360755-70889.jpg', '/files/20210819/-1629360755-70889.jpg', 'jpg', '83297', NULL, '2021-08-19 16:12:35', '2021-08-19 16:12:35');
INSERT INTO `sq_sys_file` VALUES (540, 'file_1629360868000.jpg', '-1629360868-58097.jpg', '/files/20210819/-1629360868-58097.jpg', 'jpg', '134406', NULL, '2021-08-19 16:14:28', '2021-08-19 16:14:28');
INSERT INTO `sq_sys_file` VALUES (541, 'file_1629360948000.jpg', '-1629360948-2005.jpg', '/files/20210819/-1629360948-2005.jpg', 'jpg', '37133', NULL, '2021-08-19 16:15:48', '2021-08-19 16:15:48');
INSERT INTO `sq_sys_file` VALUES (542, 'file_1629360983000.jpg', '-1629360983-11742.jpg', '/files/20210819/-1629360983-11742.jpg', 'jpg', '229134', NULL, '2021-08-19 16:16:23', '2021-08-19 16:16:23');
INSERT INTO `sq_sys_file` VALUES (543, 'file_1629361029000.jpg', '-1629361029-95849.jpg', '/files/20210819/-1629361029-95849.jpg', 'jpg', '134118', NULL, '2021-08-19 16:17:09', '2021-08-19 16:17:09');
INSERT INTO `sq_sys_file` VALUES (544, 'file_1629361494000.jpg', '-1629361494-4734.jpg', '/files/20210819/-1629361494-4734.jpg', 'jpg', '289457', NULL, '2021-08-19 16:24:54', '2021-08-19 16:24:54');
INSERT INTO `sq_sys_file` VALUES (545, 'file_1629361600000.jpg', '-1629361600-71305.jpg', '/files/20210819/-1629361600-71305.jpg', 'jpg', '134118', NULL, '2021-08-19 16:26:40', '2021-08-19 16:26:40');
INSERT INTO `sq_sys_file` VALUES (546, 'file_1629361627000.jpg', '-1629361627-74446.jpg', '/files/20210819/-1629361627-74446.jpg', 'jpg', '289457', NULL, '2021-08-19 16:27:07', '2021-08-19 16:27:07');
INSERT INTO `sq_sys_file` VALUES (547, 'file_1629361645000.jpg', '-1629361645-24557.jpg', '/files/20210819/-1629361645-24557.jpg', 'jpg', '134118', NULL, '2021-08-19 16:27:25', '2021-08-19 16:27:25');
INSERT INTO `sq_sys_file` VALUES (548, 'file_1629427025000.jpg', '-1629427025-92147.jpg', '/files/20210820/-1629427025-92147.jpg', 'jpg', '228463', NULL, '2021-08-20 10:37:05', '2021-08-20 10:37:05');
INSERT INTO `sq_sys_file` VALUES (549, 'file_1629427230000.jpg', '-1629427230-11430.jpg', '/files/20210820/-1629427230-11430.jpg', 'jpg', '228463', NULL, '2021-08-20 10:40:30', '2021-08-20 10:40:30');
INSERT INTO `sq_sys_file` VALUES (550, 'file_1629427248000.jpg', '-1629427248-94993.jpg', '/files/20210820/-1629427248-94993.jpg', 'jpg', '228463', NULL, '2021-08-20 10:40:48', '2021-08-20 10:40:48');
INSERT INTO `sq_sys_file` VALUES (551, '微信图片_20190821171102.jpg', '-1629446151-19387.jpg', '/files/20210820/-1629446151-19387.jpg', 'jpg', '204607', NULL, '2021-08-20 15:55:51', '2021-08-20 15:55:51');
INSERT INTO `sq_sys_file` VALUES (552, 'NapGU9CfoIFed0270035866e14b28970276e620a861d.jpg', '-1639626215-36954.jpg', '/files/20211216/-1639626215-36954.jpg', 'jpg', '187516', 0, '2021-12-16 11:43:35', '2021-12-16 11:43:35');
INSERT INTO `sq_sys_file` VALUES (553, 'file_1642389194000.jpg', '-1642389195-14200.jpg', '/files/20220117/-1642389195-14200.jpg', 'jpg', '67717', NULL, '2022-01-17 11:13:15', '2022-01-17 11:13:15');

-- ----------------------------
-- Table structure for sq_test_log
-- ----------------------------
DROP TABLE IF EXISTS `sq_test_log`;
CREATE TABLE `sq_test_log`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NULL DEFAULT NULL,
  `baby_id` int(11) NULL DEFAULT NULL,
  `type` tinyint(1) NULL DEFAULT NULL COMMENT '测试类型 1营养2运动3睡眠',
  `result` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '测试结果',
  `point` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '分数',
  `advice` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '建议',
  `other_advice` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '其他建议',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '添加时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 18 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of sq_test_log
-- ----------------------------
INSERT INTO `sq_test_log` VALUES (4, 5, 39, 2, '{\"grade\":\"三年级\",\"height\":\"150\",\"weight\":\"50\",\"feihuoliang\":\"2000\",\"qizuo\":\"50\",\"qianqu\":\"20\",\"tiaosheng\":\"80\"}', NULL, NULL, NULL, '2020-10-23 14:46:12', '2020-10-23 14:46:12');
INSERT INTO `sq_test_log` VALUES (5, 6, 39, 2, '{\"grade\":\"三年级\",\"height\":\"150\",\"weight\":\"50\",\"feihuoliang\":\"2000\",\"qizuo\":\"50\",\"qianqu\":\"20\",\"tiaosheng\":\"80\"}', NULL, NULL, NULL, '2020-10-23 14:46:12', '2020-10-23 14:46:12');
INSERT INTO `sq_test_log` VALUES (6, 5, 39, 3, '[{\"id\":1,\"value\":\"2\"},{\"id\":2,\"value\":\"3\"},{\"id\":3,\"value\":\"2\"},{\"id\":4,\"value\":\"3\"},{\"id\":5,\"value\":\"3\"},{\"id\":6,\"value\":\"2\"},{\"id\":7,\"value\":\"3\"},{\"id\":8,\"value\":\"2\"},{\"id\":9,\"value\":\"3\"},{\"id\":10,\"value\":\"2\"},{\"id\":11,\"value\":\"3\"},{\"id\":12,\"value\":\"2\"},{\"id\":13,\"value\":\"2\"},{\"id\":14,\"value\":\"3\"},{\"id\":15,\"value\":\"2\"},{\"id\":16,\"value\":\"4\"},{\"id\":17,\"value\":\"3\"},{\"id\":18,\"value\":\"2\"},{\"id\":19,\"value\":\"3\"},{\"id\":20,\"value\":\"3\"},{\"id\":21,\"value\":\"3\"},{\"id\":22,\"value\":\"3\"},{\"id\":23,\"value\":\"2\"},{\"id\":24,\"value\":\"3\"},{\"id\":25,\"value\":\"3\"},{\"id\":26,\"value\":\"2\"}]', NULL, NULL, NULL, '2020-10-26 11:47:44', '2020-10-26 11:47:44');
INSERT INTO `sq_test_log` VALUES (8, 5, 39, 3, '[{\"id\":1,\"value\":\"3\"},{\"id\":2,\"value\":\"2\"},{\"id\":3,\"value\":\"2\"},{\"id\":4,\"value\":\"3\"},{\"id\":5,\"value\":\"2\"},{\"id\":6,\"value\":\"2\"},{\"id\":7,\"value\":\"3\"},{\"id\":8,\"value\":\"2\"},{\"id\":9,\"value\":\"3\"},{\"id\":10,\"value\":\"3\"},{\"id\":11,\"value\":\"2\"},{\"id\":12,\"value\":\"3\"},{\"id\":13,\"value\":\"2\"},{\"id\":14,\"value\":\"3\"},{\"id\":15,\"value\":\"2\"},{\"id\":16,\"value\":\"3\"},{\"id\":17,\"value\":\"2\"},{\"id\":18,\"value\":\"3\"},{\"id\":19,\"value\":\"3\"},{\"id\":20,\"value\":\"3\"},{\"id\":21,\"value\":\"2\"},{\"id\":22,\"value\":\"2\"},{\"id\":23,\"value\":\"2\"},{\"id\":24,\"value\":\"3\"},{\"id\":25,\"value\":\"3\"},{\"id\":26,\"value\":\"3\"}]', NULL, NULL, NULL, '2020-10-26 11:51:37', '2020-10-26 11:51:37');
INSERT INTO `sq_test_log` VALUES (9, 5, 39, 3, '[{\"id\":1,\"title\":\"孩子有没有开灯睡觉的习惯？\",\"option\":{\"a\":{\"key\":\"A\",\"name\":\"喜欢开台灯睡觉，不开灯无法入睡\",\"desc\":\"\"},\"b\":{\"key\":\"B\",\"name\":\"睡觉习惯有微弱的光亮，比如开小夜灯\",\"desc\":\"\"},\"c\":{\"key\":\"C\",\"name\":\"晚上睡觉，偶尔开小夜灯\",\"desc\":\"\"},\"d\":{\"key\":\"D\",\"name\":\"有光亮无法入睡，一般不会开灯睡觉\",\"desc\":\"\"}},\"checked\":\"B\"},{\"id\":2,\"title\":\"平均每天孩子的睡眠时间可以达到多少小时（包括小睡）？\",\"option\":{\"a\":{\"key\":\"A\",\"name\":\"每天睡眠时间还不足7个小时\",\"desc\":\"\"},\"b\":{\"key\":\"B\",\"name\":\"每天能睡7-9个小时\",\"desc\":\"\"},\"c\":{\"key\":\"C\",\"name\":\"每天能睡10个小时\",\"desc\":\"\"},\"d\":{\"key\":\"D\",\"name\":\"每天加起来能睡11-12个小时\",\"desc\":\"\"}},\"checked\":\"B\"},{\"id\":3,\"title\":\"孩子晚上一般什么时间睡觉？\",\"option\":{\"a\":{\"key\":\"A\",\"name\":\"老不愿睡觉，一般要到晚上11点后才愿意上床睡觉\",\"desc\":\"\"},\"b\":{\"key\":\"B\",\"name\":\"似乎精力充沛，晚上一般要到10点才愿意乖乖睡觉\",\"desc\":\"\"},\"c\":{\"key\":\"C\",\"name\":\"虽然有时很活跃，不过通常能在9-10点上床睡觉\",\"desc\":\"\"},\"d\":{\"key\":\"D\",\"name\":\"很听话，每晚9点前就乖乖地睡着了\",\"desc\":\"\"}},\"checked\":\"B\"},{\"id\":4,\"title\":\"孩子晚上一般需要哄多久才能进入梦乡？\",\"option\":{\"a\":{\"key\":\"A\",\"name\":\"是一个折腾的小家伙，常常哄30分钟还不能入睡\",\"desc\":\"\"},\"b\":{\"key\":\"B\",\"name\":\"睡前要讲故事，一般折腾个20-30分钟后，才会乖乖睡着\",\"desc\":\"(每月1-2次）\"},\"c\":{\"key\":\"C\",\"name\":\"经常故事讲到一半，15-20分钟就乖乖静下来了\",\"desc\":\"(每月3-5次）\"},\"d\":{\"key\":\"D\",\"name\":\"一个小小的睡美人，每天基本都在15分钟内乖乖入睡\",\"desc\":\"(日常）\"}},\"checked\":\"B\"},{\"id\":5,\"title\":\"孩子夜里一般会醒来多少次？\",\"option\":{\"a\":{\"key\":\"A\",\"name\":\"一晚有3次以上，有时还会哭闹\",\"desc\":\"\"},\"b\":{\"key\":\"B\",\"name\":\"不肯乖乖地一觉睡到天亮，总会醒来两次左右\",\"desc\":\"(每月1-2次）\"},\"c\":{\"key\":\"C\",\"name\":\"平均每晚醒来1次，哄哄很快又睡着了\",\"desc\":\"(每月3-5次）\"},\"d\":{\"key\":\"D\",\"name\":\"几乎每晚都一觉睡到天亮\",\"desc\":\"(日常）\"}},\"checked\":\"B\"},{\"id\":6,\"title\":\"孩子夜里醒来后要多久才能重新进入甜甜的梦乡呢？\",\"option\":{\"a\":{\"key\":\"A\",\"name\":\"醒后总是不肯再乖乖地待在床上，常常哄30分钟还不能入睡\",\"desc\":\"\"},\"b\":{\"key\":\"B\",\"name\":\"总要闹腾个30分钟，出尽法宝才能哄他再次入睡\",\"desc\":\"(每月1-2次）\"},\"c\":{\"key\":\"C\",\"name\":\"醒后再闹闹小别扭，15分钟内还是会静下来的\",\"desc\":\"(每月3-5次）\"},\"d\":{\"key\":\"D\",\"name\":\"一般无夜醒情况\",\"desc\":\"(日常）\"}},\"checked\":\"B\"},{\"id\":7,\"title\":\"孩子夜里睡觉时有特别的现象吗？（张嘴呼吸、打鼾、睡眠暂停等）\",\"option\":{\"a\":{\"key\":\"A\",\"name\":\"每天都是会张大嘴巴呼吸，甚至还会打鼾，还会经常惊醒哭闹，让人很头疼\",\"desc\":\"\"},\"b\":{\"key\":\"B\",\"name\":\"每周总有两三晚会出现异常现象，比如张嘴呼吸、打鼾、夜惊、面红\",\"desc\":\"(每月1-2次）\"},\"c\":{\"key\":\"C\",\"name\":\"偶尔会惊醒或面红，但总的来说还是睡得比较安稳 \",\"desc\":\"(每月3-5次）\"},\"d\":{\"key\":\"D\",\"name\":\"总是睡得很香甜\",\"desc\":\"(日常）\"}},\"checked\":\"B\"},{\"id\":8,\"title\":\"孩子早上一般几点起床？\",\"option\":{\"a\":{\"key\":\"A\",\"name\":\"每天早上5点半起床，甚至更早\",\"desc\":\"\"},\"b\":{\"key\":\"B\",\"name\":\"一般6点左右起床\",\"desc\":\"(每月1-2次）\"},\"c\":{\"key\":\"C\",\"name\":\"偶尔6点起床，一般7点以后才起床\",\"desc\":\"(每月3-5次）\"},\"d\":{\"key\":\"D\",\"name\":\"比较规律，每天7点以后起床\",\"desc\":\"(日常）\"}},\"checked\":\"B\"},{\"id\":9,\"title\":\"孩子早上醒来后会乖乖地马上起床吗？\",\"option\":{\"a\":{\"key\":\"A\",\"name\":\"喜欢赖床，醒来后还会不停的打哈欠，睁不开双眼，强行拉他起床，会耍小性子，嚎啕大哭\",\"desc\":\"\"},\"b\":{\"key\":\"B\",\"name\":\"每次起床都需要哄，折腾一轮，但起来后他/她看上去还是比较困\",\"desc\":\"(每月1-2次）\"},\"c\":{\"key\":\"C\",\"name\":\"比较容易听话的起床，但不是太有精神，起来后都懒得动\",\"desc\":\"(每月3-5次）\"},\"d\":{\"key\":\"D\",\"name\":\"可爱而充满活力的孩子，每天清晨醒来精力充沛，心情愉悦\",\"desc\":\"(日常）\"}},\"checked\":\"B\"},{\"id\":10,\"title\":\"孩子早上起来后，白天有精神跟你一起玩和学习吗？\",\"option\":{\"a\":{\"key\":\"A\",\"name\":\"没精打采的，经常打哈欠 \",\"desc\":\"\"},\"b\":{\"key\":\"B\",\"name\":\"眼睛不是很有神，反应有点慢，他/她看上去还是比较困\",\"desc\":\"(每月1-2次）\"},\"c\":{\"key\":\"C\",\"name\":\"精神一般，反应也一般\",\"desc\":\"(每月3-5次）\"},\"d\":{\"key\":\"D\",\"name\":\"两眼有神，很投入，不时手舞足蹈，笑的很灿烂\",\"desc\":\"(日常）\"}},\"checked\":\"B\"}]', '50', '孩子的睡眠总体情况处于正常水平', NULL, '2020-11-16 17:31:39', '2020-11-16 17:31:39');
INSERT INTO `sq_test_log` VALUES (10, 5, 39, 3, '[{\"id\":1,\"title\":\"孩子有没有开灯睡觉的习惯？\",\"option\":{\"a\":{\"key\":\"A\",\"name\":\"喜欢开台灯睡觉，不开灯无法入睡\",\"desc\":\"\"},\"b\":{\"key\":\"B\",\"name\":\"睡觉习惯有微弱的光亮，比如开小夜灯\",\"desc\":\"\"},\"c\":{\"key\":\"C\",\"name\":\"晚上睡觉，偶尔开小夜灯\",\"desc\":\"\"},\"d\":{\"key\":\"D\",\"name\":\"有光亮无法入睡，一般不会开灯睡觉\",\"desc\":\"\"}},\"checked\":\"C\"},{\"id\":2,\"title\":\"平均每天孩子的睡眠时间可以达到多少小时（包括小睡）？\",\"option\":{\"a\":{\"key\":\"A\",\"name\":\"每天睡眠时间还不足7个小时\",\"desc\":\"\"},\"b\":{\"key\":\"B\",\"name\":\"每天能睡7-9个小时\",\"desc\":\"\"},\"c\":{\"key\":\"C\",\"name\":\"每天能睡10个小时\",\"desc\":\"\"},\"d\":{\"key\":\"D\",\"name\":\"每天加起来能睡11-12个小时\",\"desc\":\"\"}},\"checked\":\"C\"},{\"id\":3,\"title\":\"孩子晚上一般什么时间睡觉？\",\"option\":{\"a\":{\"key\":\"A\",\"name\":\"老不愿睡觉，一般要到晚上11点后才愿意上床睡觉\",\"desc\":\"\"},\"b\":{\"key\":\"B\",\"name\":\"似乎精力充沛，晚上一般要到10点才愿意乖乖睡觉\",\"desc\":\"\"},\"c\":{\"key\":\"C\",\"name\":\"虽然有时很活跃，不过通常能在9-10点上床睡觉\",\"desc\":\"\"},\"d\":{\"key\":\"D\",\"name\":\"很听话，每晚9点前就乖乖地睡着了\",\"desc\":\"\"}},\"checked\":\"C\"},{\"id\":4,\"title\":\"孩子晚上一般需要哄多久才能进入梦乡？\",\"option\":{\"a\":{\"key\":\"A\",\"name\":\"是一个折腾的小家伙，常常哄30分钟还不能入睡\",\"desc\":\"\"},\"b\":{\"key\":\"B\",\"name\":\"睡前要讲故事，一般折腾个20-30分钟后，才会乖乖睡着\",\"desc\":\"(每月1-2次）\"},\"c\":{\"key\":\"C\",\"name\":\"经常故事讲到一半，15-20分钟就乖乖静下来了\",\"desc\":\"(每月3-5次）\"},\"d\":{\"key\":\"D\",\"name\":\"一个小小的睡美人，每天基本都在15分钟内乖乖入睡\",\"desc\":\"(日常）\"}},\"checked\":\"C\"},{\"id\":5,\"title\":\"孩子夜里一般会醒来多少次？\",\"option\":{\"a\":{\"key\":\"A\",\"name\":\"一晚有3次以上，有时还会哭闹\",\"desc\":\"\"},\"b\":{\"key\":\"B\",\"name\":\"不肯乖乖地一觉睡到天亮，总会醒来两次左右\",\"desc\":\"(每月1-2次）\"},\"c\":{\"key\":\"C\",\"name\":\"平均每晚醒来1次，哄哄很快又睡着了\",\"desc\":\"(每月3-5次）\"},\"d\":{\"key\":\"D\",\"name\":\"几乎每晚都一觉睡到天亮\",\"desc\":\"(日常）\"}},\"checked\":\"C\"},{\"id\":6,\"title\":\"孩子夜里醒来后要多久才能重新进入甜甜的梦乡呢？\",\"option\":{\"a\":{\"key\":\"A\",\"name\":\"醒后总是不肯再乖乖地待在床上，常常哄30分钟还不能入睡\",\"desc\":\"\"},\"b\":{\"key\":\"B\",\"name\":\"总要闹腾个30分钟，出尽法宝才能哄他再次入睡\",\"desc\":\"(每月1-2次）\"},\"c\":{\"key\":\"C\",\"name\":\"醒后再闹闹小别扭，15分钟内还是会静下来的\",\"desc\":\"(每月3-5次）\"},\"d\":{\"key\":\"D\",\"name\":\"一般无夜醒情况\",\"desc\":\"(日常）\"}},\"checked\":\"C\"},{\"id\":7,\"title\":\"孩子夜里睡觉时有特别的现象吗？（张嘴呼吸、打鼾、睡眠暂停等）\",\"option\":{\"a\":{\"key\":\"A\",\"name\":\"每天都是会张大嘴巴呼吸，甚至还会打鼾，还会经常惊醒哭闹，让人很头疼\",\"desc\":\"\"},\"b\":{\"key\":\"B\",\"name\":\"每周总有两三晚会出现异常现象，比如张嘴呼吸、打鼾、夜惊、面红\",\"desc\":\"(每月1-2次）\"},\"c\":{\"key\":\"C\",\"name\":\"偶尔会惊醒或面红，但总的来说还是睡得比较安稳 \",\"desc\":\"(每月3-5次）\"},\"d\":{\"key\":\"D\",\"name\":\"总是睡得很香甜\",\"desc\":\"(日常）\"}},\"checked\":\"C\"},{\"id\":8,\"title\":\"孩子早上一般几点起床？\",\"option\":{\"a\":{\"key\":\"A\",\"name\":\"每天早上5点半起床，甚至更早\",\"desc\":\"\"},\"b\":{\"key\":\"B\",\"name\":\"一般6点左右起床\",\"desc\":\"(每月1-2次）\"},\"c\":{\"key\":\"C\",\"name\":\"偶尔6点起床，一般7点以后才起床\",\"desc\":\"(每月3-5次）\"},\"d\":{\"key\":\"D\",\"name\":\"比较规律，每天7点以后起床\",\"desc\":\"(日常）\"}},\"checked\":\"C\"},{\"id\":9,\"title\":\"孩子早上醒来后会乖乖地马上起床吗？\",\"option\":{\"a\":{\"key\":\"A\",\"name\":\"喜欢赖床，醒来后还会不停的打哈欠，睁不开双眼，强行拉他起床，会耍小性子，嚎啕大哭\",\"desc\":\"\"},\"b\":{\"key\":\"B\",\"name\":\"每次起床都需要哄，折腾一轮，但起来后他/她看上去还是比较困\",\"desc\":\"(每月1-2次）\"},\"c\":{\"key\":\"C\",\"name\":\"比较容易听话的起床，但不是太有精神，起来后都懒得动\",\"desc\":\"(每月3-5次）\"},\"d\":{\"key\":\"D\",\"name\":\"可爱而充满活力的孩子，每天清晨醒来精力充沛，心情愉悦\",\"desc\":\"(日常）\"}},\"checked\":\"C\"},{\"id\":10,\"title\":\"孩子早上起来后，白天有精神跟你一起玩和学习吗？\",\"option\":{\"a\":{\"key\":\"A\",\"name\":\"没精打采的，经常打哈欠 \",\"desc\":\"\"},\"b\":{\"key\":\"B\",\"name\":\"眼睛不是很有神，反应有点慢，他/她看上去还是比较困\",\"desc\":\"(每月1-2次）\"},\"c\":{\"key\":\"C\",\"name\":\"精神一般，反应也一般\",\"desc\":\"(每月3-5次）\"},\"d\":{\"key\":\"D\",\"name\":\"两眼有神，很投入，不时手舞足蹈，笑的很灿烂\",\"desc\":\"(日常）\"}},\"checked\":\"C\"}]', '80', '孩子目前的睡眠行为习惯总体情况较差', NULL, '2020-11-16 17:33:10', '2020-11-16 17:33:10');
INSERT INTO `sq_test_log` VALUES (14, 5, 39, 3, '[{\"id\":1,\"title\":\"孩子有没有运动的习惯？\",\"option\":{\"a\":{\"key\":\"A\",\"name\":\"每天都运动\",\"desc\":10},\"b\":{\"key\":\"B\",\"name\":\"一周有4-5天会运动\",\"desc\":8},\"c\":{\"key\":\"C\",\"name\":\"偶尔运动\",\"desc\":3}},\"checked\":\"B\"},{\"id\":2,\"title\":\"孩子一天运动几次？\",\"option\":{\"a\":{\"key\":\"A\",\"name\":\"一次\",\"desc\":5},\"b\":{\"key\":\"B\",\"name\":\"两次\",\"desc\":10},\"c\":{\"key\":\"C\",\"name\":\"两次或以上\",\"desc\":5}},\"checked\":\"B\"},{\"id\":3,\"title\":\"一般在什么时间运动？\",\"option\":{\"a\":{\"key\":\"A\",\"name\":\" 早上\",\"desc\":8},\"b\":{\"key\":\"B\",\"name\":\"下午\",\"desc\":10},\"c\":{\"key\":\"C\",\"name\":\"晚上\",\"desc\":5}},\"checked\":\"B\"},{\"id\":4,\"title\":\"每次运动能持续多长时间？\",\"option\":{\"a\":{\"key\":\"A\",\"name\":\"20分钟以下\",\"desc\":5},\"b\":{\"key\":\"B\",\"name\":\"20-25分钟\",\"desc\":10},\"c\":{\"key\":\"C\",\"name\":\"25分钟以上\",\"desc\":5}},\"checked\":\"B\"},{\"id\":5,\"title\":\"孩子一般做什么类型的运动？\",\"option\":{\"a\":{\"key\":\"A\",\"name\":\"拉伸、跳跃类\",\"desc\":10},\"b\":{\"key\":\"B\",\"name\":\"有氧运动\",\"desc\":8},\"c\":{\"key\":\"C\",\"name\":\"负重类（马拉松、举重类）\",\"desc\":3}},\"checked\":\"B\"},{\"id\":6,\"title\":\"一般运动前后会不会做热身和放松？\",\"option\":{\"a\":{\"key\":\"A\",\"name\":\"会\",\"desc\":10},\"b\":{\"key\":\"B\",\"name\":\"不会\",\"desc\":3}},\"checked\":\"B\"},{\"id\":7,\"title\":\"孩子一般哪种状态下进行运动？\",\"option\":{\"a\":{\"key\":\"A\",\"name\":\"空腹、低血糖\",\"desc\":10},\"b\":{\"key\":\"B\",\"name\":\"进餐后\",\"desc\":2},\"c\":{\"key\":\"C\",\"name\":\"情绪差\",\"desc\":2},\"d\":{\"key\":\"D\",\"name\":\"睡觉前\",\"desc\":2}},\"checked\":\"C\"},{\"id\":8,\"title\":\"运动强度适量吗？\",\"option\":{\"a\":{\"key\":\"A\",\"name\":\"第二天感觉疲倦，甚至疼痛\",\"desc\":3},\"b\":{\"key\":\"B\",\"name\":\"运动过后，精神状态更好，更有胃口，睡眠更好了\",\"desc\":10}},\"checked\":\"B\"},{\"id\":9,\"title\":\"运动后，会立即喝冷饮给身体降温？\",\"option\":{\"a\":{\"key\":\"A\",\"name\":\"会\",\"desc\":2},\"b\":{\"key\":\"B\",\"name\":\"不会\",\"desc\":10}},\"checked\":\"B\"},{\"id\":10,\"title\":\"运动后，怎么给身体补水？\",\"option\":{\"a\":{\"key\":\"A\",\"name\":\"一次性喝大量的水\",\"desc\":3},\"b\":{\"key\":\"B\",\"name\":\"少量多次，随时补水\",\"desc\":10}},\"checked\":\"B\"}]', '50', '您的孩子目前已经建立了良好的运动习惯，请继续保持', NULL, '2020-11-17 09:27:19', '2020-11-17 09:27:19');
INSERT INTO `sq_test_log` VALUES (15, 5, 39, 1, '[{\"id\":1,\"title\":\"谷类、豆类、奶类、蛋类、肉鱼类、蔬菜水果、以上食物每周能吃到几类\",\"option\":{\"a\":{\"key\":\"A\",\"name\":\"全部都吃\",\"desc\":\"\"},\"b\":{\"key\":\"B\",\"name\":\"任意4-5类\",\"desc\":\"\"},\"c\":{\"key\":\"C\",\"name\":\"任意3类以下\",\"desc\":\"\"}},\"checked\":\"C\"},{\"id\":2,\"title\":\"吃早餐情况\",\"option\":{\"a\":{\"key\":\"A\",\"name\":\"每天吃\",\"desc\":\"\"},\"b\":{\"key\":\"B\",\"name\":\"每周4-6次\",\"desc\":\"\"},\"c\":{\"key\":\"C\",\"name\":\"每周1-3次\",\"desc\":\"\"}},\"checked\":\"B\"},{\"id\":3,\"title\":\"鱼虾贝类摄入情况\",\"option\":{\"a\":{\"key\":\"A\",\"name\":\"每天吃\",\"desc\":\"\"},\"b\":{\"key\":\"B\",\"name\":\"每周4-6次\",\"desc\":\"\"},\"c\":{\"key\":\"C\",\"name\":\"每周1-3次\",\"desc\":\"\"},\"d\":{\"key\":\"D\",\"name\":\"每月1-3次\",\"desc\":\"\"}},\"checked\":\"B\"},{\"id\":4,\"title\":\"通常吃的比较多的鱼是哪一类\",\"option\":{\"a\":{\"key\":\"A\",\"name\":\"鳕鱼、三文鱼\",\"desc\":\"\"},\"b\":{\"key\":\"B\",\"name\":\"带鱼、鲍鱼\",\"desc\":\"\"},\"c\":{\"key\":\"C\",\"name\":\"淡水鱼\",\"desc\":\"\"}},\"checked\":\"A\"},{\"id\":5,\"title\":\"肉类摄入情况\",\"option\":{\"a\":{\"key\":\"A\",\"name\":\"每天吃\",\"desc\":\"\"},\"b\":{\"key\":\"B\",\"name\":\"每周4-6次\",\"desc\":\"\"},\"c\":{\"key\":\"C\",\"name\":\"每周1-3次\",\"desc\":\"\"},\"d\":{\"key\":\"D\",\"name\":\"每月1-3次\",\"desc\":\"\"}},\"checked\":\"C\"},{\"id\":6,\"title\":\"蛋类摄入情况\",\"option\":{\"a\":{\"key\":\"A\",\"name\":\"每天都吃\",\"desc\":\"\"},\"b\":{\"key\":\"B\",\"name\":\"每周4-6次\",\"desc\":\"\"},\"c\":{\"key\":\"C\",\"name\":\"每周1-3次\",\"desc\":\"\"},\"d\":{\"key\":\"D\",\"name\":\"每月1-3次 \",\"desc\":\"\"},\"e\":{\"key\":\"E\",\"name\":\"不吃\",\"desc\":\"\"}},\"checked\":\"C\"},{\"id\":7,\"title\":\"奶类摄入情况\",\"option\":{\"a\":{\"key\":\"A\",\"name\":\"每天都喝\",\"desc\":\"\"},\"b\":{\"key\":\"B\",\"name\":\"每周4-6次\",\"desc\":\"\"},\"c\":{\"key\":\"C\",\"name\":\"每周1-3次\",\"desc\":\"\"},\"d\":{\"key\":\"D\",\"name\":\"每月1-3次 \",\"desc\":\"\"},\"e\":{\"key\":\"E\",\"name\":\"不喝\",\"desc\":\"\"}},\"checked\":\"B\"},{\"id\":8,\"title\":\"以下哪一种类型的奶制品喝的比较多\",\"option\":{\"a\":{\"key\":\"A\",\"name\":\"鲜奶或酸奶\",\"desc\":\"\"},\"b\":{\"key\":\"B\",\"name\":\"奶粉\",\"desc\":\"\"},\"c\":{\"key\":\"C\",\"name\":\"奶饮料\",\"desc\":\"\"}},\"checked\":\"B\"},{\"id\":9,\"title\":\"通常每次喝奶多少ml奶\",\"option\":{\"a\":{\"key\":\"A\",\"name\":\"500ml及以上\",\"desc\":\"\"},\"b\":{\"key\":\"B\",\"name\":\"300-500ml \",\"desc\":\"\"},\"c\":{\"key\":\"C\",\"name\":\"100-300ml\",\"desc\":\"\"},\"d\":{\"key\":\"D\",\"name\":\"100ml以下\",\"desc\":\"\"}},\"checked\":\"C\"},{\"id\":10,\"title\":\"豆类食品摄入情况\",\"option\":{\"a\":{\"key\":\"A\",\"name\":\"每天都吃\",\"desc\":\"\"},\"b\":{\"key\":\"B\",\"name\":\"每周4-6次\",\"desc\":\"\"},\"c\":{\"key\":\"C\",\"name\":\"每周1-3次\",\"desc\":\"\"},\"d\":{\"key\":\"D\",\"name\":\"每月1-3次\",\"desc\":\"\"},\"e\":{\"key\":\"E\",\"name\":\"不吃\",\"desc\":\"\"}},\"checked\":\"C\"},{\"id\":11,\"title\":\"通常摄入最多的主食\",\"option\":{\"a\":{\"key\":\"A\",\"name\":\"大米及面食类\",\"desc\":\"\"},\"b\":{\"key\":\"B\",\"name\":\"油炸面食（如油条）\",\"desc\":\"\"},\"c\":{\"key\":\"C\",\"name\":\"点心或甜面食\",\"desc\":\"\"}},\"checked\":\"B\"},{\"id\":12,\"title\":\"海洋藻类（海带、海裙菜、紫菜等）摄入情况\",\"option\":{\"a\":{\"key\":\"A\",\"name\":\"每周1次及以上\",\"desc\":\"\"},\"b\":{\"key\":\"B\",\"name\":\"每月1-3次\",\"desc\":\"\"},\"c\":{\"key\":\"C\",\"name\":\"每年偶尔吃\",\"desc\":\"\"},\"d\":{\"key\":\"D\",\"name\":\"不吃\",\"desc\":\"\"}},\"checked\":\"C\"},{\"id\":13,\"title\":\"蔬菜摄入情况\",\"option\":{\"a\":{\"key\":\"A\",\"name\":\"每天1次及以上\",\"desc\":\"\"},\"b\":{\"key\":\"B\",\"name\":\"每周4-6次\",\"desc\":\"\"},\"c\":{\"key\":\"C\",\"name\":\"每周1-3次\",\"desc\":\"\"},\"d\":{\"key\":\"D\",\"name\":\"每月1-3次\",\"desc\":\"\"}},\"checked\":\"B\"},{\"id\":14,\"title\":\"叶菜类（菠菜、油菜等），根茎类（萝卜、芹菜等），瓜果类（黄瓜、冬瓜等），以上蔬菜主要摄入哪些类\",\"option\":{\"a\":{\"key\":\"A\",\"name\":\"全部种类都吃\",\"desc\":\"\"},\"b\":{\"key\":\"B\",\"name\":\"吃两类\",\"desc\":\"\"},\"c\":{\"key\":\"C\",\"name\":\"仅吃一类\",\"desc\":\"\"}},\"checked\":\"B\"},{\"id\":15,\"title\":\"新鲜水果摄入情况\",\"option\":{\"a\":{\"key\":\"A\",\"name\":\"每天吃1次及以上\",\"desc\":\"\"},\"b\":{\"key\":\"B\",\"name\":\"每周4-6次\",\"desc\":\"\"},\"c\":{\"key\":\"C\",\"name\":\"每周1-3次\",\"desc\":\"\"},\"d\":{\"key\":\"D\",\"name\":\"每月1-3次\",\"desc\":\"\"},\"e\":{\"key\":\"E\",\"name\":\"偶尔吃或者不吃\",\"desc\":\"\"}},\"checked\":\"B\"},{\"id\":16,\"title\":\"通常喝最多的饮品是哪一种\",\"option\":{\"a\":{\"key\":\"A\",\"name\":\"纯水或矿泉水\",\"desc\":\"\"},\"b\":{\"key\":\"B\",\"name\":\"市售果汁饮料\",\"desc\":\"\"},\"c\":{\"key\":\"C\",\"name\":\"碳酸类饮料（可乐、雪碧等）\",\"desc\":\"\"}},\"checked\":\"B\"},{\"id\":17,\"title\":\"市售零食摄入情况\",\"option\":{\"a\":{\"key\":\"A\",\"name\":\"偶尔或者不吃\",\"desc\":\"\"},\"b\":{\"key\":\"B\",\"name\":\"每月1-3次\",\"desc\":\"\"},\"c\":{\"key\":\"C\",\"name\":\"每周1-3次\",\"desc\":\"\"},\"d\":{\"key\":\"D\",\"name\":\"每周4-6次\",\"desc\":\"\"},\"e\":{\"key\":\"E\",\"name\":\"每天都吃\",\"desc\":\"\"}},\"checked\":\"B\"},{\"id\":18,\"title\":\"下列零食通常摄入最多的是\",\"option\":{\"a\":{\"key\":\"A\",\"name\":\"坚果类、肉干鱼干类\",\"desc\":\"\"},\"b\":{\"key\":\"B\",\"name\":\"油炸麻辣类\",\"desc\":\"\"},\"c\":{\"key\":\"C\",\"name\":\"膨化食品、甜点心类\",\"desc\":\"\"}},\"checked\":\"B\"},{\"id\":19,\"title\":\"通常饭菜的口感\",\"option\":{\"a\":{\"key\":\"A\",\"name\":\"淡\",\"desc\":\"\"},\"b\":{\"key\":\"B\",\"name\":\"一般\",\"desc\":\"\"},\"c\":{\"key\":\"C\",\"name\":\"很重\",\"desc\":\"\"}},\"checked\":\"C\"},{\"id\":20,\"title\":\"身体活动情况\",\"option\":{\"a\":{\"key\":\"A\",\"name\":\"经常参加户外或室内活动\",\"desc\":\"\"},\"b\":{\"key\":\"B\",\"name\":\"主要以室内活动为主\",\"desc\":\"\"},\"c\":{\"key\":\"C\",\"name\":\"不喜欢参加活动，喜欢看书、看电视、打游戏\",\"desc\":\"\"}},\"checked\":\"B\"}]', '80', '矿物质钙摄入量严重不足。维生素A.D.K摄入量严重不足。膳食不平衡，导致富含维生素A.D.K的食物摄入极少，维生素A、D摄入量严重不足，不能满足孩子生长发育的需要', NULL, '2020-11-17 10:17:03', '2020-11-17 10:17:03');
INSERT INTO `sq_test_log` VALUES (16, 5, 39, 2, '[{\"id\":1,\"title\":\"孩子有没有运动的习惯？\",\"option\":{\"a\":{\"key\":\"A\",\"name\":\"每天都运动\",\"desc\":10},\"b\":{\"key\":\"B\",\"name\":\"一周有4-5天会运动\",\"desc\":8},\"c\":{\"key\":\"C\",\"name\":\"偶尔运动\",\"desc\":3}},\"checked\":\"B\"},{\"id\":2,\"title\":\"孩子一天运动几次？\",\"option\":{\"a\":{\"key\":\"A\",\"name\":\"一次\",\"desc\":5},\"b\":{\"key\":\"B\",\"name\":\"两次\",\"desc\":10},\"c\":{\"key\":\"C\",\"name\":\"两次或以上\",\"desc\":5}},\"checked\":\"B\"},{\"id\":3,\"title\":\"一般在什么时间运动？\",\"option\":{\"a\":{\"key\":\"A\",\"name\":\" 早上\",\"desc\":8},\"b\":{\"key\":\"B\",\"name\":\"下午\",\"desc\":10},\"c\":{\"key\":\"C\",\"name\":\"晚上\",\"desc\":5}},\"checked\":\"B\"},{\"id\":4,\"title\":\"每次运动能持续多长时间？\",\"option\":{\"a\":{\"key\":\"A\",\"name\":\"20分钟以下\",\"desc\":5},\"b\":{\"key\":\"B\",\"name\":\"20-25分钟\",\"desc\":10},\"c\":{\"key\":\"C\",\"name\":\"25分钟以上\",\"desc\":5}},\"checked\":\"B\"},{\"id\":5,\"title\":\"孩子一般做什么类型的运动？\",\"option\":{\"a\":{\"key\":\"A\",\"name\":\"拉伸、跳跃类\",\"desc\":10},\"b\":{\"key\":\"B\",\"name\":\"有氧运动\",\"desc\":8},\"c\":{\"key\":\"C\",\"name\":\"负重类（马拉松、举重类）\",\"desc\":3}},\"checked\":\"B\"},{\"id\":6,\"title\":\"一般运动前后会不会做热身和放松？\",\"option\":{\"a\":{\"key\":\"A\",\"name\":\"会\",\"desc\":10},\"b\":{\"key\":\"B\",\"name\":\"不会\",\"desc\":3}},\"checked\":\"A\"},{\"id\":7,\"title\":\"孩子一般哪种状态下进行运动？\",\"option\":{\"a\":{\"key\":\"A\",\"name\":\"空腹、低血糖\",\"desc\":10},\"b\":{\"key\":\"B\",\"name\":\"进餐后\",\"desc\":2},\"c\":{\"key\":\"C\",\"name\":\"情绪差\",\"desc\":2},\"d\":{\"key\":\"D\",\"name\":\"睡觉前\",\"desc\":2}},\"checked\":\"C\"},{\"id\":8,\"title\":\"运动强度适量吗？\",\"option\":{\"a\":{\"key\":\"A\",\"name\":\"第二天感觉疲倦，甚至疼痛\",\"desc\":3},\"b\":{\"key\":\"B\",\"name\":\"运动过后，精神状态更好，更有胃口，睡眠更好了\",\"desc\":10}},\"checked\":\"A\"},{\"id\":9,\"title\":\"运动后，会立即喝冷饮给身体降温？\",\"option\":{\"a\":{\"key\":\"A\",\"name\":\"会\",\"desc\":2},\"b\":{\"key\":\"B\",\"name\":\"不会\",\"desc\":10}},\"checked\":\"B\"},{\"id\":10,\"title\":\"运动后，怎么给身体补水？\",\"option\":{\"a\":{\"key\":\"A\",\"name\":\"一次性喝大量的水\",\"desc\":3},\"b\":{\"key\":\"B\",\"name\":\"少量多次，随时补水\",\"desc\":10}},\"checked\":\"A\"}]', '60', '您的孩子目前已经建立了良好的运动习惯，请继续保持', NULL, '2020-11-17 10:22:02', '2020-11-17 10:22:02');
INSERT INTO `sq_test_log` VALUES (17, 5, 39, 3, '[{\"id\":1,\"title\":\"孩子有没有开灯睡觉的习惯？\",\"option\":{\"a\":{\"key\":\"A\",\"name\":\"喜欢开台灯睡觉，不开灯无法入睡\",\"desc\":\"\"},\"b\":{\"key\":\"B\",\"name\":\"睡觉习惯有微弱的光亮，比如开小夜灯\",\"desc\":\"\"},\"c\":{\"key\":\"C\",\"name\":\"晚上睡觉，偶尔开小夜灯\",\"desc\":\"\"},\"d\":{\"key\":\"D\",\"name\":\"有光亮无法入睡，一般不会开灯睡觉\",\"desc\":\"\"}},\"checked\":\"B\"},{\"id\":2,\"title\":\"平均每天孩子的睡眠时间可以达到多少小时（包括小睡）？\",\"option\":{\"a\":{\"key\":\"A\",\"name\":\"每天睡眠时间还不足7个小时\",\"desc\":\"\"},\"b\":{\"key\":\"B\",\"name\":\"每天能睡7-9个小时\",\"desc\":\"\"},\"c\":{\"key\":\"C\",\"name\":\"每天能睡10个小时\",\"desc\":\"\"},\"d\":{\"key\":\"D\",\"name\":\"每天加起来能睡11-12个小时\",\"desc\":\"\"}},\"checked\":\"A\"},{\"id\":3,\"title\":\"孩子晚上一般什么时间睡觉？\",\"option\":{\"a\":{\"key\":\"A\",\"name\":\"老不愿睡觉，一般要到晚上11点后才愿意上床睡觉\",\"desc\":\"\"},\"b\":{\"key\":\"B\",\"name\":\"似乎精力充沛，晚上一般要到10点才愿意乖乖睡觉\",\"desc\":\"\"},\"c\":{\"key\":\"C\",\"name\":\"虽然有时很活跃，不过通常能在9-10点上床睡觉\",\"desc\":\"\"},\"d\":{\"key\":\"D\",\"name\":\"很听话，每晚9点前就乖乖地睡着了\",\"desc\":\"\"}},\"checked\":\"A\"},{\"id\":4,\"title\":\"孩子晚上一般需要哄多久才能进入梦乡？\",\"option\":{\"a\":{\"key\":\"A\",\"name\":\"是一个折腾的小家伙，常常哄30分钟还不能入睡\",\"desc\":\"\"},\"b\":{\"key\":\"B\",\"name\":\"睡前要讲故事，一般折腾个20-30分钟后，才会乖乖睡着\",\"desc\":\"(每月1-2次）\"},\"c\":{\"key\":\"C\",\"name\":\"经常故事讲到一半，15-20分钟就乖乖静下来了\",\"desc\":\"(每月3-5次）\"},\"d\":{\"key\":\"D\",\"name\":\"一个小小的睡美人，每天基本都在15分钟内乖乖入睡\",\"desc\":\"(日常）\"}},\"checked\":\"A\"},{\"id\":5,\"title\":\"孩子夜里一般会醒来多少次？\",\"option\":{\"a\":{\"key\":\"A\",\"name\":\"一晚有3次以上，有时还会哭闹\",\"desc\":\"\"},\"b\":{\"key\":\"B\",\"name\":\"不肯乖乖地一觉睡到天亮，总会醒来两次左右\",\"desc\":\"(每月1-2次）\"},\"c\":{\"key\":\"C\",\"name\":\"平均每晚醒来1次，哄哄很快又睡着了\",\"desc\":\"(每月3-5次）\"},\"d\":{\"key\":\"D\",\"name\":\"几乎每晚都一觉睡到天亮\",\"desc\":\"(日常）\"}},\"checked\":\"A\"},{\"id\":6,\"title\":\"孩子夜里醒来后要多久才能重新进入甜甜的梦乡呢？\",\"option\":{\"a\":{\"key\":\"A\",\"name\":\"醒后总是不肯再乖乖地待在床上，常常哄30分钟还不能入睡\",\"desc\":\"\"},\"b\":{\"key\":\"B\",\"name\":\"总要闹腾个30分钟，出尽法宝才能哄他再次入睡\",\"desc\":\"(每月1-2次）\"},\"c\":{\"key\":\"C\",\"name\":\"醒后再闹闹小别扭，15分钟内还是会静下来的\",\"desc\":\"(每月3-5次）\"},\"d\":{\"key\":\"D\",\"name\":\"一般无夜醒情况\",\"desc\":\"(日常）\"}},\"checked\":\"A\"},{\"id\":7,\"title\":\"孩子夜里睡觉时有特别的现象吗？（张嘴呼吸、打鼾、睡眠暂停等）\",\"option\":{\"a\":{\"key\":\"A\",\"name\":\"每天都是会张大嘴巴呼吸，甚至还会打鼾，还会经常惊醒哭闹，让人很头疼\",\"desc\":\"\"},\"b\":{\"key\":\"B\",\"name\":\"每周总有两三晚会出现异常现象，比如张嘴呼吸、打鼾、夜惊、面红\",\"desc\":\"(每月1-2次）\"},\"c\":{\"key\":\"C\",\"name\":\"偶尔会惊醒或面红，但总的来说还是睡得比较安稳 \",\"desc\":\"(每月3-5次）\"},\"d\":{\"key\":\"D\",\"name\":\"总是睡得很香甜\",\"desc\":\"(日常）\"}},\"checked\":\"A\"},{\"id\":8,\"title\":\"孩子早上一般几点起床？\",\"option\":{\"a\":{\"key\":\"A\",\"name\":\"每天早上5点半起床，甚至更早\",\"desc\":\"\"},\"b\":{\"key\":\"B\",\"name\":\"一般6点左右起床\",\"desc\":\"(每月1-2次）\"},\"c\":{\"key\":\"C\",\"name\":\"偶尔6点起床，一般7点以后才起床\",\"desc\":\"(每月3-5次）\"},\"d\":{\"key\":\"D\",\"name\":\"比较规律，每天7点以后起床\",\"desc\":\"(日常）\"}},\"checked\":\"A\"},{\"id\":9,\"title\":\"孩子早上醒来后会乖乖地马上起床吗？\",\"option\":{\"a\":{\"key\":\"A\",\"name\":\"喜欢赖床，醒来后还会不停的打哈欠，睁不开双眼，强行拉他起床，会耍小性子，嚎啕大哭\",\"desc\":\"\"},\"b\":{\"key\":\"B\",\"name\":\"每次起床都需要哄，折腾一轮，但起来后他/她看上去还是比较困\",\"desc\":\"(每月1-2次）\"},\"c\":{\"key\":\"C\",\"name\":\"比较容易听话的起床，但不是太有精神，起来后都懒得动\",\"desc\":\"(每月3-5次）\"},\"d\":{\"key\":\"D\",\"name\":\"可爱而充满活力的孩子，每天清晨醒来精力充沛，心情愉悦\",\"desc\":\"(日常）\"}},\"checked\":\"A\"},{\"id\":10,\"title\":\"孩子早上起来后，白天有精神跟你一起玩和学习吗？\",\"option\":{\"a\":{\"key\":\"A\",\"name\":\"没精打采的，经常打哈欠 \",\"desc\":\"\"},\"b\":{\"key\":\"B\",\"name\":\"眼睛不是很有神，反应有点慢，他/她看上去还是比较困\",\"desc\":\"(每月1-2次）\"},\"c\":{\"key\":\"C\",\"name\":\"精神一般，反应也一般\",\"desc\":\"(每月3-5次）\"},\"d\":{\"key\":\"D\",\"name\":\"两眼有神，很投入，不时手舞足蹈，笑的很灿烂\",\"desc\":\"(日常）\"}},\"checked\":\"A\"}]', '32', '孩子目前的睡眠行为习惯总体情况较差', NULL, '2020-11-17 14:36:45', '2020-11-17 14:36:45');

-- ----------------------------
-- Table structure for sq_test_nutrition
-- ----------------------------
DROP TABLE IF EXISTS `sq_test_nutrition`;
CREATE TABLE `sq_test_nutrition`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NULL DEFAULT 0,
  `title` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题',
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图片',
  `sort` int(11) NULL DEFAULT NULL COMMENT '排序',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '最后修改时间',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '注册时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 66 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '营养自测题目' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of sq_test_nutrition
-- ----------------------------
INSERT INTO `sq_test_nutrition` VALUES (1, 0, '鱼', NULL, 1, NULL, NULL);
INSERT INTO `sq_test_nutrition` VALUES (2, 0, '禽肉', NULL, 2, NULL, NULL);
INSERT INTO `sq_test_nutrition` VALUES (3, 0, '畜肉', NULL, 3, NULL, NULL);
INSERT INTO `sq_test_nutrition` VALUES (4, 0, '豆及豆制品', NULL, 4, NULL, NULL);
INSERT INTO `sq_test_nutrition` VALUES (5, 0, '乳及乳制品', NULL, 5, NULL, NULL);
INSERT INTO `sq_test_nutrition` VALUES (6, 0, '动物内脏', NULL, 6, NULL, NULL);
INSERT INTO `sq_test_nutrition` VALUES (7, 0, '绿叶蔬菜', NULL, 8, NULL, NULL);
INSERT INTO `sq_test_nutrition` VALUES (8, 0, '其他蔬菜', NULL, 9, NULL, NULL);
INSERT INTO `sq_test_nutrition` VALUES (9, 0, '虾', NULL, 10, NULL, NULL);
INSERT INTO `sq_test_nutrition` VALUES (10, 0, '贝类', NULL, 11, NULL, NULL);
INSERT INTO `sq_test_nutrition` VALUES (11, 0, '全谷物', NULL, 12, NULL, NULL);
INSERT INTO `sq_test_nutrition` VALUES (12, 0, '菌菇类', NULL, 13, NULL, NULL);
INSERT INTO `sq_test_nutrition` VALUES (13, 0, '坚果类', NULL, 14, NULL, NULL);
INSERT INTO `sq_test_nutrition` VALUES (14, 0, '薯类', NULL, 15, NULL, NULL);
INSERT INTO `sq_test_nutrition` VALUES (15, 0, '不健康食物', NULL, 16, NULL, NULL);
INSERT INTO `sq_test_nutrition` VALUES (16, 1, '淡水鱼', NULL, 1, NULL, NULL);
INSERT INTO `sq_test_nutrition` VALUES (17, 1, '深海鱼', NULL, 1, NULL, NULL);
INSERT INTO `sq_test_nutrition` VALUES (18, 2, '鸡、鸭', NULL, 1, NULL, NULL);
INSERT INTO `sq_test_nutrition` VALUES (19, 3, '瘦猪肉', NULL, 1, NULL, NULL);
INSERT INTO `sq_test_nutrition` VALUES (20, 3, '牛肉、羊肉', NULL, 1, NULL, NULL);
INSERT INTO `sq_test_nutrition` VALUES (21, 4, '豌豆、绿豆', NULL, 1, NULL, NULL);
INSERT INTO `sq_test_nutrition` VALUES (22, 4, '红豆', NULL, 1, NULL, NULL);
INSERT INTO `sq_test_nutrition` VALUES (23, 4, '黄豆', NULL, 1, NULL, NULL);
INSERT INTO `sq_test_nutrition` VALUES (24, 4, '豆腐', NULL, 1, NULL, NULL);
INSERT INTO `sq_test_nutrition` VALUES (25, 5, '奶酪', NULL, 1, NULL, NULL);
INSERT INTO `sq_test_nutrition` VALUES (26, 5, '奶油', NULL, 1, NULL, NULL);
INSERT INTO `sq_test_nutrition` VALUES (27, 5, '配方奶粉/牛奶', NULL, 1, NULL, NULL);
INSERT INTO `sq_test_nutrition` VALUES (28, 6, '鸡心', NULL, 1, NULL, NULL);
INSERT INTO `sq_test_nutrition` VALUES (29, 6, '猪肝、猪血', NULL, 1, NULL, NULL);
INSERT INTO `sq_test_nutrition` VALUES (30, 45, '鸡蛋、鸭蛋', NULL, 1, NULL, NULL);
INSERT INTO `sq_test_nutrition` VALUES (45, 0, '蛋', NULL, 7, NULL, NULL);
INSERT INTO `sq_test_nutrition` VALUES (47, 7, '菠菜、莴苣叶', NULL, 1, NULL, NULL);
INSERT INTO `sq_test_nutrition` VALUES (48, 7, '西蓝花、油菜心', NULL, 1, NULL, NULL);
INSERT INTO `sq_test_nutrition` VALUES (49, 8, '南瓜', NULL, 1, NULL, NULL);
INSERT INTO `sq_test_nutrition` VALUES (50, 8, '胡萝卜', NULL, 1, NULL, NULL);
INSERT INTO `sq_test_nutrition` VALUES (51, 8, '大白菜', NULL, 1, NULL, NULL);
INSERT INTO `sq_test_nutrition` VALUES (52, 9, '虾米、河虾', NULL, 1, NULL, NULL);
INSERT INTO `sq_test_nutrition` VALUES (53, 10, '扇贝、生蚝、海蛎肉', NULL, 1, NULL, NULL);
INSERT INTO `sq_test_nutrition` VALUES (54, 11, '全麦、糙米、玉米、燕麦', NULL, 1, NULL, NULL);
INSERT INTO `sq_test_nutrition` VALUES (55, 11, '荞麦、小米、黑麦', NULL, 1, NULL, NULL);
INSERT INTO `sq_test_nutrition` VALUES (56, 12, '木耳、口蘑', NULL, 1, NULL, NULL);
INSERT INTO `sq_test_nutrition` VALUES (57, 12, '香菇', NULL, 1, NULL, NULL);
INSERT INTO `sq_test_nutrition` VALUES (58, 13, '山核桃、松子', NULL, 1, NULL, NULL);
INSERT INTO `sq_test_nutrition` VALUES (59, 14, '红薯', NULL, 1, NULL, NULL);
INSERT INTO `sq_test_nutrition` VALUES (60, 14, '白薯、紫薯', NULL, 1, NULL, NULL);
INSERT INTO `sq_test_nutrition` VALUES (61, 15, '含糖饮料', NULL, 1, NULL, NULL);
INSERT INTO `sq_test_nutrition` VALUES (62, 15, '含乳饮料', NULL, 1, NULL, NULL);
INSERT INTO `sq_test_nutrition` VALUES (63, 15, '甜点（蛋糕/饼干/奶糖/奶片）', NULL, 1, NULL, NULL);
INSERT INTO `sq_test_nutrition` VALUES (64, 15, '油炸食物（薯片/炸鸡）', NULL, 1, NULL, NULL);
INSERT INTO `sq_test_nutrition` VALUES (65, 15, '含糖豆浆', NULL, 1, NULL, NULL);

-- ----------------------------
-- Table structure for sq_test_sleep
-- ----------------------------
DROP TABLE IF EXISTS `sq_test_sleep`;
CREATE TABLE `sq_test_sleep`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '最后修改时间',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '注册时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 27 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '营养自测题目' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of sq_test_sleep
-- ----------------------------
INSERT INTO `sq_test_sleep` VALUES (1, '大多数情况，每晚（天）孩子睡眠时间有多少小时（包括小睡）', NULL, NULL);
INSERT INTO `sq_test_sleep` VALUES (2, '通常您的孩子上床睡觉后多长时间才能睡着', NULL, NULL);
INSERT INTO `sq_test_sleep` VALUES (3, '孩子不愿上床睡觉', NULL, NULL);
INSERT INTO `sq_test_sleep` VALUES (4, '晚上孩子入睡困难', NULL, NULL);
INSERT INTO `sq_test_sleep` VALUES (5, '孩子入睡会感到焦虑或恐惧', NULL, NULL);
INSERT INTO `sq_test_sleep` VALUES (6, '孩子入睡时会惊吓或抽动', NULL, NULL);
INSERT INTO `sq_test_sleep` VALUES (7, '孩子入睡时会出现重复动作，例如频繁摇头或撞击', NULL, NULL);
INSERT INTO `sq_test_sleep` VALUES (8, '孩子入睡时会经历鲜明的梦境场景', NULL, NULL);
INSERT INTO `sq_test_sleep` VALUES (9, '孩子睡觉时会过度出汗', NULL, NULL);
INSERT INTO `sq_test_sleep` VALUES (10, '每晚孩子会醒来两次以上', NULL, NULL);
INSERT INTO `sq_test_sleep` VALUES (11, '晚上醒后，孩子很难再入睡', NULL, NULL);
INSERT INTO `sq_test_sleep` VALUES (12, '睡着后，孩子频繁出现腿部抽动或痉挛，或者在深夜的时候经常改变睡姿或者踢被子', NULL, NULL);
INSERT INTO `sq_test_sleep` VALUES (13, '深夜孩子会出现呼吸困难', NULL, NULL);
INSERT INTO `sq_test_sleep` VALUES (14, '孩子睡着的时候会上气不接下气或停止呼吸', NULL, NULL);
INSERT INTO `sq_test_sleep` VALUES (15, '孩子打鼾', NULL, NULL);
INSERT INTO `sq_test_sleep` VALUES (16, '孩子夜间过度出汗', NULL, NULL);
INSERT INTO `sq_test_sleep` VALUES (17, '您发现孩子会梦游', NULL, NULL);
INSERT INTO `sq_test_sleep` VALUES (18, '您发现孩子说梦话', NULL, NULL);
INSERT INTO `sq_test_sleep` VALUES (19, '孩子睡觉时会磨牙', NULL, NULL);
INSERT INTO `sq_test_sleep` VALUES (20, '孩子会大叫而惊醒或者迷糊中您不能叫醒他/她，但是事后不能记起', NULL, NULL);
INSERT INTO `sq_test_sleep` VALUES (21, '孩子夜里会尖叫醒来或者迷迷糊糊，似乎您帮不到他/她，但第二天早上孩子不记得', NULL, NULL);
INSERT INTO `sq_test_sleep` VALUES (22, '孩子早上经常很难醒来', NULL, NULL);
INSERT INTO `sq_test_sleep` VALUES (23, '孩子早上醒来后感到疲乏', NULL, NULL);
INSERT INTO `sq_test_sleep` VALUES (24, '第二天醒来后孩子感到身体不能动', NULL, NULL);
INSERT INTO `sq_test_sleep` VALUES (25, '孩子会出现白天嗜睡', NULL, NULL);
INSERT INTO `sq_test_sleep` VALUES (26, '孩子会不分场合突然睡着', NULL, NULL);

-- ----------------------------
-- Table structure for sq_thumb
-- ----------------------------
DROP TABLE IF EXISTS `sq_thumb`;
CREATE TABLE `sq_thumb`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` tinyint(1) NULL DEFAULT NULL COMMENT '1 文章 2问答 3分享 4留言',
  `operate_id` int(11) NULL DEFAULT NULL COMMENT '操作id',
  `related_id` int(11) NULL DEFAULT NULL COMMENT '关联id',
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 41 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '点赞 （文章 分享 问答 留言）' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of sq_thumb
-- ----------------------------

-- ----------------------------
-- Table structure for sq_topic
-- ----------------------------
DROP TABLE IF EXISTS `sq_topic`;
CREATE TABLE `sq_topic`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `sub_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `is_hot` tinyint(1) NULL DEFAULT NULL COMMENT '1是 0否',
  `user_id` int(11) NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `is_show` tinyint(1) NULL DEFAULT NULL COMMENT '1是0否',
  `is_delete` tinyint(1) NULL DEFAULT 2 COMMENT '1是 2否',
  `sort` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '话题管理' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of sq_topic
-- ----------------------------

-- ----------------------------
-- Table structure for sq_topic_article
-- ----------------------------
DROP TABLE IF EXISTS `sq_topic_article`;
CREATE TABLE `sq_topic_article`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `topic_id` int(11) NULL DEFAULT NULL COMMENT '话题id',
  `article_id` int(11) NULL DEFAULT NULL COMMENT '分享id',
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 28 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '关联主题  （问答 分享 ）' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of sq_topic_article
-- ----------------------------

-- ----------------------------
-- Table structure for sq_user
-- ----------------------------
DROP TABLE IF EXISTS `sq_user`;
CREATE TABLE `sq_user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` tinyint(1) NULL DEFAULT NULL COMMENT '1普通2医生',
  `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '登录名（默认为手机号）',
  `position` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '职位',
  `department` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '部门',
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `role_id` int(11) UNSIGNED NULL DEFAULT 0 COMMENT '对应role表ID ',
  `mobile` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '联系电话',
  `password` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `user_type` tinyint(1) NULL DEFAULT 1 COMMENT '1管理员 2企业用户',
  `status` tinyint(1) NULL DEFAULT 1 COMMENT '用户状态 1正常 2冻结',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '最后修改时间',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '注册时间',
  `user_id` int(11) NULL DEFAULT 0 COMMENT '11',
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '头像',
  `good_at` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '擅长',
  `introduce` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '简介',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `user_name`(`name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of sq_user
-- ----------------------------
INSERT INTO `sq_user` VALUES (1, 1, '超级管理员', '', '', 'admin@admin.com', 1, '18686868686', 'e10adc3949ba59abbe56e057f20f883e', 1, 1, '2020-10-05 15:32:27', '2018-08-24 13:51:09', 0, '/files/20201005/-1601883145-17947.jpg', '', '');
INSERT INTO `sq_user` VALUES (3, 2, '向红', '副主任医师', '儿科', NULL, 2, '15857856859', 'e10adc3949ba59abbe56e057f20f883e', 1, 1, '2021-08-13 15:59:06', '2020-10-03 10:08:09', 1, '/files/20201003/-1601690866-74265.jpg', '玩儿翁二翁绕弯儿翁', '玩儿翁二翁绕弯儿翁认为');

-- ----------------------------
-- Table structure for sq_user_menu
-- ----------------------------
DROP TABLE IF EXISTS `sq_user_menu`;
CREATE TABLE `sq_user_menu`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '菜单名称',
  `model` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '模块名称',
  `parent_id` int(11) NULL DEFAULT 0 COMMENT '父菜单ID',
  `level` int(2) NULL DEFAULT 1 COMMENT '菜单级别',
  `icon` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'icon图表',
  `sort` int(5) NULL DEFAULT 1 COMMENT '排序 数字越大越靠前',
  `url` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '地址',
  `param` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `user_id` int(11) NULL DEFAULT 0 COMMENT '11',
  `is_show` tinyint(2) NULL DEFAULT 1 COMMENT '显示隐藏（隐藏的不必授权）',
  `is_iframe` tinyint(1) NULL DEFAULT 0 COMMENT '1是 0否',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '最后修改时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '注册时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 407 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色菜单表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of sq_user_menu
-- ----------------------------
INSERT INTO `sq_user_menu` VALUES (1, '工作台', 'admin', 0, 1, 'icon-webicon01', 5000, 'admin/index/index', NULL, 0, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `sq_user_menu` VALUES (2, '系统设置', 'admin', 0, 1, 'layui-icon layui-icon-set-sm', 1, '', NULL, 0, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `sq_user_menu` VALUES (3, '管理员管理', 'admin', 2, 2, NULL, 4000, 'admin/user/index', NULL, 0, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `sq_user_menu` VALUES (339, '内容管理', 'admin', 0, 1, 'layui-icon layui-icon-flag', 5000, '', NULL, 0, 1, 0, NULL, NULL);
INSERT INTO `sq_user_menu` VALUES (340, '话题管理', 'admin', 339, 2, NULL, 1, 'admin/topic/index', NULL, 0, 1, 0, NULL, NULL);
INSERT INTO `sq_user_menu` VALUES (341, '文章管理', 'admin', 339, 2, NULL, 1, 'admin/article/index', NULL, 0, 1, 0, NULL, NULL);
INSERT INTO `sq_user_menu` VALUES (343, '权限管理', 'admin', 2, 2, NULL, 1, 'admin/user_role/index', NULL, 0, 1, 0, NULL, NULL);
INSERT INTO `sq_user_menu` VALUES (347, '添加', 'admin', 3, 3, NULL, 1, 'admin/user/create', NULL, 0, 1, 0, NULL, NULL);
INSERT INTO `sq_user_menu` VALUES (348, '编辑', 'admin', 3, 3, NULL, 1, 'admin/user/edit', NULL, 0, 1, 0, NULL, NULL);
INSERT INTO `sq_user_menu` VALUES (349, '删除', 'admin', 3, 3, NULL, 1, 'admin/user/delete', NULL, 0, 1, 0, NULL, NULL);
INSERT INTO `sq_user_menu` VALUES (350, '重置密码', 'admin', 3, 3, NULL, 1, 'admin/user/reset', NULL, 0, 1, 0, NULL, NULL);
INSERT INTO `sq_user_menu` VALUES (351, '添加', 'admin', 343, 3, NULL, 1, 'admin/user_role/create', NULL, 0, 1, 0, NULL, NULL);
INSERT INTO `sq_user_menu` VALUES (352, '编辑', 'admin', 343, 3, NULL, 1, 'admin/user_role/edit', NULL, 0, 1, 0, NULL, NULL);
INSERT INTO `sq_user_menu` VALUES (353, '删除', 'admin', 343, 3, NULL, 1, 'admin/user_role/delete', NULL, 0, 1, 0, NULL, NULL);
INSERT INTO `sq_user_menu` VALUES (354, '添加', 'admin', 340, 3, NULL, 1, 'admin/topic/create', NULL, 0, 1, 0, NULL, NULL);
INSERT INTO `sq_user_menu` VALUES (355, '编辑', 'admin', 340, 3, NULL, 1, 'admin/topic/edit', NULL, 0, 1, 0, NULL, NULL);
INSERT INTO `sq_user_menu` VALUES (356, '删除', 'admin', 340, 3, NULL, 1, 'admin/topic/delete', NULL, 0, 1, 0, NULL, NULL);
INSERT INTO `sq_user_menu` VALUES (357, '添加', 'admin', 341, 3, NULL, 1, 'admin/article/create', NULL, 0, 1, 0, NULL, NULL);
INSERT INTO `sq_user_menu` VALUES (358, '编辑', 'admin', 341, 3, NULL, 1, 'admin/article/edit', NULL, 0, 1, 0, NULL, NULL);
INSERT INTO `sq_user_menu` VALUES (359, '删除', 'admin', 341, 3, NULL, 1, 'admin/article/delete', NULL, 0, 1, 0, NULL, NULL);
INSERT INTO `sq_user_menu` VALUES (360, '用户管理', 'admin', 0, 1, 'layui-icon layui-icon-user', 3000, '', NULL, 0, 1, 0, NULL, NULL);
INSERT INTO `sq_user_menu` VALUES (361, '家长管理', 'admin', 360, 2, NULL, 1, 'admin/parents/index', NULL, 0, 1, 0, NULL, NULL);
INSERT INTO `sq_user_menu` VALUES (362, '宝宝管理', 'admin', 360, 2, NULL, 1, 'admin/baby/index', NULL, 0, 1, 0, NULL, NULL);
INSERT INTO `sq_user_menu` VALUES (363, '删除', 'admin', 361, 3, NULL, 1, 'admin/parents/delete', NULL, 0, 1, 0, NULL, NULL);
INSERT INTO `sq_user_menu` VALUES (364, '问题管理', 'admin', 0, 1, 'layui-icon layui-icon-help', 4000, 'admin/question/index', NULL, 0, 1, 0, NULL, NULL);
INSERT INTO `sq_user_menu` VALUES (365, '回复', 'admin', 364, 3, NULL, 1, 'admin/question/replay', NULL, 0, 1, 0, NULL, NULL);
INSERT INTO `sq_user_menu` VALUES (366, '意见反馈', 'admin', 0, 1, 'layui-icon layui-icon-chat', 4000, 'admin/opinion/index', NULL, 0, 1, 0, NULL, NULL);
INSERT INTO `sq_user_menu` VALUES (367, '分享管理', 'admin', 0, 1, 'layui-icon layui-icon-link', 4500, 'admin/share/index', NULL, 0, 1, 0, NULL, NULL);
INSERT INTO `sq_user_menu` VALUES (368, '删除', 'admin', 367, 3, NULL, 1, 'admin/share/delete', NULL, 0, 1, 0, NULL, NULL);
INSERT INTO `sq_user_menu` VALUES (372, '产品管理', 'admin', 395, 2, 'layui-icon  layui-icon-app', 4000, 'admin/product/edit', 'id=1', 0, 1, 0, NULL, NULL);
INSERT INTO `sq_user_menu` VALUES (373, '添加', 'admin', 372, 3, NULL, 1, 'admin/product/create', '0', 0, 0, 0, NULL, NULL);
INSERT INTO `sq_user_menu` VALUES (374, '编辑', 'admin', 372, 3, NULL, 1, 'admin/product/edit', '0', 0, 0, 0, NULL, NULL);
INSERT INTO `sq_user_menu` VALUES (375, '删除', 'admin', 372, 3, NULL, 1, 'admin/product/delete', NULL, 0, 0, 0, NULL, NULL);
INSERT INTO `sq_user_menu` VALUES (376, '案例展示', 'admin', 395, 2, 'layui-icon  layui-icon-template-1\r\n', 4000, 'admin/case_show/edit', 'id=1', 0, 1, 0, NULL, NULL);
INSERT INTO `sq_user_menu` VALUES (377, '添加', 'admin', 376, 3, NULL, 1, 'admin/case_show/create', '', 0, 0, 0, NULL, NULL);
INSERT INTO `sq_user_menu` VALUES (378, '编辑', 'admin', 376, 3, NULL, 1, 'admin/case_show/edit', '', 0, 0, 0, NULL, NULL);
INSERT INTO `sq_user_menu` VALUES (379, '删除', 'admin', 376, 3, NULL, 1, 'admin/case_show/delete', NULL, 0, 0, 0, NULL, NULL);
INSERT INTO `sq_user_menu` VALUES (380, '活动管理', 'admin', 395, 2, 'layui-icon layui-icon-theme', 3500, 'admin/activity/index', 'type=1', 0, 1, 0, NULL, NULL);
INSERT INTO `sq_user_menu` VALUES (381, '报名详情', 'admin', 380, 3, NULL, 1, 'admin/activity_apply/index', NULL, 0, 0, 0, NULL, NULL);
INSERT INTO `sq_user_menu` VALUES (382, '添加', 'admin', 380, 3, NULL, 1, 'admin/activity/create', NULL, 0, 1, 0, NULL, NULL);
INSERT INTO `sq_user_menu` VALUES (384, '编辑', 'admin', 380, 3, NULL, 1, 'admin/activity/edit', NULL, 0, 1, 0, NULL, NULL);
INSERT INTO `sq_user_menu` VALUES (385, '删除', 'admin', 380, 3, NULL, 1, 'admin/activity/delete', NULL, 0, 1, 0, NULL, NULL);
INSERT INTO `sq_user_menu` VALUES (386, '报名', 'admin', 380, 3, NULL, 1, 'admin/activity/apply', NULL, 0, 0, 0, NULL, NULL);
INSERT INTO `sq_user_menu` VALUES (387, '活动详情', 'admin', 380, 3, NULL, 1, 'admin/activity/detail', NULL, 0, 0, 0, NULL, NULL);
INSERT INTO `sq_user_menu` VALUES (388, '明星推介', 'admin', 395, 2, 'layui-icon  layui-icon-rate', 4000, 'admin/spokesman/edit', 'id=1', 0, 1, 0, NULL, NULL);
INSERT INTO `sq_user_menu` VALUES (389, '添加', 'admin', 388, 3, NULL, 1, 'admin/spokesman/create', '', 0, 0, 0, NULL, NULL);
INSERT INTO `sq_user_menu` VALUES (390, '编辑', 'admin', 388, 3, NULL, 1, 'admin/spokesman/edit', '', 0, 0, 0, NULL, NULL);
INSERT INTO `sq_user_menu` VALUES (391, '删除', 'admin', 388, 3, NULL, 1, 'admin/spokesman/delete', '', 0, 0, 0, NULL, NULL);
INSERT INTO `sq_user_menu` VALUES (392, '咨询管理', 'admin', 395, 2, 'layui-icon layui-icon-headset', 3500, 'admin/consult/index', 'type=1', 0, 1, 0, NULL, NULL);
INSERT INTO `sq_user_menu` VALUES (393, '回复', 'admin', 392, 3, NULL, 1, 'admin/consult/reply', NULL, 0, 0, 0, NULL, NULL);
INSERT INTO `sq_user_menu` VALUES (394, '导出', 'admin', 392, 3, NULL, 1, 'admin/consult/export', NULL, 0, 1, 0, NULL, NULL);
INSERT INTO `sq_user_menu` VALUES (395, '增乐高产品', 'admin', 0, 1, 'layui-icon  layui-icon-app', 4000, 'admin/h5/index', NULL, 0, 1, 0, NULL, NULL);
INSERT INTO `sq_user_menu` VALUES (396, '添加', 'admin', 392, 3, NULL, 1, 'admin/consult/create', NULL, 0, 1, 0, NULL, NULL);
INSERT INTO `sq_user_menu` VALUES (397, '编辑', 'admin', 392, 3, NULL, 1, 'admin/consult/edit', NULL, 0, 1, 0, NULL, NULL);
INSERT INTO `sq_user_menu` VALUES (398, '删除', 'admin', 392, 3, NULL, 1, 'admin/consult/delete', NULL, 0, 1, 0, NULL, NULL);
INSERT INTO `sq_user_menu` VALUES (399, '海报管理', 'admin', 0, 1, 'layui-icon  layui-icon-picture', 4000, 'admin/poster/index', NULL, 0, 1, 0, NULL, NULL);
INSERT INTO `sq_user_menu` VALUES (400, '添加', 'admin', 399, 3, NULL, 1, 'admin/poster/create', NULL, 0, 1, 0, NULL, NULL);
INSERT INTO `sq_user_menu` VALUES (401, '编辑', 'admin', 399, 3, NULL, 1, 'admin/poster/edit', NULL, 0, 1, 0, NULL, NULL);
INSERT INTO `sq_user_menu` VALUES (402, '删除', 'admin', 399, 3, NULL, 1, 'admin/poster/delete', NULL, 0, 1, 0, NULL, NULL);
INSERT INTO `sq_user_menu` VALUES (403, '身高信息采集点', 'admin', 0, 1, 'layui-icon  layui-icon-location', 4000, 'admin/shop/index', NULL, 0, 1, 0, NULL, NULL);
INSERT INTO `sq_user_menu` VALUES (404, '添加', 'admin', 403, 3, NULL, 1, 'admin/shop/create', NULL, 0, 1, 0, NULL, NULL);
INSERT INTO `sq_user_menu` VALUES (405, '编辑', 'admin', 403, 3, NULL, 1, 'admin/shop/edit', NULL, 0, 1, 0, NULL, NULL);
INSERT INTO `sq_user_menu` VALUES (406, '删除', 'admin', 403, 3, NULL, 1, 'admin/shop/delete', NULL, 0, 1, 0, NULL, NULL);

-- ----------------------------
-- Table structure for sq_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sq_user_role`;
CREATE TABLE `sq_user_role`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `role_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色名称',
  `menu_ids` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '菜单ID字符串 多个ID之间用 , 隔开',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `sort` int(5) NULL DEFAULT 0 COMMENT '排序',
  `user_id` int(11) NULL DEFAULT 0 COMMENT '11',
  `is_lock` tinyint(1) NULL DEFAULT 2 COMMENT '是否锁定  锁定之后不能修改 1是2否',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '注册时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '最后修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of sq_user_role
-- ----------------------------
INSERT INTO `sq_user_role` VALUES (1, '超级管理员', '', NULL, 0, 0, 1, '2019-09-23 10:47:27', '2019-09-23 10:47:30');
INSERT INTO `sq_user_role` VALUES (2, '医生', '339,340,354,355,356,341,357,358,359,367,368,364,365,366', '', 0, 10, 2, '2019-04-12 13:35:04', '2021-12-16 11:23:16');

SET FOREIGN_KEY_CHECKS = 1;
